﻿using Dao;
using HospitalErpData.MenuHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace HospitalErpWinUI.Common
{
    public static class SqlDictApi
    {
        private static DaoHelper _helper = new DaoHelper();
        private static List<Perm> _perms { get; set; }

        public static List<string> sys_emp_type_para()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sys_emp_type_para";
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            return objs;

        }
    }
}
