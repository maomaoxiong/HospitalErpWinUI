﻿using Dao;
using HospitalErpData.MenuHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HospitalErpWinUI.Common
{
    public static class DataDictApi
    {
        private static DaoHelper _helper= new DaoHelper() ;
        private static List<Perm> _perms { get; set; }
        
        public static List<string> dict_yes_or_no()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "dict_yes_or_no";
            serviceParamter.Paramters = paramters;
            List<string> objs = new List<string>();
            List<CodeValue> codeValues = _helper.ReadDictForData(serviceParamter);
            foreach (CodeValue codeValue in codeValues)
            {
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            return objs;

        }
    }
}
