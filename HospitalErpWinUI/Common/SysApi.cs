﻿using Dao;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace HospitalErpWinUI.Common
{
    public class SysApi
    {
        protected DaoHelper _helper;
        public SysApi()
        {
            this._helper = new DaoHelper();
        }

        //得到系统参数
        public string GetSysParaData(string comp_code, string copy_code, string code_code)
        {
            string result = string.Empty;

            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sys_code_format_comp_copy";

            paramters.Add("comp_code", comp_code);
            paramters.Add("copy_code", copy_code);
            paramters.Add("code_code", code_code);
            serviceParamter.Paramters = paramters;

            DataTable dt = this._helper.ReadDictForSql(serviceParamter);
            result =dt.Rows[0][0].ToString();
            return result;
        }
    }
}
