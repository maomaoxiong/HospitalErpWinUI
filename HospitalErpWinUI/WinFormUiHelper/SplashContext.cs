﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HMMS.WinUI;
using System.Windows.Forms;


using DevExpress.XtraEditors;
using HospitalErpWinUI;

namespace HospitalErpWinUI
{
   
    public class SplashContext : SplashScreenApplicationContext
    {
        public SplashContext()
        { }
        public SplashContext(string xx)
        {
            this.ShowSplashScreen();//这里创建和显示启动窗体 
            this.MainFormLoad();//这里创建和显示启动主窗体 
        }
       
        protected override void OnCreateSplashScreenForm()
        {
            this.SplashScreenForm = new FrmStart();//启动窗体 
        }

        protected override void OnCreateMainForm()
        {
           
            this.PrimaryForm = new FormMain();
         //   this.PrimaryForm.CurrentUsers = this.m_user;
           
        }

        protected override void SetSeconds()
        {
            this.SecondsShow = 2;//启动窗体显示的时间(秒) 
        }
    } 
}
