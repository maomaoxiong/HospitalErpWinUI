﻿using HospitalErpData.MenuHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HospitalErpWinUI.WinFormUiHelper
{
    public class PageFormInf
    {
        public string PageName { get; set; }

        public string FormName { get; set; }

        public List<Perm> Perms { get; set; }
    }
}
