﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraTab;
using HospitalErpWinUI.WinFormUiHelper;
using DevExpress.XtraBars.Ribbon;


namespace HospitalErpWinUI
{
    public class TabPageHelper
    {

        private static Dictionary<string, XtraTabPage> TabPageDic = new Dictionary<string, XtraTabPage>();

        private static TabPageHelper form = null;
        private TabPageHelper() { }

         static TabPageHelper()
        {
            form = new TabPageHelper();
        }
        public static TabPageHelper FormOper
        {
            get
            {
                return form;
            }
        }

        /// <summary>
        /// 新增选项卡页
        /// </summary>
        /// <param name="tabControl">选项卡控件</param>
        /// <param name="tabPageName">当期选项卡页name名称</param>
        /// <param name="tabText">当前选项卡页Text标题</param>
        /// <param name="newFormName">当前选项卡中的新窗体</param>
        public void AddTabpage(XtraTabControl tabControl, PageFormInf inf)
        {
            if (IsTabpageExsit(tabControl, inf.PageName))
            {
                return;
            }
            XtraTabPage newPage = new XtraTabPage();
        //    FormBase form = AddNewForm(inf.FormName);
            RibbonForm form = AddNewForm(inf.FormName);
        //    form._perms = inf.Perms;
            newPage.Name = inf.PageName;
            newPage.Text = form.Text;
            newPage.Tooltip = inf.PageName;
            newPage.Controls.Add(form);
            newPage.Width = tabControl.Width;
           
            tabControl.TabPages.Add(newPage);
            TabPageDic.Add(inf.PageName , newPage);
           // tabControl.SelectedTabPage = newPage;
        }

        /// <summary>
        /// 新增选项卡页
        /// </summary>
        /// <param name="tabControl">选项卡控件</param>
        /// <param name="tabPageName">当期选项卡页name名称</param>
        /// <param name="tabText">当前选项卡页Text标题</param>
        /// <param name="newFormName">当前选项卡中的新窗体</param>
        public void AddTabpage(XtraTabControl tabControl, string tabPageName, string tabText, string newFormName)
        {
            if (IsTabpageExsit(tabControl, tabPageName))
            {
                return;
            }
            
            XtraTabPage newPage = new XtraTabPage();
            newPage.Name = tabPageName;
            newPage.Text = tabText;
            newPage.Tooltip = tabPageName;
            
            newPage.Controls.Add(AddNewForm(newFormName));
            tabControl.TabPages.Add(newPage);
            TabPageDic.Add(tabPageName, newPage);
          //  tabControl.SelectedTabPage = newPage;
        }


     

        /// <summary>
        /// 移除选项卡页
        /// </summary>
        /// <param name="tabControl"></param>
        /// <param name="tabPageName"></param>
        /// <param name="e"></param>
        //public void RemoveTabPage(XtraTabControl tabControl, EventArgs e)
        //{
        //    DevExpress.XtraTab.ViewInfo.ClosePageButtonEventArgs args = (DevExpress.XtraTab.ViewInfo.ClosePageButtonEventArgs)e;
        //    string name = args.Page.Tooltip;
        //    foreach (XtraTabPage item in tabControl.TabPages)
        //    {
        //        if (item.Name == name)
        //        {
        //            tabControl.TabPages.Remove(item);
        //            item.Dispose();
        //            TabPageDic.Remove(name);
        //            return;
        //        }
        //    }
        //}


        public void RemoveTabPage(XtraTabControl tabControl, PageFormInf  inf)
        {
        //    DevExpress.XtraTab.ViewInfo.ClosePageButtonEventArgs args = (DevExpress.XtraTab.ViewInfo.ClosePageButtonEventArgs)e;
            string name = inf.PageName;
            foreach (XtraTabPage item in tabControl.TabPages)
            {
                if (item.Name == name)
                {
                    tabControl.TabPages.Remove(item);
                    item.Dispose();
                    TabPageDic.Remove(name);
                    return;
                }
            }
        }

        /// <summary>
        /// 判断选项卡是否已经存在
        /// </summary>
        /// <param name="tabControl">选项卡控件</param>
        /// <param name="tabPageName">选项卡名称</param>
        /// <returns></returns>
        private bool IsTabpageExsit(XtraTabControl tabControl, string tabPageName)
        {
            foreach (var item in TabPageDic)
            {
                if (item.Key == tabPageName)
                {
                    tabControl.SelectedTabPage = item.Value;
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 在选项卡中生成窗体
        /// </summary>
        /// <param name="form">窗体名称</param>
        private RibbonForm AddNewForm(string formName)
        {
            #region 反射生成窗口对象        
            RibbonForm form = null;
            try
            { 
                Assembly assembly = Assembly.GetExecutingAssembly();
                form = assembly.CreateInstance(formName) as RibbonForm;
                form.FormBorderStyle = FormBorderStyle.None;
                form.WindowState = FormWindowState.Maximized;
                form.Dock = DockStyle.Fill;
                form.TopLevel = false;
           
                form.AutoSize = true; 
                form.AutoScaleMode = AutoScaleMode.Inherit;
                form.Visible = true;
               
            }
            catch(Exception )
            {
                throw new Exception( string.Format( "动态创建窗口{0}失败,请检查窗口命名空间.", formName));
            }
            return form;

            #endregion

        }


    }
}