﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Forms;

namespace HospitalErpWinUI.WinFormUiHelper
{
    public class FormHelper
    {

        private static string GetCtrlValue(Control.ControlCollection controls, string strCtrl)
        { 
            string result= string.Empty;

            System.Windows.Forms.Control control = null;
            foreach (System.Windows.Forms.Control item in controls)
            {
                if (item.Name.Equals(strCtrl))
                {
                    control = item;
                    break;
                }
            }
            switch (control.GetType().Name)
            {
                case "TextEdit":
                         TextEdit textEdit = control as TextEdit;
                         result = textEdit.Text;
                    break;
                case "SearchLookUpEdit":
                        SearchLookUpEdit searchLookUpEdit = control as SearchLookUpEdit;
                        string value = string.Empty;
                        if (searchLookUpEdit.EditValue == null || string.IsNullOrEmpty(searchLookUpEdit.EditValue.ToString()))
                        {
                            string[] strArray = System.Text.RegularExpressions.Regex.Split(searchLookUpEdit.Properties.NullText.ToString().Trim(), @"\s{1,}");
                            value = strArray[0];
                        }
                        else
                        {
                            value = searchLookUpEdit.EditValue.ToString();
                        }
                        result = value;
                    break;
                case "ComboBoxEdit":
                        ComboBoxEdit comboBoxEdit = control as ComboBoxEdit;
                        string[] strArray1 = System.Text.RegularExpressions.Regex.Split(comboBoxEdit.Text.Trim(), @"\s{1,}");
                        result = strArray1[0];
                    break;
                case "RadioGroup":
                    RadioGroup radioGroup = control as RadioGroup;
                    result = radioGroup.EditValue.ToString();
                    break;
                case "ButtonEdit":
                    ButtonEdit buttonEdit = control as ButtonEdit;
                    result = buttonEdit.Text;
                    break;
                case "DateEdit":
                    DateEdit dateEdit = control as DateEdit;
                    result = DateTime.ParseExact(dateEdit.Text, "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture).ToString("yyyy-MM-dd");
                    break;
                default:
                    break;
            }

            return result;
        }
        
        #region 取控件值

        public static string GetValueForDateEdit(DateEdit control)
        {
            string  result = DateTime.ParseExact(control.Text, "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture).ToString("yyyy-MM-dd");
            return result;
        }
        public static string GetValueForComboBoxEdit(ComboBoxEdit control)
        {
            string[] strArray1 = System.Text.RegularExpressions.Regex.Split(control.Text.Trim(), @"\s{1,}");
            return strArray1[0];
        }
        public static string getSplit(string str) { 
            string[]  s=str.Split(' ');
            return s[0];
        }


        public static string GetValueForCheckEdit(CheckEdit control)
        {
            if (control.EditValue.ToString()=="True")
            {
                return "1";
            }else{
                return "0";
            }
            
        }

        public static string GetValueForSearchLookUpEdit(SearchLookUpEdit control)
        {
            string value = string.Empty;
            if (control.EditValue == null || string.IsNullOrEmpty(control.EditValue.ToString()))
            {
                string[] strArray = System.Text.RegularExpressions.Regex.Split(control.Properties.NullText.ToString().Trim(), @"\s{1,}");
                value = strArray[0];
            }
            else
            {
                value = control.EditValue.ToString();
            }
            return value;
        }
        
        #endregion
        //生成sqlid的参数
        public static void GenerateParamerByControl(Control.ControlCollection controls, ref Dictionary<string, string> dict,List<string > strCtrls)
        {
         
            string value = string.Empty;
            foreach (string name in strCtrls)
            {
                value = GetCtrlValue(controls, name);
                dict.Add(name,value);
            }
        
             
            //IEnumerable<KeyValuePair<string, string>> iList = dict.Reverse();
            //foreach (KeyValuePair<string, string> item in iList)
            //{
            //  result.Add( item.Key, item.Value);
            //}
          //  return result;
        }

        //计算包含中英文字符串长度
        public static int Text_Length(string Text)
        {

            int len = 0;

            for (int i = 0; i < Text.Length; i++)
            {

                byte[] byte_len = Encoding.Default.GetBytes(Text.Substring(i, 1));

                if (byte_len.Length > 1)

                    len += 2; //如果长度大于1，是中文，占两个字节，+2

                else

                    len += 1;  //如果长度等于1，是英文，占一个字节，+1

            }

            return len;

        }

        #region 控件赋值
        public static void DateEditAssignment(DateEdit dateEdit, string value)
        {
            dateEdit.EditValue = value;
        }


        public static void RadioGroupAssignment(RadioGroup radioGroup, string value)
        {
            for (int i = 0; i < radioGroup.Properties.Items.Count; i++ )
            {
                if (radioGroup.Properties.Items[i].Value.ToString().Equals( value))
                { 
                    radioGroup.SelectedIndex = i;
                    radioGroup.EditValue = radioGroup.Properties.Items[i].Value;
                }
            }
          
        }

        public static void ComboBoxeditAssignment(ComboBoxEdit combox, string value)
        {
            if (value.Contains("|||"))
            {
             
                 value = value.Replace("|||", " ");
                string[] strArray1 = System.Text.RegularExpressions.Regex.Split(value.ToString().Trim(), @"\s{1,}");
                foreach (var item in combox.Properties.Items)
                {
                    string[] strArray = System.Text.RegularExpressions.Regex.Split(item.ToString().Trim(), @"\s{1,}");
                    if (strArray1[0].Equals(strArray[0]))
                    {
                        combox.SelectedItem = item;
                    }
                 }
            }
            else
            {
                foreach (var item in combox.Properties.Items)
                {
                    string[] strArray = System.Text.RegularExpressions.Regex.Split(item.ToString().Trim(), @"\s{1,}");
                    if (value.Equals(strArray[0]))
                    {
                        combox.SelectedItem = item;
                    }
                }
            }
          
        }
       
        public static void SearchLookUpEditAssignment(SearchLookUpEdit control, string value)
        {
            control.EditValue = null;
            control.Properties.NullText = value;

        }
        #endregion


    }
}
