﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpData.MenuHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI
{
    public class FormBase : RibbonForm 
    {
        protected DaoHelper _helper;
        public List<string> _serviceIds { get; set; }
        public List<Perm> _perms { get; set; }

        public FormBase()
        {
            _helper = new DaoHelper();
            _serviceIds = new List<string>();
            _perms = new List<Perm>();
        }
        public FormBase(DaoHelper helper, List<Perm> perms, List<string> serviceIds)
        {
            _helper = helper;
            _serviceIds = serviceIds;
            _perms = perms;
        }
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // FormBase
            // 
            this.ClientSize = new System.Drawing.Size(282, 255);
            this.Name = "FormBase";
            this.Load += new System.EventHandler(this.FormBase_Load);
            this.ResumeLayout(false);

        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        private void FormBase_Load(object sender, EventArgs e)
        {

        }
    }
}
