﻿namespace HospitalErpWinUI
{
    partial class FormChild
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormChild));
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraScrollableControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraScrollableControl1
            // 
            resources.ApplyResources(this.xtraScrollableControl1, "xtraScrollableControl1");
            this.xtraScrollableControl1.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("xtraScrollableControl1.Appearance.BackColor")));
            this.xtraScrollableControl1.Appearance.FontSizeDelta = ((int)(resources.GetObject("xtraScrollableControl1.Appearance.FontSizeDelta")));
            this.xtraScrollableControl1.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("xtraScrollableControl1.Appearance.FontStyleDelta")));
            this.xtraScrollableControl1.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("xtraScrollableControl1.Appearance.GradientMode")));
            this.xtraScrollableControl1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("xtraScrollableControl1.Appearance.Image")));
            this.xtraScrollableControl1.Appearance.Options.UseBackColor = true;
            this.xtraScrollableControl1.Controls.Add(this.xtraTabControl1);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            // 
            // xtraTabControl1
            // 
            resources.ApplyResources(this.xtraTabControl1, "xtraTabControl1");
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            this.xtraTabControl1.Resize += new System.EventHandler(this.xtraTabControl1_Resize);
            // 
            // FormChild
            // 
            resources.ApplyResources(this, "$this");
            this.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("FormChild.Appearance.BackColor")));
            this.Appearance.FontSizeDelta = ((int)(resources.GetObject("FormChild.Appearance.FontSizeDelta")));
            this.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("FormChild.Appearance.FontStyleDelta")));
            this.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("FormChild.Appearance.GradientMode")));
            this.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("FormChild.Appearance.Image")));
            this.Appearance.Options.UseBackColor = true;
            this.Controls.Add(this.xtraScrollableControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormChild";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Activated += new System.EventHandler(this.FormChild_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormChild_FormClosing);
            this.Load += new System.EventHandler(this.FormChild_Load);
          
            this.Resize += new System.EventHandler(this.FormChild_Resize);
            this.xtraScrollableControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;

    }
}