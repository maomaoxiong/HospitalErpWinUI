﻿namespace HospitalErpWinUI
{
    partial class FormLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLogin));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textPass = new DevExpress.XtraEditors.TextEdit();
            this.butLogin = new DevExpress.XtraEditors.SimpleButton();
            this.butCancle = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.chk_RemenberPassword = new System.Windows.Forms.CheckBox();
            this.btDbConfig = new System.Windows.Forms.Button();
            this.textUser = new System.Windows.Forms.ComboBox();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            ((System.ComponentModel.ISupportInitialize)(this.textPass.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(214, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "用户名:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(214, 187);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "密  码:";
            // 
            // textPass
            // 
            this.textPass.Location = new System.Drawing.Point(307, 182);
            this.textPass.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textPass.Name = "textPass";
            this.textPass.Properties.PasswordChar = '*';
            this.textPass.Size = new System.Drawing.Size(173, 24);
            this.textPass.TabIndex = 3;
            this.textPass.EditValueChanged += new System.EventHandler(this.textPass_EditValueChanged);
            // 
            // butLogin
            // 
            this.butLogin.Location = new System.Drawing.Point(237, 306);
            this.butLogin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.butLogin.Name = "butLogin";
            this.butLogin.Size = new System.Drawing.Size(75, 40);
            this.butLogin.TabIndex = 0;
            this.butLogin.Text = "登录";
            this.butLogin.Click += new System.EventHandler(this.butLogin_Click);
            // 
            // butCancle
            // 
            this.butCancle.Location = new System.Drawing.Point(359, 306);
            this.butCancle.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.butCancle.Name = "butCancle";
            this.butCancle.Size = new System.Drawing.Size(75, 40);
            this.butCancle.TabIndex = 5;
            this.butCancle.Text = "退出";
            this.butCancle.Click += new System.EventHandler(this.butCancle_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelControl3.Location = new System.Drawing.Point(174, 25);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(340, 39);
            this.labelControl3.TabIndex = 9;
            this.labelControl3.Text = "医院数据综合管理平台";
            // 
            // chk_RemenberPassword
            // 
            this.chk_RemenberPassword.AutoSize = true;
            this.chk_RemenberPassword.BackColor = System.Drawing.Color.Transparent;
            this.chk_RemenberPassword.Location = new System.Drawing.Point(307, 242);
            this.chk_RemenberPassword.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.chk_RemenberPassword.Name = "chk_RemenberPassword";
            this.chk_RemenberPassword.Size = new System.Drawing.Size(90, 22);
            this.chk_RemenberPassword.TabIndex = 10;
            this.chk_RemenberPassword.Text = "记住密码";
            this.chk_RemenberPassword.UseVisualStyleBackColor = false;
            // 
            // btDbConfig
            // 
            this.btDbConfig.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btDbConfig.Location = new System.Drawing.Point(452, 284);
            this.btDbConfig.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btDbConfig.Name = "btDbConfig";
            this.btDbConfig.Size = new System.Drawing.Size(165, 85);
            this.btDbConfig.TabIndex = 13;
            this.btDbConfig.UseVisualStyleBackColor = true;
            this.btDbConfig.Click += new System.EventHandler(this.btDbConfig_Click);
            this.btDbConfig.MouseLeave += new System.EventHandler(this.btDbConfig_MouseLeave);
            this.btDbConfig.MouseHover += new System.EventHandler(this.btDbConfig_MouseHover);
            // 
            // textUser
            // 
            this.textUser.FormattingEnabled = true;
            this.textUser.Location = new System.Drawing.Point(307, 114);
            this.textUser.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.textUser.Name = "textUser";
            this.textUser.Size = new System.Drawing.Size(173, 26);
            this.textUser.TabIndex = 1;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = global::HospitalErpWinUI.Properties.Resources.背景2;
            this.pictureEdit1.Location = new System.Drawing.Point(21, 90);
            this.pictureEdit1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit1.Size = new System.Drawing.Size(164, 175);
            this.pictureEdit1.TabIndex = 14;
            this.pictureEdit1.EditValueChanged += new System.EventHandler(this.pictureEdit1_EditValueChanged);
            // 
            // FormLogin
            // 
            this.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayoutStore = System.Windows.Forms.ImageLayout.Tile;
            this.BackgroundImageStore = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImageStore")));
            this.ClientSize = new System.Drawing.Size(630, 398);
            this.Controls.Add(this.pictureEdit1);
            this.Controls.Add(this.textUser);
            this.Controls.Add(this.btDbConfig);
            this.Controls.Add(this.chk_RemenberPassword);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.butCancle);
            this.Controls.Add(this.butLogin);
            this.Controls.Add(this.textPass);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormLogin";
            this.Text = "FormLogin";
            this.Load += new System.EventHandler(this.FormLogin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.textPass.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit textPass;
        private DevExpress.XtraEditors.SimpleButton butLogin;
        private DevExpress.XtraEditors.SimpleButton butCancle;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.CheckBox chk_RemenberPassword;
        private System.Windows.Forms.Button btDbConfig;
        private System.Windows.Forms.ComboBox textUser;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
    }
}