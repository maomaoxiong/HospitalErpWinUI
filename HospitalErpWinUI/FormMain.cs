﻿using Dao;
using DevExpress.LookAndFeel;
using DevExpress.Skins;
using DevExpress.Utils.Drawing;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Helpers;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.ColorWheel;
using DevExpress.XtraNavBar;
using DevExpress.XtraTabbedMdi;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.MenuPem;
using HospitalErpWinUI.MenuPemHelper;
using HospitalErpWinUI.WinFormUiHelper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI
{
    public partial class FormMain : RibbonForm
    {
        private DaoHelper helper  ;  
        private DateTime m_LastClick = System.DateTime.Now;
        private XtraMdiTabPage m_lastPage = null ;

        #region 工具栏   
        //设置
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage_Setup;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup prgSetp;

        private DevExpress.XtraBars.BarButtonItem barButtonItem_userSetup;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_workSetup;


        //业务模块
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage_Bus;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup prgBus;

        private DevExpress.XtraBars.BarButtonItem barButtonItem_equi;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_mate;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_acct;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_imma;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_budg;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_dept;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_direct;       
        private DevExpress.XtraBars.BarButtonItem barButtonItem_hos;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_hr;

        //皮肤
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage_Skin;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup prgAppearance;
        private DevExpress.XtraBars.RibbonGalleryBarItem rgbiSkins;
        private DevExpress.XtraBars.BarButtonItem bbColorMix;

        //帮助
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage_Help;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup prgHelp;

        private DevExpress.XtraBars.BarButtonItem barButtonItem_instruction;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_about;


        //其他
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Exit;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Login;

        #endregion
        public FormMain()
        {
            InitializeComponent();
            helper = new DaoHelper();
            InitToolsBar();

            this.Width = Screen.PrimaryScreen.Bounds.Width;
            this.Height = Screen.PrimaryScreen.Bounds.Height;

            this.ribbonControl1.MdiMergeStyle = RibbonMdiMergeStyle.Always;
            this.xtraTabbedMdiManager1.MdiParent = this;
            this.xtraTabbedMdiManager1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
           
         

            SkinHelper.InitSkinGallery(rgbiSkins);
            UserLookAndFeel.Default.SetSkinStyle(Properties.Settings.Default.SkinStyle);//设置主题样式
            rgbiSkins.Caption = "主题：" + Properties.Settings.Default.SkinStyle;
         
        }

     
        #region 事件

        private void FrmMain_Load(object sender, EventArgs e)
        {
            //  InitLeftMenu();
            try
            {
                if (string.IsNullOrEmpty(G_User.user.Comp_code) || string.IsNullOrEmpty(G_User.user.Copy_code))
                {
                    DataTable dt = DBTime_AcctYear_cbcs();
                    int Comp_code_count = Convert.ToInt32(dt.Rows[0][6]);
                    int Copy_code_count = Convert.ToInt32(dt.Rows[0][7]);

                    if (Comp_code_count == 1 && Copy_code_count == 1)
                    {
                        G_User.user.Comp_code = dt.Rows[0][4].ToString();
                        G_User.user.Copy_code = dt.Rows[0][5].ToString();
                    }
                    if (G_User.user.User_code.Equals("admin"))
                    {
                        G_User.user.Comp_code = dt.Rows[0][4].ToString();
                    }

                    G_User.user.Mod_Level = Convert.ToInt16( dt.Rows[0][8].ToString());
                    G_User.user.Begin_date = Convert.ToDateTime(dt.Rows[0][2].ToString());
                    G_User.user.End_date = Convert.ToDateTime(dt.Rows[0][3].ToString());
                }

                if (G_User.user.User_code.Equals("admin")
                    && G_User.user.Comp_code != null
                    && G_User.user.Module_name != null
                    )
                {
                    InitLeftMenu();
                    leftMenu.Nodes[0].TreeList.Nodes[0].Expanded = true;
                }

                if (!G_User.user.User_code.Equals("admin")
                    && G_User.user.Copy_code != null
                    && G_User.user.Comp_code != null
                    && G_User.user.Module_name != null
                    )
                {
                    InitLeftMenu();
                    leftMenu.Nodes[0].TreeList.Nodes[0].Expanded = true;

                }
                else
                {
                    //弹出环境变量框
                }
              
            }
            catch(Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
          
        }

        private void leftMenu_MouseDoubleClick(object sender, EventArgs e)
        {
            TreeList tree = sender as TreeList;
            TreeListColumn colName = leftMenu.Columns["name"];
            TreeListColumn colPage = leftMenu.Columns["page"];
            TreeListColumn colTab = leftMenu.Columns["tab"];
            TreeListColumn colid = leftMenu.Columns["id"];
            TreeListColumn colkey = leftMenu.Columns["key"];

            try
            {
                if (tree.FocusedNode != null)
                {
                    string nameValue = tree.FocusedNode[colName].ToString();
                    Page cellValue2 = tree.FocusedNode[colPage] as Page;
                    int keyValue = Convert.ToInt32(tree.FocusedNode[colkey]);
                    Dictionary<string, List<Menu_node>> pagePerms = new Dictionary<string, List<Menu_node>>();
                    if (G_User.user.User_code.Equals("admin"))
                    {
                        pagePerms = AdminMenuPemHelper.page;
                    }
                    else
                    {
                        pagePerms = UserMenuPemHelper.page;
                    }

                    if (pagePerms.Keys.Contains<string>(keyValue.ToString()))
                    {
                        List<Menu_node> menu_nodes = new List<Menu_node>();
                        if (G_User.user.User_code.Equals("admin"))
                        {
                            menu_nodes = AdminMenuPemHelper.page[keyValue.ToString()];
                        }
                        else
                        {
                            menu_nodes = UserMenuPemHelper.page[keyValue.ToString()];
                        }
                        List<PageFormInf> pageFormInfs = new List<PageFormInf>();
                        string paths = string.Empty;
                        foreach (var menu_node in menu_nodes)
                        {
                            string strForm = "HospitalErpWinUI." + menu_node.page.src.Replace(@"\", "/").Replace("/", @".").Substring(0, menu_node.page.src.Length - 5);
                            paths += strForm + ";     ";
                            PageFormInf inf = new PageFormInf();
                            inf.Perms = menu_node.page.perms;
                            inf.FormName = strForm;
                            inf.PageName = menu_node.name;
                            pageFormInfs.Add(inf);
                        }
                        barStatic_menuPath.Caption = paths;
                        FormChild frmChild = this.ActiveMdiChild as FormChild;
                        if (frmChild == null)
                        {
                            FormChild childForm = new FormChild();
                            childForm.Width = this.Width - leftMenu.Width-10;
                            childForm._pageFormInfs = pageFormInfs;
                            childForm.MdiParent = this;
                            childForm.WindowState = FormWindowState.Maximized;
                            childForm.Text = nameValue;
                            childForm.Dock = DockStyle.Fill;
                            childForm.Anchor = AnchorStyles.Right;
                            childForm.Text = nameValue;
                            childForm.Show();
                        }
                        else
                        {
                            Form[] forms = this.MdiChildren;
                            foreach (Form frm in forms)
                            {
                                if (frm.Text.Equals(nameValue))
                                {
                                    frm.Activate();
                                    return;
                                }
                            }
                            if (frmChild.Text.Equals("新窗口"))
                            {
                                frmChild._pageFormInfs = pageFormInfs;
                                frmChild.MdiParent = this;
                                frmChild.WindowState = FormWindowState.Maximized;
                                frmChild.Text = nameValue;
                                frmChild.Dock = DockStyle.Fill;
                                frmChild.Anchor = AnchorStyles.Right;
                                frmChild.AddPages();

                            }
                            else
                            {
                                frmChild.RemovePages();
                                frmChild._pageFormInfs = pageFormInfs;
                                frmChild.MdiParent = this;
                                frmChild.WindowState = FormWindowState.Maximized;
                                frmChild.Text = nameValue;
                                frmChild.Dock = DockStyle.Fill;
                         //       frmChild.Anchor = AnchorStyles.Right;
                                frmChild.AddPages();
                                
                            }

                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
           
        }

        private void xtraTabbedMdiManager1_MouseDown(object sender, MouseEventArgs e)
        {
            XtraMdiTabPage curPage = (sender as XtraTabbedMdiManager).SelectedPage;
            if (curPage == null)
            {
                return;
            }

            if (e.Button == MouseButtons.Left)
            {

                DateTime dt = DateTime.Now;
                TimeSpan span = dt.Subtract(m_LastClick);
                if (span.TotalMilliseconds < 300)  //如果两次点击的时间间隔小于300毫秒，则认为是双击
                {
                    // 限制只有在同一个页签上双击才能关闭.(规避两个页签切换时点太快导致意外关闭页签)
                    if (curPage.Equals(m_lastPage))
                    {
                       
                        bool flag = false;
                       if(this.MdiChildren.Length > 0)
                       {
                           foreach (Form frm in this.MdiChildren)
                           {
                               if (frm.Text.Equals("新窗口"))
                                {
                                   flag = true;
                                   break;
                                }
                           }
                       }
                       if (!flag)
                        {
                            FormChild childForm = new FormChild();
                            childForm.Width = this.Width - leftMenu.Width - 10;
                            childForm._pageFormInfs = null;
                            childForm.MdiParent = this;
                            childForm.WindowState = FormWindowState.Maximized;
                            childForm.Text = "新窗口";
                            childForm.Show();
                        }
                       

                    }
                    //else
                    //{
                    //    FormChild childForm = new FormChild();
                    //    childForm._pageFormInfs = null;
                    //    childForm.MdiParent = this;
                    //    childForm.WindowState = FormWindowState.Maximized;
                    //    childForm.Text = "新窗口";
                    //    childForm.Show();
                    //}                   
                    m_LastClick = dt.AddMinutes(-1);
                }
                else
                {
                    m_LastClick = dt;
                    m_lastPage = curPage;
                }
            }

        }

        private void ribbonControl1_Merge(object sender, RibbonMergeEventArgs e)
        {
            RibbonControl parentRRibbon = sender as RibbonControl;
            RibbonControl childRibbon = e.MergedChild;
            parentRRibbon.StatusBar.MergeStatusBar(childRibbon.StatusBar);

        }

        private void ribbonControl1_UnMerge(object sender, RibbonMergeEventArgs e)
        {
            RibbonControl parentRRibbon = sender as RibbonControl;
            parentRRibbon.StatusBar.UnMergeStatusBar();

        }

        private void ItemClick_work(object sender, ItemClickEventArgs e)
        {
            string name = e.Item.Name;
          
            switch (name)
            {
                case "barButtonItem_userSetup":
                    FormPassword password = new FormPassword();
                    password.ShowDialog();

                    break;

                case "barButtonItem_workSetup":
                    FormSetEnv env = new FormSetEnv();
                    env.ShowDialog();
                    break;
                case "barButtonItem_Exit":
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(this, "你确定要退出系统吗？", "提示！", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                    {
                        Application.Exit();
                    }
                   
                   
                    break;
                case "barButtonItem_Login":
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(this, "你确定要重新登录系统吗？", "提示！", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                    {
                        this.Close();
                        FormLogin login = new FormLogin();
                        login.Show();
                    }
                   
                    break;
                default:
                    break;

            }
        }
        #endregion

        #region 初始化
        private void InitLeftMenu()
        {

            if (G_User.user.User_code.Equals("admin"))
            {
                AdminMenuPemHelper adminMenuPemHelper = new AdminMenuPemHelper();
                adminMenuPemHelper.InitData();
                leftMenu.DataSource = AdminMenuPemHelper.leftMenu["leftMenu"];
            }
            else
            {
                UserMenuPemHelper userMenuPemHelper = new UserMenuPemHelper();
                userMenuPemHelper.InitData(G_User.user);
                leftMenu.DataSource = UserMenuPemHelper.leftMenu["leftMenu"];
            }
            //   leftMenu.DataSource = UserMenuPemHelper.originaLeftMenu["originaLeftMenu"];
            leftMenu.KeyFieldName = "oid";
            leftMenu.ParentFieldName = "parentId";

            leftMenu.OptionsBehavior.Editable = false;

            foreach (TreeListColumn col in leftMenu.Columns)
            {
                //   string dd = col.Caption;
                if (col.FieldName.Equals("id")
                    || col.FieldName.Equals("page")
                    || col.FieldName.Equals("tab")
                     || col.FieldName.Equals("isSelected")
                    || col.FieldName.Equals("key")
                    )
                {
                    col.Visible = false;
                }
                if (col.Caption.Equals("name"))
                {
                    col.OptionsColumn.ReadOnly = true;

                }
            }
            TreeListProperty(leftMenu);
        }
        #endregion

        #region 得到数据
        //得到菜单
        private string GetMenu(string ticket)
        {
            string xml = string.Empty;
            try
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                ServiceParamter paramter = new ServiceParamter("menu_sys", dic);
                xml = helper.ReadMenuForText(paramter);
            }
            catch(Exception  e)
            {
                throw new Exception(e.Message);
            }
           
            return xml;
        }

        private DataTable DBTime_AcctYear_cbcs()
        {
            DataTable dt = null;
            string url = ConfigurationManager.AppSettings["ServicesAdress"] + "/User/DBTime_AcctYear_cbcs";
            Dictionary<string, string> dic = new Dictionary<string, string>();

            try 
            { 
                string data = HttpHelper.CreateHttpGetResponse(url, dic, G_User.user.Ticket, "DBTime_AcctYear_cbcs");              
                JObject jo = (JObject)JsonConvert.DeserializeObject(data);
                dt = Newtonsoft.Json.JsonConvert.DeserializeObject(jo["Data"].ToString(), typeof(DataTable)) as DataTable;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return dt;
        }

        private DataTable GetMods()
        {                 
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sys_perm_mod_list";          
            serviceParamter.Paramters = paramters;

            DaoHelper _helper = new DaoHelper();
            DataTable mods = _helper.ReadDictForSql(serviceParamter);
        //    List<DevExpress.XtraBars.BarItem>  items=  new List<BarItem>();
            return mods;
        }
        #endregion

        #region 辅助方法
        private void InitToolsBar()
        {
            #region 设置

            //      private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage_Setup;
            //private DevExpress.XtraBars.Ribbon.RibbonPageGroup prgSetp;

            //private DevExpress.XtraBars.BarButtonItem barButtonItem_userSetup;
            //private DevExpress.XtraBars.BarButtonItem barButtonItem_workSetup;


            this.ribbonPage_Setup = new RibbonPage();
            this.prgSetp = new RibbonPageGroup();
            this.barButtonItem_userSetup = new BarButtonItem();
            this.barButtonItem_workSetup = new BarButtonItem();

            this.barButtonItem_userSetup.Caption = "设置密码";
            this.barButtonItem_userSetup.Id = 13;
            this.barButtonItem_userSetup.LargeImageIndex = 1;
            this.barButtonItem_userSetup.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P));
            this.barButtonItem_userSetup.Name = "barButtonItem_userSetup";
            this.barButtonItem_userSetup.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ItemClick_work);

            this.barButtonItem_workSetup.Caption = "环境设置";
            this.barButtonItem_workSetup.Id = 14;
            this.barButtonItem_workSetup.LargeImageIndex = 14;
            this.barButtonItem_workSetup.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W));
            this.barButtonItem_workSetup.Name = "barButtonItem_workSetup";
            this.barButtonItem_workSetup.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ItemClick_work);

            this.ribbonControl1.Items.Add(barButtonItem_workSetup);
            this.ribbonControl1.Items.Add(barButtonItem_userSetup);

            this.prgSetp.ItemLinks.Add(this.barButtonItem_workSetup);
            this.prgSetp.ItemLinks.Add(this.barButtonItem_userSetup);
            this.prgSetp.Name = "prgSetp";
            this.prgSetp.ShowCaptionButton = false;
            this.prgSetp.Text = "设置";

            this.ribbonPage_Setup.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
                this.prgSetp});
            this.ribbonPage_Setup.Name = "ribbonPage_Setup";
            this.ribbonPage_Setup.Text = "设置";

            #endregion


            #region 初始化业务模块
            this.ribbonPage_Bus = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.prgBus = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();

            #region 初始化 物流管理
            this.barButtonItem_mate = new DevExpress.XtraBars.BarButtonItem();

            this.barButtonItem_mate.Caption = "物流管理";
            this.barButtonItem_mate.Id = 1;
            this.barButtonItem_mate.LargeImageIndex = 1;
            this.barButtonItem_mate.Name = "barButtonItem_mate";
            this.barButtonItem_mate.Visibility = BarItemVisibility.Never;
            this.barButtonItem_mate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ItemClick_work);

            #endregion

            #region 初始化 会计核算
            this.barButtonItem_acct = new DevExpress.XtraBars.BarButtonItem();

            this.barButtonItem_acct.Caption = "会计核算";
            this.barButtonItem_acct.Id = 2;
            this.barButtonItem_acct.LargeImageIndex = 2;
            this.barButtonItem_acct.Name = "barButtonItem_acct";
            this.barButtonItem_acct.Visibility = BarItemVisibility.Never;
            this.barButtonItem_acct.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ItemClick_work);

            #endregion

            #region 初始化 固定资产
            this.barButtonItem_equi = new DevExpress.XtraBars.BarButtonItem();

            this.barButtonItem_equi.Caption = "固定资产";
            this.barButtonItem_equi.Id = 3;
            this.barButtonItem_equi.LargeImageIndex = 3;
            this.barButtonItem_equi.Name = "barButtonItem_equi";
            this.barButtonItem_equi.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ItemClick_work);
            this.barButtonItem_equi.Visibility = BarItemVisibility.Never;
            #endregion

            #region 初始化 无形资产
            this.barButtonItem_imma = new DevExpress.XtraBars.BarButtonItem();

            this.barButtonItem_imma.Caption = "无形资产";
            this.barButtonItem_imma.Id = 4;
            this.barButtonItem_imma.LargeImageIndex = 4;
            this.barButtonItem_imma.Name = "barButtonItem_imma";
            this.barButtonItem_imma.Visibility = BarItemVisibility.Never;
            this.barButtonItem_imma.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ItemClick_work);
            #endregion

            #region 初始化 预算管理
            this.barButtonItem_budg = new DevExpress.XtraBars.BarButtonItem();

            this.barButtonItem_budg.Caption = "预算管理";
            this.barButtonItem_budg.Id = 5;
            this.barButtonItem_budg.LargeImageIndex = 5;
            this.barButtonItem_budg.Name = "barButtonItem_budg";
            this.barButtonItem_budg.Visibility = BarItemVisibility.Never;
            this.barButtonItem_budg.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ItemClick_work);
            #endregion

            #region 初始化 科室核算系统
            this.barButtonItem_dept = new DevExpress.XtraBars.BarButtonItem();

            this.barButtonItem_dept.Caption = "科室核算系统";
            this.barButtonItem_dept.Id = 6;
            this.barButtonItem_dept.LargeImageIndex = 6;
            this.barButtonItem_dept.Name = "barButtonItem_dept";
            this.barButtonItem_dept.Visibility = BarItemVisibility.Never;
            this.barButtonItem_dept.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ItemClick_work);
            #endregion

            #region 初始化 科主任经营分析系统
            this.barButtonItem_direct = new DevExpress.XtraBars.BarButtonItem();

            this.barButtonItem_direct.Caption = "科主任分析";
            this.barButtonItem_direct.Id = 7;
            this.barButtonItem_direct.LargeImageIndex = 7;
            this.barButtonItem_direct.Name = "barButtonItem_direct";
            this.barButtonItem_direct.Visibility = BarItemVisibility.Never;
            this.barButtonItem_direct.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ItemClick_work);
            #endregion

            #region 初始化 医院运营综合分析平台
            this.barButtonItem_hos = new DevExpress.XtraBars.BarButtonItem();

            this.barButtonItem_hos.Caption = "医院分析";
            this.barButtonItem_hos.Id = 8;
            this.barButtonItem_hos.LargeImageIndex = 8;
            this.barButtonItem_hos.Name = "barButtonItem_hos";
            this.barButtonItem_hos.Visibility = BarItemVisibility.Never;
            this.barButtonItem_hos.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ItemClick_work);
            #endregion

            #region 初始化 人力资源
            this.barButtonItem_hr = new DevExpress.XtraBars.BarButtonItem();

            this.barButtonItem_hr.Caption = "人力资源";
            this.barButtonItem_hr.Id = 9;
            this.barButtonItem_hr.LargeImageIndex = 9;
            this.barButtonItem_hr.Name = "barButtonItem_hr";
            this.barButtonItem_hr.Visibility = BarItemVisibility.Never;
            this.barButtonItem_hr.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ItemClick_work);
            #endregion

            this.prgBus.ItemLinks.Add(this.barButtonItem_mate);
            this.prgBus.ItemLinks.Add(this.barButtonItem_equi);
            this.prgBus.ItemLinks.Add(this.barButtonItem_acct);
            this.prgBus.ItemLinks.Add(this.barButtonItem_imma);
            this.prgBus.ItemLinks.Add(this.barButtonItem_budg);
            this.prgBus.ItemLinks.Add(this.barButtonItem_dept);
            this.prgBus.ItemLinks.Add(this.barButtonItem_direct);
            this.prgBus.ItemLinks.Add(this.barButtonItem_hos);
            this.prgBus.ItemLinks.Add(this.barButtonItem_hr);
            this.prgBus.Name = "prgBus";
            this.prgBus.ShowCaptionButton = false;
            this.prgBus.Text = "业务模块";

            this.ribbonPage_Bus.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
                this.prgBus});
            this.ribbonPage_Bus.Name = "ribbonPage_Bus";
            this.ribbonPage_Bus.Text = "业务模块";

            #endregion

            #region  初始化皮肤
            this.ribbonPage_Skin = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.prgAppearance = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rgbiSkins = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.bbColorMix = new DevExpress.XtraBars.BarButtonItem();

            this.rgbiSkins.Id = 10;
            this.rgbiSkins.Name = "rgbiSkins";
            this.rgbiSkins.GalleryItemClick += new DevExpress.XtraBars.Ribbon.GalleryItemClickEventHandler(this.rgbiSkins_GalleryItemClick);
            // 
            // bbColorMix
            // 
            this.bbColorMix.Caption = "&Color Mix";
            this.bbColorMix.Glyph = global::HospitalErpWinUI.Properties.Resources.ColorMixer_16x16;
            this.bbColorMix.Id = 11;
            this.bbColorMix.LargeGlyph = global::HospitalErpWinUI.Properties.Resources.ColorMixer_32x32;
            this.bbColorMix.LargeImageIndex = 0;
            this.bbColorMix.Name = "bbColorMix";
            this.bbColorMix.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbColorMix_ItemClick);

            this.prgAppearance.ItemLinks.Add(this.rgbiSkins);
            this.prgAppearance.ItemLinks.Add(this.bbColorMix);
            this.prgAppearance.Name = "prgAppearance";
            this.prgAppearance.ShowCaptionButton = false;
            this.prgAppearance.Text = "更换皮肤";

            this.ribbonPage_Skin.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.prgAppearance});
            this.ribbonPage_Skin.Name = "ribbonPage_Skin";
            this.ribbonPage_Skin.Text = "皮肤";

            #endregion

            #region 帮助
            this.ribbonPage_Help = new RibbonPage();
            this.prgHelp = new RibbonPageGroup();
            this.barButtonItem_instruction = new BarButtonItem();
            this.barButtonItem_about = new BarButtonItem();

            this.barButtonItem_instruction.Caption = "使用说明";
            this.barButtonItem_instruction.Id = 11;
            this.barButtonItem_instruction.LargeImageIndex = 1;
            this.barButtonItem_instruction.Name = "barButtonItem_instruction";
            this.barButtonItem_instruction.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ItemClick_work);

            this.barButtonItem_about.Caption = "关于";
            this.barButtonItem_about.Id = 12;
            this.barButtonItem_about.LargeImageIndex = 12;
            this.barButtonItem_about.Name = "barButtonItem_about";
            this.barButtonItem_about.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ItemClick_work);

            this.ribbonControl1.Items.Add(barButtonItem_instruction);
            this.ribbonControl1.Items.Add(barButtonItem_about);

            this.prgHelp.ItemLinks.Add(this.barButtonItem_instruction);
            this.prgHelp.ItemLinks.Add(this.barButtonItem_about);
            this.prgHelp.Name = "prgHelp";
            this.prgHelp.ShowCaptionButton = false;
            this.prgHelp.Text = "帮助";

            this.ribbonPage_Help.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
                this.prgHelp});
            this.ribbonPage_Help.Name = "ribbonPage_Help";
            this.ribbonPage_Help.Text = "帮助";

            #endregion

            DataTable mods = GetMods();

            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.rgbiSkins,
            this.bbColorMix
            });
            for (int i = 0; i < mods.Rows.Count; i++)
            {
                string mod = mods.Rows[i][0].ToString();
                if (!string.IsNullOrEmpty(mod))
                {
                    string butName = "barButtonItem_" + mod.Trim();
                    DevExpress.XtraBars.BarButtonItem o = (DevExpress.XtraBars.BarButtonItem)this.GetType().GetField(butName, System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.IgnoreCase).GetValue(this);
                    o.Visibility = BarItemVisibility.Always;
                    this.ribbonControl1.Items.Add(o);
                   
                }
                if (mods.Rows[i][0].ToString().Equals(G_User.user.Module_name))
                {
                    G_User.user.Module_code= mods.Rows[i][2].ToString();
                }
            }

            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
                this.ribbonPage_Bus,
                this.ribbonPage_Skin,
                this.ribbonPage_Setup,
                this.ribbonPage_Help,              
               
            });


            // Exit           
            this.barButtonItem_Exit = new BarButtonItem();
            this.barButtonItem_Exit.Caption = "退出";
      
            this.barButtonItem_Exit.Hint = "退出";
            this.barButtonItem_Exit.Id = 13;
            this.barButtonItem_Exit.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E));
            this.barButtonItem_Exit.LargeImageIndex = 13;
            this.barButtonItem_Exit.Name = "barButtonItem_Exit";
            this.barButtonItem_Exit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ItemClick_work);
            this.ribbonControl1.Items.Add(this.barButtonItem_Exit);

            // Login       
            this.barButtonItem_Login = new BarButtonItem();
            this.barButtonItem_Login.Caption = "重新登录";

            this.barButtonItem_Login.Hint = "重新登录";
            this.barButtonItem_Login.Id = 14;
            this.barButtonItem_Login.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R));
            this.barButtonItem_Login.LargeImageIndex = 14;
            this.barButtonItem_Login.Name = "barButtonItem_Login";
            this.barButtonItem_Login.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ItemClick_work);
            this.ribbonControl1.Items.Add(this.barButtonItem_Login);

            this.ribbonControl1.Toolbar.ItemLinks.Add(this.barButtonItem_userSetup);
            this.ribbonControl1.Toolbar.ItemLinks.Add(this.barButtonItem_workSetup);
            this.ribbonControl1.Toolbar.ItemLinks.Add(this.barButtonItem_Exit);
            this.ribbonControl1.Toolbar.ItemLinks.Add(this.barButtonItem_Login);
        }

        //树的初始状态
        private void TreeListProperty(TreeList tl)
        {
            tl.OptionsView.ShowColumns = false;//是否显示选中的行
            tl.OptionsBehavior.Editable = false;//不可编辑
            tl.OptionsView.ShowHorzLines = false;//OptionsView提供对树状列表的显示选项,设置水平线是否显示
            tl.OptionsView.ShowIndicator = false;//节点的指示面板是否显示
            tl.OptionsView.ShowVertLines = false;//垂直线条是否显示
            //设置treeList的折叠样式为 +  - 号
            tl.LookAndFeel.UseDefaultLookAndFeel = false;
            tl.LookAndFeel.UseWindowsXPTheme = true;
            tl.OptionsSelection.InvertSelection = true;//聚焦的样式是否只适用于聚焦细胞或所有细胞除了聚焦对象，失去焦点后
        }

        public static void SetDouble(Control cc)
        {

            cc.GetType().GetProperty("DoubleBuffered", System.Reflection.BindingFlags.Instance |
                         System.Reflection.BindingFlags.NonPublic).SetValue(cc, true, null);

        }

        private void bbColorMix_ItemClick(object sender, ItemClickEventArgs e)
        {
            ColorWheelForm form = new ColorWheelForm();
            form.StartPosition = FormStartPosition.CenterParent;
            form.SkinMaskColor = UserLookAndFeel.Default.SkinMaskColor;
            form.SkinMaskColor2 = UserLookAndFeel.Default.SkinMaskColor2;
            form.ShowDialog(this);
        }

        private void rgbiSkins_GalleryItemClick(object sender, GalleryItemClickEventArgs e)
        {
            string name = string.Empty;
            string caption = string.Empty;
            if (rgbiSkins.Gallery == null) return;
            caption = rgbiSkins.Gallery.GetCheckedItems()[0].Caption;//主题的描述
            caption = caption.Replace("主题：", "");
            //name = bsiPaintStyle.Manager.PressedLink.Item.Tag.ToString();//主题的名称
            rgbiSkins.Caption = "主题：" + caption;

            Properties.Settings.Default.SkinStyle = caption;
            Properties.Settings.Default.Save();
        }


    
        #endregion

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

      
    }
}
