﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.acctbook.subj.totalquery.totalbyday
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        DataTable _user = new DataTable();
        //初始化绑定默认关键词（此数据源可以从数据库取） 
        List<string> _groupsInit = new List<string>();
        List<string> _groupsChange = new List<string>();
        string[] codeArray2 = new String[] { };
        string[] valueArray2 = new String[] { };
        public main()
        {
            _helper = new DaoHelper();
            InitializeComponent();
        }

        private void main_Load(object sender, EventArgs e)
        {
            init();
        }
        private void init()
        {
            getAcctBYear();
            getAcctEYear();
            getAcctBMonth();
            getAcctEMonth();
            getAcctSubj();
        }


        #region 自定义方法
        //取得开始年
        private void getAcctBYear()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_acctbook_subjYearCross";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            serviceParamter.Paramters = paramters;

            List<string> companys = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                companys.Add(codeValue.Code);
            }

            comboBoxEdit2.Properties.Items.AddRange(companys);
            comboBoxEdit2.SelectedItem = companys[0];
        }
        //取得结束年
        private void getAcctEYear()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_acctbook_subjYearCross";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            serviceParamter.Paramters = paramters;


            List<string> companys2 = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                companys2.Add(codeValue.Code);
            }

            comboBoxEdit3.Properties.Items.AddRange(companys2);
            comboBoxEdit3.SelectedItem = companys2[0];
        }
        //取得开始月
        private void getAcctBMonth()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_acctbook_subjMonth";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            serviceParamter.Paramters = paramters;
            List<string> companys3 = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                companys3.Add(codeValue.Value);
            }

            comboBoxEdit4.Properties.Items.AddRange(companys3);
            comboBoxEdit4.SelectedItem = companys3[0];
        }
        //取得结束月
        private void getAcctEMonth()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_acctbook_subjMonth";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            serviceParamter.Paramters = paramters;
            List<string> companys3 = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                companys3.Add(codeValue.Value);
            }

            comboBoxEdit5.Properties.Items.AddRange(companys3);
            comboBoxEdit5.SelectedItem = companys3[0];
        }
        //取得会计科目
        private void getAcctSubj()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_subj_specialName_autochange";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            paramters.Add("subj_code", "1001");
            paramters.Add("key", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit1).Trim());

            serviceParamter.Paramters = paramters;

            List<CodeValue> objs = new List<CodeValue>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue);
            }
            searchLookUpEdit1.Properties.ValueMember = "Code";
            searchLookUpEdit1.Properties.DisplayMember = "Value";
            searchLookUpEdit1.Properties.DataSource = objs;
        }

        private void Query()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_acctbook_subj_totalquery_totalbyday_query";
            paramters.Add("acct_year", G_User.user.Acct_year);
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("subjCode", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit1).Trim());
            paramters.Add("acct_monthB", "01");
            paramters.Add("acct_monthE", "05");
            paramters.Add("acct_YearB", "2019");
            paramters.Add("acct_YearE", "2019");
            paramters.Add("cb1", FormHelper.GetValueForCheckEdit(checkEdit3).Trim());
            paramters.Add("cb2", FormHelper.GetValueForCheckEdit(checkEdit1).Trim());
            paramters.Add("isprint", "0"); //默认是0
            paramters.Add("summary", "");
            paramters.Add("vouch_vouchType", "");
            paramters.Add("v_bvouchNo", "");
            paramters.Add("v_evouchNo", "");
            paramters.Add("attachmentS", "");
            paramters.Add("attachmentE", "");
            paramters.Add("vouch_direct", "");
            paramters.Add("b_vouch_amount", "");
            paramters.Add("e_vouch_amount", "");
            paramters.Add("check_oper", "");
            paramters.Add("emp_check", "");
            paramters.Add("mark_oper", ""); 

            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;

        }

        #endregion

        private void acct_acctbook_subj_totalquery_totalbyday_query_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
    }
}
