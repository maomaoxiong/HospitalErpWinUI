﻿

using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraGrid;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;  
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.acctbook.balance.subj
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        DataTable _user = new DataTable();
        //初始化绑定默认关键词（此数据源可以从数据库取）
        List<string> _groupsInit = new List<string>();
        List<string> _groupsChange = new List<string>();


        public main()
        {
            _helper = new DaoHelper();
            InitializeComponent();
        }

        #region 获取数据
        //获取科目类型数据
        private void getAcctSubjType()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_itemcode_type_list";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("key", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit3).Trim());

            serviceParamter.Paramters = paramters;

            List<CodeValue> objs = new List<CodeValue>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue);
            }
            //    emp_code.Properties.Items.AddRange(emps);
            searchLookUpEdit3.Properties.ValueMember = "Code";
            searchLookUpEdit3.Properties.DisplayMember = "Value";
            searchLookUpEdit3.Properties.DataSource = objs;
        }
        //获取科目字典数据
        private void getAcctSubj1()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_subj_specialName_BySubjtype";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            paramters.Add("sub_type_code", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit3).Trim());
            paramters.Add("key", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit1).Trim());

            serviceParamter.Paramters = paramters;

            List<CodeValue> objs = new List<CodeValue>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue);
            }
            searchLookUpEdit1.Properties.ValueMember = "Code";
            searchLookUpEdit1.Properties.DisplayMember = "Value";
            searchLookUpEdit1.Properties.DataSource = objs;
        }

        private void getAcctSubj2()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_subj_specialName_BySubjtype";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            paramters.Add("sub_type_code", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit3).Trim());
            paramters.Add("key", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit2).Trim());
            serviceParamter.Paramters = paramters;

            List<CodeValue> objs = new List<CodeValue>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue);
            }
            //    emp_code.Properties.Items.AddRange(emps);
            searchLookUpEdit2.Properties.ValueMember = "Code";
            searchLookUpEdit2.Properties.DisplayMember = "Value";
            searchLookUpEdit2.Properties.DataSource = objs;
        }

        // 获取科目级次
        private void getSubjLevel()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_SubLevelList";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            serviceParamter.Paramters = paramters;


            DataTable groups = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < groups.Rows.Count; i++)
            {
                _groupsInit.Add(groups.Rows[i][1].ToString());

            }
            comboBoxEdit1.Properties.Items.AddRange(_groupsInit);
        }


        #endregion


        #region 其他
        private void Init()
        {
            //科目类型数据赋值
            getAcctSubjType();
            //科目字段数据赋值
            getAcctSubj1();
            getAcctSubj2();

            //设置默认日期
            dateEdit1.Text = DateTime.Now.ToString("yyyy-MM-01");//本月第一天
            dateEdit2.Text = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-01")).AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd");//本月最后一天
            
            //初始科目级次
            getSubjLevel();

            //初始修改超链接页面
            
        }

      

        private void main_Load(object sender, EventArgs e)
        {
            Init();
        }
        #endregion


        #region 事件
        private void dateEdit1_EditValueChanged(object sender, EventArgs e)
        {
            dateEdit1.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            dateEdit1.Properties.EditMask = "yyyy-MM-dd";
        }

        private void dateEdit2_EditValueChanged(object sender, EventArgs e)
        {
            dateEdit2.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            dateEdit2.Properties.EditMask = "yyyy-MM-dd";

        }

        #endregion




        #region  主页面数据查询
        private void Query()
        {
           
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_acctbook_balance_subj";
           
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            paramters.Add("start_time", dateEdit1.DateTime.ToString("yyyy-MM-dd"));
            paramters.Add("end_time", dateEdit2.DateTime.ToString("yyyy-MM-dd"));
            paramters.Add("subj_level", FormHelper.GetValueForComboBoxEdit(comboBoxEdit1).Trim());
            paramters.Add("start_subj", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit1).Trim());
            paramters.Add("end_subj", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit2).Trim());
            paramters.Add("is_acc",FormHelper.GetValueForCheckEdit(checkEdit1).Trim());
            paramters.Add("is_cancel", FormHelper.GetValueForCheckEdit(checkEdit2).Trim());
            paramters.Add("subj_type_code", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit3).Trim());
            paramters.Add("C1",FormHelper.GetValueForCheckEdit(checkEdit3).Trim());
            paramters.Add("C2", FormHelper.GetValueForCheckEdit(checkEdit4).Trim());
            paramters.Add("C3",FormHelper.GetValueForCheckEdit(checkEdit5).Trim());
            paramters.Add("C4", FormHelper.GetValueForCheckEdit(checkEdit6).Trim());
            paramters.Add("C5", FormHelper.GetValueForCheckEdit(checkEdit7).Trim());
            paramters.Add("curr_code", "RMB");
            
          //  paramters.Add("sub_type_code", strArray2[0]);
            

            //DateTime.Parse("2016-3-16 12:12:12")

            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;

        }
        #endregion


        #region  事件
        private void acct_balance_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }



        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {

        }

      
        private void searchLookUpEdit1_EditValueChange(object sender, EventArgs e)
        {
            try
            {
                searchLookUpEdit1.EditValue = "";
                searchLookUpEdit2.EditValue = "";
                getAcctSubj1();
                getAcctSubj2();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);

            }
        }



 #endregion

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            if (bandedGridView1.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                int index = bandedGridView1.GetDataSourceRowIndex(bandedGridView1.FocusedRowHandle);
                Console.WriteLine(index);
                Console.WriteLine(_user.Rows[0]["acct_subj_code"].ToString());
                Console.WriteLine(_user.Rows[1]["acct_subj_code"].ToString());
                Console.WriteLine(_user.Rows[2]["acct_subj_code"].ToString());
               
                String acctSubjCode = _user.Rows[index]["acct_subj_code"].ToString();
                String acctSubjName = _user.Rows[index]["acct_subj_name"].ToString();
                String startTime = dateEdit1.Text;
                String endTime = dateEdit2.Text;
                String cb1 = FormHelper.GetValueForCheckEdit(checkEdit1).Trim();
                String cb2 = FormHelper.GetValueForCheckEdit(checkEdit2).Trim();

                Console.WriteLine(index);
               // Console.WriteLine(acctSubjCode);
                //Console.WriteLine(acctSubjName);
                Console.WriteLine(endTime);
                update frm = new update(this._helper, acctSubjCode, acctSubjName, startTime, endTime, cb1, cb2);
                if (frm.ShowDialog() == DialogResult.No)
                {
                    MessageForm.Warning("修改失败！");
                }

            }
        }

       
  
    }
}
