﻿using Dao;
using DevExpress.XtraGrid;
using HospitalErpWinUI.GlobalVar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
 
namespace HospitalErpWinUI.hbos.acct.acctbook.balance.subj
{

    
    public partial class update : Form
    { 
        private DaoHelper _helper;
        DataTable _user = new DataTable();
        private String _acctSubjCode;
        private String _acctSubjName;
        private String _startTime;
        private String _endTime;
        private String _cb1;
        private String _cb2;

        public update(DaoHelper helper, String acctSubjCode, String acctSubjName, String startTime, String endTime, String cb1, String cb2)
        {
            this._helper = helper;
            this._acctSubjCode = acctSubjCode;
            this._acctSubjName = acctSubjName;
            this._startTime = startTime;
            this._endTime = endTime;
            this._cb1 = cb1;
            this._cb2 = cb2;


            InitializeComponent();
        }


        public update()
        {
            InitializeComponent();
        }


        #region 访问数据
        private void selectData() {
            
             ServiceParamter serviceParamter = new ServiceParamter();
             Dictionary<string, string> paramters = new Dictionary<string, string>();
             serviceParamter.ServiceId = "acct_acctbook_subj_three_query1";
             paramters.Add("acct_year", G_User.user.Acct_year);
             paramters.Add("comp_code", G_User.user.Comp_code);
             paramters.Add("copy_code", G_User.user.Copy_code);
             paramters.Add("acct_subj_code", _acctSubjCode);
             paramters.Add("Bmonth", _startTime.Split('-')[1]);
             paramters.Add("Emonth", _endTime.Split('-')[1]);
             paramters.Add("Byear", G_User.user.Acct_year);
             paramters.Add("Eyear", G_User.user.Acct_year);
             paramters.Add("cb1", _cb1);
             paramters.Add("cb2", _cb2);
             paramters.Add("isprint", "0");
             paramters.Add("dayB", _startTime.Split('-')[1]);
             paramters.Add("dayE", _endTime.Split('-')[1]);
             paramters.Add("summar", "");
             paramters.Add("vouch_vouchType", "");
             paramters.Add("v_bvouchNo", "");
             paramters.Add("v_evouchNo", "");
             paramters.Add("attachmentS", "");
             paramters.Add("attachmentE", "");
             paramters.Add("vouch_direct", "");
             paramters.Add("b_vouch_amount", "");
             paramters.Add("e_vouch_amount", "");
             paramters.Add("check_oper", "");
             paramters.Add("emp_check", "");
             paramters.Add("mark_oper", "");
           
             serviceParamter.Paramters = paramters;

             DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
             dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
             for (int i = 0; i < dt.Rows.Count; i++)
             {
                 dt.Rows[i]["checked"] = "false";
             }
             _user = dt;
             gridControl1.DataSource = _user;
             


        }

        #endregion

        private void init() {
           selectData();
        
        }

        private void update_Load(object sender, EventArgs e)
        {
            init();

        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, DevExpress.XtraEditors.Controls.OpenLinkEventArgs e)
        {
            Console.WriteLine(111);
            if (bandedGridView1.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                Console.WriteLine(222);
                int index = bandedGridView1.GetDataSourceRowIndex(bandedGridView1.FocusedRowHandle);
                String vouch_id = _user.Rows[index]["pzid"].ToString();


                Console.WriteLine(vouch_id);

            }
                

        }

    }
}
