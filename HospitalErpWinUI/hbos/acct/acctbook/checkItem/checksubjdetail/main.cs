﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.BandedGrid;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text; 
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.acctbook.checkItem.checksubjdetail
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        DataTable _user = new DataTable();

        //初始化绑定默认关键词（此数据源可以从数据库取） 
        List<string> _groupsInit = new List<string>();
        List<string> _groupsChange = new List<string>();
        List<String> _itemsList = new List<String>();
        List<String> _itemCodeList = new List<String>();
        List<String> _itemCodeList2 = new List<String>();
        string[] codeArray2 = new String[] { };
        string[] valueArray2 = new String[] { };
        public main()
        {
            _helper = new DaoHelper();
            InitializeComponent();
        }

        private void main_Load(object sender, EventArgs e)
        {
            init();
        }

        private void init()
        {
            getAcctBYear();
            getAcctEYear();
            getAcctBMonth();
            getAcctEMonth();
            getItemList();

        }

        #region 自定义方法
        //取得开始年
        private void getAcctBYear()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_acctbook_subjYearCross";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            serviceParamter.Paramters = paramters;

            List<string> companys = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                companys.Add(codeValue.Code);
            }

            comboBoxEdit1.Properties.Items.AddRange(companys);
            comboBoxEdit1.SelectedItem = companys[0];
        }
        //取得结束年
        private void getAcctEYear()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_acctbook_subjYearCross";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            serviceParamter.Paramters = paramters;


            List<string> companys2 = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                companys2.Add(codeValue.Code);
            }

            comboBoxEdit3.Properties.Items.AddRange(companys2);
            comboBoxEdit3.SelectedItem = companys2[0];
        }
        //取得开始月
        private void getAcctBMonth()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_acctbook_subjMonth";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            serviceParamter.Paramters = paramters;
            List<string> companys3 = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                companys3.Add(codeValue.Value);
            }

            comboBoxEdit2.Properties.Items.AddRange(companys3);
            comboBoxEdit2.SelectedItem = companys3[0];
        }
        //取得结束月
        private void getAcctEMonth()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_acctbook_subjMonth";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            serviceParamter.Paramters = paramters;
            List<string> companys3 = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                companys3.Add(codeValue.Value);
            }

            comboBoxEdit4.Properties.Items.AddRange(companys3);
            comboBoxEdit4.SelectedItem = companys3[0];
        }

        //取得会计科目
        private void getAcctSubjB()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_subj_specialNameForCheck6";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            paramters.Add("subj_b", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit1).Trim());
            paramters.Add("subj_e", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit2).Trim());
            paramters.Add("key", getSubjItemParas());
            paramters.Add("py", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit1).Trim());

            serviceParamter.Paramters = paramters;

            List<CodeValue> objs = new List<CodeValue>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue);
            }
            searchLookUpEdit1.Properties.ValueMember = "Code";
            searchLookUpEdit1.Properties.DisplayMember = "Value";
            searchLookUpEdit1.Properties.DataSource = objs;
        }
        private void getAcctSubjE()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_subj_specialNameForCheck6";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            paramters.Add("subj_b", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit1).Trim());
            paramters.Add("subj_e", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit2).Trim());
            paramters.Add("key", getSubjItemParas());
            paramters.Add("py", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit2).Trim());

            serviceParamter.Paramters = paramters;

            List<CodeValue> objs = new List<CodeValue>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue);
            }
            searchLookUpEdit2.Properties.ValueMember = "Code";
            searchLookUpEdit2.Properties.DisplayMember = "Value";
            searchLookUpEdit2.Properties.DataSource = objs;
        }
        //获取核算项数据
        private void getItemList()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_allCheckItem_list";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            serviceParamter.Paramters = paramters;
            DataTable groups = _helper.ReadDictForSql(serviceParamter);
            checkedComboBoxEdit1.Properties.Items.Clear();

            CheckedListBoxItem[] itemListQuery = new CheckedListBoxItem[groups.Rows.Count + 1];
            int check = 1;
            for (int i = 0; i < groups.Rows.Count; i++)
            {
                itemListQuery[check] = new CheckedListBoxItem(groups.Rows[i][0].ToString(), groups.Rows[i][1].ToString());
                check++;

            }

            checkedComboBoxEdit1.Properties.Items.AddRange(itemListQuery);



        }

        //动态创建辅助核算下拉框
        private void createItemList(String itemNames)
        {
            string[] itemNameArry = itemNames.Split(',');
            int i = 30;
            int g = 60;
            int j = 1;
            int k = 0;

            //删除动态控件
            deleteItem(_itemsList, i, g);



            foreach (string s in itemNameArry)
            {

                if (s.Equals(""))
                {
                    return;
                }
                //通过code获取value


                LabelControl labelControl = new LabelControl();
                SearchLookUpEdit searchLookUpEdit = new SearchLookUpEdit();

                labelControl.Name = "labelControl" + i;
                labelControl.Location = new System.Drawing.Point(30 * j, 45);
                labelControl.TabIndex = i + 1;
                labelControl.Text = s + ":";


                searchLookUpEdit.Name = "searchLookUpEdit" + i;
                searchLookUpEdit.Location = new System.Drawing.Point(labelControl.Location.X + 50, 40);
                searchLookUpEdit.TabIndex = i + 1;
                searchLookUpEdit.Properties.NullText = "";
                searchLookUpEdit.Properties.ShowFooter = false;
                searchLookUpEdit.Properties.ShowClearButton = false;
                searchLookUpEdit.EditValueChanged += new System.EventHandler(searchLookUpEdit_EditValueChange);
                //searchLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(searchLookUpEdit_ButtonClick);
                // searchLookUpEdit.EditValue = "000000";
                LabelControl labelControl2 = new LabelControl();
                SearchLookUpEdit searchLookUpEdit2 = new SearchLookUpEdit();
                labelControl2.Name = "labelControl" + g;
                labelControl2.Location = new System.Drawing.Point(searchLookUpEdit.Location.X + searchLookUpEdit.Size.Width, 45);
                labelControl2.TabIndex = g + 1;
                labelControl2.Text = "到";
                searchLookUpEdit2.Name = "searchLookUpEdit" + g;
                searchLookUpEdit2.Location = new System.Drawing.Point(labelControl2.Location.X + 15, 40);
                searchLookUpEdit2.TabIndex = g + 1;
                searchLookUpEdit2.Properties.NullText = "";
                searchLookUpEdit2.Properties.ShowFooter = false;
                searchLookUpEdit2.Properties.ShowClearButton = false;
                searchLookUpEdit2.EditValueChanged += new System.EventHandler(searchLookUpEdit2_EditValueChange);
                panelControl1.Controls.Add(labelControl);
                panelControl1.Controls.Add(searchLookUpEdit);
                panelControl1.Controls.Add(labelControl2);
                panelControl1.Controls.Add(searchLookUpEdit2);


                String cid = getCid(searchLookUpEdit, k);

                getSelectItemList(cid, searchLookUpEdit);
                getSelectItemList(cid, searchLookUpEdit2);

                k++;
                i++;
                g++;
                j = j + 10;

            }

            _itemsList.Clear();

            foreach (string s in itemNameArry)
            {
                _itemsList.Add(s);
            }

            getAcctSubjB();
            getAcctSubjE();
        }

        //得到辅助核算的cid
        private String getCid(SearchLookUpEdit searchLookUpEdit, int k)
        {
            String para = "<root><tbody><tr><pk>" + codeArray2[k].Trim() + "</pk><td1>" + codeArray2[k].Trim() + "</td1><td2></td2><td3></td3><td4></td4></tr></tbody></root>";
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_subj_ActiveClassBetweenConditionFast";
            paramters.Add("acct_year", G_User.user.Acct_year);
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("subjCheckItems", para);
            paramters.Add("subjcodeB", "");
            paramters.Add("subjcodeE", "");
            paramters.Add("Curcode", "");
            paramters.Add("acct_monthB", "01");//dateEdit1.DateTime.ToString("yyyy-MM-dd"));
            paramters.Add("acct_monthE", "05");//dateEdit2.DateTime.ToString("yyyy-MM-dd"));
            paramters.Add("cb1", FormHelper.GetValueForCheckEdit(checkEdit3).Trim());
            paramters.Add("cb2", FormHelper.GetValueForCheckEdit(checkEdit1).Trim());
            paramters.Add("fieldname", codeArray2[k].Trim());
            paramters.Add("fieldname_ch", valueArray2[k].Trim());
            paramters.Add("key", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit).Trim());


            serviceParamter.Paramters = paramters;

            List<CodeValue> objs = new List<CodeValue>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);

            return dt.Rows[0][0].ToString();

        }

        //删除控件
        private void deleteItem(List<String> _itemsList, int i, int g)
        {
            for (int j = i; j <= _itemsList.Count() + i; j++)
            {
                panelControl2.Controls.RemoveByKey("searchLookUpEdit" + j);

                panelControl2.Controls.RemoveByKey("labelControl" + j);

            }

            for (int j = g; j <= _itemsList.Count() + g; j++)
            {
                panelControl2.Controls.RemoveByKey("searchLookUpEdit" + j);

                panelControl2.Controls.RemoveByKey("labelControl" + j);

            }

        }


        //获取每个核算项数据
        private void getSelectItemList(String cid, SearchLookUpEdit searchLookUpEdit)
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_subj_ActiveClassBetweenConditionFastS";
            paramters.Add("table_name", cid);
            paramters.Add("key", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit).Trim());
            // paramters.Add("acct_year", G_User.user.Acct_year);
            serviceParamter.Paramters = paramters;

            List<CodeValue> objs = new List<CodeValue>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue);
            }
            //    emp_code.Properties.Items.AddRange(emps);
            searchLookUpEdit.Properties.ValueMember = "Code";
            searchLookUpEdit.Properties.DisplayMember = "Value";
            searchLookUpEdit.Properties.DataSource = objs;

        }

        private int getIndexCheckedComboBoxEdit(String tabIndex, List<String> _itemCodeList)
        {
            // MessageBox.Show(tabIndex);
            int i = 0;
            int j = 0;
            foreach (String item in _itemCodeList)
            {
                if (item.Equals(tabIndex))
                {
                    j = i;
                }
                i++;
            }
            return j + 1;
        }
        //封装动态辅助核算参数
        private String getItemParas()
        {
            string[] codeArray = checkedComboBoxEdit1.EditValue.ToString().Trim().Split(','); //checkedComboBoxEdit1.EditValue.ToString().Trim().Split(",");
            string[] valueArray = checkedComboBoxEdit1.Text.ToString().Trim().Split(',');

            int i = 0;
            int j = 0;
            int k = _itemCodeList.Count / codeArray.Length;
            int f = _itemCodeList2.Count / codeArray.Length;
            int g = _itemsList.Count;

            String para = "";
            foreach (String code in codeArray)
            {
                if (j < k)
                {
                    para = para + "<tr><key>" + code.Trim() + "</key><keyname>" + valueArray[i].Trim() + "</keyname><value></value><valueB></valueB><valueE></valueE></tr>";
                }
               

                /*else
                {
                    para = para + "<tr><pk>" + code.Trim() + "</pk><td1>" + valueArray[i].Trim() + "</td1><value></value><td2>" + _itemCodeList[j] + "+</td2><td3>" + _itemCodeList2[j] + "</td3></tr>";
                }*/
                i++;
                j++;

            }
            para = "<root>" + para + "</root>";
            return para;
        }

        //封装动态辅助核算参数
        private String getSubjItemParas()
        {
            string[] codeArray = checkedComboBoxEdit1.EditValue.ToString().Trim().Split(','); //checkedComboBoxEdit1.EditValue.ToString().Trim().Split(",");
            string[] valueArray = checkedComboBoxEdit1.Text.ToString().Trim().Split(',');

            int i = 0;
            int j = 0;
            int k = _itemCodeList.Count / codeArray.Length;
            int f = _itemCodeList2.Count / codeArray.Length;
            int g = _itemsList.Count;

            String para = "";
            foreach (String code in valueArray)
            {
                para = para + "," + code + ",";

            }
            return para;
        }

        // 创建动态表
        private void CreateGridColumn()
        {
            BandedGridView view = this.bandedGridView1;
            view.BeginUpdate(); //开始视图的编辑，防止触发其他事件
            view.BeginDataUpdate(); //开始数据的编辑
            view.Bands.Clear();
            view.Columns.Clear();


            GridBand band2 = view.Bands.AddBand("期间");
            GridBand band2Y = band2.Children.AddBand("年");
            GridBand band2M = band2.Children.AddBand("月");
            GridBand band2D = band2.Children.AddBand("日");
            band2Y.Columns.Add(createStaticColumn("年", "Column1", true, 1, true));
            band2M.Columns.Add(createStaticColumn("月", "Column2", true, 2, true));
            band2D.Columns.Add(createStaticColumn("日", "Tdate", true, 3, true));

            GridBand bandpzid = view.Bands.AddBand("凭证id");
            bandpzid.Columns.Add(createStaticColumn("凭证id", "pzid", true, 4, true));
            bandpzid.Visible = false;

            GridBand bandpz = view.Bands.AddBand("凭证号");
            bandpz.Columns.Add(createStaticColumn("凭证号", "pz", true, 5, true));

            //MessageBox.Show(valueArray2.Length.ToString());
            //创建动态辅助列
            int j = 0;
            for (int i = 6; i < valueArray2.Length + 6; i++)
            {
                GridBand band1 = view.Bands.AddBand(valueArray2[i - 6]);

                var bandedGridColumn = new BandedGridColumn();
                bandedGridColumn.Caption = valueArray2[i - 6];
                bandedGridColumn.FieldName = codeArray2[i - 6];
                bandedGridColumn.Visible = true;
                bandedGridColumn.VisibleIndex = i;
                band1.Columns.Add(bandedGridColumn);
                band1.MinWidth = 150;
                j = i;
            }


            GridBand band3 = view.Bands.AddBand("科目");
            band3.Columns.Add(createStaticColumn("科目", "Subj", true, j, true));

            GridBand bandSum = view.Bands.AddBand("摘要");
            bandSum.Columns.Add(createStaticColumn("摘要", "Summary", true, j + 1, true));

            GridBand band4 = view.Bands.AddBand("借方");
            band4.Columns.Add(createStaticColumn("借方", "Column3", true, j + 1, true));

            GridBand band5 = view.Bands.AddBand("贷方");
            band5.Columns.Add(createStaticColumn("贷方", "Column4", true, j + 2, true));

            GridBand band6 = view.Bands.AddBand("方向");
            band6.Columns.Add(createStaticColumn("方向", "direct", true, j + 3, true));

            GridBand band7 = view.Bands.AddBand("余额");
            band7.Columns.Add(createStaticColumn("余额", "Column5", true, j + 4, true));

            ///循环对齐表头
            for (int i = 0; i < this.bandedGridView1.Columns.Count; i++)
            {
                this.bandedGridView1.Columns[i].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;//表头居中对齐
            }

            ///循环对齐BandedGridView控件的GridBand控件 表头
            for (int i = 0; i < this.bandedGridView1.Bands.Count; i++)
            {
                this.bandedGridView1.Bands[i].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;//居中对齐
            }

            view.EndDataUpdate();//结束数据的编辑
            view.EndUpdate();   //结束视图的编辑
            view.OptionsView.ShowGroupPanel = false;
            view.OptionsView.ShowColumnHeaders = false;


        }

        private BandedGridColumn createStaticColumn(String caption, String FieldName, Boolean visible, int visibleIndex, Boolean colVisible)
        {
            var bandedGridColumn = new BandedGridColumn();
            bandedGridColumn.Caption = caption;
            bandedGridColumn.FieldName = FieldName;
            bandedGridColumn.Visible = visible;
            bandedGridColumn.VisibleIndex = visibleIndex;

            return bandedGridColumn;
        }



        private void Query()
        {
            CreateGridColumn();
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_acctbook_checkSubjdetail_query";
            paramters.Add("acct_year", G_User.user.Acct_year);
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("subjCheckItems", getItemParas());
            paramters.Add("acct_monthB", "01");
            paramters.Add("acct_monthE", "05");
            paramters.Add("acct_YearB", "2019");
            paramters.Add("acct_YearE", "2019");      
            paramters.Add("cb1", FormHelper.GetValueForCheckEdit(checkEdit3).Trim());
            paramters.Add("cb2", FormHelper.GetValueForCheckEdit(checkEdit1).Trim());
            paramters.Add("subjcodeB", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit1).Trim());
            paramters.Add("subjcodeE", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit2).Trim());
            paramters.Add("summary", "");
            paramters.Add("vouch_vouchType", "");
            paramters.Add("v_bvouchNo", "");
            paramters.Add("v_evouchNo", "");
            paramters.Add("attachmentS", "");
            paramters.Add("attachmentE", "");
            paramters.Add("vouch_direct", "");
            paramters.Add("b_vouch_amount", "");
            paramters.Add("e_vouch_amount", "");
            paramters.Add("check_oper", "");
            paramters.Add("emp_check", "");
            paramters.Add("mark_oper", "");
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;

        }

        #endregion



        private void searchLookUpEdit_EditValueChange(object sender, EventArgs e)
        {
            SearchLookUpEdit searchLookUpEdit = sender as SearchLookUpEdit;

            if (searchLookUpEdit.EditValue.ToString().Trim() != "" || searchLookUpEdit.EditValue.ToString().Trim() != null)
            {
                if (_itemCodeList.Count < valueArray2.Length * 2)
                {
                    _itemCodeList.Add(searchLookUpEdit.TabIndex.ToString());
                    _itemCodeList.Add(searchLookUpEdit.EditValue.ToString().Trim());
                }
                else
                {
                    _itemCodeList[getIndexCheckedComboBoxEdit(searchLookUpEdit.TabIndex.ToString(), _itemCodeList)] = searchLookUpEdit.EditValue.ToString().Trim();
                }

            }


        }

        private void searchLookUpEdit2_EditValueChange(object sender, EventArgs e)
        {
            SearchLookUpEdit searchLookUpEdit2 = sender as SearchLookUpEdit;

            if (searchLookUpEdit2.EditValue.ToString().Trim() != "" || searchLookUpEdit2.EditValue.ToString().Trim() != null)
            {
                if (_itemCodeList2.Count < valueArray2.Length * 2)
                {
                    _itemCodeList2.Add(searchLookUpEdit2.TabIndex.ToString());
                    _itemCodeList2.Add(searchLookUpEdit2.EditValue.ToString().Trim());
                }
                else
                {
                    _itemCodeList2[getIndexCheckedComboBoxEdit(searchLookUpEdit2.TabIndex.ToString(), _itemCodeList2)] = searchLookUpEdit2.EditValue.ToString().Trim();
                }
            }
        }

        private void checkedComboBoxEdit1_TextChanged(object sender, EventArgs e)
        {
            codeArray2 = checkedComboBoxEdit1.EditValue.ToString().Trim().Split(',');
            valueArray2 = checkedComboBoxEdit1.Text.ToString().Trim().Split(',');
            createItemList(checkedComboBoxEdit1.Text.ToString().Trim());
            _itemCodeList.RemoveRange(0, _itemCodeList.Count);
            _itemCodeList2.RemoveRange(0, _itemCodeList2.Count);
        }

        private void acct_acctbook_checkSubjdetail_query_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
    }
}
