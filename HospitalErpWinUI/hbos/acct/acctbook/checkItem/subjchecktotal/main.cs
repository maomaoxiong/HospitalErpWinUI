﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.BandedGrid;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.acctbook.checkItem.subjchecktotal
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        DataTable _user = new DataTable();
        //初始化绑定默认关键词（此数据源可以从数据库取） 
        List<string> _groupsInit = new List<string>();
        List<string> _groupsChange = new List<string>();
        string[] codeArray2 = new String[] { };
        string[] valueArray2 = new String[] { };
        public main()
        {
            _helper = new DaoHelper();
            InitializeComponent();
        }

        private void main_Load(object sender, EventArgs e)
        {
            init();
        }
        private void init() 
        {
            getAcctBYear();
            getAcctEYear();
            getAcctBMonth();
            getAcctEMonth();
            getAcctSubj();
        }


        #region 自定义方法
        //取得开始年
        private void getAcctBYear()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_acctbook_subjYearCross";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            serviceParamter.Paramters = paramters;

            List<string> companys = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                companys.Add(codeValue.Code);
            }

            comboBoxEdit1.Properties.Items.AddRange(companys);
            comboBoxEdit1.SelectedItem = companys[0];
        }
        //取得结束年
        private void getAcctEYear()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_acctbook_subjYearCross";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            serviceParamter.Paramters = paramters;


            List<string> companys2 = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                companys2.Add(codeValue.Code);
            }

            comboBoxEdit3.Properties.Items.AddRange(companys2);
            comboBoxEdit3.SelectedItem = companys2[0];
        }
        //取得开始月
        private void getAcctBMonth()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_acctbook_subjMonth";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            serviceParamter.Paramters = paramters;
            List<string> companys3 = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                companys3.Add(codeValue.Value);
            }

            comboBoxEdit2.Properties.Items.AddRange(companys3);
            comboBoxEdit2.SelectedItem = companys3[0];
        }
        //取得结束月
        private void getAcctEMonth()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_acctbook_subjMonth";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            serviceParamter.Paramters = paramters;
            List<string> companys3 = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                companys3.Add(codeValue.Value);
            }

            comboBoxEdit4.Properties.Items.AddRange(companys3);
            comboBoxEdit4.SelectedItem = companys3[0];
        }
        //取得会计科目
        private void getAcctSubj()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_subj_specialNameForCheck";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            paramters.Add("key", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit1).Trim());

            serviceParamter.Paramters = paramters;

            List<CodeValue> objs = new List<CodeValue>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue);
            }
            searchLookUpEdit1.Properties.ValueMember = "Code";
            searchLookUpEdit1.Properties.DisplayMember = "Value";
            searchLookUpEdit1.Properties.DataSource = objs;
        }
        //获取核算项数据
        private void getItemList()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_subj_checkItemClass_subj";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            paramters.Add("acct_subj_code", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit1).Trim());
            serviceParamter.Paramters = paramters;
            DataTable groups = _helper.ReadDictForSql(serviceParamter);
            checkedComboBoxEdit1.Properties.Items.Clear();

            CheckedListBoxItem[] itemListQuery = new CheckedListBoxItem[groups.Rows.Count + 1];
            int check = 1;
            for (int i = 0; i < groups.Rows.Count; i++)
            {
                itemListQuery[check] = new CheckedListBoxItem(groups.Rows[i][0].ToString(), groups.Rows[i][1].ToString());
                check++;

            }

            checkedComboBoxEdit1.Properties.Items.AddRange(itemListQuery);



        }

        //封装动态辅助核算参数
        private String getItemParas()
        {
            string[] codeArray = checkedComboBoxEdit1.EditValue.ToString().Trim().Split(',');
            string[] valueArray = checkedComboBoxEdit1.Text.ToString().Trim().Split(',');

            int i = 0;


            String para = "";
            foreach (String code in valueArray)
            {
                if (i < valueArray.Length)
                {
                                  // <tr><pk>部门</pk><td1>部门</td1><td2>|||</td2><td3>|||</td3></tr><tr>
                    para = para + "<tr><pk>" + code.Trim() + "</pk><td1>" + valueArray[i].Trim() + "</td1><value></value><td2>|||</td2><td3>|||</td3></tr>";
                }
                i++; 

            }
            para = "<root><tbody>" + para + "</tbody></root>";
            
            return para;
        }

        // 创建动态表
        private void CreateGridColumn()
        {
            BandedGridView view = this.bandedGridView1;
            view.BeginUpdate(); //开始视图的编辑，防止触发其他事件
            view.BeginDataUpdate(); //开始数据的编辑
            view.Bands.Clear();
            view.Columns.Clear();


            GridBand band2 = view.Bands.AddBand("期间");
            GridBand band2J = band2.Children.AddBand("年");
            GridBand band2D = band2.Children.AddBand("月");

            band2J.Columns.Add(createStaticColumn("年", "Tdate", true, 1, true));
            band2D.Columns.Add(createStaticColumn("月", "Tmonth", true, 2, true));

           // MessageBox.Show(valueArray2.Length.ToString());
            //创建动态辅助列
            int j = 0;
            for (int i = 2; i < valueArray2.Length + 2; i++)
            {
                GridBand band1 = view.Bands.AddBand(valueArray2[i-2]);

                var bandedGridColumn = new BandedGridColumn();
                bandedGridColumn.Caption = valueArray2[i-2];
                bandedGridColumn.FieldName = codeArray2[i-2];
                bandedGridColumn.Visible = true;
                bandedGridColumn.VisibleIndex = i;
                band1.Columns.Add(bandedGridColumn);
                band1.MinWidth = 150;
                j = i;
            }


            GridBand band3 = view.Bands.AddBand("摘要");
            band3.Columns.Add(createStaticColumn("摘要", "Summary", true, j, true));

            GridBand band4 = view.Bands.AddBand("借方");
            band4.Columns.Add(createStaticColumn("借方", "J", true, j+1, true));

            GridBand band5 = view.Bands.AddBand("贷方");
            band5.Columns.Add(createStaticColumn("贷方", "D", true, j+2, true));

            GridBand band6 = view.Bands.AddBand("方向");
            band6.Columns.Add(createStaticColumn("方向", "direct", true, j+3, true));

            GridBand band7 = view.Bands.AddBand("余额");
            band7.Columns.Add(createStaticColumn("余额", "leave", true, j+4, true));

            ///循环对齐表头
            for (int i = 0; i < this.bandedGridView1.Columns.Count; i++)
            {
                this.bandedGridView1.Columns[i].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;//表头居中对齐
            }

            ///循环对齐BandedGridView控件的GridBand控件 表头
            for (int i = 0; i < this.bandedGridView1.Bands.Count; i++)
            {
                this.bandedGridView1.Bands[i].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;//居中对齐
            }

            view.EndDataUpdate();//结束数据的编辑
            view.EndUpdate();   //结束视图的编辑
            view.OptionsView.ShowGroupPanel = false;
            view.OptionsView.ShowColumnHeaders = false;


        }

        private BandedGridColumn createStaticColumn(String caption, String FieldName, Boolean visible, int visibleIndex, Boolean colVisible)
        {
            var bandedGridColumn = new BandedGridColumn();
            bandedGridColumn.Caption = caption;
            bandedGridColumn.FieldName = FieldName;
            bandedGridColumn.Visible = visible;
            bandedGridColumn.VisibleIndex = visibleIndex;

            return bandedGridColumn;
        }


        private void Query()
        {
            CreateGridColumn();
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_acctbook_checkItems_query";
            paramters.Add("acct_year", G_User.user.Acct_year);
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("Currlist", "");
            paramters.Add("acct_YearB", "2019");
            paramters.Add("acct_monthB", "01");
            paramters.Add("acct_YearE", "2019");
            paramters.Add("acct_monthE", "05"); //FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit1).Trim(
            paramters.Add("cb1", FormHelper.GetValueForCheckEdit(checkEdit3).Trim());
            paramters.Add("cb2", FormHelper.GetValueForCheckEdit(checkEdit1).Trim());
            paramters.Add("torder", FormHelper.GetValueForCheckEdit(checkEdit2).Trim());
            paramters.Add("subjCheckItems", getItemParas());
            paramters.Add("subjCode", FormHelper.GetValueForSearchLookUpEdit(searchLookUpEdit1).Trim());
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;

        }



        #endregion

        #region 事件
        private void checkedComboBoxEdit1_TextChanged(object sender, EventArgs e)
        {
            codeArray2 = checkedComboBoxEdit1.EditValue.ToString().Trim().Split(',');
            valueArray2 = checkedComboBoxEdit1.Text.ToString().Trim().Split(',');
        }

        
        private void searchLookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            getItemList();
        }



        private void acct_acctbook_checkItems_query_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

      
        #endregion

    }
}
