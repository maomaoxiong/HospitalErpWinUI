﻿namespace HospitalErpWinUI.hbos.acct.vouch.query.newinsert
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(main));
            this.budg_vouch_no = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.budg_type = new DevExpress.XtraEditors.LookUpEdit();
            this.gridbudg = new DevExpress.XtraGrid.GridControl();
            this.gridViewbudg = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridLookUpbudg_acct_subj = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.GridLookUpViewbudg_acct_subj = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.griditems = new DevExpress.XtraGrid.GridControl();
            this.gridViewitems = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridLookUpAcct_acct_subj = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.GridLookUpViewAcct_acct_subj = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewacct = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.vouch_detail_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.vouch_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.vouch_page = new DevExpress.XtraGrid.Columns.GridColumn();
            this.vouch_row = new DevExpress.XtraGrid.Columns.GridColumn();
            this.summary = new DevExpress.XtraGrid.Columns.GridColumn();
            this.acct_subj_code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.amt_debit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.amt_credit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridacct = new DevExpress.XtraGrid.GridControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.but_vouch_save = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.but_close = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.acct_vouch_no = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.vouch_date = new DevExpress.XtraEditors.DateEdit();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.acct_type = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.bill_num = new DevExpress.XtraEditors.TextEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.budg_vouch_no.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.budg_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridbudg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewbudg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridLookUpbudg_acct_subj)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridLookUpViewbudg_acct_subj)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.griditems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewitems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridLookUpAcct_acct_subj)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridLookUpViewAcct_acct_subj)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewacct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridacct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_vouch_no.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vouch_date.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vouch_date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bill_num.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // budg_vouch_no
            // 
            this.budg_vouch_no.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.budg_vouch_no.Enabled = false;
            this.budg_vouch_no.Location = new System.Drawing.Point(223, 168);
            this.budg_vouch_no.Name = "budg_vouch_no";
            this.budg_vouch_no.Size = new System.Drawing.Size(49, 20);
            this.budg_vouch_no.TabIndex = 39;
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl3.Location = new System.Drawing.Point(177, 171);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(40, 14);
            this.labelControl3.TabIndex = 38;
            this.labelControl3.Text = "凭证号:";
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl5.Location = new System.Drawing.Point(4, 171);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(52, 14);
            this.labelControl5.TabIndex = 36;
            this.labelControl5.Text = "预算会计:";
            // 
            // budg_type
            // 
            this.budg_type.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.budg_type.Enabled = false;
            this.budg_type.Location = new System.Drawing.Point(59, 168);
            this.budg_type.Name = "budg_type";
            this.budg_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.budg_type.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("vouch_type_code", "类型编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("vouch_type_name", "类型")});
            this.budg_type.Properties.NullText = "";
            this.budg_type.Size = new System.Drawing.Size(112, 20);
            this.budg_type.TabIndex = 48;
            // 
            // gridbudg
            // 
            this.gridbudg.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridbudg.EmbeddedNavigator.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridbudg.Location = new System.Drawing.Point(2, 0);
            this.gridbudg.MainView = this.gridViewbudg;
            this.gridbudg.Name = "gridbudg";
            this.gridbudg.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.GridLookUpbudg_acct_subj});
            this.gridbudg.Size = new System.Drawing.Size(932, 151);
            this.gridbudg.TabIndex = 49;
            this.gridbudg.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewbudg});
            // 
            // gridViewbudg
            // 
            this.gridViewbudg.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8});
            this.gridViewbudg.GridControl = this.gridbudg;
            this.gridViewbudg.Name = "gridViewbudg";
            this.gridViewbudg.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridViewbudg.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridViewbudg.OptionsView.ShowGroupPanel = false;
            this.gridViewbudg.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewbudg_FocusedRowChanged);
            this.gridViewbudg.GotFocus += new System.EventHandler(this.gridViewbudg_GotFocus);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "明细ID";
            this.gridColumn1.FieldName = "vouch_detail_id";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "凭证ID";
            this.gridColumn2.FieldName = "vouch_id";
            this.gridColumn2.Name = "gridColumn2";
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "页数";
            this.gridColumn3.FieldName = "vouch_page";
            this.gridColumn3.Name = "gridColumn3";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "行数";
            this.gridColumn4.FieldName = "vouch_row";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "摘要";
            this.gridColumn5.FieldName = "summary";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 239;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "科目";
            this.gridColumn6.ColumnEdit = this.GridLookUpbudg_acct_subj;
            this.gridColumn6.FieldName = "acct_subj_code";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            this.gridColumn6.Width = 479;
            // 
            // GridLookUpbudg_acct_subj
            // 
            this.GridLookUpbudg_acct_subj.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.GridLookUpbudg_acct_subj.AutoComplete = false;
            this.GridLookUpbudg_acct_subj.AutoHeight = false;
            this.GridLookUpbudg_acct_subj.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.GridLookUpbudg_acct_subj.DisplayMember = "acct_subj_name";
            this.GridLookUpbudg_acct_subj.ImmediatePopup = true;
            this.GridLookUpbudg_acct_subj.Name = "GridLookUpbudg_acct_subj";
            this.GridLookUpbudg_acct_subj.NullText = "";
            this.GridLookUpbudg_acct_subj.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.GridLookUpbudg_acct_subj.PopupFormSize = new System.Drawing.Size(600, 0);
            this.GridLookUpbudg_acct_subj.ShowFooter = false;
            this.GridLookUpbudg_acct_subj.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.GridLookUpbudg_acct_subj.ValueMember = "acct_subj_code";
            this.GridLookUpbudg_acct_subj.View = this.GridLookUpViewbudg_acct_subj;
            // 
            // GridLookUpViewbudg_acct_subj
            // 
            this.GridLookUpViewbudg_acct_subj.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn9,
            this.gridColumn10});
            this.GridLookUpViewbudg_acct_subj.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.GridLookUpViewbudg_acct_subj.Name = "GridLookUpViewbudg_acct_subj";
            this.GridLookUpViewbudg_acct_subj.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.GridLookUpViewbudg_acct_subj.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "科目编码";
            this.gridColumn9.FieldName = "acct_subj_code";
            this.gridColumn9.Name = "gridColumn9";
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "科目名称";
            this.gridColumn10.FieldName = "acct_subj_name";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 0;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceCell.Image = ((System.Drawing.Image)(resources.GetObject("gridColumn7.AppearanceCell.Image")));
            this.gridColumn7.AppearanceCell.Options.UseImage = true;
            this.gridColumn7.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn7.Caption = "借方金额";
            this.gridColumn7.DisplayFormat.FormatString = "N2";
            this.gridColumn7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn7.FieldName = "amt_debit";
            this.gridColumn7.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 2;
            this.gridColumn7.Width = 180;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "贷方金额";
            this.gridColumn8.DisplayFormat.FormatString = "N2";
            this.gridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn8.FieldName = "amt_credit";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 3;
            this.gridColumn8.Width = 177;
            // 
            // griditems
            // 
            this.griditems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.griditems.EmbeddedNavigator.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.griditems.Location = new System.Drawing.Point(2, 2);
            this.griditems.MainView = this.gridViewitems;
            this.griditems.Name = "griditems";
            this.griditems.Size = new System.Drawing.Size(933, 162);
            this.griditems.TabIndex = 50;
            this.griditems.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewitems});
            // 
            // gridViewitems
            // 
            this.gridViewitems.GridControl = this.griditems;
            this.gridViewitems.Name = "gridViewitems";
            this.gridViewitems.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridViewitems.OptionsView.ColumnAutoWidth = false;
            this.gridViewitems.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridViewitems.OptionsView.ShowGroupPanel = false;
            this.gridViewitems.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewitems_CellValueChanged);
            // 
            // GridLookUpAcct_acct_subj
            // 
            this.GridLookUpAcct_acct_subj.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.GridLookUpAcct_acct_subj.AutoComplete = false;
            this.GridLookUpAcct_acct_subj.AutoHeight = false;
            this.GridLookUpAcct_acct_subj.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.GridLookUpAcct_acct_subj.DisplayMember = "acct_subj_name";
            this.GridLookUpAcct_acct_subj.ImmediatePopup = true;
            this.GridLookUpAcct_acct_subj.Name = "GridLookUpAcct_acct_subj";
            this.GridLookUpAcct_acct_subj.NullText = "";
            this.GridLookUpAcct_acct_subj.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.GridLookUpAcct_acct_subj.PopupFormSize = new System.Drawing.Size(600, 0);
            this.GridLookUpAcct_acct_subj.ShowFooter = false;
            this.GridLookUpAcct_acct_subj.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.GridLookUpAcct_acct_subj.ValueMember = "acct_subj_code";
            this.GridLookUpAcct_acct_subj.View = this.GridLookUpViewAcct_acct_subj;
            // 
            // GridLookUpViewAcct_acct_subj
            // 
            this.GridLookUpViewAcct_acct_subj.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn11,
            this.gridColumn12});
            this.GridLookUpViewAcct_acct_subj.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.GridLookUpViewAcct_acct_subj.Name = "GridLookUpViewAcct_acct_subj";
            this.GridLookUpViewAcct_acct_subj.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.GridLookUpViewAcct_acct_subj.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "编码";
            this.gridColumn11.FieldName = "acct_subj_code";
            this.gridColumn11.Name = "gridColumn11";
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "名称";
            this.gridColumn12.FieldName = "acct_subj_name";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 0;
            // 
            // gridViewacct
            // 
            this.gridViewacct.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.vouch_detail_id,
            this.vouch_id,
            this.vouch_page,
            this.vouch_row,
            this.summary,
            this.acct_subj_code,
            this.amt_debit,
            this.amt_credit});
            this.gridViewacct.GridControl = this.gridacct;
            this.gridViewacct.Name = "gridViewacct";
            this.gridViewacct.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridViewacct.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridViewacct.OptionsView.ShowGroupPanel = false;
            this.gridViewacct.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewacct_FocusedRowChanged);
            this.gridViewacct.GotFocus += new System.EventHandler(this.gridViewacct_GotFocus);
            // 
            // vouch_detail_id
            // 
            this.vouch_detail_id.Caption = "明细ID";
            this.vouch_detail_id.FieldName = "vouch_detail_id";
            this.vouch_detail_id.Name = "vouch_detail_id";
            // 
            // vouch_id
            // 
            this.vouch_id.Caption = "凭证ID";
            this.vouch_id.FieldName = "vouch_id";
            this.vouch_id.Name = "vouch_id";
            // 
            // vouch_page
            // 
            this.vouch_page.Caption = "页数";
            this.vouch_page.FieldName = "vouch_page";
            this.vouch_page.Name = "vouch_page";
            // 
            // vouch_row
            // 
            this.vouch_row.Caption = "行数";
            this.vouch_row.FieldName = "vouch_row";
            this.vouch_row.Name = "vouch_row";
            // 
            // summary
            // 
            this.summary.Caption = "摘要";
            this.summary.FieldName = "summary";
            this.summary.Name = "summary";
            this.summary.Visible = true;
            this.summary.VisibleIndex = 0;
            this.summary.Width = 238;
            // 
            // acct_subj_code
            // 
            this.acct_subj_code.Caption = "科目";
            this.acct_subj_code.ColumnEdit = this.GridLookUpAcct_acct_subj;
            this.acct_subj_code.FieldName = "acct_subj_code";
            this.acct_subj_code.Name = "acct_subj_code";
            this.acct_subj_code.Visible = true;
            this.acct_subj_code.VisibleIndex = 1;
            this.acct_subj_code.Width = 478;
            // 
            // amt_debit
            // 
            this.amt_debit.AppearanceCell.Image = ((System.Drawing.Image)(resources.GetObject("amt_debit.AppearanceCell.Image")));
            this.amt_debit.AppearanceCell.Options.UseImage = true;
            this.amt_debit.AppearanceCell.Options.UseTextOptions = true;
            this.amt_debit.Caption = "借方金额";
            this.amt_debit.DisplayFormat.FormatString = "N2";
            this.amt_debit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.amt_debit.FieldName = "amt_debit";
            this.amt_debit.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.amt_debit.Name = "amt_debit";
            this.amt_debit.Visible = true;
            this.amt_debit.VisibleIndex = 2;
            this.amt_debit.Width = 183;
            // 
            // amt_credit
            // 
            this.amt_credit.Caption = "贷方金额";
            this.amt_credit.DisplayFormat.FormatString = "N2";
            this.amt_credit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.amt_credit.FieldName = "amt_credit";
            this.amt_credit.Name = "amt_credit";
            this.amt_credit.Visible = true;
            this.amt_credit.VisibleIndex = 3;
            this.amt_credit.Width = 176;
            // 
            // gridacct
            // 
            this.gridacct.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridacct.EmbeddedNavigator.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridacct.Location = new System.Drawing.Point(2, 0);
            this.gridacct.MainView = this.gridViewacct;
            this.gridacct.Name = "gridacct";
            this.gridacct.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.GridLookUpAcct_acct_subj});
            this.gridacct.Size = new System.Drawing.Size(932, 165);
            this.gridacct.TabIndex = 35;
            this.gridacct.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewacct});
            // 
            // panelControl1
            // 
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(944, 91);
            this.panelControl1.TabIndex = 53;
            // 
            // but_vouch_save
            // 
            this.but_vouch_save.Location = new System.Drawing.Point(1, 3);
            this.but_vouch_save.Name = "but_vouch_save";
            this.but_vouch_save.Size = new System.Drawing.Size(65, 23);
            this.but_vouch_save.TabIndex = 0;
            this.but_vouch_save.Text = "保存";
            this.but_vouch_save.Click += new System.EventHandler(this.but_vouch_save_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(70, 3);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(65, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "场景提取";
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(139, 3);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(65, 23);
            this.simpleButton3.TabIndex = 2;
            this.simpleButton3.Text = "场景学习";
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(207, 3);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(65, 23);
            this.simpleButton4.TabIndex = 5;
            this.simpleButton4.Text = "删除";
            // 
            // but_close
            // 
            this.but_close.Location = new System.Drawing.Point(275, 3);
            this.but_close.Name = "but_close";
            this.but_close.Size = new System.Drawing.Size(65, 23);
            this.but_close.TabIndex = 6;
            this.but_close.Text = "关闭";
            this.but_close.Click += new System.EventHandler(this.but_close_Click);
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit1.Location = new System.Drawing.Point(1, 31);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Black;
            this.pictureEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.Appearance.Options.UseFont = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.pictureEdit1.Size = new System.Drawing.Size(940, 1);
            this.pictureEdit1.TabIndex = 7;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.BottomLeft;
            this.labelControl1.LineColor = System.Drawing.Color.Black;
            this.labelControl1.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.labelControl1.LineStyle = System.Drawing.Drawing2D.DashStyle.DashDot;
            this.labelControl1.Location = new System.Drawing.Point(404, 35);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(56, 17);
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "记账凭证";
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Location = new System.Drawing.Point(402, 54);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Black;
            this.pictureEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.Appearance.Options.UseFont = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.pictureEdit2.Size = new System.Drawing.Size(60, 1);
            this.pictureEdit2.TabIndex = 9;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(4, 62);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(52, 14);
            this.labelControl9.TabIndex = 23;
            this.labelControl9.Text = "财务会计:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(177, 62);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(40, 14);
            this.labelControl2.TabIndex = 25;
            this.labelControl2.Text = "凭证号:";
            // 
            // acct_vouch_no
            // 
            this.acct_vouch_no.Location = new System.Drawing.Point(223, 59);
            this.acct_vouch_no.Name = "acct_vouch_no";
            this.acct_vouch_no.Properties.Mask.EditMask = "[0-9]*";
            this.acct_vouch_no.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.acct_vouch_no.Size = new System.Drawing.Size(49, 20);
            this.acct_vouch_no.TabIndex = 26;
            this.acct_vouch_no.EditValueChanged += new System.EventHandler(this.acct_vouch_no_EditValueChanged);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(365, 62);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(28, 14);
            this.labelControl4.TabIndex = 28;
            this.labelControl4.Text = "日期:";
            // 
            // vouch_date
            // 
            this.vouch_date.EditValue = new System.DateTime(2019, 2, 8, 16, 24, 42, 847);
            this.vouch_date.Location = new System.Drawing.Point(394, 59);
            this.vouch_date.Name = "vouch_date";
            this.vouch_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.vouch_date.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.vouch_date.Size = new System.Drawing.Size(97, 20);
            this.vouch_date.TabIndex = 29;
            this.vouch_date.EditValueChanged += new System.EventHandler(this.vouch_date_EditValueChanged);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.linkLabel1.Location = new System.Drawing.Point(285, 62);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(35, 14);
            this.linkLabel1.TabIndex = 30;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "上张";
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.linkLabel2.Location = new System.Drawing.Point(320, 62);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(35, 14);
            this.linkLabel2.TabIndex = 31;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "下张";
            // 
            // simpleButton8
            // 
            this.simpleButton8.Location = new System.Drawing.Point(497, 58);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(65, 23);
            this.simpleButton8.TabIndex = 32;
            this.simpleButton8.Text = "并行记账";
            // 
            // simpleButton9
            // 
            this.simpleButton9.Location = new System.Drawing.Point(567, 58);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(65, 23);
            this.simpleButton9.TabIndex = 33;
            this.simpleButton9.Text = "标记";
            // 
            // simpleButton10
            // 
            this.simpleButton10.Location = new System.Drawing.Point(639, 58);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(65, 23);
            this.simpleButton10.TabIndex = 34;
            this.simpleButton10.Text = "合并记账";
            // 
            // acct_type
            // 
            this.acct_type.Location = new System.Drawing.Point(59, 59);
            this.acct_type.Name = "acct_type";
            this.acct_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.acct_type.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("vouch_type_code", "类型编码", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("vouch_type_name", "类型")});
            this.acct_type.Properties.NullText = "";
            this.acct_type.Size = new System.Drawing.Size(112, 20);
            this.acct_type.TabIndex = 47;
            this.acct_type.EditValueChanged += new System.EventHandler(this.acct_type_EditValueChanged);
            // 
            // labelControl12
            // 
            this.labelControl12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl12.Location = new System.Drawing.Point(846, 64);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(28, 14);
            this.labelControl12.TabIndex = 51;
            this.labelControl12.Text = "附件:";
            // 
            // bill_num
            // 
            this.bill_num.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bill_num.EditValue = "0";
            this.bill_num.Location = new System.Drawing.Point(880, 61);
            this.bill_num.Name = "bill_num";
            this.bill_num.Properties.Mask.EditMask = "[0-9]*";
            this.bill_num.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.bill_num.Size = new System.Drawing.Size(61, 20);
            this.bill_num.TabIndex = 52;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridacct);
            this.panelControl2.Controls.Add(this.budg_vouch_no);
            this.panelControl2.Controls.Add(this.labelControl5);
            this.panelControl2.Controls.Add(this.labelControl3);
            this.panelControl2.Controls.Add(this.budg_type);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(3, 3);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(937, 193);
            this.panelControl2.TabIndex = 54;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.labelControl11);
            this.panelControl3.Controls.Add(this.labelControl10);
            this.panelControl3.Controls.Add(this.labelControl8);
            this.panelControl3.Controls.Add(this.labelControl7);
            this.panelControl3.Controls.Add(this.labelControl6);
            this.panelControl3.Controls.Add(this.gridbudg);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(3, 202);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(937, 193);
            this.panelControl3.TabIndex = 55;
            // 
            // labelControl11
            // 
            this.labelControl11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl11.Location = new System.Drawing.Point(4, 176);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(28, 14);
            this.labelControl11.TabIndex = 54;
            this.labelControl11.Text = "科目:";
            // 
            // labelControl10
            // 
            this.labelControl10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl10.Location = new System.Drawing.Point(724, 156);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(40, 14);
            this.labelControl10.TabIndex = 53;
            this.labelControl10.Text = "制单人:";
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl8.Location = new System.Drawing.Point(490, 156);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(40, 14);
            this.labelControl8.TabIndex = 52;
            this.labelControl8.Text = "审核人:";
            // 
            // labelControl7
            // 
            this.labelControl7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl7.Location = new System.Drawing.Point(251, 156);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(40, 14);
            this.labelControl7.TabIndex = 51;
            this.labelControl7.Text = "记账人:";
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl6.Location = new System.Drawing.Point(4, 156);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(28, 14);
            this.labelControl6.TabIndex = 50;
            this.labelControl6.Text = "主管:";
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.griditems);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(3, 401);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(937, 166);
            this.panelControl4.TabIndex = 56;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panelControl2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panelControl4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panelControl3, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(1, 91);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(943, 570);
            this.tableLayoutPanel1.TabIndex = 57;
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 661);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.bill_num);
            this.Controls.Add(this.labelControl12);
            this.Controls.Add(this.acct_type);
            this.Controls.Add(this.simpleButton10);
            this.Controls.Add(this.simpleButton9);
            this.Controls.Add(this.simpleButton8);
            this.Controls.Add(this.linkLabel2);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.vouch_date);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.acct_vouch_no);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.pictureEdit2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.pictureEdit1);
            this.Controls.Add(this.but_close);
            this.Controls.Add(this.simpleButton4);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.but_vouch_save);
            this.Controls.Add(this.panelControl1);
            this.Name = "main";
            this.Text = "凭证制作";
            this.Load += new System.EventHandler(this.main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.budg_vouch_no.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.budg_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridbudg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewbudg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridLookUpbudg_acct_subj)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridLookUpViewbudg_acct_subj)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.griditems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewitems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridLookUpAcct_acct_subj)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridLookUpViewAcct_acct_subj)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewacct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridacct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_vouch_no.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vouch_date.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vouch_date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bill_num.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit budg_vouch_no;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LookUpEdit budg_type;
        private DevExpress.XtraGrid.GridControl gridbudg;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewbudg;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.GridControl griditems;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewitems;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit GridLookUpbudg_acct_subj;
        private DevExpress.XtraGrid.Views.Grid.GridView GridLookUpViewbudg_acct_subj;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit GridLookUpAcct_acct_subj;
        private DevExpress.XtraGrid.Views.Grid.GridView GridLookUpViewAcct_acct_subj;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewacct;
        private DevExpress.XtraGrid.Columns.GridColumn vouch_detail_id;
        private DevExpress.XtraGrid.Columns.GridColumn vouch_id;
        private DevExpress.XtraGrid.Columns.GridColumn vouch_page;
        private DevExpress.XtraGrid.Columns.GridColumn vouch_row;
        private DevExpress.XtraGrid.Columns.GridColumn summary;
        private DevExpress.XtraGrid.Columns.GridColumn acct_subj_code;
        private DevExpress.XtraGrid.Columns.GridColumn amt_debit;
        private DevExpress.XtraGrid.Columns.GridColumn amt_credit;
        private DevExpress.XtraGrid.GridControl gridacct;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton but_vouch_save;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton but_close;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit acct_vouch_no;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.DateEdit vouch_date;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private DevExpress.XtraEditors.LookUpEdit acct_type;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit bill_num;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}