﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpWinUI.GlobalVar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.bank.init.bank
{
    public partial class main : RibbonForm
    {

        protected DaoHelper _helper;
        private string _c_code;
        private DataTable _subjCode;
        
        public main()
        {
            _helper = new DaoHelper();
            InitializeComponent();
            this.Text = "银行未达账";
        }

        //加载页面
        private void main_Load(object sender, EventArgs e)
        {
            LoadSubjType();
        }

        private void labelControl1_Click(object sender, EventArgs e)
        {

        }

        private void c_subj_code_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        //科目银行下拉数据加载
        private void LoadSubjType()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "dict_acct_subj_list2";

            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("c_subj_code", "银行");
            paramters.Add("is_last", "1");
            serviceParamter.Paramters = paramters;

            List<string> companys = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                companys.Add(codeValue.Code + " " + codeValue.Value);
            }

            c_subj_code.Properties.Items.AddRange(companys);
            c_subj_code.SelectedItem = companys[0];

        }

        private void ButtonSelect_Click(object sender, EventArgs e)
        {
            Query();
        }

        //查询
        private void Query()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "acctBankInitBank_select";

            Dictionary<string, string> dict = new Dictionary<string, string>();
            string[] strArray1 = System.Text.RegularExpressions.Regex.Split(c_subj_code.Text.Trim(), @"\s{1,}");

            _c_code = strArray1[0];
            dict.Add("comp_code", G_User.user.Comp_code);
            dict.Add("copy_code", G_User.user.Copy_code);
            dict.Add("dateFrom", dateFrom.Text);
            dict.Add("dateEnd", dateEnd.Text);
            dict.Add("c_subj_code", strArray1[0]);
            dict.Add("moneyFrom", moneyFrom.Text);
            dict.Add("moneyEnd", moneyEnd.Text);
            serviceParamter.Paramters = dict;
            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
           
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
                string[] arragy2 = dt.Rows[i]["Column2"].ToString().Split('|');
                dt.Rows[i]["Column2"] = arragy2[3].ToString();

            }
            _subjCode = dt;
            gridControl1.DataSource = _subjCode;

        }

        private void QueryPriseMoney()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "acctBankInitBankEnterpriseMoney_load";

            Dictionary<string, string> dict = new Dictionary<string, string>();
            string[] strArray1 = System.Text.RegularExpressions.Regex.Split(c_subj_code.Text.Trim(), @"\s{1,}");

            _c_code = strArray1[0];
            dict.Add("comp_code", G_User.user.Comp_code);
            dict.Add("copy_code", G_User.user.Copy_code);
            dict.Add("c_subj_code", strArray1[0]);
            serviceParamter.Paramters = dict;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);

            moneyInit.Text = dt.Rows[0][0].ToString();

        }
        private void dateFrom_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void c_subj_code_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void ButtonSelect_Click_1(object sender, EventArgs e)
        {
            Query();
            QueryPriseMoney();
        }

        private void groupControl1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void moneyFrom_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void dateFrom_EditValueChanged_1(object sender, EventArgs e)
        {

        }
    }
}
