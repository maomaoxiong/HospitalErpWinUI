﻿namespace HospitalErpWinUI.hbos.acct.bank.init.comp
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.ButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.moneyInit = new DevExpress.XtraEditors.TextEdit();
            this.moneyEnd = new DevExpress.XtraEditors.TextEdit();
            this.moneyFrom = new DevExpress.XtraEditors.TextEdit();
            this.c_subj_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEnd = new DevExpress.XtraEditors.DateEdit();
            this.dateFrom = new DevExpress.XtraEditors.DateEdit();
            this.ButtonSelect = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.ButtonImport = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonDelete = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonEdit = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Column0 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.Column1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Column2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Column3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Column4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Column5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Column6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.moneyInit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_subj_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.ButtonSave);
            this.groupControl1.Controls.Add(this.moneyInit);
            this.groupControl1.Controls.Add(this.moneyEnd);
            this.groupControl1.Controls.Add(this.moneyFrom);
            this.groupControl1.Controls.Add(this.c_subj_code);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.dateEnd);
            this.groupControl1.Controls.Add(this.dateFrom);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1299, 114);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "groupControl1";
            this.groupControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.groupControl1_Paint);
            // 
            // ButtonSave
            // 
            this.ButtonSave.Location = new System.Drawing.Point(224, 77);
            this.ButtonSave.Name = "ButtonSave";
            this.ButtonSave.Size = new System.Drawing.Size(75, 23);
            this.ButtonSave.TabIndex = 6;
            this.ButtonSave.Text = "保存";
            // 
            // moneyInit
            // 
            this.moneyInit.Location = new System.Drawing.Point(85, 78);
            this.moneyInit.Name = "moneyInit";
            this.moneyInit.Size = new System.Drawing.Size(115, 20);
            this.moneyInit.TabIndex = 5;
            // 
            // moneyEnd
            // 
            this.moneyEnd.Location = new System.Drawing.Point(963, 40);
            this.moneyEnd.Name = "moneyEnd";
            this.moneyEnd.Size = new System.Drawing.Size(115, 20);
            this.moneyEnd.TabIndex = 5;
            // 
            // moneyFrom
            // 
            this.moneyFrom.Location = new System.Drawing.Point(828, 40);
            this.moneyFrom.Name = "moneyFrom";
            this.moneyFrom.Size = new System.Drawing.Size(115, 20);
            this.moneyFrom.TabIndex = 5;
            // 
            // c_subj_code
            // 
            this.c_subj_code.Location = new System.Drawing.Point(475, 40);
            this.c_subj_code.Name = "c_subj_code";
            this.c_subj_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.c_subj_code.Size = new System.Drawing.Size(214, 20);
            this.c_subj_code.TabIndex = 4;
            this.c_subj_code.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit1_SelectedIndexChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(413, 43);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 14);
            this.labelControl3.TabIndex = 3;
            this.labelControl3.Text = "银行科目：";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(947, 43);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(12, 14);
            this.labelControl5.TabIndex = 3;
            this.labelControl5.Text = "至";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(206, 43);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(12, 14);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "至";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(23, 81);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(60, 14);
            this.labelControl6.TabIndex = 3;
            this.labelControl6.Text = "初始余额：";
            this.labelControl6.Click += new System.EventHandler(this.labelControl4_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(774, 43);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(52, 14);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "金    额：";
            this.labelControl4.Click += new System.EventHandler(this.labelControl4_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(23, 43);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "日      期：";
            // 
            // dateEnd
            // 
            this.dateEnd.EditValue = null;
            this.dateEnd.Location = new System.Drawing.Point(224, 40);
            this.dateEnd.Name = "dateEnd";
            this.dateEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEnd.Size = new System.Drawing.Size(115, 20);
            this.dateEnd.TabIndex = 2;
            // 
            // dateFrom
            // 
            this.dateFrom.EditValue = null;
            this.dateFrom.Location = new System.Drawing.Point(85, 40);
            this.dateFrom.Name = "dateFrom";
            this.dateFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateFrom.Size = new System.Drawing.Size(115, 20);
            this.dateFrom.TabIndex = 2;
            this.dateFrom.EditValueChanged += new System.EventHandler(this.dateEdit1_EditValueChanged);
            // 
            // ButtonSelect
            // 
            this.ButtonSelect.Location = new System.Drawing.Point(1163, 32);
            this.ButtonSelect.Name = "ButtonSelect";
            this.ButtonSelect.Size = new System.Drawing.Size(75, 23);
            this.ButtonSelect.TabIndex = 6;
            this.ButtonSelect.Text = "查询";
            this.ButtonSelect.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.ButtonSelect);
            this.groupControl2.Controls.Add(this.ButtonImport);
            this.groupControl2.Controls.Add(this.ButtonDelete);
            this.groupControl2.Controls.Add(this.ButtonEdit);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl2.Location = new System.Drawing.Point(0, 114);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1299, 60);
            this.groupControl2.TabIndex = 2;
            this.groupControl2.Text = "groupControl2";
            // 
            // ButtonImport
            // 
            this.ButtonImport.Location = new System.Drawing.Point(209, 32);
            this.ButtonImport.Name = "ButtonImport";
            this.ButtonImport.Size = new System.Drawing.Size(75, 23);
            this.ButtonImport.TabIndex = 6;
            this.ButtonImport.Text = "导入";
            // 
            // ButtonDelete
            // 
            this.ButtonDelete.Location = new System.Drawing.Point(110, 32);
            this.ButtonDelete.Name = "ButtonDelete";
            this.ButtonDelete.Size = new System.Drawing.Size(75, 23);
            this.ButtonDelete.TabIndex = 6;
            this.ButtonDelete.Text = "删除";
            // 
            // ButtonEdit
            // 
            this.ButtonEdit.Location = new System.Drawing.Point(19, 32);
            this.ButtonEdit.Name = "ButtonEdit";
            this.ButtonEdit.Size = new System.Drawing.Size(75, 23);
            this.ButtonEdit.TabIndex = 6;
            this.ButtonEdit.Text = "编辑";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(0, 174);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEdit2});
            this.gridControl1.Size = new System.Drawing.Size(1299, 531);
            this.gridControl1.TabIndex = 3;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControl1.Click += new System.EventHandler(this.gridControl1_Click);
            // 
            // gridView1
            // 
            this.gridView1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Column0,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // Column0
            // 
            this.Column0.Caption = "选择";
            this.Column0.ColumnEdit = this.repositoryItemCheckEdit2;
            this.Column0.FieldName = "checked";
            this.Column0.Name = "Column0";
            this.Column0.Visible = true;
            this.Column0.VisibleIndex = 0;
            this.Column0.Width = 55;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // Column1
            // 
            this.Column1.Caption = "业务日期";
            this.Column1.FieldName = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.Visible = true;
            this.Column1.VisibleIndex = 1;
            this.Column1.Width = 200;
            // 
            // Column2
            // 
            this.Column2.Caption = "结算方式";
            this.Column2.FieldName = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.Visible = true;
            this.Column2.VisibleIndex = 2;
            this.Column2.Width = 200;
            // 
            // Column3
            // 
            this.Column3.Caption = "票据号";
            this.Column3.FieldName = "Column3";
            this.Column3.Name = "Column3";
            this.Column3.Visible = true;
            this.Column3.VisibleIndex = 3;
            this.Column3.Width = 200;
            // 
            // Column4
            // 
            this.Column4.Caption = "摘要";
            this.Column4.FieldName = "Column4";
            this.Column4.Name = "Column4";
            this.Column4.Visible = true;
            this.Column4.VisibleIndex = 4;
            this.Column4.Width = 200;
            // 
            // Column5
            // 
            this.Column5.Caption = "借方金额";
            this.Column5.FieldName = "Column5";
            this.Column5.Name = "Column5";
            this.Column5.Visible = true;
            this.Column5.VisibleIndex = 5;
            this.Column5.Width = 200;
            // 
            // Column6
            // 
            this.Column6.Caption = "贷方金额";
            this.Column6.FieldName = "Column6";
            this.Column6.Name = "Column6";
            this.Column6.Visible = true;
            this.Column6.VisibleIndex = 6;
            this.Column6.Width = 200;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1299, 705);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Name = "main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "main";
            this.Load += new System.EventHandler(this.main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.moneyInit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_subj_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.DateEdit dateEnd;
        private DevExpress.XtraEditors.DateEdit dateFrom;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit c_subj_code;
        private DevExpress.XtraEditors.TextEdit moneyEnd;
        private DevExpress.XtraEditors.TextEdit moneyFrom;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit moneyInit;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.SimpleButton ButtonSelect;
        private DevExpress.XtraEditors.SimpleButton ButtonSave;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton ButtonImport;
        private DevExpress.XtraEditors.SimpleButton ButtonDelete;
        private DevExpress.XtraEditors.SimpleButton ButtonEdit;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn Column1;
        private DevExpress.XtraGrid.Columns.GridColumn Column2;
        private DevExpress.XtraGrid.Columns.GridColumn Column3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn Column0;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn Column4;
        private DevExpress.XtraGrid.Columns.GridColumn Column5;
        private DevExpress.XtraGrid.Columns.GridColumn Column6;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;





    }
}