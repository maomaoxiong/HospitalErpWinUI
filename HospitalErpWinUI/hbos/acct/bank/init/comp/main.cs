﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpWinUI.GlobalVar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.bank.init.comp
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        private string _c_code;
        private DataTable _subjCode;
        public main()
        {
            _helper = new DaoHelper();
            InitializeComponent();
            this.Text = "单位未达账";
        }

      

        private void buttonEdit1_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void main_Load(object sender, EventArgs e)
        {
            LoadSubjType();
        }

        private void LoadSubjType() {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "dict_acct_subj_list2";

            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("c_subj_code", "银行");
            paramters.Add("is_last", "1");
            serviceParamter.Paramters = paramters;

            List<string> companys = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                companys.Add(codeValue.Code + " " + codeValue.Value);
            }
            
            c_subj_code.Properties.Items.AddRange(companys);
            c_subj_code.SelectedItem = companys[0];
        
        }
        private void Query()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "acctBankInitComp_select";

            Dictionary<string, string> dict = new Dictionary<string, string>();
            string[] strArray1 = System.Text.RegularExpressions.Regex.Split(c_subj_code.Text.Trim(), @"\s{1,}");

            _c_code = strArray1[0];
            dict.Add("comp_code", G_User.user.Comp_code);
            dict.Add("copy_code", G_User.user.Copy_code);
            dict.Add("dateFrom", dateFrom.Text);
            dict.Add("dateEnd", dateEnd.Text);
            dict.Add("c_subj_code", strArray1[0]);
            dict.Add("moneyFrom", moneyFrom.Text);
            dict.Add("moneyEnd", moneyEnd.Text);
            dict.Add("moneyInit", moneyInit.Text);
            serviceParamter.Paramters = dict;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            _subjCode = dt;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
                string[] arragy1 = dt.Rows[i]["Column1"].ToString().Split('|');
                string[] arragy2 = dt.Rows[i]["Column2"].ToString().Split('|');
                string[] arragy3 = dt.Rows[i]["Column3"].ToString().Split('|');

                dt.Rows[i]["Column1"] = arragy1[3].ToString();
                dt.Rows[i]["Column2"] = arragy2[3].ToString();
                dt.Rows[i]["Column3"] = arragy3[3].ToString();


            }
            //    _user = dt;
            gridControl1.DataSource = _subjCode;

        }

        private void sysDictsAcctSubjCode_select_Click(object sender, EventArgs e)
        {


        }

        private void dateEdit1_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxEdit1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void groupControl1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void labelControl4_Click(object sender, EventArgs e)
        {

        }

        private void gridControl1_Click(object sender, EventArgs e)
        {

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Query();
        }

    }
}