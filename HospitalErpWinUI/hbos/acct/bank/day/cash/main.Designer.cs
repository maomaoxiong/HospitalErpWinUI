﻿namespace HospitalErpWinUI.hbos.acct.bank.day.cash
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.bz2 = new DevExpress.XtraEditors.LabelControl();
            this.subj_code_df = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.is_check_f = new DevExpress.XtraEditors.TextEdit();
            this.bizhong = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.mode_subj = new System.Windows.Forms.CheckBox();
            this.include_detail = new System.Windows.Forms.CheckBox();
            this.e_vouch_amount = new DevExpress.XtraEditors.TextEdit();
            this.b_vouch_amount = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.vouch_direct = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.summary1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.e_vouch_no = new DevExpress.XtraEditors.TextEdit();
            this.b_vouch_no = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.vouch_type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.acct_subj_code_xj = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEnd = new DevExpress.XtraEditors.DateEdit();
            this.dateFrom = new DevExpress.XtraEditors.DateEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.ButtonPrint = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonSelect = new DevExpress.XtraEditors.SimpleButton();
            this.sqlDataSource1 = new DevExpress.DataAccess.Sql.SqlDataSource();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.year1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.month1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.day1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.vouch_id = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.summary = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.amt_debit = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.amt_credit = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.direction = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.result = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand23 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.subj_code_df.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_check_f.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.e_vouch_amount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.b_vouch_amount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vouch_direct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.summary1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.e_vouch_no.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.b_vouch_no.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vouch_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_code_xj.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.bz2);
            this.panelControl1.Controls.Add(this.subj_code_df);
            this.panelControl1.Controls.Add(this.is_check_f);
            this.panelControl1.Controls.Add(this.bizhong);
            this.panelControl1.Controls.Add(this.labelControl13);
            this.panelControl1.Controls.Add(this.labelControl12);
            this.panelControl1.Controls.Add(this.mode_subj);
            this.panelControl1.Controls.Add(this.include_detail);
            this.panelControl1.Controls.Add(this.e_vouch_amount);
            this.panelControl1.Controls.Add(this.b_vouch_amount);
            this.panelControl1.Controls.Add(this.labelControl10);
            this.panelControl1.Controls.Add(this.labelControl11);
            this.panelControl1.Controls.Add(this.vouch_direct);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.summary1);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.e_vouch_no);
            this.panelControl1.Controls.Add(this.b_vouch_no);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.vouch_type);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.acct_subj_code_xj);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.dateEnd);
            this.panelControl1.Controls.Add(this.dateFrom);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1622, 159);
            this.panelControl1.TabIndex = 0;
            this.panelControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.panelControl1_Paint);
            // 
            // bz2
            // 
            this.bz2.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.bz2.Location = new System.Drawing.Point(890, 28);
            this.bz2.Name = "bz2";
            this.bz2.Size = new System.Drawing.Size(93, 16);
            this.bz2.TabIndex = 39;
            this.bz2.Text = "labelControl14";
            // 
            // subj_code_df
            // 
            this.subj_code_df.EditValue = " ";
            this.subj_code_df.Location = new System.Drawing.Point(1140, 26);
            this.subj_code_df.Name = "subj_code_df";
            this.subj_code_df.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.subj_code_df.Properties.View = this.searchLookUpEdit2View;
            this.subj_code_df.Size = new System.Drawing.Size(245, 20);
            this.subj_code_df.TabIndex = 38;
            this.subj_code_df.EditValueChanged += new System.EventHandler(this.subj_code_df_EditValueChanged);
            // 
            // searchLookUpEdit2View
            // 
            this.searchLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit2View.Name = "searchLookUpEdit2View";
            this.searchLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            // 
            // is_check_f
            // 
            this.is_check_f.Location = new System.Drawing.Point(1361, 98);
            this.is_check_f.Name = "is_check_f";
            this.is_check_f.Size = new System.Drawing.Size(33, 20);
            this.is_check_f.TabIndex = 34;
            this.is_check_f.Visible = false;
            // 
            // bizhong
            // 
            this.bizhong.Location = new System.Drawing.Point(892, 29);
            this.bizhong.Name = "bizhong";
            this.bizhong.Size = new System.Drawing.Size(8, 14);
            this.bizhong.TabIndex = 33;
            this.bizhong.Text = "  ";
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(898, 29);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(0, 14);
            this.labelControl13.TabIndex = 32;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(854, 29);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(36, 14);
            this.labelControl12.TabIndex = 31;
            this.labelControl12.Text = "币别：";
            // 
            // mode_subj
            // 
            this.mode_subj.AutoSize = true;
            this.mode_subj.Location = new System.Drawing.Point(1206, 100);
            this.mode_subj.Name = "mode_subj";
            this.mode_subj.Size = new System.Drawing.Size(110, 18);
            this.mode_subj.TabIndex = 30;
            this.mode_subj.Text = "按对方科目展开";
            this.mode_subj.UseVisualStyleBackColor = true;
            // 
            // include_detail
            // 
            this.include_detail.AutoSize = true;
            this.include_detail.Location = new System.Drawing.Point(1073, 100);
            this.include_detail.Name = "include_detail";
            this.include_detail.Size = new System.Drawing.Size(86, 18);
            this.include_detail.TabIndex = 29;
            this.include_detail.Text = "含对账明细";
            this.include_detail.UseVisualStyleBackColor = true;
            // 
            // e_vouch_amount
            // 
            this.e_vouch_amount.Location = new System.Drawing.Point(740, 97);
            this.e_vouch_amount.Name = "e_vouch_amount";
            this.e_vouch_amount.Size = new System.Drawing.Size(111, 20);
            this.e_vouch_amount.TabIndex = 27;
            // 
            // b_vouch_amount
            // 
            this.b_vouch_amount.Location = new System.Drawing.Point(606, 97);
            this.b_vouch_amount.Name = "b_vouch_amount";
            this.b_vouch_amount.Size = new System.Drawing.Size(111, 20);
            this.b_vouch_amount.TabIndex = 28;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(722, 99);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(12, 14);
            this.labelControl10.TabIndex = 25;
            this.labelControl10.Text = "至";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(539, 100);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(60, 14);
            this.labelControl11.TabIndex = 26;
            this.labelControl11.Text = "金额范围：";
            // 
            // vouch_direct
            // 
            this.vouch_direct.Location = new System.Drawing.Point(98, 98);
            this.vouch_direct.Name = "vouch_direct";
            this.vouch_direct.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.vouch_direct.Size = new System.Drawing.Size(245, 20);
            this.vouch_direct.TabIndex = 24;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(30, 100);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(60, 14);
            this.labelControl9.TabIndex = 23;
            this.labelControl9.Text = "金额方向：";
            // 
            // summary1
            // 
            this.summary1.Location = new System.Drawing.Point(1140, 59);
            this.summary1.Name = "summary1";
            this.summary1.Size = new System.Drawing.Size(245, 20);
            this.summary1.TabIndex = 22;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(1073, 61);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(60, 14);
            this.labelControl8.TabIndex = 21;
            this.labelControl8.Text = "摘      要：";
            // 
            // e_vouch_no
            // 
            this.e_vouch_no.Location = new System.Drawing.Point(740, 60);
            this.e_vouch_no.Name = "e_vouch_no";
            this.e_vouch_no.Size = new System.Drawing.Size(111, 20);
            this.e_vouch_no.TabIndex = 19;
            // 
            // b_vouch_no
            // 
            this.b_vouch_no.Location = new System.Drawing.Point(606, 60);
            this.b_vouch_no.Name = "b_vouch_no";
            this.b_vouch_no.Size = new System.Drawing.Size(111, 20);
            this.b_vouch_no.TabIndex = 20;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(722, 61);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(12, 14);
            this.labelControl6.TabIndex = 17;
            this.labelControl6.Text = "至";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(543, 63);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(56, 14);
            this.labelControl7.TabIndex = 18;
            this.labelControl7.Text = "凭 证 号：";
            // 
            // vouch_type
            // 
            this.vouch_type.Location = new System.Drawing.Point(98, 61);
            this.vouch_type.Name = "vouch_type";
            this.vouch_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.vouch_type.Size = new System.Drawing.Size(245, 20);
            this.vouch_type.TabIndex = 16;
            this.vouch_type.SelectedValueChanged += new System.EventHandler(this.searchControl1_SelectedIndexChanged);
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(30, 63);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 14);
            this.labelControl5.TabIndex = 15;
            this.labelControl5.Text = "凭证类型：";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(1073, 29);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 14);
            this.labelControl4.TabIndex = 13;
            this.labelControl4.Text = "对方科目：";
            // 
            // acct_subj_code_xj
            // 
            this.acct_subj_code_xj.Location = new System.Drawing.Point(606, 26);
            this.acct_subj_code_xj.Name = "acct_subj_code_xj";
            this.acct_subj_code_xj.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.acct_subj_code_xj.Size = new System.Drawing.Size(245, 20);
            this.acct_subj_code_xj.TabIndex = 12;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(214, 26);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(12, 14);
            this.labelControl2.TabIndex = 10;
            this.labelControl2.Text = "至";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(539, 29);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 14);
            this.labelControl3.TabIndex = 11;
            this.labelControl3.Text = "现金科目：";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(30, 26);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 11;
            this.labelControl1.Text = "日      期：";
            // 
            // dateEnd
            // 
            this.dateEnd.EditValue = null;
            this.dateEnd.Location = new System.Drawing.Point(232, 24);
            this.dateEnd.Name = "dateEnd";
            this.dateEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEnd.Size = new System.Drawing.Size(111, 20);
            this.dateEnd.TabIndex = 8;
            this.dateEnd.EditValueChanged += new System.EventHandler(this.dateEnd_EditValueChanged);
            // 
            // dateFrom
            // 
            this.dateFrom.EditValue = null;
            this.dateFrom.Location = new System.Drawing.Point(98, 24);
            this.dateFrom.Name = "dateFrom";
            this.dateFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateFrom.Size = new System.Drawing.Size(111, 20);
            this.dateFrom.TabIndex = 9;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.ButtonPrint);
            this.panelControl2.Controls.Add(this.ButtonSelect);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 159);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1622, 43);
            this.panelControl2.TabIndex = 1;
            // 
            // ButtonPrint
            // 
            this.ButtonPrint.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ButtonPrint.Location = new System.Drawing.Point(1504, 9);
            this.ButtonPrint.Margin = new System.Windows.Forms.Padding(3, 3, 233, 3);
            this.ButtonPrint.Name = "ButtonPrint";
            this.ButtonPrint.Size = new System.Drawing.Size(87, 27);
            this.ButtonPrint.TabIndex = 9;
            this.ButtonPrint.Text = "打印";
            // 
            // ButtonSelect
            // 
            this.ButtonSelect.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ButtonSelect.Location = new System.Drawing.Point(1385, 9);
            this.ButtonSelect.Margin = new System.Windows.Forms.Padding(3, 3, 233, 3);
            this.ButtonSelect.Name = "ButtonSelect";
            this.ButtonSelect.Size = new System.Drawing.Size(87, 27);
            this.ButtonSelect.TabIndex = 8;
            this.ButtonSelect.Text = "查询";
            this.ButtonSelect.Click += new System.EventHandler(this.ButtonSelect_Click);
            // 
            // sqlDataSource1
            // 
            this.sqlDataSource1.Name = "sqlDataSource1";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 202);
            this.gridControl1.MainView = this.bandedGridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1622, 591);
            this.gridControl1.TabIndex = 2;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1});
            this.gridControl1.Click += new System.EventHandler(this.gridControl1_Click);
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.year1,
            this.vouch_id,
            this.summary,
            this.gridBand5,
            this.gridBand6,
            this.gridBand7,
            this.gridBand8,
            this.gridBand9,
            this.gridBand12,
            this.gridBand15,
            this.amt_debit,
            this.amt_credit,
            this.direction,
            this.result,
            this.gridBand23});
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn1,
            this.bandedGridColumn2,
            this.bandedGridColumn3,
            this.bandedGridColumn4,
            this.bandedGridColumn5,
            this.bandedGridColumn6,
            this.bandedGridColumn7,
            this.bandedGridColumn8});
            this.bandedGridView1.GridControl = this.gridControl1;
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsView.ShowColumnHeaders = false;
            this.bandedGridView1.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            // 
            // year1
            // 
            this.year1.AppearanceHeader.Options.UseTextOptions = true;
            this.year1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.year1.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.month1,
            this.day1,
            this.gridBand1});
            this.year1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.year1.Name = "year1";
            this.year1.VisibleIndex = 0;
            this.year1.Width = 150;
            // 
            // month1
            // 
            this.month1.AppearanceHeader.Options.UseTextOptions = true;
            this.month1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.month1.Caption = "月";
            this.month1.Columns.Add(this.bandedGridColumn1);
            this.month1.Name = "month1";
            this.month1.VisibleIndex = 0;
            this.month1.Width = 75;
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn1.Caption = "月";
            this.bandedGridColumn1.FieldName = "month1";
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            this.bandedGridColumn1.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn1.Visible = true;
            // 
            // day1
            // 
            this.day1.AppearanceHeader.Options.UseTextOptions = true;
            this.day1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.day1.Caption = "日";
            this.day1.Columns.Add(this.bandedGridColumn2);
            this.day1.Name = "day1";
            this.day1.VisibleIndex = 1;
            this.day1.Width = 75;
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn2.Caption = "日";
            this.bandedGridColumn2.FieldName = "day1";
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            this.bandedGridColumn2.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn2.Visible = true;
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "gridBand1";
            this.gridBand1.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2});
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.Visible = false;
            this.gridBand1.VisibleIndex = -1;
            this.gridBand1.Width = 179;
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "gridBand2";
            this.gridBand2.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand14});
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.Visible = false;
            this.gridBand2.VisibleIndex = -1;
            this.gridBand2.Width = 179;
            // 
            // gridBand14
            // 
            this.gridBand14.Caption = "gridBand14";
            this.gridBand14.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand3,
            this.gridBand13,
            this.gridBand4});
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.Visible = false;
            this.gridBand14.VisibleIndex = -1;
            this.gridBand14.Width = 179;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "gridBand3";
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 0;
            this.gridBand3.Width = 61;
            // 
            // gridBand13
            // 
            this.gridBand13.Caption = "gridBand13";
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 1;
            this.gridBand13.Width = 56;
            // 
            // gridBand4
            // 
            this.gridBand4.Caption = "gridBand4";
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 2;
            this.gridBand4.Width = 62;
            // 
            // vouch_id
            // 
            this.vouch_id.AppearanceHeader.Options.UseTextOptions = true;
            this.vouch_id.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.vouch_id.Caption = "凭证号";
            this.vouch_id.Columns.Add(this.bandedGridColumn8);
            this.vouch_id.Name = "vouch_id";
            this.vouch_id.VisibleIndex = 1;
            this.vouch_id.Width = 75;
            // 
            // bandedGridColumn8
            // 
            this.bandedGridColumn8.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn8.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn8.Caption = "凭证号";
            this.bandedGridColumn8.FieldName = "vouch_id";
            this.bandedGridColumn8.Name = "bandedGridColumn8";
            this.bandedGridColumn8.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn8.Visible = true;
            // 
            // summary
            // 
            this.summary.AppearanceHeader.Options.UseTextOptions = true;
            this.summary.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.summary.Caption = "摘要";
            this.summary.Columns.Add(this.bandedGridColumn3);
            this.summary.Name = "summary";
            this.summary.VisibleIndex = 2;
            this.summary.Width = 75;
            // 
            // bandedGridColumn3
            // 
            this.bandedGridColumn3.Caption = "摘要";
            this.bandedGridColumn3.FieldName = "summary";
            this.bandedGridColumn3.Name = "bandedGridColumn3";
            this.bandedGridColumn3.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn3.Visible = true;
            // 
            // gridBand5
            // 
            this.gridBand5.Caption = "gridBand5";
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.Visible = false;
            this.gridBand5.VisibleIndex = -1;
            // 
            // gridBand6
            // 
            this.gridBand6.Caption = "gridBand6";
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.Visible = false;
            this.gridBand6.VisibleIndex = -1;
            // 
            // gridBand7
            // 
            this.gridBand7.Caption = "gridBand7";
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.Visible = false;
            this.gridBand7.VisibleIndex = -1;
            // 
            // gridBand8
            // 
            this.gridBand8.Caption = "gridBand8";
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.Visible = false;
            this.gridBand8.VisibleIndex = -1;
            // 
            // gridBand9
            // 
            this.gridBand9.Caption = "gridBand9";
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.Visible = false;
            this.gridBand9.VisibleIndex = -1;
            // 
            // gridBand12
            // 
            this.gridBand12.Caption = "gridBand12";
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.RowCount = 2;
            this.gridBand12.Visible = false;
            this.gridBand12.VisibleIndex = -1;
            // 
            // gridBand15
            // 
            this.gridBand15.Caption = "gridBand15";
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.RowCount = 2;
            this.gridBand15.Visible = false;
            this.gridBand15.VisibleIndex = -1;
            // 
            // amt_debit
            // 
            this.amt_debit.AppearanceHeader.Options.UseTextOptions = true;
            this.amt_debit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.amt_debit.Caption = "借方金额";
            this.amt_debit.Columns.Add(this.bandedGridColumn4);
            this.amt_debit.Name = "amt_debit";
            this.amt_debit.VisibleIndex = 3;
            this.amt_debit.Width = 75;
            // 
            // bandedGridColumn4
            // 
            this.bandedGridColumn4.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.bandedGridColumn4.Caption = "借方金额";
            this.bandedGridColumn4.FieldName = "amt_debit";
            this.bandedGridColumn4.Name = "bandedGridColumn4";
            this.bandedGridColumn4.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn4.Visible = true;
            // 
            // amt_credit
            // 
            this.amt_credit.AppearanceHeader.Options.UseTextOptions = true;
            this.amt_credit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.amt_credit.Caption = "贷方金额";
            this.amt_credit.Columns.Add(this.bandedGridColumn5);
            this.amt_credit.Name = "amt_credit";
            this.amt_credit.VisibleIndex = 4;
            this.amt_credit.Width = 75;
            // 
            // bandedGridColumn5
            // 
            this.bandedGridColumn5.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.bandedGridColumn5.Caption = "贷方金额";
            this.bandedGridColumn5.FieldName = "amt_credit";
            this.bandedGridColumn5.Name = "bandedGridColumn5";
            this.bandedGridColumn5.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn5.Visible = true;
            // 
            // direction
            // 
            this.direction.AppearanceHeader.Options.UseTextOptions = true;
            this.direction.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.direction.Caption = "方向";
            this.direction.Columns.Add(this.bandedGridColumn6);
            this.direction.Name = "direction";
            this.direction.VisibleIndex = 5;
            this.direction.Width = 75;
            // 
            // bandedGridColumn6
            // 
            this.bandedGridColumn6.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bandedGridColumn6.Caption = "方向";
            this.bandedGridColumn6.FieldName = "direction";
            this.bandedGridColumn6.Name = "bandedGridColumn6";
            this.bandedGridColumn6.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn6.Visible = true;
            // 
            // result
            // 
            this.result.AppearanceHeader.Options.UseTextOptions = true;
            this.result.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.result.Caption = "余额";
            this.result.Columns.Add(this.bandedGridColumn7);
            this.result.Name = "result";
            this.result.VisibleIndex = 6;
            this.result.Width = 75;
            // 
            // bandedGridColumn7
            // 
            this.bandedGridColumn7.AppearanceCell.Options.UseTextOptions = true;
            this.bandedGridColumn7.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.bandedGridColumn7.Caption = "余额";
            this.bandedGridColumn7.FieldName = "result";
            this.bandedGridColumn7.Name = "bandedGridColumn7";
            this.bandedGridColumn7.OptionsColumn.AllowEdit = false;
            this.bandedGridColumn7.Visible = true;
            // 
            // gridBand23
            // 
            this.gridBand23.Caption = "gridBand23";
            this.gridBand23.Name = "gridBand23";
            this.gridBand23.Visible = false;
            this.gridBand23.VisibleIndex = -1;
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1622, 793);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "main";
            this.Text = "main";
            this.Load += new System.EventHandler(this.main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.subj_code_df.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_check_f.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.e_vouch_amount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.b_vouch_amount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vouch_direct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.summary1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.e_vouch_no.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.b_vouch_no.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vouch_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_code_xj.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEnd;
        private DevExpress.XtraEditors.DateEdit dateFrom;
        private DevExpress.XtraEditors.ComboBoxEdit acct_subj_code_xj;
        private DevExpress.XtraEditors.ComboBoxEdit vouch_type;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit e_vouch_amount;
        private DevExpress.XtraEditors.TextEdit b_vouch_amount;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.ComboBoxEdit vouch_direct;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit summary1;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit e_vouch_no;
        private DevExpress.XtraEditors.TextEdit b_vouch_no;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private System.Windows.Forms.CheckBox mode_subj;
        private System.Windows.Forms.CheckBox include_detail;
        private DevExpress.XtraEditors.SimpleButton ButtonPrint;
        private DevExpress.XtraEditors.SimpleButton ButtonSelect;
        private DevExpress.DataAccess.Sql.SqlDataSource sqlDataSource1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand year1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand month1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand day1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand vouch_id;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand summary;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand amt_debit;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand amt_credit;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand direction;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand result;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand23;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl bizhong;
        private DevExpress.XtraEditors.TextEdit is_check_f;
        private DevExpress.XtraEditors.SearchLookUpEdit subj_code_df;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit2View;
        private DevExpress.XtraEditors.LabelControl bz2;

    }
}