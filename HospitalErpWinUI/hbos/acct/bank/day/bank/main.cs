﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.bank.day.bank
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        private DataTable _subjCode;
        public main()
        {
            _helper = new DaoHelper();
            InitializeComponent();
            this.Text = "银行日记账";
        }

        private void textEdit1_EditValueChanged(object sender, EventArgs e)
        {

        }

        /**
        下拉数据加载
        */
        //现金银行科目
        private void LoadSubjType()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "dict_acct_year_subj_list";

            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_subj_code_xj", "现金");
            paramters.Add("is_last", "0");
            paramters.Add("acct_year", G_User.user.Acct_year);
            serviceParamter.Paramters = paramters;

            List<string> companys = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                companys.Add(codeValue.Code + " " + codeValue.Value);
            }

            acct_subj_code_xj.Properties.Items.AddRange(companys);
            acct_subj_code_xj.SelectedItem = companys[0];

        }
        //对方银行科目
        private void LoadSubjTypeDf()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_all_subj_list_spell_last";

            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            paramters.Add("key", FormHelper.GetValueForSearchLookUpEdit(subj_code_df).Trim());
            serviceParamter.Paramters = paramters;

            List<CodeValue> objs = new List<CodeValue>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue);
            }

            subj_code_df.Properties.ValueMember = "Code";
            subj_code_df.Properties.DisplayMember = "Value";
            subj_code_df.Properties.DataSource = objs;

        }
        //凭证类型
        private void LoadVouchType()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_vouch_type_list";

            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            serviceParamter.Paramters = paramters;

            List<string> companys = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                companys.Add(codeValue.Code + " " + codeValue.Value);
            }

            vouch_type.Properties.Items.AddRange(companys);
            vouch_type.SelectedItem = companys[0];

        }
        //金额方向
        private void LoadvouchDirect()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "dict_in_or_out";
            // serviceParamter.Paramters = paramters;

            List<string> companys = new List<string>();
            List<CodeValue> codeValues = _helper.ReadDictForData(serviceParamter);
            foreach (CodeValue codeValue in codeValues)
            {
                companys.Add(codeValue.Value);
            }

            vouch_direct.Properties.Items.AddRange(companys);
            vouch_direct.SelectedItem = companys[0];

        }

        //获取币种
        private void getBiZhong()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "get_waibi_info";

            Dictionary<string, string> dict = new Dictionary<string, string>();
            string[] strArray1 = System.Text.RegularExpressions.Regex.Split(acct_subj_code_xj.Text.Trim(), @"\s{1,}");

            dict.Add("acct_subj_code_xj", strArray1[0]);
            dict.Add("comp_code", G_User.user.Comp_code);
            dict.Add("copy_code", G_User.user.Copy_code);
            serviceParamter.Paramters = dict;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            if (dt.Rows.Count > 0)
            {
                bz2.Text = dt.Rows[0][1].ToString();
                is_check_f.Text = dt.Rows[0][0].ToString();
            }
            else
            {
                bz2.Text = "本币";
                is_check_f.Text = "0";
            }

        }

        /**
         复选框
         */
        String val;
        private String checkValue(Boolean chekced)
        {

            if (chekced)
            {
                val = "1";
            }
            else
            {
                val = "0";
            }
            return val;
        }

        private void dateEnd_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void main_Load(object sender, EventArgs e)
        {
            year1.Caption = G_User.user.Acct_year;
            LoadSubjType();
            LoadVouchType();
            LoadvouchDirect();
            getBiZhong();
            LoadSubjTypeDf();
            //设置默认日期
            dateFrom.Text = Convert.ToDateTime(G_User.user.LoginDate).ToString("yyyy/MM/01");
            dateEnd.Text = Convert.ToDateTime(G_User.user.LoginDate).AddMonths(1).AddDays(-Convert.ToDateTime(G_User.user.LoginDate).AddMonths(1).Day).ToString("yyyy/MM/dd"); //本月最后一天
        }
        //查询
        private void Query()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "acctBankDayBank_select";

            Dictionary<string, string> dict = new Dictionary<string, string>();
            string[] strArray1 = System.Text.RegularExpressions.Regex.Split(acct_subj_code_xj.Text.Trim(), @"\s{1,}");
            string[] strArray2 = System.Text.RegularExpressions.Regex.Split(subj_code_df.Text.Trim(), @"\s{1,}");
            string[] strArray3 = System.Text.RegularExpressions.Regex.Split(vouch_type.Text.Trim(), @"\s{1,}");
            string[] strArray4 = System.Text.RegularExpressions.Regex.Split(vouch_direct.Text.Trim(), @"\s{1,}");

            dict.Add("comp_code", G_User.user.Comp_code);
            dict.Add("copy_code", G_User.user.Copy_code);
            dict.Add("acct_year", G_User.user.Acct_year);
            dict.Add("dateFrom", dateFrom.Text);
            dict.Add("dateEnd", dateEnd.Text);
            dict.Add("acct_subj_code_xj", strArray1[0]);
            dict.Add("acct_subj_code_df", strArray2[0]);
            dict.Add("vouch_type", strArray3[0]);
            dict.Add("b_vouch_no", b_vouch_no.Text);
            dict.Add("e_vouch_no", e_vouch_no.Text);
            dict.Add("summary", summary1.Text);
            dict.Add("vouch_direct", strArray4[0]);
            dict.Add("b_vouch_amount", b_vouch_amount.Text);
            dict.Add("e_vouch_amount", e_vouch_amount.Text);
            dict.Add("bill_no", e_vouch_amount.Text);
            dict.Add("include_uncheck", checkValue(include_uncheck.Checked));
            dict.Add("mode_subj", checkValue(mode_subj.Checked));
            dict.Add("is_check_f", is_check_f.Text);
            serviceParamter.Paramters = dict;
            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);

            /*for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
                string[] arragy2 = dt.Rows[i]["Column2"].ToString().Split('|');
                dt.Rows[i]["Column2"] = arragy2[3].ToString();

            }*/
            _subjCode = dt;
            gridControl1.DataSource = _subjCode;

        }

        private void ButtonSelect_Click(object sender, EventArgs e)
        {
            Query();
        }

        private void acct_subj_code_xj_SelectedIndexChanged(object sender, EventArgs e)
        {
                 
        }
    }
}
