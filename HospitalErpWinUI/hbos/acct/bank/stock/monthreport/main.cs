﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

namespace HospitalErpWinUI.hbos.acct.bank.stock.monthreport
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        private DataTable _subjCode;
        public main()
        {
            _helper = new DaoHelper();
            InitializeComponent();
            this.Text = "库存月报";
        }

        private void ButtonSelect_Click(object sender, EventArgs e)
        {
            Query();
        }

        private void main_Load(object sender, EventArgs e)
        {
            LoadMonth();
        }
        //查询
        private void Query()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "acctBankStorckMonthReport_select";

            Dictionary<string, string> dict = new Dictionary<string, string>();

            dict.Add("comp_code", G_User.user.Comp_code);
            dict.Add("copy_code", G_User.user.Copy_code);
            dict.Add("acct_year", G_User.user.Acct_year);
            dict.Add("month", month.Text);
            serviceParamter.Paramters = dict;

            try 
            {
                DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
                _subjCode = dt;
                gridControl1.DataSource = _subjCode;
            }
            catch (Exception e)
            {
                MessageForm.Warning(e.Message);
            }
        
        }

        /**
       下拉数据加载
       */
        //会计期间
        private void LoadMonth()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acctPeriodAcct";

            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            serviceParamter.Paramters = paramters;

            List<string> companys = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                companys.Add(codeValue.Value);
            }

            month.Properties.Items.AddRange(companys);
            month.SelectedItem = companys[0];

        }
        private void vouch_direct_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}
