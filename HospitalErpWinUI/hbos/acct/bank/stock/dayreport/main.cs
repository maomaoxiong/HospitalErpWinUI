﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpWinUI.GlobalVar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.bank.stock.dayreport
{
    
    public partial class main : RibbonForm
    {

        protected DaoHelper _helper;
        private DataTable _subjCode;
        public main()
        {
            _helper = new DaoHelper();
            InitializeComponent();
            this.Text = "库存日报";
        }

        private void main_Load(object sender, EventArgs e)
        {
            dateFrom.Text = G_User.user.LoginDate;
        }

        private void ButtonSelect_Click(object sender, EventArgs e)
        {
            Query();
        }

        //查询
        private void Query()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "acctBankStorckDayReport_select";

            Dictionary<string, string> dict = new Dictionary<string, string>();

            dict.Add("comp_code", G_User.user.Comp_code);
            dict.Add("copy_code", G_User.user.Copy_code);
            dict.Add("acct_year", G_User.user.Acct_year);
            dict.Add("dateFrom", dateFrom.Text);          
            serviceParamter.Paramters = dict;
            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);

            /*for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
                string[] arragy2 = dt.Rows[i]["Column2"].ToString().Split('|');
                dt.Rows[i]["Column2"] = arragy2[3].ToString();

            }*/
            _subjCode = dt;
            gridControl1.DataSource = _subjCode;

        }

        private void dateFrom_EditValueChanged(object sender, EventArgs e)
        {

        }
    }
}
