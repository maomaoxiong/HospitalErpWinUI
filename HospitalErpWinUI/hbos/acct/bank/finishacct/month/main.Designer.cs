﻿namespace HospitalErpWinUI.hbos.acct.bank.finishacct.month
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.b_yuejie = new DevExpress.XtraEditors.SimpleButton();
            this.b_fanjie = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.account_month = new DevExpress.XtraEditors.TextEdit();
            this.teller_month = new DevExpress.XtraEditors.TextEdit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.account_month.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teller_month.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.panelControl1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panelControl2, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.04478F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 78.95522F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1567, 782);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.teller_month);
            this.panelControl1.Controls.Add(this.account_month);
            this.panelControl1.Controls.Add(this.b_yuejie);
            this.panelControl1.Controls.Add(this.b_fanjie);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Location = new System.Drawing.Point(3, 3);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1561, 158);
            this.panelControl1.TabIndex = 0;
            // 
            // b_yuejie
            // 
            this.b_yuejie.Location = new System.Drawing.Point(509, 87);
            this.b_yuejie.Name = "b_yuejie";
            this.b_yuejie.Size = new System.Drawing.Size(90, 27);
            this.b_yuejie.TabIndex = 19;
            this.b_yuejie.Text = "月结";
            this.b_yuejie.Click += new System.EventHandler(this.b_yuejie_Click);
            // 
            // b_fanjie
            // 
            this.b_fanjie.Location = new System.Drawing.Point(509, 31);
            this.b_fanjie.Name = "b_fanjie";
            this.b_fanjie.Size = new System.Drawing.Size(90, 27);
            this.b_fanjie.TabIndex = 20;
            this.b_fanjie.Text = "反结";
            this.b_fanjie.Click += new System.EventHandler(this.b_fanjie_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(135, 98);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(72, 14);
            this.labelControl2.TabIndex = 16;
            this.labelControl2.Text = "当前出纳月：";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(135, 37);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(72, 14);
            this.labelControl1.TabIndex = 15;
            this.labelControl1.Text = "上次结账月：";
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.labelControl6);
            this.panelControl2.Controls.Add(this.labelControl5);
            this.panelControl2.Location = new System.Drawing.Point(3, 167);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1561, 612);
            this.panelControl2.TabIndex = 1;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl6.Location = new System.Drawing.Point(33, 58);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(252, 14);
            this.labelControl6.TabIndex = 6;
            this.labelControl6.Text = "月结后，可以查询当前出纳月的资金库存月报。";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl5.Location = new System.Drawing.Point(33, 22);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(504, 14);
            this.labelControl5.TabIndex = 5;
            this.labelControl5.Text = "月结时，将检测当前出纳月的会计期间起始日期内是否存在未确认的出纳现金账、出纳银行账。";
            // 
            // account_month
            // 
            this.account_month.Enabled = false;
            this.account_month.Location = new System.Drawing.Point(226, 37);
            this.account_month.Name = "account_month";
            this.account_month.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.account_month.Properties.Appearance.Options.UseFont = true;
            this.account_month.Size = new System.Drawing.Size(187, 20);
            this.account_month.TabIndex = 21;
            // 
            // teller_month
            // 
            this.teller_month.Enabled = false;
            this.teller_month.Location = new System.Drawing.Point(226, 95);
            this.teller_month.Name = "teller_month";
            this.teller_month.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.teller_month.Properties.Appearance.Options.UseFont = true;
            this.teller_month.Size = new System.Drawing.Size(187, 20);
            this.teller_month.TabIndex = 22;
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1567, 782);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "main";
            this.Text = "main";
            this.Load += new System.EventHandler(this.main_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.account_month.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teller_month.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton b_yuejie;
        private DevExpress.XtraEditors.SimpleButton b_fanjie;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit teller_month;
        private DevExpress.XtraEditors.TextEdit account_month;
    }
}