﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.bank.finishacct.month
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        public main()
        {
            _helper = new DaoHelper();
            InitializeComponent();
            this.Text = "月结";
        }

        private void main_Load(object sender, EventArgs e)
        {
            LoadDate();
        }

        //获取结转月，出纳月
        private void LoadDate()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_bank_finish_month_new";

            paramters.Add("acct_year", G_User.user.Acct_year);
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            serviceParamter.Paramters = paramters;

            try
            {
                DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);

                account_month.Text = dt.Rows[0][0].ToString();
                teller_month.Text = dt.Rows[0][1].ToString();

                b_yuejie.Enabled = false;
                b_fanjie.Enabled = false;
                if (dt.Rows[0][2].ToString() == "1")
                {
                    b_fanjie.Enabled = true;
                }
                else if (dt.Rows[0][3].ToString() == "1")
                {
                    b_yuejie.Enabled = true;
                }

            }
            catch (Exception e)
            {

                MessageForm.Warning(e.Message);
            }

        }

        //反结
        private void b_fanjie_Click(object sender, EventArgs e)
        {
            fanjie();
            LoadDate();
        }

        private void fanjie() 
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "acct_bank_finishacct_month_back";

            Dictionary<string, string> dict = new Dictionary<string, string>();

            dict.Add("acct_year", G_User.user.Acct_year);
            dict.Add("comp_code", G_User.user.Comp_code);
            dict.Add("copy_code", G_User.user.Copy_code);

            serviceParamter.Paramters = dict;

            try
            {
                DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
                b_fanjie.Enabled = false;
                b_yuejie.Enabled = true;

            }
            catch (Exception e)
            {
                MessageForm.Warning(e.Message);
            }          
        }
        //日结
        private void b_yuejie_Click(object sender, EventArgs e)
        {
            yuejie();
            LoadDate();
        }

        private void yuejie()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "acct_bank_finishacct_month_fin";

            Dictionary<string, string> dict = new Dictionary<string, string>();

            dict.Add("acct_year", G_User.user.Acct_year);
            dict.Add("comp_code", G_User.user.Comp_code);
            dict.Add("copy_code", G_User.user.Copy_code);

            serviceParamter.Paramters = dict;

            try
            {
                DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
                b_fanjie.Enabled = true;
                b_yuejie.Enabled = false;

            }
            catch (Exception e)
            {
                MessageForm.Warning(e.Message);
            }
        }     
    }
}
