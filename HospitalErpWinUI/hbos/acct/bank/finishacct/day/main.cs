﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.bank.finishacct.day

{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        public main()
        {
            _helper = new DaoHelper();
            InitializeComponent();
            this.Text = "日结";
        }

        private void labelControl4_Click(object sender, EventArgs e)
        {

        }

        private void main_Load(object sender, EventArgs e)
        {
           
            LoadDate();
            
        }

        //获取结转日期，出纳日期
        private void LoadDate() 
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_bank_finish_day_bank";

            paramters.Add("acct_year", G_User.user.Acct_year);
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            serviceParamter.Paramters = paramters;
                
            try 
            {
                DataTable dt = _helper.ReadDictForSql(serviceParamter);
                string[] vals = System.Text.RegularExpressions.Regex.Split(dt.Rows[0][0].ToString().Trim(), @";");

                account_date.Text = vals[0];
                taller_date.Text = vals[1];

                b_rijie.Enabled = false;
                b_fanjie.Enabled = false;
                if (vals[2]=="1")
                {
                    b_fanjie.Enabled = true;
                }
                else if (vals[3] == "1")
                {
                    b_rijie.Enabled = true;
                }

            }catch(Exception e){

                MessageForm.Warning(e.Message);
            }
            
            
           
        }

        //日反结
        private void fanjie_Click(object sender, EventArgs e)
        {
            fanjie();
            LoadDate();
        }
        private void fanjie() {
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "acct_bank_finishacct_day_back";

            Dictionary<string, string> dict = new Dictionary<string, string>();

            dict.Add("acct_year", G_User.user.Acct_year);
            dict.Add("comp_code", G_User.user.Comp_code);
            dict.Add("copy_code", G_User.user.Copy_code);

            serviceParamter.Paramters = dict;

            try
            {
                DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
                b_fanjie.Enabled = false;
                b_rijie.Enabled = true;

            }catch(Exception e)
            {
                MessageForm.Warning(e.Message);
            }
           
        
        }

        //日结
        private void rijie_Click(object sender, EventArgs e)
        {
            rijie();
            LoadDate();
        }
        private void rijie() 
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "acct_bank_finishacct_day_fin";

            Dictionary<string, string> dict = new Dictionary<string, string>();

            dict.Add("acct_year", G_User.user.Acct_year);
            dict.Add("comp_code", G_User.user.Comp_code);
            dict.Add("copy_code", G_User.user.Copy_code);

            serviceParamter.Paramters = dict;

            try
            {
                DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
                b_fanjie.Enabled = true;
                b_rijie.Enabled = false;

            }
            catch (Exception e)
            {
                MessageForm.Warning(e.Message);
            }
        }

        private void panelControl2_Paint(object sender, PaintEventArgs e)
        {

        }
            

    }
}
