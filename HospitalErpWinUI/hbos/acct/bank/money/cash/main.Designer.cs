﻿namespace HospitalErpWinUI.hbos.acct.bank.money.cash
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.hyperLinkEdit22 = new DevExpress.XtraEditors.HyperLinkEdit();
            ((System.ComponentModel.ISupportInitialize)(this.hyperLinkEdit22.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // hyperLinkEdit22
            // 
            this.hyperLinkEdit22.EditValue = "现金出纳账";
            this.hyperLinkEdit22.Location = new System.Drawing.Point(71, 69);
            this.hyperLinkEdit22.Name = "hyperLinkEdit22";
            this.hyperLinkEdit22.Size = new System.Drawing.Size(100, 20);
            this.hyperLinkEdit22.TabIndex = 0;
            this.hyperLinkEdit22.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.hyperLinkEdit1_OpenLink);
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.hyperLinkEdit22);
            this.Name = "main";
            this.Text = "main";
            ((System.ComponentModel.ISupportInitialize)(this.hyperLinkEdit22.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.HyperLinkEdit hyperLinkEdit22;
    }
}