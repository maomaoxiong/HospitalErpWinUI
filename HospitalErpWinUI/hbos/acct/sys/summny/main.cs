﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.sys.summny
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;

        public List<Perm> _perms { get; set; }


        //  public List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public main()
        {
            InitializeComponent();
            _helper = new DaoHelper();
            
        }

        private void acctsyssummny_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acctsyssummny_select";
            paramters.Add("summ_code", summ_code.Text.Trim());
            paramters.Add("summary", summary.Text.Trim());
             
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;
        }
         

        private void gridView1_FocusedRowChanged_1(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int selectedID = e.PrevFocusedRowHandle;//e.FocusedRowHandle;
            DataRow row = gridView1.GetDataRow(selectedID);
            if (row != null)
            {
                if (row["_vouch_type_code"].ToString() == null || row["_vouch_type_code"].ToString().Equals(""))
                {
                    if (!Validate())
                    {
                        return;
                    }
                    try
                    {
                        Insert(row);
                        this.DialogResult = System.Windows.Forms.DialogResult.OK;
                        //     MessageForm.Show("新增角色成功！");
                      //  this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageForm.Exception(ex.Message, "异常");
                    }
                }
                else {
                    if (!Validate( row))
                    {
                        return;
                    }
                    try
                    {
                        u_update1(row);
                        this.DialogResult = System.Windows.Forms.DialogResult.OK;
                        //     MessageForm.Show("新增角色成功！");
                        //  this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageForm.Exception(ex.Message, "异常");
                    }
                }
               // MessageBox.Show(row["_vouch_type_code"].ToString() + row["summ_code"].ToString() + row["summary"].ToString());
            }
        }
        private bool u_update1(DataRow row)
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "acctsyssummny_update";
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("_vouch_type_code", row["_vouch_type_code"].ToString());
            paramters.Add("summ_code", row["summ_code"].ToString());
            paramters.Add("summary", row["summary"].ToString());
            serviceParamter.Paramters = paramters;

            return _helper.WirteSql(serviceParamter);

        }
        private bool Insert(DataRow row)
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "acctsyssummny_insert";
            Dictionary<string, string> paramters = new Dictionary<string, string>();

            paramters.Add("summ_code", row["summ_code"].ToString());
            paramters.Add("summary", row["summary"].ToString());
            serviceParamter.Paramters = paramters;

            return _helper.WirteSql(serviceParamter);

        }
        private bool Validate(DataRow row)
        {
                
            if (!Validator.IsInteger(row["summ_code"].ToString()))
            {
                MessageBox.Show("摘要编码请正确输入数字");
                return false;
            }
            if (string.IsNullOrEmpty(row["summary"].ToString()))
            {
                MessageForm.Warning("摘要名称不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(row["summ_code"].ToString()))
            {
                MessageForm.Warning("摘要编码不能为空!");
                return false;
            }
            return true;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            bool value = false;
            string strSelected = string.Empty;

            try
            {
                List<string> objs = new List<string>();
                for (int i = 0; i < gridView1.RowCount-1; i++)
                {
                    value = Convert.ToBoolean(gridView1.GetDataRow(i)["checked"]);
                    if (value)
                    {
                        string code = gridView1.GetDataRow(i)["_vouch_type_code"].ToString();
                        objs.Add(code);

                    }
                }
                if (objs.Count == 0)
                {
                    MessageForm.Show("请选择要删除的数据!");
                    return;
                }
                if (MessageForm.ShowMsg("确认是否删除?", MsgType.YesNo) == DialogResult.Yes)
                {
                    foreach (string obj in objs)
                    {
                        if (!Delete(obj))
                        {
                            return;
                        }
                    }
                    //       MessageForm.Show("操作成功!");
                    Query();
                }

            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private bool Delete(string co_code)
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acctsyssummny_delete";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("_vouch_type_code", co_code);
            serviceParamter.Paramters = paramters;
            bool result = false;
            try
            {
                _helper.WirteSql(serviceParamter);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }
    }
}
