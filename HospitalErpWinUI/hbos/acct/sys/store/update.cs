﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.sys.store
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        public update()
        {
            InitializeComponent();
        }
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Enter)　　// 按下的是回车键
            {
                foreach (Control c in this.Controls)
                {
                    if (c is System.Windows.Forms.TextBox)　　// 当前控件是文本框控件
                    {
                        keyData = Keys.Tab;
                    }
                }
                keyData = Keys.Tab;
            }
            return base.ProcessDialogKey(keyData);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
        private bool o_Validate()
        {

            if (string.IsNullOrEmpty(view_id.Text))
            {
                MessageForm.Warning("虚仓编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(view_name.Text))
            {
                MessageForm.Warning("虚仓名称不能为空!");
                return false;
            }
            return true;
        }

        private void acctSysStore_update_Click(object sender, EventArgs e)
        {
            if (!o_Validate())
            {
                return;
            }
            try
            {
                o_update();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
        private bool o_update()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "acctSysStore_update";
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            paramters.Add("view_id1", view_id.Text);
            paramters.Add("view_id", view_id.Text);
            paramters.Add("view_name", view_name.Text);

            serviceParamter.Paramters = paramters;

            return _helper.WirteSql(serviceParamter);

        }
        public update(DaoHelper helper, List<Perm> perms, string view_id, string view_name)
        {

            InitializeComponent();
            _helper = helper;
            _perms = perms;
            this.Text = "录入";
            this.StartPosition = FormStartPosition.CenterParent;
            this.view_id.Text = view_id;
            this.view_name.Text = view_name;
        }
    }
}
