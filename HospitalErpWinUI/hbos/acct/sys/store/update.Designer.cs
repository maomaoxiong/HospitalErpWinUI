﻿namespace HospitalErpWinUI.hbos.acct.sys.store
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.acctSysStore_update = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.view_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.view_id = new DevExpress.XtraEditors.TextEdit();
            this.qqq = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.view_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.view_id.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(284, 262);
            this.panelControl1.TabIndex = 0;
            // 
            // acctSysStore_update
            // 
            this.acctSysStore_update.Location = new System.Drawing.Point(37, 121);
            this.acctSysStore_update.Name = "acctSysStore_update";
            this.acctSysStore_update.Size = new System.Drawing.Size(75, 23);
            this.acctSysStore_update.TabIndex = 2;
            this.acctSysStore_update.Text = "保存";
            this.acctSysStore_update.Click += new System.EventHandler(this.acctSysStore_update_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.simpleButton2);
            this.panelControl2.Controls.Add(this.acctSysStore_update);
            this.panelControl2.Controls.Add(this.view_name);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Controls.Add(this.view_id);
            this.panelControl2.Controls.Add(this.qqq);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(284, 262);
            this.panelControl2.TabIndex = 0;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(154, 121);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 3;
            this.simpleButton2.Text = "关闭";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // view_name
            // 
            this.view_name.Location = new System.Drawing.Point(92, 75);
            this.view_name.Name = "view_name";
            this.view_name.Size = new System.Drawing.Size(168, 20);
            this.view_name.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(26, 78);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 104;
            this.labelControl1.Text = "虚仓名称：";
            // 
            // view_id
            // 
            this.view_id.Enabled = false;
            this.view_id.Location = new System.Drawing.Point(92, 33);
            this.view_id.Name = "view_id";
            this.view_id.Size = new System.Drawing.Size(168, 20);
            this.view_id.TabIndex = 0;
            // 
            // qqq
            // 
            this.qqq.Location = new System.Drawing.Point(26, 36);
            this.qqq.Name = "qqq";
            this.qqq.Size = new System.Drawing.Size(60, 14);
            this.qqq.TabIndex = 103;
            this.qqq.Text = "虚仓编码：";
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "update";
            this.Text = "update";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.view_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.view_id.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton acctSysStore_update;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.TextEdit view_name;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit view_id;
        private DevExpress.XtraEditors.LabelControl qqq;
    }
}