﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.sys.store
{
    public partial class detail : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public detail()
        {
            InitializeComponent();
        }
        public detail(DaoHelper helper, List<Perm> perms, string view_id, string view_name)
        {

            InitializeComponent();
            _helper = helper;
            _perms = perms;
            this.Text = "设置";
            this.StartPosition = FormStartPosition.CenterParent;
            this.view_id.Text = view_id;
            this.view_name.Text = view_name;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            Query();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
        private void Query()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acctSysStore_insert_select";
            paramters.Add("view_id", view_id.Text.Trim());
            paramters.Add("view_name", view_name.Text.Trim());
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["_checkbox_value"].ToString() == "1")
                {
                    dt.Rows[i]["checked"] = "true";
                }
                else
                {
                    dt.Rows[i]["checked"] = "false";
                }
                
            }
            _user = dt;
            gridControl1.DataSource = _user;
        }

        private void acctSysStore_insert_detail_Click(object sender, EventArgs e)
        {
             int value =0;
            string strSelected = string.Empty;

            try
            {
                List<string> objs = new List<string>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    value = Convert.ToInt32(gridView1.GetDataRow(i)["checked"]);
                    if(value!=Convert.ToInt32(gridView1.GetDataRow(i)["_checkbox_value"].ToString())){
                        string code = gridView1.GetDataRow(i)["store_code"].ToString();
                        save(view_id.Text.ToString(), code, value);
                    }
                    
                }
                MessageBox.Show("操作成功");
                         
                    Query();
             
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void save(string view_id,string co_code,int value)
        {

            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "acctSysStore_insert_detail";
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            paramters.Add("view_id1", view_id);
            paramters.Add("view_id", co_code);
            paramters.Add("view_name", value.ToString());

            serviceParamter.Paramters = paramters;

              _helper.WirteSql(serviceParamter);

        }
    }
}
