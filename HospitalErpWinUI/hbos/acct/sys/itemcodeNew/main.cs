﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.sys.itemcodeNew
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        
        public List<Perm> _perms { get; set; }

        
      //  public List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        //初始化绑定默认关键词（此数据源可以从数据库取）
        List<string> _groupsInit = new List<string>();
        List<string> _groupsChange = new List<string>();
        public main()
        {
            InitializeComponent();
            _helper = new DaoHelper();
        }

        private void main_Load(object sender, EventArgs e)
        {
        
            Load_check_method();
        }
        //查询会计科目类别：
        private void Load_acct_itemcode_type_list()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_itemcode_type_list";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
          

            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            subj_type_code.Properties.Items.AddRange(objs);

        }
        //会计核算类别：
        private void Load_check_method()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_check_method_type";   
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            check_method.Properties.Items.AddRange(objs);

        }

        private void acctItemcNewCode_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();    
            }
            catch(Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acctItemcNewCode_select";
            paramters.Add("Comp_code", G_User.user.Comp_code);
            paramters.Add("Copy_code", G_User.user.Copy_code);
            paramters.Add("Acct_year", G_User.user.Acct_year);
            paramters.Add("acct_subj_code", acct_subj_code.Text.Trim());
            paramters.Add("acct_subj_name", acct_subj_name.Text.Trim());
            paramters.Add("check_method", check_method.Text.Trim());
            paramters.Add("subj_type_code", subj_type_code.Text.Trim());
            char[] array = { ' ' };
            // string [] strArray= userGroup_code.Text.Trim().Split(array);
           // string[] strArray = System.Text.RegularExpressions.Regex.Split(userGroup_code.Text.Trim(), @"\s{1,}");
            //paramters.Add("group_code", strArray[0]);
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;

        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("111");
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                insert insertFrm = new insert(this._helper, this._perms);

                if (insertFrm.ShowDialog() == DialogResult.OK)
                {
                    Query();
                }
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
    }
}
