﻿namespace HospitalErpWinUI.hbos.acct.sys.itemcodeNew
{
    partial class insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comp_code = new DevExpress.XtraEditors.TextEdit();
            this.copy_code = new DevExpress.XtraEditors.TextEdit();
            this.acct_year = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.acct_subj_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.super_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.acct_subj_name = new DevExpress.XtraEditors.TextEdit();
            this.subj_type_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.contact_type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.subj_nature_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.check_method = new DevExpress.XtraEditors.ComboBoxEdit();
            this.qq = new DevExpress.XtraEditors.LabelControl();
            this.direction = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.is_cash_1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.is_cash = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.is_fc_string = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.is_budge_1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.is_budge = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.is_fc_value = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.is_fc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.is_num_1 = new DevExpress.XtraEditors.TextEdit();
            this.is_num = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.count_unit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.is_check_1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.is_check = new DevExpress.XtraEditors.TextEdit();
            this.check_type1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.check_type2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.check_type4 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.check_type3 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.check_type6 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.check_type5 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.check_type8 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.check_type7 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.group_type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.group_value = new DevExpress.XtraEditors.TextEdit();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.super_code_ = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.set_fc = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.comp_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.copy_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_year.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.super_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subj_type_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contact_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subj_nature_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_method.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.direction.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_cash_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_cash.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_fc_string.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_budge_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_budge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_fc_value.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_fc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_num_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_num.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.count_unit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_check_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_check.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_type1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_type2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_type4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_type3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_type6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_type5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_type8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_type7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_value.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.super_code_.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            this.SuspendLayout();
            // 
            // comp_code
            // 
            this.comp_code.Location = new System.Drawing.Point(42, 12);
            this.comp_code.Name = "comp_code";
            this.comp_code.Size = new System.Drawing.Size(100, 20);
            this.comp_code.TabIndex = 0;
            this.comp_code.Visible = false;
            // 
            // copy_code
            // 
            this.copy_code.Location = new System.Drawing.Point(173, 12);
            this.copy_code.Name = "copy_code";
            this.copy_code.Size = new System.Drawing.Size(100, 20);
            this.copy_code.TabIndex = 1;
            this.copy_code.Visible = false;
            // 
            // acct_year
            // 
            this.acct_year.Location = new System.Drawing.Point(318, 12);
            this.acct_year.Name = "acct_year";
            this.acct_year.Size = new System.Drawing.Size(100, 20);
            this.acct_year.TabIndex = 2;
            this.acct_year.Visible = false;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(27, 63);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(84, 14);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "科目编码规则：";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(420, 74);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "上级科目：";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(51, 93);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 14);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "科目编码：";
            // 
            // acct_subj_code
            // 
            this.acct_subj_code.Location = new System.Drawing.Point(126, 90);
            this.acct_subj_code.Name = "acct_subj_code";
            this.acct_subj_code.Size = new System.Drawing.Size(100, 20);
            this.acct_subj_code.TabIndex = 9;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(444, 18);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(72, 14);
            this.labelControl4.TabIndex = 10;
            this.labelControl4.Text = "父级科目编码";
            this.labelControl4.Visible = false;
            // 
            // super_code
            // 
            this.super_code.Location = new System.Drawing.Point(522, 15);
            this.super_code.Name = "super_code";
            this.super_code.Size = new System.Drawing.Size(100, 20);
            this.super_code.TabIndex = 11;
            this.super_code.Visible = false;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(420, 104);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 14);
            this.labelControl5.TabIndex = 12;
            this.labelControl5.Text = "科目名称：";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(51, 123);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(60, 14);
            this.labelControl6.TabIndex = 14;
            this.labelControl6.Text = "科目类别：";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(420, 134);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(60, 14);
            this.labelControl7.TabIndex = 16;
            this.labelControl7.Text = "科目性质：";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(51, 154);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(60, 14);
            this.labelControl8.TabIndex = 18;
            this.labelControl8.Text = "往来类别：";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(396, 165);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(84, 14);
            this.labelControl9.TabIndex = 20;
            this.labelControl9.Text = "会计核算类别：";
            // 
            // acct_subj_name
            // 
            this.acct_subj_name.Location = new System.Drawing.Point(502, 98);
            this.acct_subj_name.Name = "acct_subj_name";
            this.acct_subj_name.Size = new System.Drawing.Size(152, 20);
            this.acct_subj_name.TabIndex = 13;
            // 
            // subj_type_code
            // 
            this.subj_type_code.Location = new System.Drawing.Point(126, 120);
            this.subj_type_code.Name = "subj_type_code";
            this.subj_type_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.subj_type_code.Size = new System.Drawing.Size(100, 20);
            this.subj_type_code.TabIndex = 21;
            // 
            // contact_type
            // 
            this.contact_type.Location = new System.Drawing.Point(126, 151);
            this.contact_type.Name = "contact_type";
            this.contact_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.contact_type.Size = new System.Drawing.Size(100, 20);
            this.contact_type.TabIndex = 22;
            // 
            // subj_nature_code
            // 
            this.subj_nature_code.Location = new System.Drawing.Point(502, 131);
            this.subj_nature_code.Name = "subj_nature_code";
            this.subj_nature_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.subj_nature_code.Size = new System.Drawing.Size(152, 20);
            this.subj_nature_code.TabIndex = 23;
            // 
            // check_method
            // 
            this.check_method.Location = new System.Drawing.Point(502, 162);
            this.check_method.Name = "check_method";
            this.check_method.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.check_method.Size = new System.Drawing.Size(152, 20);
            this.check_method.TabIndex = 24;
            // 
            // qq
            // 
            this.qq.Location = new System.Drawing.Point(51, 183);
            this.qq.Name = "qq";
            this.qq.Size = new System.Drawing.Size(60, 14);
            this.qq.TabIndex = 25;
            this.qq.Text = "余额方向：";
            // 
            // direction
            // 
            this.direction.Location = new System.Drawing.Point(126, 180);
            this.direction.Name = "direction";
            this.direction.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.direction.Size = new System.Drawing.Size(100, 20);
            this.direction.TabIndex = 26;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(408, 194);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(72, 14);
            this.labelControl10.TabIndex = 27;
            this.labelControl10.Text = "核算现金流：";
            // 
            // is_cash_1
            // 
            this.is_cash_1.Location = new System.Drawing.Point(502, 191);
            this.is_cash_1.Name = "is_cash_1";
            this.is_cash_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_cash_1.Size = new System.Drawing.Size(152, 20);
            this.is_cash_1.TabIndex = 28;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(751, 82);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(96, 14);
            this.labelControl11.TabIndex = 29;
            this.labelControl11.Text = "核算现金流量标志";
            this.labelControl11.Visible = false;
            // 
            // is_cash
            // 
            this.is_cash.EditValue = "0";
            this.is_cash.Location = new System.Drawing.Point(751, 102);
            this.is_cash.Name = "is_cash";
            this.is_cash.Size = new System.Drawing.Size(100, 20);
            this.is_cash.TabIndex = 30;
            this.is_cash.Visible = false;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(51, 213);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(60, 14);
            this.labelControl12.TabIndex = 31;
            this.labelControl12.Text = "核算外币：";
            // 
            // is_fc_string
            // 
            this.is_fc_string.Location = new System.Drawing.Point(126, 210);
            this.is_fc_string.Name = "is_fc_string";
            this.is_fc_string.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_fc_string.Size = new System.Drawing.Size(100, 20);
            this.is_fc_string.TabIndex = 32;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(420, 224);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(60, 14);
            this.labelControl13.TabIndex = 33;
            this.labelControl13.Text = "预算标志：";
            // 
            // is_budge_1
            // 
            this.is_budge_1.Location = new System.Drawing.Point(502, 221);
            this.is_budge_1.Name = "is_budge_1";
            this.is_budge_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_budge_1.Size = new System.Drawing.Size(152, 20);
            this.is_budge_1.TabIndex = 34;
            // 
            // is_budge
            // 
            this.is_budge.EditValue = "0";
            this.is_budge.Location = new System.Drawing.Point(809, 128);
            this.is_budge.Name = "is_budge";
            this.is_budge.Size = new System.Drawing.Size(48, 20);
            this.is_budge.TabIndex = 36;
            this.is_budge.Visible = false;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(755, 128);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(48, 14);
            this.labelControl14.TabIndex = 35;
            this.labelControl14.Text = "预算标志";
            this.labelControl14.Visible = false;
            // 
            // is_fc_value
            // 
            this.is_fc_value.EditValue = "";
            this.is_fc_value.Location = new System.Drawing.Point(811, 151);
            this.is_fc_value.Name = "is_fc_value";
            this.is_fc_value.Size = new System.Drawing.Size(48, 20);
            this.is_fc_value.TabIndex = 38;
            this.is_fc_value.Visible = false;
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(745, 154);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(60, 14);
            this.labelControl15.TabIndex = 37;
            this.labelControl15.Text = "核算外币值";
            this.labelControl15.Visible = false;
            // 
            // is_fc
            // 
            this.is_fc.EditValue = "0";
            this.is_fc.Location = new System.Drawing.Point(809, 177);
            this.is_fc.Name = "is_fc";
            this.is_fc.Size = new System.Drawing.Size(48, 20);
            this.is_fc.TabIndex = 40;
            this.is_fc.Visible = false;
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(743, 180);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(72, 14);
            this.labelControl16.TabIndex = 39;
            this.labelControl16.Text = "核算外币标志";
            this.labelControl16.Visible = false;
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(743, 214);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(60, 14);
            this.labelControl17.TabIndex = 41;
            this.labelControl17.Text = "yc核算数量";
            this.labelControl17.Visible = false;
            // 
            // is_num_1
            // 
            this.is_num_1.EditValue = "0";
            this.is_num_1.Location = new System.Drawing.Point(816, 211);
            this.is_num_1.Name = "is_num_1";
            this.is_num_1.Size = new System.Drawing.Size(48, 20);
            this.is_num_1.TabIndex = 42;
            this.is_num_1.Visible = false;
            // 
            // is_num
            // 
            this.is_num.EditValue = "0";
            this.is_num.Location = new System.Drawing.Point(816, 237);
            this.is_num.Name = "is_num";
            this.is_num.Size = new System.Drawing.Size(48, 20);
            this.is_num.TabIndex = 44;
            this.is_num.Visible = false;
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(743, 240);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(84, 14);
            this.labelControl18.TabIndex = 43;
            this.labelControl18.Text = "yc核算数量标志";
            this.labelControl18.Visible = false;
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(51, 244);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(60, 14);
            this.labelControl19.TabIndex = 45;
            this.labelControl19.Text = "计量单位：";
            // 
            // count_unit
            // 
            this.count_unit.Location = new System.Drawing.Point(126, 241);
            this.count_unit.Name = "count_unit";
            this.count_unit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.count_unit.Size = new System.Drawing.Size(100, 20);
            this.count_unit.TabIndex = 46;
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(420, 258);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(60, 14);
            this.labelControl20.TabIndex = 47;
            this.labelControl20.Text = "辅助核算：";
            // 
            // is_check_1
            // 
            this.is_check_1.Location = new System.Drawing.Point(502, 255);
            this.is_check_1.Name = "is_check_1";
            this.is_check_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_check_1.Size = new System.Drawing.Size(152, 20);
            this.is_check_1.TabIndex = 48;
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(745, 266);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(84, 14);
            this.labelControl21.TabIndex = 49;
            this.labelControl21.Text = "辅助核算标志：";
            this.labelControl21.Visible = false;
            // 
            // is_check
            // 
            this.is_check.EditValue = "0";
            this.is_check.Location = new System.Drawing.Point(821, 263);
            this.is_check.Name = "is_check";
            this.is_check.Size = new System.Drawing.Size(48, 20);
            this.is_check.TabIndex = 50;
            this.is_check.Visible = false;
            // 
            // check_type1
            // 
            this.check_type1.Location = new System.Drawing.Point(126, 276);
            this.check_type1.Name = "check_type1";
            this.check_type1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.check_type1.Size = new System.Drawing.Size(100, 20);
            this.check_type1.TabIndex = 52;
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(51, 279);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(55, 14);
            this.labelControl22.TabIndex = 51;
            this.labelControl22.Text = "核算类1：";
            // 
            // check_type2
            // 
            this.check_type2.Location = new System.Drawing.Point(502, 287);
            this.check_type2.Name = "check_type2";
            this.check_type2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.check_type2.Size = new System.Drawing.Size(152, 20);
            this.check_type2.TabIndex = 54;
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(420, 290);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(55, 14);
            this.labelControl23.TabIndex = 53;
            this.labelControl23.Text = "核算类2：";
            // 
            // check_type4
            // 
            this.check_type4.Location = new System.Drawing.Point(502, 313);
            this.check_type4.Name = "check_type4";
            this.check_type4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.check_type4.Size = new System.Drawing.Size(152, 20);
            this.check_type4.TabIndex = 58;
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(420, 316);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(55, 14);
            this.labelControl24.TabIndex = 57;
            this.labelControl24.Text = "核算类4：";
            // 
            // check_type3
            // 
            this.check_type3.Location = new System.Drawing.Point(126, 302);
            this.check_type3.Name = "check_type3";
            this.check_type3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.check_type3.Size = new System.Drawing.Size(100, 20);
            this.check_type3.TabIndex = 56;
            // 
            // labelControl25
            // 
            this.labelControl25.Location = new System.Drawing.Point(51, 305);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(55, 14);
            this.labelControl25.TabIndex = 55;
            this.labelControl25.Text = "核算类3：";
            // 
            // check_type6
            // 
            this.check_type6.Location = new System.Drawing.Point(502, 339);
            this.check_type6.Name = "check_type6";
            this.check_type6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.check_type6.Size = new System.Drawing.Size(152, 20);
            this.check_type6.TabIndex = 62;
            // 
            // labelControl26
            // 
            this.labelControl26.Location = new System.Drawing.Point(420, 342);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(55, 14);
            this.labelControl26.TabIndex = 61;
            this.labelControl26.Text = "核算类6：";
            // 
            // check_type5
            // 
            this.check_type5.Location = new System.Drawing.Point(126, 328);
            this.check_type5.Name = "check_type5";
            this.check_type5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.check_type5.Size = new System.Drawing.Size(100, 20);
            this.check_type5.TabIndex = 60;
            // 
            // labelControl27
            // 
            this.labelControl27.Location = new System.Drawing.Point(51, 331);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(55, 14);
            this.labelControl27.TabIndex = 59;
            this.labelControl27.Text = "核算类5：";
            // 
            // check_type8
            // 
            this.check_type8.Location = new System.Drawing.Point(502, 365);
            this.check_type8.Name = "check_type8";
            this.check_type8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.check_type8.Size = new System.Drawing.Size(152, 20);
            this.check_type8.TabIndex = 66;
            // 
            // labelControl28
            // 
            this.labelControl28.Location = new System.Drawing.Point(420, 368);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(55, 14);
            this.labelControl28.TabIndex = 65;
            this.labelControl28.Text = "核算类8：";
            // 
            // check_type7
            // 
            this.check_type7.Location = new System.Drawing.Point(126, 354);
            this.check_type7.Name = "check_type7";
            this.check_type7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.check_type7.Size = new System.Drawing.Size(100, 20);
            this.check_type7.TabIndex = 64;
            // 
            // labelControl29
            // 
            this.labelControl29.Location = new System.Drawing.Point(51, 357);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(55, 14);
            this.labelControl29.TabIndex = 63;
            this.labelControl29.Text = "核算类7：";
            // 
            // labelControl30
            // 
            this.labelControl30.Location = new System.Drawing.Point(427, 394);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(48, 14);
            this.labelControl30.TabIndex = 67;
            this.labelControl30.Text = "零差率：";
            this.labelControl30.Visible = false;
            // 
            // group_type
            // 
            this.group_type.Location = new System.Drawing.Point(126, 380);
            this.group_type.Name = "group_type";
            this.group_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.group_type.Size = new System.Drawing.Size(100, 20);
            this.group_type.TabIndex = 70;
            // 
            // labelControl31
            // 
            this.labelControl31.Location = new System.Drawing.Point(51, 383);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(60, 14);
            this.labelControl31.TabIndex = 69;
            this.labelControl31.Text = "科目分组：";
            // 
            // group_value
            // 
            this.group_value.EditValue = "0";
            this.group_value.Location = new System.Drawing.Point(502, 391);
            this.group_value.Name = "group_value";
            this.group_value.Size = new System.Drawing.Size(152, 20);
            this.group_value.TabIndex = 71;
            this.group_value.Visible = false;
            // 
            // labelControl32
            // 
            this.labelControl32.Location = new System.Drawing.Point(126, 63);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(84, 14);
            this.labelControl32.TabIndex = 72;
            this.labelControl32.Text = "科目编码规则：";
            // 
            // super_code_
            // 
            this.super_code_.EditValue = "";
            this.super_code_.Location = new System.Drawing.Point(502, 68);
            this.super_code_.Name = "super_code_";
            this.super_code_.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.super_code_.Properties.NullText = "";
            this.super_code_.Properties.View = this.searchLookUpEdit1View;
            this.super_code_.Size = new System.Drawing.Size(152, 20);
            this.super_code_.TabIndex = 73;
            this.super_code_.EditValueChanged += new System.EventHandler(this.super_code__EditValueChanged);
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // set_fc
            // 
            this.set_fc.Location = new System.Drawing.Point(244, 204);
            this.set_fc.Name = "set_fc";
            this.set_fc.Size = new System.Drawing.Size(75, 23);
            this.set_fc.TabIndex = 74;
            this.set_fc.Text = "设置";
            // 
            // insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(893, 439);
            this.Controls.Add(this.set_fc);
            this.Controls.Add(this.super_code_);
            this.Controls.Add(this.labelControl32);
            this.Controls.Add(this.group_value);
            this.Controls.Add(this.group_type);
            this.Controls.Add(this.labelControl31);
            this.Controls.Add(this.labelControl30);
            this.Controls.Add(this.check_type8);
            this.Controls.Add(this.labelControl28);
            this.Controls.Add(this.check_type7);
            this.Controls.Add(this.labelControl29);
            this.Controls.Add(this.check_type6);
            this.Controls.Add(this.labelControl26);
            this.Controls.Add(this.check_type5);
            this.Controls.Add(this.labelControl27);
            this.Controls.Add(this.check_type4);
            this.Controls.Add(this.labelControl24);
            this.Controls.Add(this.check_type3);
            this.Controls.Add(this.labelControl25);
            this.Controls.Add(this.check_type2);
            this.Controls.Add(this.labelControl23);
            this.Controls.Add(this.check_type1);
            this.Controls.Add(this.labelControl22);
            this.Controls.Add(this.is_check);
            this.Controls.Add(this.labelControl21);
            this.Controls.Add(this.is_check_1);
            this.Controls.Add(this.labelControl20);
            this.Controls.Add(this.count_unit);
            this.Controls.Add(this.labelControl19);
            this.Controls.Add(this.is_num);
            this.Controls.Add(this.labelControl18);
            this.Controls.Add(this.is_num_1);
            this.Controls.Add(this.labelControl17);
            this.Controls.Add(this.is_fc);
            this.Controls.Add(this.labelControl16);
            this.Controls.Add(this.is_fc_value);
            this.Controls.Add(this.labelControl15);
            this.Controls.Add(this.is_budge);
            this.Controls.Add(this.labelControl14);
            this.Controls.Add(this.is_budge_1);
            this.Controls.Add(this.labelControl13);
            this.Controls.Add(this.is_fc_string);
            this.Controls.Add(this.labelControl12);
            this.Controls.Add(this.is_cash);
            this.Controls.Add(this.labelControl11);
            this.Controls.Add(this.is_cash_1);
            this.Controls.Add(this.labelControl10);
            this.Controls.Add(this.direction);
            this.Controls.Add(this.qq);
            this.Controls.Add(this.check_method);
            this.Controls.Add(this.subj_nature_code);
            this.Controls.Add(this.contact_type);
            this.Controls.Add(this.subj_type_code);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.acct_subj_name);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.super_code);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.acct_subj_code);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.acct_year);
            this.Controls.Add(this.copy_code);
            this.Controls.Add(this.comp_code);
            this.Name = "insert";
            this.Text = "添加";
            this.Load += new System.EventHandler(this.insert_Load);
            ((System.ComponentModel.ISupportInitialize)(this.comp_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.copy_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_year.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.super_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subj_type_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contact_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subj_nature_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_method.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.direction.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_cash_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_cash.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_fc_string.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_budge_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_budge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_fc_value.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_fc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_num_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_num.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.count_unit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_check_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_check.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_type1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_type2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_type4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_type3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_type6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_type5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_type8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_type7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_value.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.super_code_.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit comp_code;
        private DevExpress.XtraEditors.TextEdit copy_code;
        private DevExpress.XtraEditors.TextEdit acct_year;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit acct_subj_code;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit super_code;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit acct_subj_name;
        private DevExpress.XtraEditors.ComboBoxEdit subj_type_code;
        private DevExpress.XtraEditors.ComboBoxEdit contact_type;
        private DevExpress.XtraEditors.ComboBoxEdit subj_nature_code;
        private DevExpress.XtraEditors.ComboBoxEdit check_method;
        private DevExpress.XtraEditors.LabelControl qq;
        private DevExpress.XtraEditors.ComboBoxEdit direction;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.ComboBoxEdit is_cash_1;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit is_cash;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.ComboBoxEdit is_fc_string;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.ComboBoxEdit is_budge_1;
        private DevExpress.XtraEditors.TextEdit is_budge;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit is_fc_value;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit is_fc;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit is_num_1;
        private DevExpress.XtraEditors.TextEdit is_num;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.ComboBoxEdit count_unit;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.ComboBoxEdit is_check_1;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit is_check;
        private DevExpress.XtraEditors.ComboBoxEdit check_type1;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.ComboBoxEdit check_type2;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.ComboBoxEdit check_type4;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.ComboBoxEdit check_type3;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.ComboBoxEdit check_type6;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.ComboBoxEdit check_type5;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.ComboBoxEdit check_type8;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.ComboBoxEdit check_type7;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.ComboBoxEdit group_type;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.TextEdit group_value;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.SearchLookUpEdit super_code_;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraEditors.SimpleButton set_fc;
    }
}