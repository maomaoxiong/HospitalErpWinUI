﻿namespace HospitalErpWinUI.hbos.acct.sys.itemcodeNew
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.查询条件 = new DevExpress.XtraEditors.PanelControl();
            this.acctVouchPrint = new DevExpress.XtraEditors.SimpleButton();
            this.acctVouchPrint2 = new DevExpress.XtraEditors.SimpleButton();
            this.acctItemcNewCode_select = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.subj_type_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.check_method = new DevExpress.XtraEditors.ComboBoxEdit();
            this.acct_subj_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.acct_subj_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.acct_subj = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.acct_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.acct_subj_name_all = new DevExpress.XtraGrid.Columns.GridColumn();
            this.subj_type_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.check_method2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.subj_nature_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.contact_type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.direction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.is_cash = new DevExpress.XtraGrid.Columns.GridColumn();
            this.is_check = new DevExpress.XtraGrid.Columns.GridColumn();
            this.check_type1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.check_type2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.check_type3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.check_type4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.check_type5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.check_type6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.check_type7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.check_type8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.is_budge = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.查询条件)).BeginInit();
            this.查询条件.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.subj_type_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_method.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // 查询条件
            // 
            this.查询条件.Controls.Add(this.acctVouchPrint);
            this.查询条件.Controls.Add(this.acctVouchPrint2);
            this.查询条件.Controls.Add(this.acctItemcNewCode_select);
            this.查询条件.Controls.Add(this.labelControl4);
            this.查询条件.Controls.Add(this.subj_type_code);
            this.查询条件.Controls.Add(this.labelControl3);
            this.查询条件.Controls.Add(this.check_method);
            this.查询条件.Controls.Add(this.acct_subj_name);
            this.查询条件.Controls.Add(this.labelControl2);
            this.查询条件.Controls.Add(this.acct_subj_code);
            this.查询条件.Controls.Add(this.labelControl1);
            this.查询条件.Dock = System.Windows.Forms.DockStyle.Top;
            this.查询条件.Location = new System.Drawing.Point(0, 0);
            this.查询条件.Name = "查询条件";
            this.查询条件.Size = new System.Drawing.Size(1142, 57);
            this.查询条件.TabIndex = 0;
            // 
            // acctVouchPrint
            // 
            this.acctVouchPrint.Location = new System.Drawing.Point(940, 34);
            this.acctVouchPrint.Name = "acctVouchPrint";
            this.acctVouchPrint.Size = new System.Drawing.Size(75, 23);
            this.acctVouchPrint.TabIndex = 10;
            this.acctVouchPrint.Text = "打印";
            // 
            // acctVouchPrint2
            // 
            this.acctVouchPrint2.Location = new System.Drawing.Point(1041, 34);
            this.acctVouchPrint2.Name = "acctVouchPrint2";
            this.acctVouchPrint2.Size = new System.Drawing.Size(75, 23);
            this.acctVouchPrint2.TabIndex = 9;
            this.acctVouchPrint2.Text = "打印设置";
            // 
            // acctItemcNewCode_select
            // 
            this.acctItemcNewCode_select.Location = new System.Drawing.Point(852, 33);
            this.acctItemcNewCode_select.Name = "acctItemcNewCode_select";
            this.acctItemcNewCode_select.Size = new System.Drawing.Size(75, 23);
            this.acctItemcNewCode_select.TabIndex = 8;
            this.acctItemcNewCode_select.Text = "查询";
            this.acctItemcNewCode_select.Click += new System.EventHandler(this.acctItemcNewCode_select_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(745, 9);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(84, 14);
            this.labelControl4.TabIndex = 7;
            this.labelControl4.Text = "会计科目类别：";
            // 
            // subj_type_code
            // 
            this.subj_type_code.Location = new System.Drawing.Point(852, 7);
            this.subj_type_code.Name = "subj_type_code";
            this.subj_type_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.subj_type_code.Size = new System.Drawing.Size(100, 20);
            this.subj_type_code.TabIndex = 6;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(502, 7);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(84, 14);
            this.labelControl3.TabIndex = 5;
            this.labelControl3.Text = "会计核算类别：";
            // 
            // check_method
            // 
            this.check_method.Location = new System.Drawing.Point(608, 5);
            this.check_method.Name = "check_method";
            this.check_method.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.check_method.Size = new System.Drawing.Size(100, 20);
            this.check_method.TabIndex = 4;
            // 
            // acct_subj_name
            // 
            this.acct_subj_name.Location = new System.Drawing.Point(367, 6);
            this.acct_subj_name.Name = "acct_subj_name";
            this.acct_subj_name.Size = new System.Drawing.Size(129, 20);
            this.acct_subj_name.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(257, 7);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(84, 14);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "会计科目名称：";
            // 
            // acct_subj_code
            // 
            this.acct_subj_code.Location = new System.Drawing.Point(111, 6);
            this.acct_subj_code.Name = "acct_subj_code";
            this.acct_subj_code.Size = new System.Drawing.Size(129, 20);
            this.acct_subj_code.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 7);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(84, 14);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "会计科目编码：";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton5);
            this.panelControl1.Controls.Add(this.simpleButton4);
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 57);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1142, 40);
            this.panelControl1.TabIndex = 1;
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(367, 11);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(68, 23);
            this.simpleButton5.TabIndex = 15;
            this.simpleButton5.Text = "删除";
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(273, 11);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(68, 23);
            this.simpleButton4.TabIndex = 14;
            this.simpleButton4.Text = "导入";
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(185, 11);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(68, 23);
            this.simpleButton3.TabIndex = 13;
            this.simpleButton3.Text = "检查";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(95, 11);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(68, 23);
            this.simpleButton2.TabIndex = 12;
            this.simpleButton2.Text = "复制";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(12, 11);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(68, 23);
            this.simpleButton1.TabIndex = 11;
            this.simpleButton1.Text = "添加";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 97);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1142, 404);
            this.gridControl1.TabIndex = 2;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.acct_subj,
            this.acct_name,
            this.acct_subj_name_all,
            this.subj_type_name,
            this.check_method2,
            this.subj_nature_name,
            this.contact_type,
            this.direction,
            this.is_cash,
            this.is_check,
            this.check_type1,
            this.check_type2,
            this.check_type3,
            this.check_type4,
            this.check_type5,
            this.check_type6,
            this.check_type7,
            this.check_type8,
            this.is_budge});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "选择";
            this.gridColumn2.FieldName = "checked";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // acct_subj
            // 
            this.acct_subj.Caption = "编码";
            this.acct_subj.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.acct_subj.FieldName = "acct_subj_code";
            this.acct_subj.Name = "acct_subj";
            this.acct_subj.Visible = true;
            this.acct_subj.VisibleIndex = 1;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit1_Click);
            // 
            // acct_name
            // 
            this.acct_name.Caption = "名称";
            this.acct_name.FieldName = "acct_subj_name";
            this.acct_name.Name = "acct_name";
            this.acct_name.Visible = true;
            this.acct_name.VisibleIndex = 2;
            // 
            // acct_subj_name_all
            // 
            this.acct_subj_name_all.Caption = "全称";
            this.acct_subj_name_all.FieldName = "acct_subj_name_all";
            this.acct_subj_name_all.Name = "acct_subj_name_all";
            this.acct_subj_name_all.Visible = true;
            this.acct_subj_name_all.VisibleIndex = 3;
            // 
            // subj_type_name
            // 
            this.subj_type_name.Caption = "类别";
            this.subj_type_name.FieldName = "subj_type_name";
            this.subj_type_name.Name = "subj_type_name";
            this.subj_type_name.Visible = true;
            this.subj_type_name.VisibleIndex = 4;
            // 
            // check_method2
            // 
            this.check_method2.Caption = "会计核算类别";
            this.check_method2.FieldName = "Column1";
            this.check_method2.Name = "check_method2";
            this.check_method2.Visible = true;
            this.check_method2.VisibleIndex = 5;
            // 
            // subj_nature_name
            // 
            this.subj_nature_name.Caption = "性质";
            this.subj_nature_name.FieldName = "subj_nature_name";
            this.subj_nature_name.Name = "subj_nature_name";
            this.subj_nature_name.Visible = true;
            this.subj_nature_name.VisibleIndex = 6;
            // 
            // contact_type
            // 
            this.contact_type.Caption = "往来类别";
            this.contact_type.FieldName = "Column2";
            this.contact_type.Name = "contact_type";
            this.contact_type.Visible = true;
            this.contact_type.VisibleIndex = 7;
            // 
            // direction
            // 
            this.direction.Caption = "方向";
            this.direction.FieldName = "direction";
            this.direction.Name = "direction";
            this.direction.Visible = true;
            this.direction.VisibleIndex = 8;
            // 
            // is_cash
            // 
            this.is_cash.Caption = "现金流量";
            this.is_cash.FieldName = "is_cash";
            this.is_cash.Name = "is_cash";
            this.is_cash.Visible = true;
            this.is_cash.VisibleIndex = 9;
            // 
            // is_check
            // 
            this.is_check.Caption = "是否";
            this.is_check.FieldName = "is_check";
            this.is_check.Name = "is_check";
            this.is_check.Visible = true;
            this.is_check.VisibleIndex = 10;
            // 
            // check_type1
            // 
            this.check_type1.Caption = "核算1";
            this.check_type1.FieldName = "check_type1";
            this.check_type1.Name = "check_type1";
            this.check_type1.Visible = true;
            this.check_type1.VisibleIndex = 11;
            // 
            // check_type2
            // 
            this.check_type2.Caption = "核算2";
            this.check_type2.FieldName = "check_type2";
            this.check_type2.Name = "check_type2";
            this.check_type2.Visible = true;
            this.check_type2.VisibleIndex = 12;
            // 
            // check_type3
            // 
            this.check_type3.Caption = "核算3";
            this.check_type3.FieldName = "check_type3";
            this.check_type3.Name = "check_type3";
            this.check_type3.Visible = true;
            this.check_type3.VisibleIndex = 13;
            // 
            // check_type4
            // 
            this.check_type4.Caption = "核算4";
            this.check_type4.FieldName = "check_type4";
            this.check_type4.Name = "check_type4";
            this.check_type4.Visible = true;
            this.check_type4.VisibleIndex = 14;
            // 
            // check_type5
            // 
            this.check_type5.Caption = "核算5";
            this.check_type5.FieldName = "check_type5";
            this.check_type5.Name = "check_type5";
            this.check_type5.Visible = true;
            this.check_type5.VisibleIndex = 15;
            // 
            // check_type6
            // 
            this.check_type6.Caption = "核算6";
            this.check_type6.FieldName = "check_type6";
            this.check_type6.Name = "check_type6";
            this.check_type6.Visible = true;
            this.check_type6.VisibleIndex = 16;
            // 
            // check_type7
            // 
            this.check_type7.Caption = "核算7";
            this.check_type7.FieldName = "check_type7";
            this.check_type7.Name = "check_type7";
            this.check_type7.Visible = true;
            this.check_type7.VisibleIndex = 17;
            // 
            // check_type8
            // 
            this.check_type8.Caption = "核算8";
            this.check_type8.FieldName = "check_type8";
            this.check_type8.Name = "check_type8";
            this.check_type8.Visible = true;
            this.check_type8.VisibleIndex = 18;
            // 
            // is_budge
            // 
            this.is_budge.Caption = "预算标志";
            this.is_budge.FieldName = "check_type9";
            this.is_budge.Name = "is_budge";
            this.is_budge.Visible = true;
            this.is_budge.VisibleIndex = 19;
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1142, 501);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.查询条件);
            this.Name = "main";
            this.Text = "会计科目";
            this.Load += new System.EventHandler(this.main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.查询条件)).EndInit();
            this.查询条件.ResumeLayout(false);
            this.查询条件.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.subj_type_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_method.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl 查询条件;
        private DevExpress.XtraEditors.TextEdit acct_subj_code;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit acct_subj_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.ComboBoxEdit check_method;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.ComboBoxEdit subj_type_code;
        private DevExpress.XtraEditors.SimpleButton acctItemcNewCode_select;
        private DevExpress.XtraEditors.SimpleButton acctVouchPrint;
        private DevExpress.XtraEditors.SimpleButton acctVouchPrint2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn acct_subj;
        private DevExpress.XtraGrid.Columns.GridColumn acct_name;
        private DevExpress.XtraGrid.Columns.GridColumn acct_subj_name_all;
        private DevExpress.XtraGrid.Columns.GridColumn subj_type_name;
        private DevExpress.XtraGrid.Columns.GridColumn check_method2;
        private DevExpress.XtraGrid.Columns.GridColumn subj_nature_name;
        private DevExpress.XtraGrid.Columns.GridColumn contact_type;
        private DevExpress.XtraGrid.Columns.GridColumn direction;
        private DevExpress.XtraGrid.Columns.GridColumn is_cash;
        private DevExpress.XtraGrid.Columns.GridColumn is_check;
        private DevExpress.XtraGrid.Columns.GridColumn check_type1;
        private DevExpress.XtraGrid.Columns.GridColumn check_type2;
        private DevExpress.XtraGrid.Columns.GridColumn check_type3;
        private DevExpress.XtraGrid.Columns.GridColumn check_type4;
        private DevExpress.XtraGrid.Columns.GridColumn check_type5;
        private DevExpress.XtraGrid.Columns.GridColumn check_type6;
        private DevExpress.XtraGrid.Columns.GridColumn check_type7;
        private DevExpress.XtraGrid.Columns.GridColumn check_type8;
        private DevExpress.XtraGrid.Columns.GridColumn is_budge;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}