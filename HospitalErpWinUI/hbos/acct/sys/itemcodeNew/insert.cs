﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using Dao;
using HospitalErpWinUI.WinFormUiHelper;

namespace HospitalErpWinUI.hbos.acct.sys.itemcodeNew
{
    public partial class insert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        public insert()
        {
            InitializeComponent();
            _helper = new DaoHelper();
        }

        private void insert_Load(object sender, EventArgs e)
        {
            // labelControl32.ReadOnly = true;
            try
            {
                is_fc_string.ReadOnly = true;
                getSuper_code_();//上级科目
                getCheck_method();//会计核算类别
                check_method.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }

        }
        //会计核算类别
        private void getCheck_method() {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_check_method_type";
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            check_method.Properties.Items.AddRange(objs);
        }
        private void super_code__SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        public insert(Dao.DaoHelper helper, List<Perm> perms)
        {
            InitializeComponent();
            this._helper = helper;
            this._perms = perms;
            Init();
            getkmbmgz();
        }
        //初始化页面
        private void Init()
        {

        }
        //获取科目编码规则
        private void getkmbmgz()
        {
            //sysNextLevelDba_select
             ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_itemcode_getaccrule";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            this.labelControl32.Text = dt.Rows[0][0].ToString();
        }
        private void getSuper_code_()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_subj_specialName";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("acct_year", G_User.user.Acct_year);
            paramters.Add("key", FormHelper.GetValueForSearchLookUpEdit(super_code_).Trim());
            serviceParamter.Paramters = paramters;

            List<CodeValue> objs = new List<CodeValue>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue);
            }
            //    emp_code.Properties.Items.AddRange(emps);
            super_code_.Properties.ValueMember = "Code";
            super_code_.Properties.DisplayMember = "Value";
            super_code_.Properties.DataSource = objs;
        }

        private void super_code__EditValueChanged(object sender, EventArgs e)
        {
            MessageBox.Show("点击");
            acct_subj_code.Text = FormHelper.GetValueForSearchLookUpEdit(super_code_).Trim();
            datachanged("super_code");  
        }
        private void datachanged(string colid)
        {
            string parentcode = "";
            
             switch (colid) {
            //    case "is_budge":
            //        if (is_budge_1.checked) {
            //            is_budge.value = "1";
            //        } else {
            //            is_budge.value = "0";
            //        }
            //        break;
                    
                 case "super_code":
                    if ((acct_subj_code.Text != null) && (acct_subj_code.Text != "")) {
                        set_fc.Visible = false;
                        parentcode = FormHelper.GetValueForSearchLookUpEdit(super_code_).Trim();
                        
                    //    parentcode = checkrulecodeformatbysql(acct_subj_code.Text + "00", true, "acct_itemcode_getaccrule", "<comp_code>" + comp_code.value + "</comp_code><copy_code>" + copy_code.value + "</copy_code>");
                        if (parentcode != null) {
                            string subtype = parentcode.Substring(0, 1);
                            if (subtype == "1" || subtype == "2" || subtype == "3" || subtype == "4" || subtype == "5") {
                                // check_method.setvalue("1");
                                FormHelper.ComboBoxeditAssignment(check_method, "1");//.ComboBoxeditAssignment(check_method,1);
                             }
                    //        else if (subtype = "6" || subtype == "7" || subtype == "8") {
                    //            check_method.setvalue("2");
                    //        }
                    //        else {
                    //            check_method.setvalue(" ");
                    //        }
                    //        setdata(parentcode);
                         }
                    //    else {
                    //        super_code.value = "null";
                    //        MessageBox.Show("数据输入不符合定义格式，请重新输入。")
                    //        acct_subj_code.Text = "";
                    //        set_fc.disabled = false;
                    //    }
                    //    naturechange();
                    }
                    //else {
                    //    set_fc.disabled = false;
                    //}
                    break;
            //    case "acct_subj_code":
            //        if ((acct_subj_code.Text != null) && (acct_subj_code.Text != "")) {
            //            set_fc.disabled = false;
            //            parentcode = checkRuleCodeFormatBySql(acct_subj_code.Text, true, "acct_itemcode_getaccrule", "<comp_code>" + comp_code.value + "</comp_code><copy_code>" + copy_code.value + "</copy_code>");
            //            if (parentcode != null) {
            //                var subType = parentcode.substring(0, 1);
            //                if (subType == "1" || subType == "2" || subType == "3" || subType == "4" || subType == "5") {
            //                    check_method.setValue("1");
            //                }
            //                else if (subType = "6" || subType == "7" || subType == "8") {
            //                    check_method.setValue("2");
            //                }
            //                else {
            //                    check_method.setValue(" ");
            //                }
            //                setdata(parentcode);
            //            }
            //            else {
            //                super_code.value = "null";
            //                MessageBox.Show("数据输入不符合定义格式，请重新输入。")
            //                acct_subj_code.Text = "";
            //                set_fc.disabled = false;
            //            }
            //            naturechange();
            //        }
            //        else {
            //            set_fc.disabled = false;
            //        }
            //        break;
            //    case "is_fc":
            //        if (is_fc_1.checked) {
            //            is_fc.value = "1";
            //        }
            //        else {
            //            is_fc.value = "0";
            //        }
            //        break;
            //    case "is_num":
            //        if (is_num_1.checked) {
            //            count_unit.setEnabled(true);
            //            is_num.value = "1";

            //        }
            //        else {
            //            count_unit.setEnabled(false);
            //            count_unit.setValue("");
            //            is_num.value = "0";
            //        }
            //        count_unit.refresh();
            //        break;
            //    case "is_check":
            //        if (is_check_1.checked) {
            //            check_type1.setEnabled(true);
            //            check_type1.refresh();
            //            is_check.value = "1";
            //        }
            //        else {
            //            check_type1.setValue("");
            //            check_type2.setValue("");
            //            check_type3.setValue("");
            //            check_type4.setValue("");
            //            check_type5.setValue("");
            //            check_type6.setValue("");
            //            check_type7.setValue("");
            //            check_type8.setValue("");
            //            check_type1.setEnabled(false);
            //            check_type2.setEnabled(false);
            //            check_type3.setEnabled(false);
            //            check_type4.setEnabled(false);
            //            check_type5.setEnabled(false);
            //            check_type6.setEnabled(false);
            //            check_type7.setEnabled(false);
            //            check_type8.setEnabled(false);
            //            is_check.value = "0";
            //        }
            //        break;
            //    case "is_cash":
            //        if (is_cash_1.checked) {
            //            is_cash.value = "1";
            //        }
            //        else {
            //            is_cash.value = "0";
            //        }
            //        break;
            //    case "check_type1":
            //        if ((check_type1.value != null) && (check_type1.value != "")) {
            //            check_type2.setEnabled(true);
            //        }
            //        else {
            //            check_type2.setEnabled(false);
            //            check_type2.setValue("");
            //        }
            //    case "check_type2":
            //        if ((check_type2.value != null) && (check_type2.value != "")) {
            //            check_type3.setEnabled(true);			
            //        }
            //        else {
            //            check_type3.setEnabled(false);
            //            check_type3.setValue("");
            //        }
            //    case "check_type3":
            //        if ((check_type3.value != null) && (check_type3.value != "")) {
            //            check_type4.setEnabled(true);
            //        }
            //        else {
            //            check_type4.setEnabled(false);
            //            check_type4.setValue("");
            //        }
            //    case "check_type4":
            //        if ((check_type4.value != null) && (check_type4.value != "")) {
            //            check_type5.setEnabled(true);				
            //        }
            //        else {
            //            check_type5.setEnabled(false);
            //            check_type5.setValue("");
            //        }
            //    case "check_type5":
            //        if ((check_type5.value != null) && (check_type5.value != "")) {
            //            check_type6.setEnabled(true);			
            //        }
            //        else {
            //            check_type6.setEnabled(false);
            //            check_type6.setValue("");
            //        }
            //    case "check_type6":
            //        if ((check_type6.value != null) && (check_type6.value != "")) {
            //            check_type7.setEnabled(true);			
            //        }
            //        else {
            //            check_type7.setEnabled(false);
            //            check_type7.setValue("");
            //        }
            //    case "check_type7":
            //        if ((check_type7.value != null) && (check_type7.value != "")) {
            //            check_type8.setEnabled(true);				
            //        }
            //        else {
            //            check_type8.setEnabled(false);
            //            check_type8.setValue("");
            //        }
            //        break;
            //    case "subj_type_code":
            //        if (subj_type_code.value == "01" || subj_type_code.value == "02" || subj_type_code.value == "03" || subj_type_code.value == "04" || subj_type_code.value == "05") {
            //            check_method.setValue("1");
            //        }
            //        else if (subj_type_code.value == "06" || subj_type_code.value == "07" || subj_type_code.value == "08") {
            //            check_method.setValue("2");
            //        }
            //        if (subj_type_code.value == "01") {
            //            is_cash_1.disabled = false;
            //        }
            //        else {
            //            is_cash_1.disabled = true;
            //            is_cash_1.checked = false;
            //            is_cash.value = "0";

            //        }
            //        setGroupKind();
            //        setGroupValue()
            //        break;
            }

        }
    }
}
