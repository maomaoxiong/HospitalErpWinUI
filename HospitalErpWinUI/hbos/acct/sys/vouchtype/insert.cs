﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.sys.vouchtype
{
    public partial class insert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        public insert()
        {
            InitializeComponent();
            _helper = new DaoHelper();
        }
        public insert(Dao.DaoHelper helper, List<Perm> perms)
        {
            InitializeComponent();
            this._helper = helper;
            this._perms = perms;
        }
        private bool Validate()
        {
            if (!Validator.IsInteger(acct_subj_code.Text))
            {
                MessageBox.Show("凭证类型编码请正确输入数字");
                return false;
            }
            if (string.IsNullOrEmpty(acct_subj_name.Text))
            {
                MessageForm.Warning("凭证类型名称不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(acct_subj_code.Text))
            {
                MessageForm.Warning("凭证类型编码不能为空!");
                return false;
            }
            return true;
        }
        private void acctsysvouchtype_insert_Click(object sender, EventArgs e)
        {
            if (!Validate()) {
                return;
            }
            try
            {
                Insert();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
        private bool Insert()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "acctsysvouchtype_insert";
            Dictionary<string, string> paramters = new Dictionary<string, string>();
  
            paramters.Add("acct_subj_code", acct_subj_code.Text);
            paramters.Add("acct_subj_name", acct_subj_name.Text);
            serviceParamter.Paramters = paramters;

            return _helper.WirteSql(serviceParamter);

        }
    }
}
