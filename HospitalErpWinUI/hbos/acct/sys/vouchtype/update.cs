﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.sys.vouchtype
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        private Validator validator;
        private List<Perm> _perms { get; set; }
 
        public update()
        {
            InitializeComponent();
        }
        public update(DaoHelper helper, List<Perm> perms, string acct_subj_code, string acct_subj_name, string _acct_subj_code)
        {
            _helper = helper;
            _perms = perms;
            validator = new Validator();
           
           
            InitializeComponent();
            this._acct_subj_code.Text = _acct_subj_code;
             this.acct_subj_code.Text = acct_subj_code;
             this.acct_subj_name.Text = acct_subj_name;
           // Init();

        }
        private bool Validate()
        {
            if (!Validator.IsInteger(acct_subj_code.Text))
            {
                MessageBox.Show("凭证类型编码请正确输入数字");
                return false;
            }

            if (string.IsNullOrEmpty(acct_subj_name.Text))
            {
                MessageForm.Warning("凭证类型名称不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(acct_subj_code.Text))
            {
                MessageForm.Warning("凭证类型编码不能为空!");
                return false;
            }
            return true;
        }
        private void acctsysvouchtype_update_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                u_update1();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
        private bool u_update1()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "acctsysvouchtype_update";
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            paramters.Add("_acct_subj_code", _acct_subj_code.Text);
            paramters.Add("acct_subj_code", acct_subj_code.Text);
            paramters.Add("acct_subj_name", acct_subj_name.Text);
            serviceParamter.Paramters = paramters;

            return _helper.WirteSql(serviceParamter);

        }
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

    }
}
