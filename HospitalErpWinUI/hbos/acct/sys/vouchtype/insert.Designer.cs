﻿namespace HospitalErpWinUI.hbos.acct.sys.vouchtype
{
    partial class insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.acct_subj_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.acct_subj_code = new DevExpress.XtraEditors.TextEdit();
            this.acctsysvouchtype_insert = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // acct_subj_name
            // 
            this.acct_subj_name.Location = new System.Drawing.Point(95, 79);
            this.acct_subj_name.Name = "acct_subj_name";
            this.acct_subj_name.Size = new System.Drawing.Size(147, 20);
            this.acct_subj_name.TabIndex = 7;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(5, 82);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(84, 14);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "凭证类型名称：";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(7, 41);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(84, 14);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "凭证类型编码：";
            // 
            // acct_subj_code
            // 
            this.acct_subj_code.Location = new System.Drawing.Point(95, 38);
            this.acct_subj_code.Name = "acct_subj_code";
            this.acct_subj_code.Size = new System.Drawing.Size(147, 20);
            this.acct_subj_code.TabIndex = 4;
            // 
            // acctsysvouchtype_insert
            // 
            this.acctsysvouchtype_insert.Location = new System.Drawing.Point(16, 138);
            this.acctsysvouchtype_insert.Name = "acctsysvouchtype_insert";
            this.acctsysvouchtype_insert.Size = new System.Drawing.Size(75, 23);
            this.acctsysvouchtype_insert.TabIndex = 8;
            this.acctsysvouchtype_insert.Text = "保存";
            this.acctsysvouchtype_insert.Click += new System.EventHandler(this.acctsysvouchtype_insert_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(133, 138);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 9;
            this.simpleButton2.Text = "关闭";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(301, 262);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.acctsysvouchtype_insert);
            this.Controls.Add(this.acct_subj_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.acct_subj_code);
            this.Name = "insert";
            this.Text = "insert";
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit acct_subj_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit acct_subj_code;
        private DevExpress.XtraEditors.SimpleButton acctsysvouchtype_insert;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
    }
}