﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.sys.check.checkclass
{
    public partial class main : RibbonForm
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public main()
        {
            InitializeComponent();
            _helper = new DaoHelper();
            Load_acct_sys_is_stop();//是否下拉框
            
           // this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
           // this.gridView1.OptionsSelection.EnableAppearanceFocusedRow = false;
            //this.gridView1.OptionsSelection.EnableAppearanceHideSelection = false;
           // this.gridview1.SelectedIndex = -1;
            //this.gridView1.selectedi
           // this.gridView1.ClearSelection();
        }
        private void Load_acct_sys_is_stop()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_sys_is_stop";
            
            serviceParamter.Paramters = paramters;
            List<string> objs = new List<string>();
            List<CodeValue> codeValues = _helper.ReadDictForData(serviceParamter);
            foreach (CodeValue codeValue in codeValues)
            {
                objs.Add(codeValue.Code + " " + codeValue.Value);
                repositoryItemComboBox1.Items.Add(codeValue.Code + " " + codeValue.Value);
              //  MessageBox.Show(codeValue.Code + " " + codeValue.Value);
            }
            is_stop.Properties.Items.AddRange(objs);


        }

        

        private void acctItemcCode_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void Query()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acctsyscheckclass_select";
            //，，，，，，，，
            paramters.Add("acct_subj_name", acct_subj_name.Text.ToString());
            paramters.Add("is_stop", FormHelper.GetValueForComboBoxEdit(is_stop));

            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            //  List<string> objs = Load_acct_cashdire();//方向 下拉
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
                if (dt.Rows[i]["Column1"].ToString() == "否")
                {
                    dt.Rows[i]["Column1"] = "0 否";
                }else{
                    dt.Rows[i]["Column1"] = "1 是";
                }
               // dt.Rows[i]["Column1"] = dt.Rows[i]["Column1"].ToString().Replace("|||", " ");
            }
            _user = dt;
            gridControl1.DataSource = _user;
        }
        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int selectedID = e.PrevFocusedRowHandle;//e.FocusedRowHandle;
            DataRow row = gridView1.GetDataRow(selectedID);
            if (row != null)
            {
                if (row["_vouch_type_code"].ToString() == null || row["_vouch_type_code"].ToString().Equals(""))
                {
                    if (!Validate(row))
                    {
                        return;
                    }
                    try
                    {
                        Insert(row);
                        this.DialogResult = System.Windows.Forms.DialogResult.OK;
                        //     MessageForm.Show("新增角色成功！");
                        //  this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageForm.Exception(ex.Message, "异常");
                    }
                }
                else
                {
                    if (!Validate(row))
                    {
                        return;
                    }
                    try
                    {
                        u_update1(row);
                        this.DialogResult = System.Windows.Forms.DialogResult.OK;
                        //     MessageForm.Show("新增角色成功！");
                        //  this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageForm.Exception(ex.Message, "异常");
                    }
                }

            }
        }
        private bool Validate(DataRow row)
        {
            //   MessageBox.Show(row["Column1"].ToString());
            if (string.IsNullOrEmpty(row["check_type"].ToString()))
            {
                MessageForm.Warning("核算类别名称不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(row["check_text"].ToString()))
            {
                MessageForm.Warning("核算类别描述不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(row["Column1"].ToString()))
            {
                MessageForm.Warning("是否停用不能为空!");
                return false;
            }
            return true;
        }
        private bool Insert(DataRow row)
        {

            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "acctsyscheckclass_insert";
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            paramters.Add("check_type", row["check_type"].ToString());
            paramters.Add("check_text", row["check_text"].ToString());
            paramters.Add("Column1", FormHelper.getSplit(row["Column1"].ToString()));
            serviceParamter.Paramters = paramters;

            return _helper.WirteSql(serviceParamter);

        }
        private bool u_update1(DataRow row)
        {
           // MessageBox.Show(row["Column1"].ToString());
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "acctsyscheckclass_update";
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            paramters.Add("Comp_code", G_User.user.Comp_code);
            paramters.Add("Copy_code", G_User.user.Copy_code);
            paramters.Add("_vouch_type_code", row["_vouch_type_code"].ToString());
            paramters.Add("check_type", row["check_type"].ToString());
            paramters.Add("check_text", row["check_text"].ToString());
            paramters.Add("Column1", FormHelper.getSplit(row["Column1"].ToString()));
            serviceParamter.Paramters = paramters;

            return _helper.WirteSql(serviceParamter);

        }

        private void acctsyscheckclass_delete_Click(object sender, EventArgs e)
        {
            bool value = false;
            string strSelected = string.Empty;

            try
            {
                List<string> objs = new List<string>();
                for (int i = 0; i < gridView1.RowCount-1; i++)
                {
                    value = Convert.ToBoolean(gridView1.GetDataRow(i)["checked"]);
                    if (value)
                    {
                        string code = gridView1.GetDataRow(i)["_vouch_type_code"].ToString();
                        objs.Add(code);

                    }
                }
                if (objs.Count == 0)
                {
                    MessageForm.Show("请选择要删除的数据!");
                    return;
                }
                if (MessageForm.ShowMsg("确认是否删除?", MsgType.YesNo) == DialogResult.Yes)
                {
                    foreach (string obj in objs)
                    {
                        if (!Delete(obj))
                        {
                            return;
                        }
                    }
                    //       MessageForm.Show("操作成功!");
                    Query();
                }

            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private bool Delete(string co_code)
        {

            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acctsyscheckclass_delete";

            paramters.Add("Comp_code", G_User.user.Comp_code);
            paramters.Add("Copy_code", G_User.user.Copy_code);
            paramters.Add("_vouch_type_code", co_code);
             
            serviceParamter.Paramters = paramters;
            bool result = false;
            try
            {
                _helper.WirteSql(serviceParamter);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }

    }
}
