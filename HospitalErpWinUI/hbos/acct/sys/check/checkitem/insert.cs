﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.sys.check.checkitem
{
    public partial class insert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        public insert()
        {
            InitializeComponent();
            _helper = new DaoHelper();
        }
        public insert(Dao.DaoHelper helper, List<Perm> perms)
        {
            InitializeComponent();
            this._helper = helper;
            this._perms = perms;
            Load_acct_checkclass();//核算类下拉框
            Load_acct_sys_is_stop();//是否下拉框
            Load_acctsuperselfitem_select();//上级编码
        }
        private void Load_acct_sys_is_stop()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_sys_is_stop";
            serviceParamter.Paramters = paramters;
            List<string> objs = new List<string>();
            List<CodeValue> codeValues = _helper.ReadDictForData(serviceParamter);
            foreach (CodeValue codeValue in codeValues)
            {
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            is_stop.Properties.Items.AddRange(objs);
        }
        private void Load_acctsuperselfitem_select()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acctsuperselfitem_select";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("Copy_code", G_User.user.Copy_code);
            paramters.Add("Acct_year", G_User.user.Acct_year);
            paramters.Add("item_code", item_code.Text.ToString());
            paramters.Add("item_type", FormHelper.getSplit(item_type.Text.ToString()));
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            super_code.Properties.Items.AddRange(objs);
            /// super_code.SelectedItem = objs[0];
        }
        private void Load_acct_checkclass()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_checkclass";
            paramters.Add("Comp_code", G_User.user.Comp_code);
            paramters.Add("Copy_code", G_User.user.Copy_code);
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            item_type.Properties.Items.AddRange(objs);
            if (objs.Count > 0)
            {
                item_type.SelectedItem = objs[0];
            }

        }

        private void acctsyscheckitem_insert_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Insert();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
        private bool Validate()
        {
             
            if (string.IsNullOrEmpty(item_code.Text))
            {
                MessageForm.Warning("核算项编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(item_name.Text))
            {
                MessageForm.Warning("核算项名称不能为空!");
                return false;
            }
            return true;
        }
        private bool Insert()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "acctsyscheckitem_insert";
            Dictionary<string, string> paramters = new Dictionary<string, string>();

            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("Copy_code", G_User.user.Copy_code);
            paramters.Add("Acct_year", G_User.user.Acct_year);
            
            paramters.Add("item_type", FormHelper.getSplit(item_type.Text.ToString()));
            paramters.Add("item_code", item_code.Text.ToString());
            paramters.Add("item_name", item_name.Text.ToString());
            paramters.Add("super_code", FormHelper.GetValueForComboBoxEdit(super_code));
            paramters.Add("is_stop", FormHelper.GetValueForComboBoxEdit(is_stop));

            serviceParamter.Paramters = paramters;

            return _helper.WirteSql(serviceParamter);

        }

        private void insert_Load(object sender, EventArgs e)
        {

        }

    }
}
