﻿namespace HospitalErpWinUI.hbos.acct.sys.check.checkitem
{
    partial class insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.acctsyscheckitem_insert = new DevExpress.XtraEditors.SimpleButton();
            this.super_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.is_stop = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.item_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.item_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.item_type = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.super_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_stop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item_type.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(204, 266);
            this.simpleButton2.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(100, 29);
            this.simpleButton2.TabIndex = 6;
            this.simpleButton2.Text = "关闭";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // acctsyscheckitem_insert
            // 
            this.acctsyscheckitem_insert.Location = new System.Drawing.Point(48, 266);
            this.acctsyscheckitem_insert.Margin = new System.Windows.Forms.Padding(4);
            this.acctsyscheckitem_insert.Name = "acctsyscheckitem_insert";
            this.acctsyscheckitem_insert.Size = new System.Drawing.Size(100, 29);
            this.acctsyscheckitem_insert.TabIndex = 5;
            this.acctsyscheckitem_insert.Text = "保存";
            this.acctsyscheckitem_insert.Click += new System.EventHandler(this.acctsyscheckitem_insert_Click);
            // 
            // super_code
            // 
            this.super_code.Location = new System.Drawing.Point(136, 165);
            this.super_code.Margin = new System.Windows.Forms.Padding(4);
            this.super_code.Name = "super_code";
            this.super_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.super_code.Size = new System.Drawing.Size(196, 24);
            this.super_code.TabIndex = 3;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(52, 169);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(75, 18);
            this.labelControl6.TabIndex = 95;
            this.labelControl6.Text = "上级编码：";
            // 
            // is_stop
            // 
            this.is_stop.Location = new System.Drawing.Point(136, 208);
            this.is_stop.Margin = new System.Windows.Forms.Padding(4);
            this.is_stop.Name = "is_stop";
            this.is_stop.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_stop.Size = new System.Drawing.Size(196, 24);
            this.is_stop.TabIndex = 4;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(53, 211);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(75, 18);
            this.labelControl5.TabIndex = 93;
            this.labelControl5.Text = "是否停用：";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(52, 125);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(75, 18);
            this.labelControl4.TabIndex = 92;
            this.labelControl4.Text = "核算名称：";
            // 
            // item_name
            // 
            this.item_name.Location = new System.Drawing.Point(136, 121);
            this.item_name.Margin = new System.Windows.Forms.Padding(4);
            this.item_name.Name = "item_name";
            this.item_name.Size = new System.Drawing.Size(196, 24);
            this.item_name.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(52, 81);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(75, 18);
            this.labelControl2.TabIndex = 90;
            this.labelControl2.Text = "核算编码：";
            // 
            // item_code
            // 
            this.item_code.Location = new System.Drawing.Point(136, 78);
            this.item_code.Margin = new System.Windows.Forms.Padding(4);
            this.item_code.Name = "item_code";
            this.item_code.Size = new System.Drawing.Size(196, 24);
            this.item_code.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(67, 36);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 18);
            this.labelControl1.TabIndex = 88;
            this.labelControl1.Text = "核算类：";
            // 
            // item_type
            // 
            this.item_type.Location = new System.Drawing.Point(136, 32);
            this.item_type.Margin = new System.Windows.Forms.Padding(4);
            this.item_type.Name = "item_type";
            this.item_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.item_type.Size = new System.Drawing.Size(196, 24);
            this.item_type.TabIndex = 0;
            // 
            // insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 328);
            this.Controls.Add(this.item_type);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.acctsyscheckitem_insert);
            this.Controls.Add(this.super_code);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.is_stop);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.item_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.item_code);
            this.Controls.Add(this.labelControl1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "insert";
            this.Text = "添加";
            this.Load += new System.EventHandler(this.insert_Load);
            ((System.ComponentModel.ISupportInitialize)(this.super_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_stop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item_type.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton acctsyscheckitem_insert;
        private DevExpress.XtraEditors.ComboBoxEdit super_code;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.ComboBoxEdit is_stop;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit item_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit item_code;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit item_type;
    }
}