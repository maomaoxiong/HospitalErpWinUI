﻿namespace HospitalErpWinUI.hbos.acct.sys.check.checkitem
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.item_type = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.item_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.olditem_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.item_name = new DevExpress.XtraEditors.TextEdit();
            this.is_stop = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.super_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.acctsyscheckitem_update = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.item_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.olditem_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_stop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.super_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(50, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(48, 14);
            this.labelControl1.TabIndex = 19;
            this.labelControl1.Text = "核算类：";
            // 
            // item_type
            // 
            this.item_type.Enabled = false;
            this.item_type.Location = new System.Drawing.Point(102, 24);
            this.item_type.Name = "item_type";
            this.item_type.Size = new System.Drawing.Size(147, 20);
            this.item_type.TabIndex = 18;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(39, 63);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 21;
            this.labelControl2.Text = "核算编码：";
            // 
            // item_code
            // 
            this.item_code.Location = new System.Drawing.Point(102, 60);
            this.item_code.Name = "item_code";
            this.item_code.Size = new System.Drawing.Size(147, 20);
            this.item_code.TabIndex = 20;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(39, 314);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(72, 14);
            this.labelControl3.TabIndex = 23;
            this.labelControl3.Text = "老核算编码：";
            this.labelControl3.Visible = false;
            // 
            // olditem_code
            // 
            this.olditem_code.Enabled = false;
            this.olditem_code.Location = new System.Drawing.Point(102, 311);
            this.olditem_code.Name = "olditem_code";
            this.olditem_code.Size = new System.Drawing.Size(147, 20);
            this.olditem_code.TabIndex = 22;
            this.olditem_code.Visible = false;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(39, 98);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 14);
            this.labelControl4.TabIndex = 25;
            this.labelControl4.Text = "核算名称：";
            // 
            // item_name
            // 
            this.item_name.Location = new System.Drawing.Point(102, 95);
            this.item_name.Name = "item_name";
            this.item_name.Size = new System.Drawing.Size(147, 20);
            this.item_name.TabIndex = 24;
            // 
            // is_stop
            // 
            this.is_stop.Location = new System.Drawing.Point(102, 164);
            this.is_stop.Name = "is_stop";
            this.is_stop.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_stop.Size = new System.Drawing.Size(147, 20);
            this.is_stop.TabIndex = 82;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(40, 167);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 14);
            this.labelControl5.TabIndex = 81;
            this.labelControl5.Text = "是否停用：";
            // 
            // super_code
            // 
            this.super_code.Enabled = false;
            this.super_code.Location = new System.Drawing.Point(102, 130);
            this.super_code.Name = "super_code";
            this.super_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.super_code.Size = new System.Drawing.Size(147, 20);
            this.super_code.TabIndex = 84;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(39, 133);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(60, 14);
            this.labelControl6.TabIndex = 83;
            this.labelControl6.Text = "上级编码：";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(153, 211);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 86;
            this.simpleButton2.Text = "关闭";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // acctsyscheckitem_update
            // 
            this.acctsyscheckitem_update.Location = new System.Drawing.Point(36, 211);
            this.acctsyscheckitem_update.Name = "acctsyscheckitem_update";
            this.acctsyscheckitem_update.Size = new System.Drawing.Size(75, 23);
            this.acctsyscheckitem_update.TabIndex = 85;
            this.acctsyscheckitem_update.Text = "保存";
            this.acctsyscheckitem_update.Click += new System.EventHandler(this.acctsyscheckitem_update_Click);
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(306, 343);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.acctsyscheckitem_update);
            this.Controls.Add(this.super_code);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.is_stop);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.item_name);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.olditem_code);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.item_code);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.item_type);
            this.Name = "update";
            this.Text = "修改";
            ((System.ComponentModel.ISupportInitialize)(this.item_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.olditem_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_stop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.super_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit item_type;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit item_code;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit olditem_code;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit item_name;
        private DevExpress.XtraEditors.ComboBoxEdit is_stop;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.ComboBoxEdit super_code;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton acctsyscheckitem_update;
    }
}