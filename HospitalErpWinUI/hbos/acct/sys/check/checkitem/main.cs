﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraGrid;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.sys.check.checkitem
{
    public partial class main : RibbonForm
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public main()
        {
            InitializeComponent();
            _helper = new DaoHelper();
            Load_acct_checkclass();//核算类下拉框
        }
        private void Load_acct_checkclass()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_checkclass";
            paramters.Add("Comp_code", G_User.user.Comp_code);
            paramters.Add("Copy_code", G_User.user.Copy_code);
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            acct_subj_code.Properties.Items.AddRange(objs);
            if (objs.Count > 0)
            {
                acct_subj_code.SelectedItem = objs[0];
            }
            
        }

        private void acctsyscheckitem_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void Query()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acctsyscheckitem_select";
            //，，，，，，，，

            paramters.Add("acct_subj_code", FormHelper.GetValueForComboBoxEdit(acct_subj_code));
            paramters.Add("acct_subj_name", acct_subj_name.Text.ToString());
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            //  List<string> objs = Load_acct_cashdire();//方向 下拉
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
                if (dt.Rows[i]["Column2"].ToString() == "0|||否")
                {
                    dt.Rows[i]["Column2"] = "0 否";
                }
                else
                {
                    dt.Rows[i]["Column2"] = "1 是";
                }
                // dt.Rows[i]["Column1"] = dt.Rows[i]["Column1"].ToString().Replace("|||", " ");
            }
            _user = dt;
            gridControl1.DataSource = _user;
        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
           
        }

        private void repositoryItemHyperLinkEdit1_Click_1(object sender, EventArgs e)
        {
          //  MessageBox.Show("111");
            if (gridView1.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                int index = gridView1.GetDataSourceRowIndex(gridView1.FocusedRowHandle);
                string item_code = _user.Rows[index]["item_code"].ToString();
                string item_name = _user.Rows[index]["item_name"].ToString();
                string Column1 = _user.Rows[index]["Column1"].ToString();
                string super_code = _user.Rows[index]["super_code"].ToString();
                string Column2 = _user.Rows[index]["Column2"].ToString();

                update frm = new update(this._helper, this._perms, acct_subj_code.Text.ToString(), item_code, item_name, super_code, Column2);

                if (frm.ShowDialog() == DialogResult.No)
                {
                    MessageForm.Warning("修改失败！");
                }
                Query();
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                insert insertFrm = new insert(this._helper, this._perms);

                if (insertFrm.ShowDialog() == DialogResult.OK)
                {
                    Query();
                }
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
    }
}
