﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.sys.check.checkitem
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        private Validator validator;
        private List<Perm> _perms { get; set; }
        public update()
        {
            InitializeComponent();
        }
        public update(DaoHelper helper, List<Perm> perms, string item_type, string item_code, string item_name, string super_cod1e, string Column2)
        {

            InitializeComponent();
            _helper = helper;
            _perms = perms;
            validator = new Validator();
            Load_acct_sys_is_stop();//是否下拉框
            Load_acctsuperselfitem_select();//上级编码
            this.item_type.Text = item_type;
            this.item_code.Text = item_code;
            this.olditem_code.Text = item_code;
            this.item_name.Text = item_name;
            FormHelper.ComboBoxeditAssignment(super_code, super_cod1e);
          //  MessageBox.Show(Column2);
            FormHelper.ComboBoxeditAssignment(is_stop, FormHelper.getSplit(Column2.ToString()));
        }
        private void Load_acct_sys_is_stop()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_sys_is_stop";
            serviceParamter.Paramters = paramters;
            List<string> objs = new List<string>();
            List<CodeValue> codeValues = _helper.ReadDictForData(serviceParamter);
            foreach (CodeValue codeValue in codeValues)
            {
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            is_stop.Properties.Items.AddRange(objs);
        }
        private void Load_acctsuperselfitem_select()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acctsuperselfitem_select";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("Copy_code", G_User.user.Copy_code);
            paramters.Add("Acct_year", G_User.user.Acct_year);
            paramters.Add("item_code", item_code.Text.ToString());
            paramters.Add("item_type", FormHelper.getSplit(item_type.Text.ToString()));
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            super_code.Properties.Items.AddRange(objs);
           /// super_code.SelectedItem = objs[0];
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void acctsyscheckitem_update_Click(object sender, EventArgs e)
        {
            try
            {
                u_update1();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
        private bool u_update1()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "acctsyscheckitem_update";
            Dictionary<string, string> paramters = new Dictionary<string, string>();

            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("Copy_code", G_User.user.Copy_code);
            paramters.Add("Acct_year", G_User.user.Acct_year);
            paramters.Add("olditem_code", olditem_code.Text.ToString());
            paramters.Add("item_type", FormHelper.getSplit(item_type.Text.ToString()));
            paramters.Add("item_code", item_code.Text.ToString());
            paramters.Add("item_name", item_name.Text.ToString());
            paramters.Add("super_code", FormHelper.GetValueForComboBoxEdit(super_code));
            paramters.Add("is_stop", FormHelper.GetValueForComboBoxEdit(is_stop));
             
            serviceParamter.Paramters = paramters;

            return _helper.WirteSql(serviceParamter);

        }
    }
}
