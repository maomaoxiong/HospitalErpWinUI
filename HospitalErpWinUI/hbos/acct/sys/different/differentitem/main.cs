﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.sys.different.differentitem
{
    public partial class main : RibbonForm
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public main()
        {
            InitializeComponent();
            _helper = new DaoHelper();
            Load_acct_sys_is_stop();//是否下拉框
            Load_acct_diffclass();//差异类别下拉框
        }
        private void Load_acct_sys_is_stop()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_sys_is_stop";
            serviceParamter.Paramters = paramters;
            List<string> objs = new List<string>();
            List<CodeValue> codeValues = _helper.ReadDictForData(serviceParamter);
            foreach (CodeValue codeValue in codeValues)
            {
                objs.Add(codeValue.Code + " " + codeValue.Value);
                repositoryItemComboBox1.Items.Add(codeValue.Code + " " + codeValue.Value);
                repositoryItemComboBox2.Items.Add(codeValue.Code + " " + codeValue.Value);
            }
            is_stop.Properties.Items.AddRange(objs);
        }
        private void Load_acct_diffclass()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_diffclass";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("copy_code", G_User.user.Copy_code);
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add( codeValue.Value);
            }
            diff_type_code.Properties.Items.AddRange(objs);
            if (objs.Count > 0)
            {
                diff_type_code.SelectedItem = objs[0];
            }

        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {

        }

        private void acctsysdiffitem_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void Query()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acctsysdiffitem_select";
            paramters.Add("diff_type_code", FormHelper.GetValueForComboBoxEdit(diff_type_code));
            paramters.Add("is_stop", FormHelper.GetValueForComboBoxEdit(is_stop));
            paramters.Add("diff_item_name", diff_item_name.Text.ToString());
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
                if (dt.Rows[i]["Column3"].ToString() == "否")
                {
                    dt.Rows[i]["Column3"] = "0 否";
                }
                else
                {
                    dt.Rows[i]["Column3"] = "1 是";
                }
                if (dt.Rows[i]["Column4"].ToString() == "否")
                {
                    dt.Rows[i]["Column4"] = "0 否";
                }
                else
                {
                    dt.Rows[i]["Column4"] = "1 是";
                }
                // dt.Rows[i]["Column1"] = dt.Rows[i]["Column1"].ToString().Replace("|||", " ");
            }
            _user = dt;
            gridControl1.DataSource = _user;
        }

        private void gridView1_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
              GridView view = sender as GridView;

            //view.SetRowCellValue(e.RowHandle, view.Columns["具体列名"], 默认值);

            //如下例子：
             // view.
            //view.SetRowCellValue(e.RowHandle, view.Columns["sex"], "男");
 
        }
        //凭证中财务会计科目
        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {

        }

    }
}
