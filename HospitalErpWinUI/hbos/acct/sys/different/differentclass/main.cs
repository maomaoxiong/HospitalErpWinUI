﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.sys.different.differentclass
{
    public partial class main : RibbonForm
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public main()
        {
            InitializeComponent();
            _helper = new DaoHelper();
            Load_acct_sys_add_reduce();//是否下拉框
        }
        private void Load_acct_sys_add_reduce()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_sys_add_reduce";
            serviceParamter.Paramters = paramters;
            List<string> objs = new List<string>();
            List<CodeValue> codeValues = _helper.ReadDictForData(serviceParamter);
            foreach (CodeValue codeValue in codeValues)
            {
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            direction.Properties.Items.AddRange(objs);


        }
        private void acctItemcCode_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void Query()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acctsysdiffclass_select";
            paramters.Add("diff_type_code", diff_type_code.Text.ToString());
            paramters.Add("direction", FormHelper.GetValueForComboBoxEdit(direction));
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            _user = dt;
            gridControl1.DataSource = _user;
        }

        private void labelControl1_Click(object sender, EventArgs e)
        {

        }

        private void diff_type_code_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void labelControl3_Click(object sender, EventArgs e)
        {

        }

        private void direction_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
