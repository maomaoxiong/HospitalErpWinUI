﻿namespace HospitalErpWinUI.hbos.acct.sys.cash.cashkind
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.acctsyscashkind_delete = new DevExpress.XtraEditors.SimpleButton();
            this.acctItemcCode_select = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.acct_subj_name = new DevExpress.XtraEditors.TextEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this._vouch_type_code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cash_item_type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cash_item_name = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.acctsyscashkind_delete);
            this.panelControl1.Controls.Add(this.acctItemcCode_select);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.acct_subj_name);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(791, 52);
            this.panelControl1.TabIndex = 0;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(506, 12);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 8;
            this.simpleButton1.Text = "添加";
            // 
            // acctsyscashkind_delete
            // 
            this.acctsyscashkind_delete.Location = new System.Drawing.Point(608, 12);
            this.acctsyscashkind_delete.Name = "acctsyscashkind_delete";
            this.acctsyscashkind_delete.Size = new System.Drawing.Size(75, 23);
            this.acctsyscashkind_delete.TabIndex = 7;
            this.acctsyscashkind_delete.Text = "删除";
            this.acctsyscashkind_delete.Click += new System.EventHandler(this.acctsyscashkind_delete_Click);
            // 
            // acctItemcCode_select
            // 
            this.acctItemcCode_select.Location = new System.Drawing.Point(401, 11);
            this.acctItemcCode_select.Name = "acctItemcCode_select";
            this.acctItemcCode_select.Size = new System.Drawing.Size(75, 23);
            this.acctItemcCode_select.TabIndex = 6;
            this.acctItemcCode_select.Text = "查询";
            this.acctItemcCode_select.Click += new System.EventHandler(this.acctItemcCode_select_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(25, 15);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "类别名称：";
            // 
            // acct_subj_name
            // 
            this.acct_subj_name.Location = new System.Drawing.Point(140, 12);
            this.acct_subj_name.Name = "acct_subj_name";
            this.acct_subj_name.Size = new System.Drawing.Size(219, 20);
            this.acct_subj_name.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 52);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(791, 243);
            this.panelControl2.TabIndex = 1;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(787, 239);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this._vouch_type_code,
            this.cash_item_type,
            this.cash_item_name});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged_1);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "选择";
            this.gridColumn1.FieldName = "checked";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // _vouch_type_code
            // 
            this._vouch_type_code.Caption = "修改前id";
            this._vouch_type_code.FieldName = "_vouch_type_code";
            this._vouch_type_code.Name = "_vouch_type_code";
            // 
            // cash_item_type
            // 
            this.cash_item_type.Caption = "类别编码";
            this.cash_item_type.FieldName = "cash_item_type";
            this.cash_item_type.Name = "cash_item_type";
            this.cash_item_type.Visible = true;
            this.cash_item_type.VisibleIndex = 1;
            // 
            // cash_item_name
            // 
            this.cash_item_name.Caption = "类别名称";
            this.cash_item_name.FieldName = "cash_item_name";
            this.cash_item_name.Name = "cash_item_name";
            this.cash_item_name.Visible = true;
            this.cash_item_name.VisibleIndex = 2;
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(791, 295);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "main";
            this.Text = "现金流量类别";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TextEdit acct_subj_name;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn _vouch_type_code;
        private DevExpress.XtraGrid.Columns.GridColumn cash_item_type;
        private DevExpress.XtraGrid.Columns.GridColumn cash_item_name;
        private DevExpress.XtraEditors.SimpleButton acctItemcCode_select;
        private DevExpress.XtraEditors.SimpleButton acctsyscashkind_delete;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}