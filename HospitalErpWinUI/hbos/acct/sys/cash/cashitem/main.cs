﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.sys.cash.cashitem
{
    public partial class main : RibbonForm
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public main()
        {
            InitializeComponent();
            _helper = new DaoHelper();
            Load_acct_cashkind();//项目级别 下拉
            Load_acct_cashdire();
           
        }
        private void Load_acct_cashdire()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_cashdire";
            
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            repositoryItemComboBox1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                repositoryItemComboBox1.Items.Add(codeValue.Code + " " + codeValue.Value);
                
            }
           // gridColumn4. = "0";
           // repositoryItemComboBox1.SelectedIndexChanged = 0;
            // acct_subj_name.SelectedItem = objs[0];
           // repositoryItemComboBox1.Items.Add("请选择");
            //repositoryItemComboBox1.Items.Add("男");
            //repositoryItemComboBox1.Items.Add("女");
 
          //  return objs;
          //  gridControl1.get
           // acct_subj_name.Properties.Items.AddRange(objs);
            
        } 
        private void Load_acct_cashkind()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_cashkind";
            paramters.Add("Comp_code", G_User.user.Comp_code);
            paramters.Add("Copy_code", G_User.user.Copy_code);
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            acct_subj_name.Properties.Items.AddRange(objs);
            acct_subj_name.SelectedItem = objs[0];
        } 
        private void acctItemcCode_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acctsyscashitem_select";
            //，，，，，，，，
            paramters.Add("acct_subj_name", FormHelper.GetValueForComboBoxEdit(acct_subj_name));
             
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
          //  List<string> objs = Load_acct_cashdire();//方向 下拉
            for (int i = 0; i < dt.Rows.Count; i++)
            {
         

                dt.Rows[i]["checked"] = "false";
                dt.Rows[i]["Column1"] = dt.Rows[i]["Column1"].ToString().Replace("|||", " ");
 
            }
            _user = dt;
            gridControl1.DataSource = _user;
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int selectedID = e.PrevFocusedRowHandle;//e.FocusedRowHandle;
            DataRow row = gridView1.GetDataRow(selectedID);
            if (row != null)
            {
                if (row["_cash_item_code"].ToString() == null || row["_cash_item_code"].ToString().Equals(""))
                {
                    if (!Validate(row))
                    {
                        return;
                    }
                    try
                    {
                        Insert(row);
                        this.DialogResult = System.Windows.Forms.DialogResult.OK;
                        //     MessageForm.Show("新增角色成功！");
                        //  this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageForm.Exception(ex.Message, "异常");
                    }
                }
                else
                {
                    if (!Validate(row))
                    {
                        return;
                    }
                    try
                    {
                        u_update1(row);
                        this.DialogResult = System.Windows.Forms.DialogResult.OK;
                        //     MessageForm.Show("新增角色成功！");
                        //  this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageForm.Exception(ex.Message, "异常");
                    }
                }
                
            }
        }
        private bool Validate(DataRow row)
        {
         //   MessageBox.Show(row["Column1"].ToString());
            if (!Validator.IsInteger(row["cash_item_code"].ToString()))
            {
                MessageBox.Show("项目编码请正确输入数字");
                return false;
            }
            if (string.IsNullOrEmpty(row["cash_item_name"].ToString()))
            {
                MessageForm.Warning("项目名称不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(row["Column1"].ToString()))
            {
                MessageForm.Warning("方向不能为空!");
                return false;
            }
            return true;
        }
        private bool Insert(DataRow row)
        {

            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "acctsyscashitem_insert";
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            
            paramters.Add("cash_item_type", FormHelper.GetValueForComboBoxEdit(acct_subj_name));
            paramters.Add("cash_item_code", row["cash_item_code"].ToString());
            paramters.Add("cash_item_name", row["cash_item_name"].ToString());
            paramters.Add("Column1", FormHelper.getSplit(row["Column1"].ToString()));
            serviceParamter.Paramters = paramters;

            return _helper.WirteSql(serviceParamter);

        }
        private bool u_update1(DataRow row)
        {
           
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "acctsyscashitem_update";
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            paramters.Add("_cash_item_code", row["_cash_item_code"].ToString());
            paramters.Add("cash_item_type", FormHelper.GetValueForComboBoxEdit(acct_subj_name));
            paramters.Add("cash_item_code", row["cash_item_code"].ToString());
            paramters.Add("cash_item_name", row["cash_item_name"].ToString());
            paramters.Add("Column1", FormHelper.getSplit(row["Column1"].ToString()));
            serviceParamter.Paramters = paramters;

            return _helper.WirteSql(serviceParamter);

        }

        private void acctsyscashitem_delete_Click(object sender, EventArgs e)
        {
            bool value = false;
            string strSelected = string.Empty;

            try
            {
                List<string[]> objs = new List<string[]>();
                for (int i = 0; i < gridView1.RowCount - 1; i++)
                {
                    value = Convert.ToBoolean(gridView1.GetDataRow(i)["checked"]);
                    if (value)
                    {
                        string[] code = { gridView1.GetDataRow(i)["_cash_item_code"].ToString(), gridView1.GetDataRow(i)["_cash_item_type"].ToString() };
                        objs.Add(code);

                    }
                }
                if (objs.Count == 0)
                {
                    MessageForm.Show("请选择要删除的数据!");
                    return;
                }
                if (MessageForm.ShowMsg("确认是否删除?", MsgType.YesNo) == DialogResult.Yes)
                {
                    foreach (string[] obj in objs)
                    {
                        if (!Delete(obj[0],obj[1]))
                        {
                            return;
                        }
                    }
                    //       MessageForm.Show("操作成功!");
                    Query();
                }

            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private bool Delete(string cash_item_code, string cash_item_type)
        {

            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acctsyscashitem_delete";
            paramters.Add("cash_item_code", cash_item_code);
            paramters.Add("cash_item_type", cash_item_type);
            
            serviceParamter.Paramters = paramters;
            bool result = false;
            try
            {
                _helper.WirteSql(serviceParamter);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }
    }
}
