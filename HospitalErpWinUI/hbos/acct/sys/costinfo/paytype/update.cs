﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.sys.costinfo.paytype
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        private Validator validator;
        private List<Perm> _perms { get; set; }

        public update()
        {
            InitializeComponent();
        }
        private void Load_acct_all_pay_type_list()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_all_pay_type_list";
            paramters.Add("Comp_code", G_User.user.Comp_code);
            paramters.Add("Copy_code", G_User.user.Copy_code);
            paramters.Add("Acct_year", G_User.user.Acct_year);
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            settle_way.Properties.Items.AddRange(objs);

        }
        private void Load_acctPayAttrList()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acctPayAttrList";
            // DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            List<CodeValue> codeValues = _helper.ReadDictForData(serviceParamter);
            foreach (CodeValue codeValue in codeValues)
            {
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            pay_attr.Properties.Items.AddRange(objs);
            pay_attr.SelectedItem = objs[0];
        }
        private bool Validate()
        {

            if (string.IsNullOrEmpty(type_code.Text))
            {
                MessageForm.Warning("支付编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(type_name.Text))
            {
                MessageForm.Warning("支付名称不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(pay_attr.Text))
            {
                MessageForm.Warning("支付属性不能为空!");
                return false;
            }
            return true;
        }
        public update(DaoHelper helper, List<Perm> perms, string pay_type_code )
        {
            _helper = helper;
            _perms = perms;
            validator = new Validator();
            InitializeComponent();
            Load_acctPayAttrList();//支付属性
            Load_acct_all_pay_type_list();//结算方式
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acctPayType_update_load";
            paramters.Add("pay_type_code", pay_type_code);
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("查询失败！");

            }
            else
            {
                this.type_code.Text = dt.Rows[0]["pay_type_code"].ToString();
                this.type_name.Text = dt.Rows[0]["pay_type_name"].ToString();
                FormHelper.ComboBoxeditAssignment(pay_attr, dt.Rows[0]["pay_attr"].ToString());
                if (dt.Rows[0]["pay_attr"].ToString() == "2") {
                    this.jsfs.Visible = true;
                    this.settle_way.Visible = true;
                    FormHelper.ComboBoxeditAssignment(settle_way, dt.Rows[0]["cheq_type_code"].ToString());
                }
                else
                {
                    this.jsfs.Visible = false;
                    this.settle_way.Visible = false;
                    FormHelper.ComboBoxeditAssignment(settle_way, "");
                }
            }
            // Init();

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void acctsysvouchtype_insert_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                u_update1();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
        private bool u_update1()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "acctPayType_save";
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            paramters.Add("type_code", type_code.Text);
            paramters.Add("type_name", type_name.Text);
            paramters.Add("pay_attr", FormHelper.GetValueForComboBoxEdit(pay_attr));
            if (FormHelper.GetValueForComboBoxEdit(pay_attr) == "2")
            {
                paramters.Add("settle_way", FormHelper.GetValueForComboBoxEdit(settle_way));
            }
            else
            {
                paramters.Add("settle_way", "");
            }
            paramters.Add("insertF", "1");//1修改 2添加
            serviceParamter.Paramters = paramters;
            return _helper.WirteSql(serviceParamter);

        }

        private void pay_attr_SelectedValueChanged(object sender, EventArgs e)
        {
            if (FormHelper.GetValueForComboBoxEdit(pay_attr) == "2")
            {
                this.jsfs.Visible = true;
                this.settle_way.Visible = true;
            }
            else
            {
                this.jsfs.Visible = false;
                this.settle_way.Visible = false;
                FormHelper.ComboBoxeditAssignment(settle_way,"");
            }
            
        }
    }
}
