﻿namespace HospitalErpWinUI.hbos.acct.sys.costinfo.paytype
{
    partial class insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pay_attr = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.acctsysvouchtype_insert = new DevExpress.XtraEditors.SimpleButton();
            this.type_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.type_code = new DevExpress.XtraEditors.TextEdit();
            this.settle_way = new DevExpress.XtraEditors.ComboBoxEdit();
            this.jsfs = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.pay_attr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.type_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.type_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.settle_way.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pay_attr
            // 
            this.pay_attr.Location = new System.Drawing.Point(108, 107);
            this.pay_attr.Name = "pay_attr";
            this.pay_attr.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.pay_attr.Size = new System.Drawing.Size(147, 20);
            this.pay_attr.TabIndex = 67;
            this.pay_attr.SelectedValueChanged += new System.EventHandler(this.pay_attr_SelectedValueChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(36, 110);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 14);
            this.labelControl3.TabIndex = 66;
            this.labelControl3.Text = "收费性质：";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(147, 186);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 65;
            this.simpleButton2.Text = "关闭";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // acctsysvouchtype_insert
            // 
            this.acctsysvouchtype_insert.Location = new System.Drawing.Point(30, 186);
            this.acctsysvouchtype_insert.Name = "acctsysvouchtype_insert";
            this.acctsysvouchtype_insert.Size = new System.Drawing.Size(75, 23);
            this.acctsysvouchtype_insert.TabIndex = 64;
            this.acctsysvouchtype_insert.Text = "保存";
            this.acctsysvouchtype_insert.Click += new System.EventHandler(this.acctsysvouchtype_insert_Click);
            // 
            // type_name
            // 
            this.type_name.Location = new System.Drawing.Point(108, 69);
            this.type_name.Name = "type_name";
            this.type_name.Size = new System.Drawing.Size(147, 20);
            this.type_name.TabIndex = 63;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(55, 72);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(36, 14);
            this.labelControl2.TabIndex = 62;
            this.labelControl2.Text = "名称：";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(57, 31);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(36, 14);
            this.labelControl1.TabIndex = 61;
            this.labelControl1.Text = "编码：";
            // 
            // type_code
            // 
            this.type_code.Location = new System.Drawing.Point(108, 28);
            this.type_code.Name = "type_code";
            this.type_code.Size = new System.Drawing.Size(147, 20);
            this.type_code.TabIndex = 60;
            // 
            // settle_way
            // 
            this.settle_way.Location = new System.Drawing.Point(108, 150);
            this.settle_way.Name = "settle_way";
            this.settle_way.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.settle_way.Size = new System.Drawing.Size(147, 20);
            this.settle_way.TabIndex = 71;
            // 
            // jsfs
            // 
            this.jsfs.Location = new System.Drawing.Point(36, 153);
            this.jsfs.Name = "jsfs";
            this.jsfs.Size = new System.Drawing.Size(60, 14);
            this.jsfs.TabIndex = 70;
            this.jsfs.Text = "结算方式：";
            // 
            // insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 281);
            this.Controls.Add(this.settle_way);
            this.Controls.Add(this.jsfs);
            this.Controls.Add(this.pay_attr);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.acctsysvouchtype_insert);
            this.Controls.Add(this.type_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.type_code);
            this.Name = "insert";
            this.Text = "添加";
            ((System.ComponentModel.ISupportInitialize)(this.pay_attr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.type_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.type_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.settle_way.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.ComboBoxEdit pay_attr;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton acctsysvouchtype_insert;
        private DevExpress.XtraEditors.TextEdit type_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit type_code;
        private DevExpress.XtraEditors.ComboBoxEdit settle_way;
        private DevExpress.XtraEditors.LabelControl jsfs;
    }
}