﻿namespace HospitalErpWinUI.hbos.acct.sys.costinfo.costkind
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.old_kind_code = new DevExpress.XtraEditors.TextEdit();
            this.inout_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.acctsysvouchtype_insert = new DevExpress.XtraEditors.SimpleButton();
            this.kind_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.kind_code = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.old_kind_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inout_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kind_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kind_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(40, 217);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(48, 14);
            this.labelControl4.TabIndex = 69;
            this.labelControl4.Text = "旧编码：";
            this.labelControl4.Visible = false;
            // 
            // old_kind_code
            // 
            this.old_kind_code.Location = new System.Drawing.Point(91, 214);
            this.old_kind_code.Name = "old_kind_code";
            this.old_kind_code.Size = new System.Drawing.Size(147, 20);
            this.old_kind_code.TabIndex = 68;
            this.old_kind_code.Visible = false;
            // 
            // inout_code
            // 
            this.inout_code.Location = new System.Drawing.Point(108, 107);
            this.inout_code.Name = "inout_code";
            this.inout_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.inout_code.Size = new System.Drawing.Size(147, 20);
            this.inout_code.TabIndex = 67;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(36, 110);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 14);
            this.labelControl3.TabIndex = 66;
            this.labelControl3.Text = "收费性质：";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(146, 162);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 65;
            this.simpleButton2.Text = "关闭";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // acctsysvouchtype_insert
            // 
            this.acctsysvouchtype_insert.Location = new System.Drawing.Point(29, 162);
            this.acctsysvouchtype_insert.Name = "acctsysvouchtype_insert";
            this.acctsysvouchtype_insert.Size = new System.Drawing.Size(75, 23);
            this.acctsysvouchtype_insert.TabIndex = 64;
            this.acctsysvouchtype_insert.Text = "保存";
            this.acctsysvouchtype_insert.Click += new System.EventHandler(this.acctsysvouchtype_insert_Click);
            // 
            // kind_name
            // 
            this.kind_name.Location = new System.Drawing.Point(108, 69);
            this.kind_name.Name = "kind_name";
            this.kind_name.Size = new System.Drawing.Size(147, 20);
            this.kind_name.TabIndex = 63;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(55, 72);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(36, 14);
            this.labelControl2.TabIndex = 62;
            this.labelControl2.Text = "名称：";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(57, 31);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(36, 14);
            this.labelControl1.TabIndex = 61;
            this.labelControl1.Text = "编码：";
            // 
            // kind_code
            // 
            this.kind_code.Enabled = false;
            this.kind_code.Location = new System.Drawing.Point(108, 28);
            this.kind_code.Name = "kind_code";
            this.kind_code.Size = new System.Drawing.Size(147, 20);
            this.kind_code.TabIndex = 60;
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.old_kind_code);
            this.Controls.Add(this.inout_code);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.acctsysvouchtype_insert);
            this.Controls.Add(this.kind_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.kind_code);
            this.Name = "update";
            this.Text = "update";
            ((System.ComponentModel.ISupportInitialize)(this.old_kind_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inout_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kind_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kind_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit old_kind_code;
        private DevExpress.XtraEditors.ComboBoxEdit inout_code;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton acctsysvouchtype_insert;
        private DevExpress.XtraEditors.TextEdit kind_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit kind_code;
    }
}