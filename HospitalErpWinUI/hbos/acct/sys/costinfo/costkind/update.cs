﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.sys.costinfo.costkind
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        private Validator validator;
        private List<Perm> _perms { get; set; }
        public update()
        {
            InitializeComponent();
        }
        public update(DaoHelper helper, List<Perm> perms, string acct_subj_code )
        {
            _helper = helper;
            _perms = perms;
            validator = new Validator();


            InitializeComponent();
            this.kind_code.Text = acct_subj_code;
            this.old_kind_code.Text = acct_subj_code;
             
            // Init();
            Load_sysHisChargeType_list();
            validator = new Validator();
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acctSysCostinfoGathering_update_load";
            paramters.Add("content_code", acct_subj_code);
            paramters.Add("Comp_code", G_User.user.Comp_code);
            paramters.Add("Copy_code", G_User.user.Copy_code);
            paramters.Add("Acct_year", G_User.user.Acct_year);
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("查询失败！");

            }
            else
            {

                this.kind_code.Text = dt.Rows[0]["kind_code"].ToString();
                this.kind_name.Text = dt.Rows[0]["kind_name"].ToString();
                //this.u_bname.Text 
                FormHelper.ComboBoxeditAssignment(inout_code, dt.Rows[0]["inout_type_code"].ToString());
                this.old_kind_code.Text = dt.Rows[0]["kind_code"].ToString();
                 
                 
            }
        }
        private void Load_sysHisChargeType_list()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysHisChargeType_list";
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            inout_code.Properties.Items.AddRange(objs);

        }
        private bool Validate()
        {

            if (string.IsNullOrEmpty(kind_code.Text))
            {
                MessageForm.Warning("类别编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(kind_code.Text))
            {
                MessageForm.Warning("类别名称不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(inout_code.Text))
            {
                MessageForm.Warning("收费性质不能为空!");
                return false;
            }
            return true;
        }
        private void acctsysvouchtype_insert_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                u_update1();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
        private bool u_update1()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "acctSysCostinfoGathering_update";
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            paramters.Add("kind_code", kind_code.Text);
            paramters.Add("kind_name", kind_code.Text);
            paramters.Add("inout_code", FormHelper.GetValueForComboBoxEdit(inout_code));
            paramters.Add("old_kind_code", old_kind_code.Text);
            serviceParamter.Paramters = paramters;

            return _helper.WirteSql(serviceParamter);

        }
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
