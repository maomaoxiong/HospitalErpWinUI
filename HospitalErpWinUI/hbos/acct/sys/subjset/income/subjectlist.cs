﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.sys.subjset.income
{
    public partial class subjectlist : Form
    {
        private DaoHelper _helper;
       
        private List<Perm> _perms { get; set; }
        public delegate void TcpStatedelegate(string txt,int id);//定义一个委托
        public event TcpStatedelegate myevent;//定义一个上述委托的事件
        int index1 = 0;
        DataTable _user = new DataTable();
        //触发事件的方法

           
        public subjectlist()
        {
            InitializeComponent();
        }
        public subjectlist(DaoHelper helper, List<Perm> perms, string content_code, int index)
        {
            
            InitializeComponent();
            _helper = helper;
            _perms = perms;
            this.StartPosition = FormStartPosition.CenterParent;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.Text = "选择科目";

            index1 = index;
            DataTable dt = _helper.ReadSqlForDataTable("acctSysSubjsetIntercalateSubjectlist_select", 
                new[] { G_User.user.User_id, "" });
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;
 
        }

        

        private void acctSysSubjsetIntercalate_select_Click_1(object sender, EventArgs e)
        {
            List<string> objs = new List<string>();
            bool value = false;
            string code = "";
            for (int i = 0; i < gridView1.RowCount; i++)
            {
                value = Convert.ToBoolean(gridView1.GetDataRow(i)["checked"]);
                if (value)
                {
                    code = gridView1.GetDataRow(i)["acct_subj_code"].ToString() + " " +
                        gridView1.GetDataRow(i)["acct_subj_name_all"].ToString();
                    objs.Add(code);

                }
            }
            if (objs.Count != 1)
            {
                MessageForm.Show("请选择一条数据!");
                return;
            }
            if (myevent != null)//检查事件是否注册
            {
                myevent(code, index1);
            }
             this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
             this.Close();
        }
    }
}
