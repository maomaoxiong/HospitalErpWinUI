﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraGrid;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.Common;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.sys.subjset.income
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        //  public List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public main()
        {
            InitializeComponent();
            _helper = new DaoHelper();
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.Text = "收入设置";
            Load_acct_his_advice_type();
            Load_dict_yes_or_no();//是否下拉框
        }
        private void Load_dict_yes_or_no()
        {

            List<string> objs = DataDictApi.dict_yes_or_no();
            is_medicare.Properties.Items.AddRange(objs);
            is_medicare.SelectedItem = objs[0];
             

        }
        private void Load_acct_his_advice_type() {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_his_advice_type";
            serviceParamter.Paramters = paramters;
            List<string> objs = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + ' ' + codeValue.Value);
            }
            doc_type.Properties.Items.AddRange(objs);
            if(objs.Count>0)
                doc_type.SelectedItem = objs[0];
        }

        private void acctSysSubjsetIntercalate_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        DataTable dt;
        private void Query()
        {
            dt = _helper.ReadSqlForDataTable("acctSysSubjsetIntercalate_select",
                new[] { FormHelper.GetValueForComboBoxEdit(doc_type), FormHelper.GetValueForComboBoxEdit(is_medicare) });
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                //  gridView1.SetRowCellValue(i, gridView1.Columns["checked"], "设置");
                //  gridView1.UpdateCurrentRow();
                //gridview1.SetRowCellValue(int rowHandler,string feildName,object value);
                dt.Rows[i]["Column2"] = dt.Rows[i]["Column2"]+"      设置";
            }
            _user = dt;
            gridControl1.DataSource = _user;
        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                int index = gridView1.GetDataSourceRowIndex(gridView1.FocusedRowHandle);
                string acct_subj_code = _user.Rows[index]["kind_code"].ToString();
               subjectlist frm = new subjectlist(this._helper, this._perms, acct_subj_code, index);

               frm.myevent += new subjectlist.TcpStatedelegate(tcpConfig_myevent);//注册事件
               frm.ShowDialog();
            
            }
        }
        public void tcpConfig_myevent(string str,int id)
        {
           // gridView1.SetRowCellValue(id)["Column2"] = str;
           // gridView1.SetRowCellValue(id)["Column2"] = str;
             
            dt.Rows[id]["Column2"] = str + "      设置";
            _user = dt;
            gridControl1.DataSource = _user;
          //  gridView1.GetDataRow
        }
    }
}
