﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraGrid;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.sys.subjset.rec
{
    public partial class main : RibbonForm
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public main()
        {
            InitializeComponent();
            _helper = new DaoHelper();
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.Text = "应收设置";
        }

        private void acctSysSubjsetRecItem_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            DataTable dt = _helper.ReadSqlForDataTable("acctSysSubjsetRecItem_select",
                new[] {FormHelper.GetValueForSearchLookUpEdit(subj_code),
                   FormHelper.GetValueForComboBoxEdit(doc_type) ,
                   FormHelper.GetValueForComboBoxEdit(pay_type) ,
                   FormHelper.GetValueForComboBoxEdit(pati_type) ,
                    G_User.user.Comp_code, G_User.user.Copy_code, G_User.user.Acct_year });
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            //  List<string> objs = Load_acct_cashdire();//方向 下拉
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";

            }
            _user = dt;
            gridControl1.DataSource = _user;
        }

        private void sysDictsUnitinfoVenData_insert_Click(object sender, EventArgs e)
        {
            try
            {
                insert insertFrm = new insert(this._helper, this._perms);

                if (insertFrm.ShowDialog() == DialogResult.OK)
                {
                    Query();
                }
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                int index = gridView1.GetDataSourceRowIndex(gridView1.FocusedRowHandle);
                string kind_code = _user.Rows[index]["id"].ToString();


                update frm = new update(this._helper, this._perms, kind_code);

                if (frm.ShowDialog() == DialogResult.No)
                {
                    MessageForm.Warning("修改失败！");
                }
                Query();
            }
        }
    }
}
