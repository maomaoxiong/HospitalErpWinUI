﻿namespace HospitalErpWinUI.hbos.acct.sys.subjset.rec
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.pati_type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.pay_type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.doc_type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.subj_code = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.sysDictsUnitinfoVenData_delete = new DevExpress.XtraEditors.SimpleButton();
            this.sysDictsUnitinfoVenData_insert = new DevExpress.XtraEditors.SimpleButton();
            this.acctSysSubjsetRecItem_select = new DevExpress.XtraEditors.SimpleButton();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pati_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pay_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.doc_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subj_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl3);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.panelControl1.Size = new System.Drawing.Size(918, 306);
            this.panelControl1.TabIndex = 0;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.gridControl1);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(14, 72);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(902, 232);
            this.panelControl3.TabIndex = 1;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.gridControl1.Size = new System.Drawing.Size(898, 228);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "选择";
            this.gridColumn1.FieldName = "checked";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 50;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "序号";
            this.gridColumn2.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.gridColumn2.FieldName = "id";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 50;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "医嘱类型";
            this.gridColumn3.FieldName = "Column1";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 100;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "结算方式";
            this.gridColumn4.FieldName = "Column2";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 130;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "病人类别";
            this.gridColumn5.FieldName = "Column3";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            this.gridColumn5.Width = 130;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "应收科目";
            this.gridColumn6.FieldName = "Column4";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            this.gridColumn6.Width = 420;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.pati_type);
            this.panelControl2.Controls.Add(this.labelControl2);
            this.panelControl2.Controls.Add(this.pay_type);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Controls.Add(this.doc_type);
            this.panelControl2.Controls.Add(this.labelControl24);
            this.panelControl2.Controls.Add(this.labelControl7);
            this.panelControl2.Controls.Add(this.subj_code);
            this.panelControl2.Controls.Add(this.sysDictsUnitinfoVenData_delete);
            this.panelControl2.Controls.Add(this.sysDictsUnitinfoVenData_insert);
            this.panelControl2.Controls.Add(this.acctSysSubjsetRecItem_select);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(14, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(902, 70);
            this.panelControl2.TabIndex = 0;
            // 
            // pati_type
            // 
            this.pati_type.Location = new System.Drawing.Point(389, 40);
            this.pati_type.Name = "pati_type";
            this.pati_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.pati_type.Size = new System.Drawing.Size(168, 20);
            this.pati_type.TabIndex = 110;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(315, 43);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 111;
            this.labelControl2.Text = "病人类别：";
            // 
            // pay_type
            // 
            this.pay_type.Location = new System.Drawing.Point(106, 40);
            this.pay_type.Name = "pay_type";
            this.pay_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.pay_type.Size = new System.Drawing.Size(188, 20);
            this.pay_type.TabIndex = 108;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 43);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(84, 14);
            this.labelControl1.TabIndex = 109;
            this.labelControl1.Text = "收费支付方式：";
            // 
            // doc_type
            // 
            this.doc_type.Location = new System.Drawing.Point(583, 7);
            this.doc_type.Name = "doc_type";
            this.doc_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.doc_type.Size = new System.Drawing.Size(168, 20);
            this.doc_type.TabIndex = 106;
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(509, 10);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(60, 14);
            this.labelControl24.TabIndex = 107;
            this.labelControl24.Text = "医嘱类别：";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(30, 13);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(60, 14);
            this.labelControl7.TabIndex = 105;
            this.labelControl7.Text = "应收科目：";
            // 
            // subj_code
            // 
            this.subj_code.Location = new System.Drawing.Point(106, 10);
            this.subj_code.Name = "subj_code";
            this.subj_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.subj_code.Properties.NullText = "";
            this.subj_code.Properties.View = this.searchLookUpEdit1View;
            this.subj_code.Size = new System.Drawing.Size(377, 20);
            this.subj_code.TabIndex = 104;
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // sysDictsUnitinfoVenData_delete
            // 
            this.sysDictsUnitinfoVenData_delete.Location = new System.Drawing.Point(821, 38);
            this.sysDictsUnitinfoVenData_delete.Name = "sysDictsUnitinfoVenData_delete";
            this.sysDictsUnitinfoVenData_delete.Size = new System.Drawing.Size(75, 24);
            this.sysDictsUnitinfoVenData_delete.TabIndex = 103;
            this.sysDictsUnitinfoVenData_delete.Text = "删除";
            // 
            // sysDictsUnitinfoVenData_insert
            // 
            this.sysDictsUnitinfoVenData_insert.Location = new System.Drawing.Point(710, 38);
            this.sysDictsUnitinfoVenData_insert.Name = "sysDictsUnitinfoVenData_insert";
            this.sysDictsUnitinfoVenData_insert.Size = new System.Drawing.Size(75, 24);
            this.sysDictsUnitinfoVenData_insert.TabIndex = 102;
            this.sysDictsUnitinfoVenData_insert.Text = "添加";
            this.sysDictsUnitinfoVenData_insert.Click += new System.EventHandler(this.sysDictsUnitinfoVenData_insert_Click);
            // 
            // acctSysSubjsetRecItem_select
            // 
            this.acctSysSubjsetRecItem_select.Location = new System.Drawing.Point(596, 38);
            this.acctSysSubjsetRecItem_select.Name = "acctSysSubjsetRecItem_select";
            this.acctSysSubjsetRecItem_select.Size = new System.Drawing.Size(75, 24);
            this.acctSysSubjsetRecItem_select.TabIndex = 101;
            this.acctSysSubjsetRecItem_select.Text = "查询";
            this.acctSysSubjsetRecItem_select.Click += new System.EventHandler(this.acctSysSubjsetRecItem_select_Click);
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit1_Click);
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(918, 306);
            this.Controls.Add(this.panelControl1);
            this.Name = "main";
            this.Text = "main";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pati_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pay_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.doc_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subj_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton sysDictsUnitinfoVenData_delete;
        private DevExpress.XtraEditors.SimpleButton sysDictsUnitinfoVenData_insert;
        private DevExpress.XtraEditors.SimpleButton acctSysSubjsetRecItem_select;
        private DevExpress.XtraEditors.SearchLookUpEdit subj_code;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.ComboBoxEdit pati_type;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ComboBoxEdit pay_type;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit doc_type;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
    }
}