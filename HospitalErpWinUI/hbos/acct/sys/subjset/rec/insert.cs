﻿using Dao;
using HospitalErpData.MenuHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.acct.sys.subjset.rec
{
    public partial class insert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        public insert()
        {
            InitializeComponent();
            _helper = new DaoHelper();
        }
        public insert(DaoHelper helper, List<Perm> perms)
        {
            InitializeComponent();
            this._helper = helper;
            this._perms = perms;
            this.StartPosition = FormStartPosition.CenterParent;
            this.Text = "录入";
        }
    }
}
