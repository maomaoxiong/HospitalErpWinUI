﻿namespace HospitalErpWinUI.hbos.acct.sys.subjset.rec
{
    partial class insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.subj_code = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.pati_type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.pay_type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.doc_type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.acctSysSubjsetRecItem_insert = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.subj_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pati_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pay_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.doc_type.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.subj_code);
            this.panelControl1.Controls.Add(this.pati_type);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.pay_type);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.doc_type);
            this.panelControl1.Controls.Add(this.labelControl24);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.acctSysSubjsetRecItem_insert);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(447, 262);
            this.panelControl1.TabIndex = 0;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(28, 147);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(60, 14);
            this.labelControl7.TabIndex = 115;
            this.labelControl7.Text = "应收科目：";
            // 
            // subj_code
            // 
            this.subj_code.Location = new System.Drawing.Point(104, 144);
            this.subj_code.Name = "subj_code";
            this.subj_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.subj_code.Properties.NullText = "";
            this.subj_code.Properties.View = this.searchLookUpEdit1View;
            this.subj_code.Size = new System.Drawing.Size(311, 20);
            this.subj_code.TabIndex = 114;
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // pati_type
            // 
            this.pati_type.Location = new System.Drawing.Point(104, 102);
            this.pati_type.Name = "pati_type";
            this.pati_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.pati_type.Size = new System.Drawing.Size(168, 20);
            this.pati_type.TabIndex = 112;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(30, 105);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 113;
            this.labelControl2.Text = "病人类别：";
            // 
            // pay_type
            // 
            this.pay_type.Location = new System.Drawing.Point(104, 60);
            this.pay_type.Name = "pay_type";
            this.pay_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.pay_type.Size = new System.Drawing.Size(168, 20);
            this.pay_type.TabIndex = 110;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(5, 63);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(84, 14);
            this.labelControl1.TabIndex = 111;
            this.labelControl1.Text = "收费支付方式：";
            // 
            // doc_type
            // 
            this.doc_type.Location = new System.Drawing.Point(104, 23);
            this.doc_type.Name = "doc_type";
            this.doc_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.doc_type.Size = new System.Drawing.Size(168, 20);
            this.doc_type.TabIndex = 108;
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(30, 26);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(60, 14);
            this.labelControl24.TabIndex = 109;
            this.labelControl24.Text = "医嘱类别：";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(197, 209);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 17;
            this.simpleButton2.Text = "关闭";
            // 
            // acctSysSubjsetRecItem_insert
            // 
            this.acctSysSubjsetRecItem_insert.Location = new System.Drawing.Point(80, 209);
            this.acctSysSubjsetRecItem_insert.Name = "acctSysSubjsetRecItem_insert";
            this.acctSysSubjsetRecItem_insert.Size = new System.Drawing.Size(75, 23);
            this.acctSysSubjsetRecItem_insert.TabIndex = 16;
            this.acctSysSubjsetRecItem_insert.Text = "保存";
            // 
            // insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(447, 262);
            this.Controls.Add(this.panelControl1);
            this.Name = "insert";
            this.Text = "insert";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.subj_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pati_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pay_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.doc_type.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton acctSysSubjsetRecItem_insert;
        private DevExpress.XtraEditors.ComboBoxEdit doc_type;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.ComboBoxEdit pay_type;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit pati_type;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.SearchLookUpEdit subj_code;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
    }
}