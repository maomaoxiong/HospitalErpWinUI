﻿namespace HospitalErpWinUI.hbos.sys.user.usergroup
{
    partial class insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.sysUserGroup_insert = new DevExpress.XtraEditors.SimpleButton();
            this.CloseButton = new DevExpress.XtraEditors.SimpleButton();
            this.dbaEmp_code = new DevExpress.XtraEditors.TextEdit();
            this.group_code = new DevExpress.XtraEditors.TextEdit();
            this.group_name = new DevExpress.XtraEditors.TextEdit();
            this.group_desc = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.dbaEmp_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_desc.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(69, 65);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 18);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "角色编码";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(69, 109);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 18);
            this.labelControl3.TabIndex = 1;
            this.labelControl3.Text = "角色名称";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(69, 153);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 18);
            this.labelControl4.TabIndex = 1;
            this.labelControl4.Text = "角色描述";
            // 
            // sysUserGroup_insert
            // 
            this.sysUserGroup_insert.Location = new System.Drawing.Point(112, 219);
            this.sysUserGroup_insert.Name = "sysUserGroup_insert";
            this.sysUserGroup_insert.Size = new System.Drawing.Size(80, 25);
            this.sysUserGroup_insert.TabIndex = 3;
            this.sysUserGroup_insert.Text = "添加";
            this.sysUserGroup_insert.Click += new System.EventHandler(this.sysUserGroup_insert_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(253, 219);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(80, 25);
            this.CloseButton.TabIndex = 4;
            this.CloseButton.Text = "关闭";
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // dbaEmp_code
            // 
            this.dbaEmp_code.Location = new System.Drawing.Point(185, 12);
            this.dbaEmp_code.Name = "dbaEmp_code";
            this.dbaEmp_code.Size = new System.Drawing.Size(100, 24);
            this.dbaEmp_code.TabIndex = 3;
            // 
            // group_code
            // 
            this.group_code.Location = new System.Drawing.Point(185, 62);
            this.group_code.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.group_code.Name = "group_code";
            this.group_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.group_code.Properties.Appearance.Options.UseBackColor = true;
            this.group_code.Size = new System.Drawing.Size(178, 24);
            this.group_code.TabIndex = 0;
            // 
            // group_name
            // 
            this.group_name.Location = new System.Drawing.Point(185, 104);
            this.group_name.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.group_name.Name = "group_name";
            this.group_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.group_name.Properties.Appearance.Options.UseBackColor = true;
            this.group_name.Size = new System.Drawing.Size(178, 24);
            this.group_name.TabIndex = 1;
            // 
            // group_desc
            // 
            this.group_desc.Location = new System.Drawing.Point(185, 148);
            this.group_desc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.group_desc.Name = "group_desc";
            this.group_desc.Size = new System.Drawing.Size(178, 24);
            this.group_desc.TabIndex = 2;
            // 
            // insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 315);
            this.Controls.Add(this.group_desc);
            this.Controls.Add(this.group_name);
            this.Controls.Add(this.group_code);
            this.Controls.Add(this.dbaEmp_code);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.sysUserGroup_insert);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Name = "insert";
            this.Text = "新增角色";
            this.Load += new System.EventHandler(this.insert_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dbaEmp_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_desc.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton sysUserGroup_insert;
        private DevExpress.XtraEditors.SimpleButton CloseButton;
        private DevExpress.XtraEditors.TextEdit dbaEmp_code;
        private DevExpress.XtraEditors.TextEdit group_code;
        private DevExpress.XtraEditors.TextEdit group_name;
        private DevExpress.XtraEditors.TextEdit group_desc;
    }
}