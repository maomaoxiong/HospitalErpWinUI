﻿namespace HospitalErpWinUI.hbos.sys.user.usergroup
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.checkCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.codeCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.nameCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.descCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.sysUserGroup_delete = new DevExpress.XtraEditors.SimpleButton();
            this.sysUserGroup_insert = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.sysUserGroup_select = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.group_name = new DevExpress.XtraEditors.TextEdit();
            this.group_code = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.group_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridControl1);
            this.panelControl2.Controls.Add(this.panelControl4);
            this.panelControl2.Controls.Add(this.panelControl3);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.panelControl2.Size = new System.Drawing.Size(1270, 596);
            this.panelControl2.TabIndex = 1;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControl1.Location = new System.Drawing.Point(12, 70);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemHyperLinkEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1246, 524);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.checkCol,
            this.codeCol,
            this.nameCol,
            this.descCol});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // checkCol
            // 
            this.checkCol.AppearanceHeader.Options.UseTextOptions = true;
            this.checkCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.checkCol.Caption = "选中";
            this.checkCol.ColumnEdit = this.repositoryItemCheckEdit1;
            this.checkCol.FieldName = "checked";
            this.checkCol.Name = "checkCol";
            this.checkCol.Visible = true;
            this.checkCol.VisibleIndex = 1;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // codeCol
            // 
            this.codeCol.AppearanceHeader.Options.UseTextOptions = true;
            this.codeCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.codeCol.Caption = "角色编码";
            this.codeCol.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.codeCol.FieldName = "group_code";
            this.codeCol.Name = "codeCol";
            this.codeCol.OptionsColumn.ReadOnly = true;
            this.codeCol.Visible = true;
            this.codeCol.VisibleIndex = 2;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // nameCol
            // 
            this.nameCol.AppearanceHeader.Options.UseTextOptions = true;
            this.nameCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.nameCol.Caption = "角色名称";
            this.nameCol.FieldName = "group_name";
            this.nameCol.Name = "nameCol";
            this.nameCol.OptionsColumn.ReadOnly = true;
            this.nameCol.Visible = true;
            this.nameCol.VisibleIndex = 3;
            // 
            // descCol
            // 
            this.descCol.AppearanceHeader.Options.UseTextOptions = true;
            this.descCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.descCol.Caption = "角色描述";
            this.descCol.FieldName = "group_desc";
            this.descCol.Name = "descCol";
            this.descCol.OptionsColumn.ReadOnly = true;
            this.descCol.Visible = true;
            this.descCol.VisibleIndex = 4;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.sysUserGroup_delete);
            this.panelControl4.Controls.Add(this.sysUserGroup_insert);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(12, 36);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1246, 34);
            this.panelControl4.TabIndex = 2;
            // 
            // sysUserGroup_delete
            // 
            this.sysUserGroup_delete.Location = new System.Drawing.Point(93, 4);
            this.sysUserGroup_delete.Name = "sysUserGroup_delete";
            this.sysUserGroup_delete.Size = new System.Drawing.Size(80, 25);
            this.sysUserGroup_delete.TabIndex = 17;
            this.sysUserGroup_delete.Text = "删除";
            this.sysUserGroup_delete.Click += new System.EventHandler(this.sysUserGroup_delete_Click);
            // 
            // sysUserGroup_insert
            // 
            this.sysUserGroup_insert.Location = new System.Drawing.Point(5, 4);
            this.sysUserGroup_insert.Name = "sysUserGroup_insert";
            this.sysUserGroup_insert.Size = new System.Drawing.Size(80, 25);
            this.sysUserGroup_insert.TabIndex = 17;
            this.sysUserGroup_insert.Text = "新增";
            this.sysUserGroup_insert.Click += new System.EventHandler(this.sysUserGroup_insert_Click);
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.sysUserGroup_select);
            this.panelControl3.Controls.Add(this.labelControl2);
            this.panelControl3.Controls.Add(this.labelControl1);
            this.panelControl3.Controls.Add(this.group_name);
            this.panelControl3.Controls.Add(this.group_code);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(12, 2);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1246, 34);
            this.panelControl3.TabIndex = 1;
            // 
            // sysUserGroup_select
            // 
            this.sysUserGroup_select.Location = new System.Drawing.Point(626, 3);
            this.sysUserGroup_select.Name = "sysUserGroup_select";
            this.sysUserGroup_select.Size = new System.Drawing.Size(80, 25);
            this.sysUserGroup_select.TabIndex = 17;
            this.sysUserGroup_select.Text = "查询";
            this.sysUserGroup_select.Click += new System.EventHandler(this.sysUserGroup_select_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(330, 5);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 18);
            this.labelControl2.TabIndex = 15;
            this.labelControl2.Text = "角色名称";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(3, 7);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.labelControl1.Size = new System.Drawing.Size(80, 18);
            this.labelControl1.TabIndex = 16;
            this.labelControl1.Text = "角色编码";
            // 
            // group_name
            // 
            this.group_name.Location = new System.Drawing.Point(406, 4);
            this.group_name.Name = "group_name";
            this.group_name.Size = new System.Drawing.Size(200, 24);
            this.group_name.TabIndex = 13;
            // 
            // group_code
            // 
            this.group_code.Location = new System.Drawing.Point(85, 4);
            this.group_code.Name = "group_code";
            this.group_code.Size = new System.Drawing.Size(200, 24);
            this.group_code.TabIndex = 14;
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1270, 596);
            this.Controls.Add(this.panelControl2);
            this.Name = "main";
            this.Text = "角色管理";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.group_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_code.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton sysUserGroup_delete;
        private DevExpress.XtraEditors.SimpleButton sysUserGroup_insert;
        private DevExpress.XtraEditors.SimpleButton sysUserGroup_select;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit group_name;
        private DevExpress.XtraEditors.TextEdit group_code;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn checkCol;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn codeCol;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn nameCol;
        private DevExpress.XtraGrid.Columns.GridColumn descCol;


    }
}