﻿namespace HospitalErpWinUI.hbos.sys.user.usergroup
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.group_desc = new DevExpress.XtraEditors.TextEdit();
            this.group_name = new DevExpress.XtraEditors.TextEdit();
            this.group_code = new DevExpress.XtraEditors.TextEdit();
            this.CloseButton = new DevExpress.XtraEditors.SimpleButton();
            this.sysUserGroup_update = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.group_desc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // group_desc
            // 
            this.group_desc.Location = new System.Drawing.Point(193, 143);
            this.group_desc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.group_desc.Name = "group_desc";
            this.group_desc.Size = new System.Drawing.Size(178, 24);
            this.group_desc.TabIndex = 10;
            // 
            // group_name
            // 
            this.group_name.Location = new System.Drawing.Point(193, 99);
            this.group_name.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.group_name.Name = "group_name";
            this.group_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.group_name.Properties.Appearance.Options.UseBackColor = true;
            this.group_name.Size = new System.Drawing.Size(178, 24);
            this.group_name.TabIndex = 6;
            // 
            // group_code
            // 
            this.group_code.Location = new System.Drawing.Point(193, 57);
            this.group_code.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.group_code.Name = "group_code";
            this.group_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.group_code.Properties.Appearance.Options.UseBackColor = true;
            this.group_code.Size = new System.Drawing.Size(178, 24);
            this.group_code.TabIndex = 5;
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(261, 214);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(80, 25);
            this.CloseButton.TabIndex = 13;
            this.CloseButton.Text = "关闭";
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // sysUserGroup_update
            // 
            this.sysUserGroup_update.Location = new System.Drawing.Point(120, 214);
            this.sysUserGroup_update.Name = "sysUserGroup_update";
            this.sysUserGroup_update.Size = new System.Drawing.Size(80, 25);
            this.sysUserGroup_update.TabIndex = 12;
            this.sysUserGroup_update.Text = "修改";
            this.sysUserGroup_update.Click += new System.EventHandler(this.sysUserGroup_update_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(77, 148);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 18);
            this.labelControl4.TabIndex = 7;
            this.labelControl4.Text = "角色描述";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(77, 104);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 18);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "角色名称";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(77, 60);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 18);
            this.labelControl2.TabIndex = 9;
            this.labelControl2.Text = "角色编码";
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 315);
            this.Controls.Add(this.group_desc);
            this.Controls.Add(this.group_name);
            this.Controls.Add(this.group_code);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.sysUserGroup_update);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Name = "update";
            this.Text = "修改角色";
            this.Load += new System.EventHandler(this.UpdateFrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.group_desc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit group_desc;
        private DevExpress.XtraEditors.TextEdit group_name;
        private DevExpress.XtraEditors.TextEdit group_code;
        private DevExpress.XtraEditors.SimpleButton CloseButton;
        private DevExpress.XtraEditors.SimpleButton sysUserGroup_update;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;

    }
}