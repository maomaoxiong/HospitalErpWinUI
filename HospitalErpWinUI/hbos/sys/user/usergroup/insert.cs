﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.user.usergroup
{
    public partial class insert : Form
    {
        private DaoHelper _helper;    
        private List<Perm> _perms { get; set; }

        public insert()
        {
            InitializeComponent();
            Init();
        }

        public insert(DaoHelper helper, List<Perm> perms)
        {
            _helper = helper;           
            _perms = perms;

            InitializeComponent();
            Init();
        }
      
        
        #region 事件
        private void insert_Load(object sender, EventArgs e)
        {

        }


        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void sysUserGroup_insert_Click(object sender, EventArgs e)
        {

            if (!Validate())
            {
                return;
            }
            try
            {
                Insert();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
           //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
        #endregion
       
        #region  获取数据
        //dbaEmp_code 
        private bool Insert()
        {
            ServiceParamter serviceParamter = new ServiceParamter();

            serviceParamter.ServiceId = "sysUserGroup_insert";
            Dictionary<string, string> dict = new Dictionary<string, string>();
            List<string> strCtrls = new List<string>();
            strCtrls.Add("dbaEmp_code");
            strCtrls.Add("group_code");
            strCtrls.Add("group_name");
            strCtrls.Add("group_desc");
            FormHelper.GenerateParamerByControl(this.Controls, ref dict, strCtrls);
            serviceParamter.Paramters = dict;

           return  _helper.WirteSql(serviceParamter);
        
        }

        #endregion

        #region 其他
        private bool Validate()
        {
           
            if(string.IsNullOrEmpty( group_code.Text)      )
            {
                MessageForm.Warning("角色编码不能为空!");
                return false;
            }
            if ( string.IsNullOrEmpty(group_name.Text))
            {
                MessageForm.Warning("角色名称不能为空!");
                return false;
            }

            if (FormHelper.Text_Length (group_code.Text) >20)
            {
                MessageForm.Warning("角色编码字符串长度超过数据库允许的长度20!");
                return false;
            }
            if (FormHelper.Text_Length(group_name.Text) > 20)
            {
                MessageForm.Warning("角色名称字符串长度超过数据库允许的长度20!");
                return false;
            }
            if (FormHelper.Text_Length(group_desc.Text) > 40)
            {
                MessageForm.Warning("角色描述字符串长度超过数据库允许的长度40!");
                return false;
            }
            return true;
        }

        private void Init()
        {
            this.dbaEmp_code.Visible = false;
            this.dbaEmp_code.Text = G_User.user.User_id;
            this.StartPosition = FormStartPosition.CenterParent;
            this.group_code.Properties.MaxLength = 20;
            this.group_name.Properties.MaxLength = 20;
            this.group_desc.Properties.MaxLength = 40;
        }

     
        #endregion
    }
}
