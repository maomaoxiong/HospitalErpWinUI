﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.user.usergroup
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        private List<string> _serviceIds { get; set; }
        private List<Perm> _perms { get; set; }
        private string _currentCode { get; set; }
        public update(DaoHelper helper, List<Perm> perms, string code)
        {
            _helper = helper;
          //  _serviceIds = serviceIds;
            _perms = perms;
            _currentCode = code;

            InitializeComponent();
            Init();
        }
      
        public update()
        {
            InitializeComponent();
            Init();
        }

        private void UpdateFrm_Load(object sender, EventArgs e)
        {
            LoadDate();
        }

        #region 事件
        private void sysUserGroup_update_Click(object sender, EventArgs e)
        {
            // 	select group_code, group_name, group_desc from sys_group where group_code = ?
            if (!Validate())
            {
                return;
            }
            try
            {
                Update();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
        #endregion

        #region 其他
        private bool Validate()
        {

            if (string.IsNullOrEmpty(group_code.Text))
            {
                MessageForm.Warning("角色编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(group_name.Text))
            {
                MessageForm.Warning("角色名称不能为空!");
                return false;
            }

            if (FormHelper.Text_Length(group_code.Text) > 20)
            {
                MessageForm.Warning("角色编码字符串长度超过数据库允许的长度20!");
                return false;
            }
            if (FormHelper.Text_Length(group_name.Text) > 20)
            {
                MessageForm.Warning("角色名称字符串长度超过数据库允许的长度20!");
                return false;
            }
            if (FormHelper.Text_Length(group_desc.Text) > 40)
            {
                MessageForm.Warning("角色描述字符串长度超过数据库允许的长度40!");
                return false;
            }
            return true;
        }

        private void Init()
        {
            this.StartPosition = FormStartPosition.CenterParent; 
           
            this.StartPosition = FormStartPosition.CenterParent;
            this.group_code.Properties.ReadOnly = true;
            this.group_code.Properties.MaxLength = 20;
            this.group_name.Properties.MaxLength = 20;
            this.group_desc.Properties.MaxLength = 40;
        }

        #endregion

        #region 获取数据

        private void LoadDate()
        {
            string sqlstr = "select group_code, group_name, group_desc from sys_group where group_code = '?'";
            sqlstr = sqlstr.Replace("?",_currentCode);
            DataTable dt = _helper.ExecReadSql(sqlstr);
            if(dt != null && dt.Rows.Count !=0 )
            {
                this.group_code.Text = dt.Rows[0]["group_code"].ToString();
                this.group_name.Text = dt.Rows[0]["group_name"].ToString();
                this.group_desc.Text = dt.Rows[0]["group_desc"].ToString();
            }

        }
        private bool Update()
        {
            ServiceParamter serviceParamter = new ServiceParamter();

            serviceParamter.ServiceId = "sysUserGroup_update";
            Dictionary<string, string> dict = new Dictionary<string, string>();
            List<string> strCtrls = new List<string>();
         
            strCtrls.Add("group_code");
            strCtrls.Add("group_name");
            strCtrls.Add("group_desc");
            FormHelper.GenerateParamerByControl(this.Controls, ref dict, strCtrls);
            serviceParamter.Paramters = dict;

            return _helper.WirteSql(serviceParamter);

        }
        #endregion 

    }
}
