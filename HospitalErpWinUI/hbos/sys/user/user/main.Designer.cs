﻿namespace HospitalErpWinUI.hbos.sys.user.user
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Column8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.sysUser_delete = new DevExpress.XtraEditors.SimpleButton();
            this.setGroup = new DevExpress.XtraEditors.SimpleButton();
            this.sysUserPwd_update = new DevExpress.XtraEditors.SimpleButton();
            this.sysUser_insert = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.sysUser_select = new DevExpress.XtraEditors.SimpleButton();
            this.userGroup_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.user_name = new DevExpress.XtraEditors.TextEdit();
            this.user_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userGroup_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.gridControl1);
            this.panelControl1.Controls.Add(this.panelControl3);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Padding = new System.Windows.Forms.Padding(9, 0, 0, 0);
            this.panelControl1.Size = new System.Drawing.Size(1104, 630);
            this.panelControl1.TabIndex = 0;
            this.panelControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.panelControl1_Paint);
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(11, 66);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemHyperLinkEdit2,
            this.repositoryItemHyperLinkEdit3});
            this.gridControl1.Size = new System.Drawing.Size(1091, 562);
            this.gridControl1.TabIndex = 2;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.Column8,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "选中";
            this.gridColumn1.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn1.FieldName = "checked";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "用户编码";
            this.gridColumn2.ColumnEdit = this.repositoryItemHyperLinkEdit3;
            this.gridColumn2.FieldName = "user_code";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // repositoryItemHyperLinkEdit3
            // 
            this.repositoryItemHyperLinkEdit3.AutoHeight = false;
            this.repositoryItemHyperLinkEdit3.Name = "repositoryItemHyperLinkEdit3";
            this.repositoryItemHyperLinkEdit3.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit3_OpenLink);
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "用户名称";
            this.gridColumn3.FieldName = "user_name";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "用户描述";
            this.gridColumn4.FieldName = "user_desc";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // Column8
            // 
            this.Column8.Caption = "类别";
            this.Column8.FieldName = "Column1";
            this.Column8.Name = "Column8";
            this.Column8.Visible = true;
            this.Column8.VisibleIndex = 4;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "是否停用";
            this.gridColumn5.FieldName = "Column2";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 5;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "分配角色";
            this.gridColumn6.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.gridColumn6.FieldName = "Column3";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 6;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "复制权限";
            this.gridColumn7.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.gridColumn7.FieldName = "Column4";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 7;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.sysUser_delete);
            this.panelControl3.Controls.Add(this.setGroup);
            this.panelControl3.Controls.Add(this.sysUserPwd_update);
            this.panelControl3.Controls.Add(this.sysUser_insert);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(11, 34);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1091, 32);
            this.panelControl3.TabIndex = 1;
            this.panelControl3.Paint += new System.Windows.Forms.PaintEventHandler(this.panelControl3_Paint);
            // 
            // sysUser_delete
            // 
            this.sysUser_delete.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.sysUser_delete.Location = new System.Drawing.Point(293, 4);
            this.sysUser_delete.Name = "sysUser_delete";
            this.sysUser_delete.Size = new System.Drawing.Size(70, 23);
            this.sysUser_delete.TabIndex = 18;
            this.sysUser_delete.Text = "删除";
            this.sysUser_delete.Click += new System.EventHandler(this.sysUser_delete_Click);
            // 
            // setGroup
            // 
            this.setGroup.Location = new System.Drawing.Point(157, 4);
            this.setGroup.Name = "setGroup";
            this.setGroup.Size = new System.Drawing.Size(131, 23);
            this.setGroup.TabIndex = 17;
            this.setGroup.Text = "批量分配角色";
            this.setGroup.Click += new System.EventHandler(this.setGroup_Click);
            // 
            // sysUserPwd_update
            // 
            this.sysUserPwd_update.Location = new System.Drawing.Point(80, 4);
            this.sysUserPwd_update.Name = "sysUserPwd_update";
            this.sysUserPwd_update.Size = new System.Drawing.Size(70, 23);
            this.sysUserPwd_update.TabIndex = 16;
            this.sysUserPwd_update.Text = "重置密码";
            this.sysUserPwd_update.Click += new System.EventHandler(this.sysUserPwd_update_Click);
            // 
            // sysUser_insert
            // 
            this.sysUser_insert.Location = new System.Drawing.Point(5, 4);
            this.sysUser_insert.Name = "sysUser_insert";
            this.sysUser_insert.Size = new System.Drawing.Size(70, 23);
            this.sysUser_insert.TabIndex = 15;
            this.sysUser_insert.Text = "添加";
            this.sysUser_insert.Click += new System.EventHandler(this.sysUser_insert_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.sysUser_select);
            this.panelControl2.Controls.Add(this.userGroup_code);
            this.panelControl2.Controls.Add(this.user_name);
            this.panelControl2.Controls.Add(this.user_code);
            this.panelControl2.Controls.Add(this.labelControl3);
            this.panelControl2.Controls.Add(this.labelControl2);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(11, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1091, 32);
            this.panelControl2.TabIndex = 0;
            this.panelControl2.Paint += new System.Windows.Forms.PaintEventHandler(this.panelControl2_Paint);
            // 
            // sysUser_select
            // 
            this.sysUser_select.Location = new System.Drawing.Point(646, 4);
            this.sysUser_select.Name = "sysUser_select";
            this.sysUser_select.Size = new System.Drawing.Size(70, 23);
            this.sysUser_select.TabIndex = 22;
            this.sysUser_select.Text = "查询";
            this.sysUser_select.Click += new System.EventHandler(this.sysUser_select_Click);
            // 
            // userGroup_code
            // 
            this.userGroup_code.Location = new System.Drawing.Point(493, 5);
            this.userGroup_code.Name = "userGroup_code";
            this.userGroup_code.Properties.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.userGroup_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.userGroup_code.Size = new System.Drawing.Size(148, 20);
            this.userGroup_code.TabIndex = 21;
            this.userGroup_code.TextChanged += new System.EventHandler(this.userGroup_code_TextChanged);
            // 
            // user_name
            // 
            this.user_name.EditValue = "";
            this.user_name.Location = new System.Drawing.Point(292, 4);
            this.user_name.Name = "user_name";
            this.user_name.Size = new System.Drawing.Size(148, 20);
            this.user_name.TabIndex = 6;
            // 
            // user_code
            // 
            this.user_code.Location = new System.Drawing.Point(66, 4);
            this.user_code.Name = "user_code";
            this.user_code.Size = new System.Drawing.Size(148, 20);
            this.user_code.TabIndex = 5;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(461, 8);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(24, 14);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "角色";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(234, 9);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(48, 14);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "用户名称";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 10);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(48, 14);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "用户编码";
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1104, 630);
            this.Controls.Add(this.panelControl1);
            this.Name = "main";
            this.Text = "用户管理";
            this.Load += new System.EventHandler(this.main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userGroup_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_code.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit user_code;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit user_name;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.ComboBoxEdit userGroup_code;
        private DevExpress.XtraEditors.SimpleButton sysUser_select;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton sysUser_delete;
        private DevExpress.XtraEditors.SimpleButton setGroup;
        private DevExpress.XtraEditors.SimpleButton sysUserPwd_update;
        private DevExpress.XtraEditors.SimpleButton sysUser_insert;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn Column8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
    }
}