﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.user.user
{
    public partial class js_list : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        private string _user_code { get; set; }
        private string _user_name { get; set; }

        public js_list(DaoHelper helper, List<Perm> perms, string user_code,string user_name)
        {
            _helper = helper;
            _perms = perms;
            _user_code = user_code;
            _user_name = user_name;
            InitializeComponent();
            Init();
        }

        public js_list()
        {
            InitializeComponent();
            Init();
        }

   


        #region 其他
        private void Init()
        {          
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.StartPosition = FormStartPosition.CenterScreen;
        }
        #endregion

       

        #region 事件
        private void js_list_Load(object sender, EventArgs e)
        {
            user_code.Text = _user_code;
            user_name.Text = _user_name;
            LoadGroup();
        }
        private void jsfp_insert_detail_Click(object sender, EventArgs e)
        {
            bool value = false;
            string realValue = "0";
            string strSelected = string.Empty;

            try
            {
                List<string> groupsCheckes = new List<string>();
                List<string> groupsNoCheckes = new List<string>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    value = Convert.ToBoolean(gridView1.GetDataRow(i)["checked"]);
                    realValue = gridView1.GetDataRow(i)["_checkbox_value"].ToString();
                    if (value && realValue.Equals("0"))
                    {
                        string group_id = gridView1.GetDataRow(i)["_group_id"].ToString();
                        groupsCheckes.Add(group_id);

                    }
                    if (!value && realValue.Equals("1"))
                    {
                        string group_id = gridView1.GetDataRow(i)["_group_id"].ToString();
                        groupsNoCheckes.Add(group_id);

                    }
                }
                if (groupsCheckes.Count == 0 && groupsNoCheckes.Count == 0)
                {
                    MessageForm.Show("请选择要角色!");
                    return;
                }

                foreach (string group in groupsCheckes)
                {
                    if (!setGroup(_user_code, group,"1"))
                    {
                        break;
                    }
                }

                foreach (string group in groupsNoCheckes)
                {
                    if (!setGroup(_user_code, group, "0"))
                    {
                        break;
                    }
                }
                MessageForm.Show("操作成功!");
                LoadGroup();
                //   this.Close();               

            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        #endregion

        #region 访问数据
        private void LoadGroup()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "jsfp_insert_select";

            paramters.Add("curr_user", G_User.user.User_id);
            paramters.Add("user_code", _user_code);
            paramters.Add("user_name", _user_name);

            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["_checkbox_value"].ToString().Equals("1"))
                {
                    dt.Rows[i]["checked"] = "true";
                }
                else
                {
                    dt.Rows[i]["checked"] = "false";
                }
               
            }

            gridControl1.DataSource = dt;

        }

        private bool setGroup(string  user_code,string group_id, string isExise)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "jsfp_insert_detail";
            dict.Add("user_code", user_code);
            dict.Add("group_id", group_id);
            dict.Add("isexists", isExise);

            serviceParamter.Paramters = dict;

            return _helper.WirteSql(serviceParamter);

        }
        #endregion
    }
}
