﻿namespace HospitalErpWinUI.hbos.sys.user.user
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.close_buteen = new DevExpress.XtraEditors.SimpleButton();
            this.sysUser_update = new DevExpress.XtraEditors.SimpleButton();
            this.is_dba = new DevExpress.XtraEditors.ComboBoxEdit();
            this.emp_code = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.is_stop = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.comp_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.user_desc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.user_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.user_code = new DevExpress.XtraEditors.TextEdit();
            this.parent_code = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.is_dba.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emp_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_stop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_desc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.parent_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // close_buteen
            // 
            this.close_buteen.Location = new System.Drawing.Point(203, 391);
            this.close_buteen.Name = "close_buteen";
            this.close_buteen.Size = new System.Drawing.Size(80, 25);
            this.close_buteen.TabIndex = 30;
            this.close_buteen.Text = "关闭";
            this.close_buteen.Click += new System.EventHandler(this.close_buteen_Click);
            // 
            // sysUser_update
            // 
            this.sysUser_update.Location = new System.Drawing.Point(79, 391);
            this.sysUser_update.Name = "sysUser_update";
            this.sysUser_update.Size = new System.Drawing.Size(80, 25);
            this.sysUser_update.TabIndex = 29;
            this.sysUser_update.Text = "修改";
            this.sysUser_update.Click += new System.EventHandler(this.sysUser_update_Click);
            // 
            // is_dba
            // 
            this.is_dba.Location = new System.Drawing.Point(132, 295);
            this.is_dba.Name = "is_dba";
            this.is_dba.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.is_dba.Properties.Appearance.Options.UseBackColor = true;
            this.is_dba.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_dba.Size = new System.Drawing.Size(200, 24);
            this.is_dba.TabIndex = 28;
            // 
            // emp_code
            // 
            this.emp_code.EditValue = "  ";
            this.emp_code.Location = new System.Drawing.Point(132, 212);
            this.emp_code.Name = "emp_code";
            this.emp_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.emp_code.Size = new System.Drawing.Size(200, 24);
            this.emp_code.TabIndex = 26;
            // 
            // is_stop
            // 
            this.is_stop.Location = new System.Drawing.Point(132, 253);
            this.is_stop.Name = "is_stop";
            this.is_stop.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.is_stop.Properties.Appearance.Options.UseBackColor = true;
            this.is_stop.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_stop.Size = new System.Drawing.Size(200, 24);
            this.is_stop.TabIndex = 27;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(51, 298);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(60, 18);
            this.labelControl9.TabIndex = 14;
            this.labelControl9.Text = "类      别";
            // 
            // comp_code
            // 
            this.comp_code.Location = new System.Drawing.Point(132, 170);
            this.comp_code.Name = "comp_code";
            this.comp_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.comp_code.Properties.Appearance.Options.UseBackColor = true;
            this.comp_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comp_code.Size = new System.Drawing.Size(200, 24);
            this.comp_code.TabIndex = 25;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(51, 215);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(60, 18);
            this.labelControl7.TabIndex = 16;
            this.labelControl7.Text = "职      工";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(51, 256);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(60, 18);
            this.labelControl8.TabIndex = 17;
            this.labelControl8.Text = "是否停用";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(51, 173);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(60, 18);
            this.labelControl6.TabIndex = 19;
            this.labelControl6.Text = "单      位";
            // 
            // user_desc
            // 
            this.user_desc.Location = new System.Drawing.Point(132, 128);
            this.user_desc.Name = "user_desc";
            this.user_desc.Size = new System.Drawing.Size(200, 24);
            this.user_desc.TabIndex = 22;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(51, 131);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 18);
            this.labelControl3.TabIndex = 13;
            this.labelControl3.Text = "用户描述";
            // 
            // user_name
            // 
            this.user_name.Location = new System.Drawing.Point(132, 87);
            this.user_name.Name = "user_name";
            this.user_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.user_name.Properties.Appearance.Options.UseBackColor = true;
            this.user_name.Size = new System.Drawing.Size(200, 24);
            this.user_name.TabIndex = 21;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(51, 90);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 18);
            this.labelControl2.TabIndex = 12;
            this.labelControl2.Text = "用户名称";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(51, 49);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 18);
            this.labelControl1.TabIndex = 11;
            this.labelControl1.Text = "用户编码";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(51, 341);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(75, 18);
            this.labelControl5.TabIndex = 31;
            this.labelControl5.Text = "上级管理员";
            // 
            // user_code
            // 
            this.user_code.Enabled = false;
            this.user_code.Location = new System.Drawing.Point(132, 46);
            this.user_code.Name = "user_code";
            this.user_code.Size = new System.Drawing.Size(200, 24);
            this.user_code.TabIndex = 32;
            // 
            // parent_code
            // 
            this.parent_code.Enabled = false;
            this.parent_code.Location = new System.Drawing.Point(132, 338);
            this.parent_code.Name = "parent_code";
            this.parent_code.Size = new System.Drawing.Size(200, 24);
            this.parent_code.TabIndex = 33;
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 485);
            this.Controls.Add(this.parent_code);
            this.Controls.Add(this.user_code);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.close_buteen);
            this.Controls.Add(this.sysUser_update);
            this.Controls.Add(this.is_dba);
            this.Controls.Add(this.emp_code);
            this.Controls.Add(this.is_stop);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.comp_code);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.user_desc);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.user_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Name = "update";
            this.Text = "编辑";
            this.Load += new System.EventHandler(this.update_Load);
            ((System.ComponentModel.ISupportInitialize)(this.is_dba.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emp_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_stop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_desc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.parent_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton close_buteen;
        private DevExpress.XtraEditors.SimpleButton sysUser_update;
        private DevExpress.XtraEditors.ComboBoxEdit is_dba;
        private DevExpress.XtraEditors.SearchLookUpEdit emp_code;
        private DevExpress.XtraEditors.ComboBoxEdit is_stop;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.ComboBoxEdit comp_code;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit user_desc;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit user_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit user_code;
        private DevExpress.XtraEditors.ComboBoxEdit parent_code;
    }
}