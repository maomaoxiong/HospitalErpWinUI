﻿namespace HospitalErpWinUI.hbos.sys.user.user
{
    partial class copy_perm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.user_code = new DevExpress.XtraEditors.TextEdit();
            this.user_name = new DevExpress.XtraEditors.TextEdit();
            this.target_user = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.sysUser_copyperm = new DevExpress.XtraEditors.SimpleButton();
            this.close_Button = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.user_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.target_user.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(78, 34);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 18);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "用户编码";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(78, 80);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 18);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "用户名称";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(78, 126);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 18);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "目标用户";
            // 
            // user_code
            // 
            this.user_code.Enabled = false;
            this.user_code.Location = new System.Drawing.Point(163, 31);
            this.user_code.Name = "user_code";
            this.user_code.Size = new System.Drawing.Size(200, 24);
            this.user_code.TabIndex = 1;
            // 
            // user_name
            // 
            this.user_name.Enabled = false;
            this.user_name.Location = new System.Drawing.Point(163, 74);
            this.user_name.Name = "user_name";
            this.user_name.Size = new System.Drawing.Size(200, 24);
            this.user_name.TabIndex = 1;
            // 
            // target_user
            // 
            this.target_user.EditValue = "";
            this.target_user.Location = new System.Drawing.Point(163, 123);
            this.target_user.Name = "target_user";
            this.target_user.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.target_user.Properties.View = this.searchLookUpEdit1View;
            this.target_user.Size = new System.Drawing.Size(200, 24);
            this.target_user.TabIndex = 2;
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // sysUser_copyperm
            // 
            this.sysUser_copyperm.Location = new System.Drawing.Point(94, 177);
            this.sysUser_copyperm.Name = "sysUser_copyperm";
            this.sysUser_copyperm.Size = new System.Drawing.Size(100, 25);
            this.sysUser_copyperm.TabIndex = 3;
            this.sysUser_copyperm.Text = "权限复制";
            this.sysUser_copyperm.Click += new System.EventHandler(this.sysUser_copyperm_Click);
            // 
            // close_Button
            // 
            this.close_Button.Location = new System.Drawing.Point(240, 177);
            this.close_Button.Name = "close_Button";
            this.close_Button.Size = new System.Drawing.Size(100, 25);
            this.close_Button.TabIndex = 3;
            this.close_Button.Text = "关闭";
            this.close_Button.Click += new System.EventHandler(this.close_Button_Click);
            // 
            // copy_perm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 263);
            this.Controls.Add(this.close_Button);
            this.Controls.Add(this.sysUser_copyperm);
            this.Controls.Add(this.target_user);
            this.Controls.Add(this.user_name);
            this.Controls.Add(this.user_code);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Name = "copy_perm";
            this.Text = "复制权限";
            this.Load += new System.EventHandler(this.copy_perm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.user_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.target_user.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit user_code;
        private DevExpress.XtraEditors.TextEdit user_name;
        private DevExpress.XtraEditors.SearchLookUpEdit target_user;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraEditors.SimpleButton sysUser_copyperm;
        private DevExpress.XtraEditors.SimpleButton close_Button;
    }
}