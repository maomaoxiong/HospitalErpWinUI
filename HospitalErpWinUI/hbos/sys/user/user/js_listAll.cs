﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.user.user
{
    public partial class js_listAll : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        private string _user_codes { get; set; }

        public js_listAll(DaoHelper helper, List<Perm> perms, string user_codes)
        {
            _helper = helper;
            _perms = perms;
            _user_codes = user_codes;
            InitializeComponent();
            Init();
        }

        public js_listAll()
        {
            InitializeComponent();
            Init();
        }

        #region 其他
        private void Init()
        {
            this.Text = "批量分配角色";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.StartPosition = FormStartPosition.CenterScreen;
        }
        #endregion

        #region 事件

        private void js_listAllFrm_Load(object sender, EventArgs e)
        {
            LoadGroup();
        }

        private void jsfp_insert_detailAll_Click(object sender, EventArgs e)
        {
            bool value = false;
            string strSelected = string.Empty;

            try
            {
                List<string> groups = new List<string>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    value = Convert.ToBoolean(gridView1.GetDataRow(i)["checked"]);
                    if (value)
                    {
                        string code = gridView1.GetDataRow(i)["_group_id"].ToString();
                        groups.Add(code);

                    }
                }
                if (groups.Count == 0)
                {
                    MessageForm.Show("请选择要角色!");
                    return;
                }

                foreach (string group in groups)
                    {
                        if (!setGroup(group))
                        {
                            return;
                        }
                    }
                   MessageForm.Show("操作成功!");
                //   this.Close();               

            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }

        }

     
        #endregion

        #region 获取数据
        private void LoadGroup()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "jsfp_insert_selectAll";
           
            paramters.Add("curr_user", G_User.user.User_id);
            paramters.Add("user_code", _user_codes);
            paramters.Add("user_name", "");
          
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
          
            gridControl1.DataSource = dt;
           
        }

        private bool setGroup(string group_id)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "jsfp_insert_detailAll";
            dict.Add("user_code", _user_codes);
            dict.Add("group_id",group_id);
            dict.Add("isexists", "1");

            serviceParamter.Paramters = dict;

            return _helper.WirteSql(serviceParamter);

        }
        #endregion

      

    }
}
