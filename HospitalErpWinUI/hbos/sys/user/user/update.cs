﻿using Dao;
using DevExpress.XtraEditors;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.user.user
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        private string _user_code { get; set; }

        public update(DaoHelper helper, List<Perm> perms, string user_code)
        {
            _helper = helper;
            _perms = perms;
            _user_code = user_code;
            InitializeComponent();
            Init();
           
        }

        public update()
        {
            InitializeComponent();
            Init();
        }

        #region 其他
        private bool Validate()
        {
            
            if (string.IsNullOrEmpty(user_code.Text))
            {
                MessageForm.Warning("用户编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(user_name.Text))
            {
                MessageForm.Warning("用户名称不能为空!");
                return false;
            }
            //if (string.IsNullOrEmpty(user_desc.Text))
            //{
            //    MessageForm.Warning("用户描述不能为空!");
            //    return false;
            //}
        

            if (FormHelper.Text_Length(user_code.Text) > 20)
            {
                MessageForm.Warning("用户编码字符串长度超过数据库允许的长度20!");
                return false;
            }
            if (FormHelper.Text_Length(user_name.Text) > 40)
            {
                MessageForm.Warning("用户名称字符串长度超过数据库允许的长度40!");
                return false;
            }
            if (FormHelper.Text_Length(user_desc.Text) > 40)
            {
                MessageForm.Warning("用户描述字符串长度超过数据库允许的长度40!");
                return false;
            }
         
            return true;
        }

        private void Init()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.user_code.Properties.MaxLength = 20;
            this.user_name.Properties.MaxLength = 40;
            this.user_desc.Properties.MaxLength = 40;
         

            //   user_code.Text.
        }
      
        #endregion

        #region 访问数据
        private void LoadCompany()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "comp_code_para";
            paramters.Add("user_id", G_User.user.User_id);
            serviceParamter.Paramters = paramters;

            List<string> companys = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                companys.Add(codeValue.Code + " " + codeValue.Value);
            }
            comp_code.Properties.Items.AddRange(companys);
            comp_code.SelectedItem = companys[0];


        }
        private void LoadEmplyee()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "emp_code_para_spell";

            char[] array = { ' ' };
            // string [] strArray= userGroup_code.Text.Trim().Split(array);
            string[] strArray = System.Text.RegularExpressions.Regex.Split(comp_code.Text.Trim(), @"\s{1,}");
            paramters.Add("comp_code", strArray[0].ToString());
            paramters.Add("spell", null);
            serviceParamter.Paramters = paramters;

            List<CodeValue> emps = new List<CodeValue>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                emps.Add(codeValue);
            }
            //    emp_code.Properties.Items.AddRange(emps);
            emp_code.Properties.ValueMember = "Code";
            emp_code.Properties.DisplayMember = "Value";
            emp_code.Properties.DataSource = emps;



        }
        private void LoadIsStop()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "emp_is_stop";
            serviceParamter.Paramters = paramters;


            List<CodeValue> objs = _helper.ReadDictForData(serviceParamter);
            foreach (CodeValue codeValue in objs)
            {
                is_stop.Properties.Items.Add(codeValue.Code + " " + codeValue.Value);
            }
            is_stop.SelectedItem = is_stop.Properties.Items[0];
        }
        private void LoadUserType()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sys_emp_is_kind";
            serviceParamter.Paramters = paramters;


            List<CodeValue> objs = _helper.ReadDictForData(serviceParamter);
            foreach (CodeValue codeValue in objs)
            {
                is_dba.Properties.Items.Add(codeValue.Code + " " + codeValue.Value);
            }
            is_dba.SelectedItem = is_dba.Properties.Items[0];
        }
        private void LoadParent()
        { 
            
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sys_parent_user_dba";
            paramters.Add("user_code", _user_code);
            paramters.Add("py", null);
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++ )
            {
                parent_code.Properties.Items.Add(dt.Rows[i][0] + " " + dt.Rows[i][1]);
            }
            this.parent_code.SelectedItem = this.parent_code.Properties.Items[0];
        }
        private void LoadUser()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysUser_select_load";
            paramters.Add("user_code", _user_code);       
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);

            user_code.Text = dt.Rows[0][0].ToString();
            user_name.Text = dt.Rows[0][1].ToString();
            user_desc.Text = dt.Rows[0][2].ToString();

            FormHelper.ComboBoxeditAssignment(comp_code, dt.Rows[0][3].ToString());
            FormHelper.SearchLookUpEditAssignment(emp_code, dt.Rows[0][4].ToString().Replace("|||", " "));
            FormHelper.ComboBoxeditAssignment(is_stop, dt.Rows[0][5].ToString().ToLower().Equals("false") ? "0" : "1");
            FormHelper.ComboBoxeditAssignment(is_dba, dt.Rows[0][6].ToString().ToLower().Equals("false") ? "0" : "1");
            FormHelper.ComboBoxeditAssignment(parent_code,dt.Rows[0][7].ToString());
 
        }

        private bool updateUser()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sysUser_update";

            List<string> strConstrols = new List<string>();

            strConstrols.Add("user_code");
            strConstrols.Add("user_name");
            strConstrols.Add("user_desc");
            strConstrols.Add("comp_code");
            strConstrols.Add("emp_code");
            strConstrols.Add("is_stop");
            strConstrols.Add("is_dba");
            strConstrols.Add("parent_code");
           
            FormHelper.GenerateParamerByControl(this.Controls, ref dict,  strConstrols);

            serviceParamter.Paramters = dict;

            return _helper.WirteSql(serviceParamter);
        }
        
        #endregion

        #region 事件
       

        private void update_Load(object sender, EventArgs e)
        {
            LoadCompany();
            LoadEmplyee();
            LoadIsStop();
            LoadUserType();
            LoadParent();
            LoadUser();
        }

        private void close_buteen_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void sysUser_update_Click(object sender, EventArgs e)
        {
         
            if (!Validate())
            {
                return;
            }
            try
            {
                if (updateUser())
                {
                    this.DialogResult = System.Windows.Forms.DialogResult.Yes;
                    //  MessageForm.Information("操作成功");
                }
                else
                {
                    this.DialogResult = System.Windows.Forms.DialogResult.No;
                }
            }
            catch(Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }

        }

        #endregion

        
      

    }
}
