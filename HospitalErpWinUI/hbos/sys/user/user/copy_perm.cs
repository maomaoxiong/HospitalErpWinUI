﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.user.user
{
    public partial class copy_perm : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        private string _user_code { get; set; }
        public copy_perm()
        {
            InitializeComponent();
            Init();
        }
        public copy_perm(DaoHelper helper, List<Perm> perms, string user_code)
        {
            _helper = helper;
            _perms = perms;
            _user_code = user_code;
            InitializeComponent();
            Init();
        }

        #region 事件
        private void close_Button_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
        private void copy_perm_Load(object sender, EventArgs e)
        {
            LoadData();
            LoadEmplyee();
        }
        #endregion
      
        #region 其他
        private void Init()
        {
            this.StartPosition = FormStartPosition.CenterParent;
        }
        #endregion

        #region 访问数据
        
         private void LoadEmplyee()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysUser_sel1";
             paramters.Add("user_name", null);         
            serviceParamter.Paramters = paramters;
             

            List<CodeValue> users = new List<CodeValue>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                users.Add(codeValue);
            }
            //    emp_code.Properties.Items.AddRange(emps);
            target_user.Properties.ValueMember = "Code";
            target_user.Properties.DisplayMember = "Value";
            target_user.Properties.DataSource = users;

        }

         private bool CopyPerm()
         {
             Dictionary<string, string> dict = new Dictionary<string, string>();
             ServiceParamter serviceParamter = new ServiceParamter();
             serviceParamter.ServiceId = "sysUser_copyperm";

             List<string> strConstrols = new List<string>();

             strConstrols.Add("user_code");
             strConstrols.Add("user_name");
             strConstrols.Add("target_user");


             FormHelper.GenerateParamerByControl(this.Controls, ref dict, strConstrols);

             serviceParamter.Paramters = dict;

             return _helper.WirteSql(serviceParamter);
         }

         private void LoadData()
         { 
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysUser_select_load";
            paramters.Add("user_code", _user_code);       
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);

            user_code.Text = dt.Rows[0][0].ToString();
            user_name.Text = dt.Rows[0][1].ToString();
          
 
         }
        #endregion 

         private void sysUser_copyperm_Click(object sender, EventArgs e)
         {
             try
             {
                 if (CopyPerm())
                 {
                     this.DialogResult = System.Windows.Forms.DialogResult.Yes;
                     //  MessageForm.Information("操作成功");
                 }
                 else
                 {
                     this.DialogResult = System.Windows.Forms.DialogResult.No;
                 }
             }
             catch (Exception ex)
             {
                 MessageForm.Exception(ex.Message);
             }
         }

        
    }
}
;