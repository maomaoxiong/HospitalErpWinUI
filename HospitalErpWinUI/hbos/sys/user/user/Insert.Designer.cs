﻿namespace HospitalErpWinUI.hbos.sys.user.user
{
    partial class insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.user_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.user_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.user_desc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.password = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.pswd = new DevExpress.XtraEditors.TextEdit();
            this.comp_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.emp_code = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.is_stop = new DevExpress.XtraEditors.ComboBoxEdit();
            this.is_dba = new DevExpress.XtraEditors.ComboBoxEdit();
            this.sysUser_insert = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.user_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_desc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.password.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pswd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emp_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_stop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_dba.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(53, 37);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 18);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "用户编码";
            // 
            // user_code
            // 
            this.user_code.Location = new System.Drawing.Point(134, 34);
            this.user_code.Name = "user_code";
            this.user_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.user_code.Properties.Appearance.Options.UseBackColor = true;
            this.user_code.Size = new System.Drawing.Size(200, 24);
            this.user_code.TabIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(53, 78);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 18);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "用户名称";
            // 
            // user_name
            // 
            this.user_name.Location = new System.Drawing.Point(134, 75);
            this.user_name.Name = "user_name";
            this.user_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.user_name.Properties.Appearance.Options.UseBackColor = true;
            this.user_name.Size = new System.Drawing.Size(200, 24);
            this.user_name.TabIndex = 1;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(53, 119);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 18);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "用户描述";
            // 
            // user_desc
            // 
            this.user_desc.Location = new System.Drawing.Point(134, 116);
            this.user_desc.Name = "user_desc";
            this.user_desc.Size = new System.Drawing.Size(200, 24);
            this.user_desc.TabIndex = 2;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(53, 161);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 18);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "用户密码";
            // 
            // password
            // 
            this.password.Location = new System.Drawing.Point(134, 158);
            this.password.Name = "password";
            this.password.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.password.Properties.Appearance.Options.UseBackColor = true;
            this.password.Size = new System.Drawing.Size(200, 24);
            this.password.TabIndex = 3;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(53, 203);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 18);
            this.labelControl5.TabIndex = 0;
            this.labelControl5.Text = "确认密码";
            // 
            // pswd
            // 
            this.pswd.Location = new System.Drawing.Point(134, 200);
            this.pswd.Name = "pswd";
            this.pswd.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.pswd.Properties.Appearance.Options.UseBackColor = true;
            this.pswd.Size = new System.Drawing.Size(200, 24);
            this.pswd.TabIndex = 4;
            // 
            // comp_code
            // 
            this.comp_code.Location = new System.Drawing.Point(134, 242);
            this.comp_code.Name = "comp_code";
            this.comp_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.comp_code.Properties.Appearance.Options.UseBackColor = true;
            this.comp_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comp_code.Size = new System.Drawing.Size(200, 24);
            this.comp_code.TabIndex = 5;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(53, 245);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(60, 18);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = "单      位";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(53, 287);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(60, 18);
            this.labelControl7.TabIndex = 0;
            this.labelControl7.Text = "职      工";
            // 
            // emp_code
            // 
            this.emp_code.EditValue = " \"\"";
            this.emp_code.Location = new System.Drawing.Point(134, 284);
            this.emp_code.Name = "emp_code";
            this.emp_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.emp_code.Size = new System.Drawing.Size(200, 24);
            this.emp_code.TabIndex = 6;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(53, 328);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(60, 18);
            this.labelControl8.TabIndex = 0;
            this.labelControl8.Text = "是否停用";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(53, 370);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(60, 18);
            this.labelControl9.TabIndex = 0;
            this.labelControl9.Text = "类      别";
            // 
            // is_stop
            // 
            this.is_stop.Location = new System.Drawing.Point(134, 325);
            this.is_stop.Name = "is_stop";
            this.is_stop.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.is_stop.Properties.Appearance.Options.UseBackColor = true;
            this.is_stop.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_stop.Size = new System.Drawing.Size(200, 24);
            this.is_stop.TabIndex = 7;
            // 
            // is_dba
            // 
            this.is_dba.Location = new System.Drawing.Point(134, 367);
            this.is_dba.Name = "is_dba";
            this.is_dba.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.is_dba.Properties.Appearance.Options.UseBackColor = true;
            this.is_dba.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_dba.Size = new System.Drawing.Size(200, 24);
            this.is_dba.TabIndex = 8;
            // 
            // sysUser_insert
            // 
            this.sysUser_insert.Location = new System.Drawing.Point(81, 417);
            this.sysUser_insert.Name = "sysUser_insert";
            this.sysUser_insert.Size = new System.Drawing.Size(80, 25);
            this.sysUser_insert.TabIndex = 9;
            this.sysUser_insert.Text = "添加";
            this.sysUser_insert.Click += new System.EventHandler(this.sysUser_insert_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(205, 417);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(80, 25);
            this.simpleButton2.TabIndex = 10;
            this.simpleButton2.Text = "关闭";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 485);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.sysUser_insert);
            this.Controls.Add(this.is_dba);
            this.Controls.Add(this.emp_code);
            this.Controls.Add(this.is_stop);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.comp_code);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.pswd);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.password);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.user_desc);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.user_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.user_code);
            this.Controls.Add(this.labelControl1);
            this.Name = "insert";
            this.Text = "添加用户";
            this.Load += new System.EventHandler(this.insert_Load);
            ((System.ComponentModel.ISupportInitialize)(this.user_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.user_desc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.password.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pswd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emp_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_stop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_dba.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit user_code;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit user_name;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit user_desc;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit password;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit pswd;
        private DevExpress.XtraEditors.ComboBoxEdit comp_code;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.SearchLookUpEdit emp_code;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.ComboBoxEdit is_stop;
        private DevExpress.XtraEditors.ComboBoxEdit is_dba;
        private DevExpress.XtraEditors.SimpleButton sysUser_insert;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
    }
}