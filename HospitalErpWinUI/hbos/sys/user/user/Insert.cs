﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.user.user
{
    public partial class insert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
    
        public insert()
        {
            InitializeComponent();
            Init();
            
        }

        public insert(DaoHelper helper, List<Perm> perms)
        {
            _helper = helper;
            _perms = perms;
          
            InitializeComponent();
            Init();
        }

       
        #region 事件
        private void insert_Load(object sender, EventArgs e)
        {
            LoadCompany();
            LoadEmplyee();
            LoadIsStop();
            LoadUserType();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void sysUser_insert_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Insert();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
        #endregion

        #region  访问数据

        private void LoadCompany()
        {           
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "comp_code_para";
            paramters.Add("user_id", G_User.user.User_id);
            serviceParamter.Paramters = paramters;

            List<string> companys= new List<string>();
            DataTable dt= _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++ )
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                companys.Add(codeValue.Code  +" " + codeValue.Value);
            }
            comp_code.Properties.Items.AddRange(companys);
            comp_code.SelectedItem = companys[0];
           

        }
        private void LoadEmplyee()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "emp_code_para_spell";

            char[] array = { ' ' };
            // string [] strArray= userGroup_code.Text.Trim().Split(array);
            string[] strArray = System.Text.RegularExpressions.Regex.Split(comp_code.Text.Trim(), @"\s{1,}");
            paramters.Add("comp_code", strArray[0].ToString());
            paramters.Add("spell", null);
            serviceParamter.Paramters = paramters;

            List<CodeValue> emps = new List<CodeValue>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                emps.Add(codeValue);
            }
        //    emp_code.Properties.Items.AddRange(emps);
            emp_code.Properties.ValueMember = "Code";
            emp_code.Properties.DisplayMember = "Value";
            emp_code.Properties.DataSource = emps;
           


        }
        private void LoadIsStop()   
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "emp_is_stop";         
            serviceParamter.Paramters = paramters;

         
            List<CodeValue> objs = _helper.ReadDictForData(serviceParamter);
            foreach (CodeValue codeValue in objs)
            {
                is_stop.Properties.Items.Add(codeValue.Code + " " + codeValue.Value);
            }
            is_stop.SelectedItem = is_stop.Properties.Items[0];           
        }
        private void LoadUserType()
        {            
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sys_emp_is_kind";
            serviceParamter.Paramters = paramters;


            List<CodeValue> objs = _helper.ReadDictForData(serviceParamter);
            foreach (CodeValue codeValue in objs)
            {
                is_dba.Properties.Items.Add(codeValue.Code + " " + codeValue.Value);
            }
            is_dba.SelectedItem = is_dba.Properties.Items[0];    
        }
        private bool Insert()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sysUser_insert";
            dict.Add ("current_userID", G_User.user.User_id);

            List<string> strCtrls = new List<string>();
            strCtrls.Add("user_code");
            strCtrls.Add("user_name");
            strCtrls.Add("user_desc");
            strCtrls.Add("password");
            strCtrls.Add("comp_code");
            strCtrls.Add("emp_code");
            strCtrls.Add("is_stop");
            strCtrls.Add("is_dba");

            FormHelper.GenerateParamerByControl(this.Controls, ref dict, strCtrls);
               
            serviceParamter.Paramters = dict;

            return _helper.WirteSql(serviceParamter);

        }
        #endregion

        #region 其他
        private bool Validate()
        {
            if(!this.password.Text.Equals(this.pswd.Text))
            {
                MessageForm.Warning("确认密码不一致!");
                return false;
            }
            if (string.IsNullOrEmpty(user_code.Text))
            {
                MessageForm.Warning("用户编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(user_name.Text))
            {
                MessageForm.Warning("用户名称不能为空!");
                return false;
            }
            //if (string.IsNullOrEmpty(user_desc.Text))
            //{
            //    MessageForm.Warning("用户描述不能为空!");
            //    return false;
            //}
            if (string.IsNullOrEmpty(password.Text))
            {
                MessageForm.Warning("密码不能为空!");
                return false;
            }

            if (FormHelper.Text_Length(user_code.Text) > 20)
            {
                MessageForm.Warning("用户编码字符串长度超过数据库允许的长度20!");
                return false;
            }
            if (FormHelper.Text_Length(user_name.Text) > 40)
            {
                MessageForm.Warning("用户名称字符串长度超过数据库允许的长度40!");
                return false;
            }
            if (FormHelper.Text_Length(user_desc.Text) > 40)
            {
                MessageForm.Warning("用户描述字符串长度超过数据库允许的长度40!");
                return false;
            }
            if (FormHelper.Text_Length(password.Text) > 15)
            {
                MessageForm.Warning("密码字符串长度超过数据库允许的长度15!");
                return false;
            }
            return true;
        }

        private void Init()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.user_code.Properties.MaxLength = 20;
            this.user_name.Properties.MaxLength = 40;
            this.user_desc.Properties.MaxLength = 40;
            this.password.Properties.MaxLength = 15;           

         //   user_code.Text.
        }
        #endregion

    
    }
}
