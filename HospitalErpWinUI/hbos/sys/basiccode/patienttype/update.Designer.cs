﻿namespace HospitalErpWinUI.hbos.sys.basiccode.patienttype
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.is_medicare = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.CloseBut = new DevExpress.XtraEditors.SimpleButton();
            this.AddBut = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.patienttype_flag = new DevExpress.XtraEditors.CheckEdit();
            this.patienttype_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.patienttype_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.is_medicare.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.patienttype_flag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.patienttype_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.patienttype_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // is_medicare
            // 
            this.is_medicare.Location = new System.Drawing.Point(161, 121);
            this.is_medicare.Name = "is_medicare";
            this.is_medicare.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.is_medicare.Properties.Appearance.Options.UseBackColor = true;
            this.is_medicare.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_medicare.Size = new System.Drawing.Size(200, 24);
            this.is_medicare.TabIndex = 26;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(51, 124);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 18);
            this.labelControl5.TabIndex = 25;
            this.labelControl5.Text = "是否医保";
            // 
            // CloseBut
            // 
            this.CloseBut.Location = new System.Drawing.Point(242, 210);
            this.CloseBut.Name = "CloseBut";
            this.CloseBut.Size = new System.Drawing.Size(80, 25);
            this.CloseBut.TabIndex = 23;
            this.CloseBut.Text = "关闭";
            this.CloseBut.Click += new System.EventHandler(this.CloseBut_Click);
            // 
            // AddBut
            // 
            this.AddBut.Location = new System.Drawing.Point(103, 210);
            this.AddBut.Name = "AddBut";
            this.AddBut.Size = new System.Drawing.Size(80, 25);
            this.AddBut.TabIndex = 24;
            this.AddBut.Text = "修改";
            this.AddBut.Click += new System.EventHandler(this.UpdateBut_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(51, 167);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 18);
            this.labelControl4.TabIndex = 22;
            this.labelControl4.Text = "是否停用";
            // 
            // patienttype_flag
            // 
            this.patienttype_flag.Location = new System.Drawing.Point(161, 166);
            this.patienttype_flag.Name = "patienttype_flag";
            this.patienttype_flag.Properties.Caption = "";
            this.patienttype_flag.Size = new System.Drawing.Size(75, 19);
            this.patienttype_flag.TabIndex = 21;
            // 
            // patienttype_name
            // 
            this.patienttype_name.Location = new System.Drawing.Point(161, 78);
            this.patienttype_name.Name = "patienttype_name";
            this.patienttype_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.patienttype_name.Properties.Appearance.Options.UseBackColor = true;
            this.patienttype_name.Size = new System.Drawing.Size(200, 24);
            this.patienttype_name.TabIndex = 19;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(51, 81);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(90, 18);
            this.labelControl2.TabIndex = 17;
            this.labelControl2.Text = "病人类别名称";
            // 
            // patienttype_code
            // 
            this.patienttype_code.Enabled = false;
            this.patienttype_code.Location = new System.Drawing.Point(161, 33);
            this.patienttype_code.Name = "patienttype_code";
            this.patienttype_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.patienttype_code.Properties.Appearance.Options.UseBackColor = true;
            this.patienttype_code.Size = new System.Drawing.Size(200, 24);
            this.patienttype_code.TabIndex = 20;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(51, 36);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(90, 18);
            this.labelControl1.TabIndex = 18;
            this.labelControl1.Text = "病人类别编码";
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 275);
            this.Controls.Add(this.is_medicare);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.CloseBut);
            this.Controls.Add(this.AddBut);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.patienttype_flag);
            this.Controls.Add(this.patienttype_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.patienttype_code);
            this.Controls.Add(this.labelControl1);
            this.Name = "update";
            this.Text = "修改";
            this.Load += new System.EventHandler(this.update_Load);
            ((System.ComponentModel.ISupportInitialize)(this.is_medicare.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.patienttype_flag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.patienttype_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.patienttype_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.ComboBoxEdit is_medicare;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton CloseBut;
        private DevExpress.XtraEditors.SimpleButton AddBut;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.CheckEdit patienttype_flag;
        private DevExpress.XtraEditors.TextEdit patienttype_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit patienttype_code;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}