﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.basiccode.patienttype
{
    public partial class insert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }

        public insert()
        {
            InitializeComponent();
            Init();
        }

        public insert(DaoHelper helper, List<Perm> perms)
        {
            _helper = helper;
            _perms = perms;

            InitializeComponent();
            Init();
        }

        #region 其他
        private void Init()
        {
            this.StartPosition = FormStartPosition.CenterParent;
            this.patienttype_code.Properties.MaxLength = 20;
            this.patienttype_name.Properties.MaxLength = 20;
           
        }

        private bool Validate()
        {

            if (string.IsNullOrEmpty(patienttype_code.Text))
            {
                MessageForm.Warning("病人类别编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(patienttype_name.Text))
            {
                MessageForm.Warning("病人类别名称不能为空!");
                return false;
            }
           
            return true;
        }
        #endregion

        #region 数据访问
        private void LoadYesNo()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "dict_yes_or_no";          
            serviceParamter.Paramters = paramters;

            List<CodeValue> codeValues = _helper.ReadDictForData(serviceParamter);
            List<string> objs = new List<string>();
            foreach (CodeValue codeValue in codeValues)
            {
                objs.Add( codeValue.Value);
            }

            is_medicare.Properties.Items.AddRange(objs);
            is_medicare.SelectedItem = objs[0];


        }
        private bool Insert()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r patienttype_code="05" patienttype_name="医疗纠纷病人" patienttype_flag="1" insertFlag="1" is_medicare="0"></r></root>
            serviceParamter.ServiceId = "sysBasiccodePatienttype_save";
            Dictionary<string, string> dict = new Dictionary<string, string>();
            StringBuilder str = new StringBuilder();
            str.Append(" <root><r ");
            str.Append("patienttype_code=\"" + patienttype_code.Text + "\" ");
            str.Append("patienttype_name=\"" + patienttype_name.Text + "\" ");
            str.Append("is_medicare=\"" + FormHelper.GetValueForComboBoxEdit(is_medicare) + "\" "); 
            int flag = 0;
            if (Convert.ToBoolean(patienttype_flag.EditValue))
                flag = 1;
            else
                flag = 0;

            str.Append("patienttype_flag=\"" + flag + "\" ");
            str.Append("insertFlag=\"2\" ");
            str.Append("></r></root>");

            dict.Add("doc", str.ToString());
            serviceParamter.Paramters = dict;

            return _helper.WirteSql(serviceParamter);

        }
        #endregion

        private void insert_Load(object sender, EventArgs e)
        {
            LoadYesNo();
        }

        private void AddBut_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Insert();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }

        private void CloseBut_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
