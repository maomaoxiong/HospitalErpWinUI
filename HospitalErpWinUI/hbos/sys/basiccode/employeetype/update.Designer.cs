﻿namespace HospitalErpWinUI.hbos.sys.basiccode.employeetype
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DeleteBut = new DevExpress.XtraEditors.SimpleButton();
            this.UpdateBut = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.employeetype_flag = new DevExpress.XtraEditors.CheckEdit();
            this.employeetype_desc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.employeetype_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.employeetype_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.employeetype_flag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeetype_desc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeetype_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeetype_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // DeleteBut
            // 
            this.DeleteBut.Location = new System.Drawing.Point(233, 216);
            this.DeleteBut.Name = "DeleteBut";
            this.DeleteBut.Size = new System.Drawing.Size(80, 25);
            this.DeleteBut.TabIndex = 23;
            this.DeleteBut.Text = "关闭";
            this.DeleteBut.Click += new System.EventHandler(this.DeleteBut_Click);
            // 
            // UpdateBut
            // 
            this.UpdateBut.Location = new System.Drawing.Point(94, 216);
            this.UpdateBut.Name = "UpdateBut";
            this.UpdateBut.Size = new System.Drawing.Size(80, 25);
            this.UpdateBut.TabIndex = 24;
            this.UpdateBut.Text = "修改";
            this.UpdateBut.Click += new System.EventHandler(this.UpdateBut_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(51, 169);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 18);
            this.labelControl4.TabIndex = 22;
            this.labelControl4.Text = "是否停用";
            // 
            // employeetype_flag
            // 
            this.employeetype_flag.Location = new System.Drawing.Point(161, 168);
            this.employeetype_flag.Name = "employeetype_flag";
            this.employeetype_flag.Properties.Caption = "";
            this.employeetype_flag.Size = new System.Drawing.Size(75, 19);
            this.employeetype_flag.TabIndex = 21;
            // 
            // employeetype_desc
            // 
            this.employeetype_desc.Location = new System.Drawing.Point(161, 124);
            this.employeetype_desc.Name = "employeetype_desc";
            this.employeetype_desc.Size = new System.Drawing.Size(200, 24);
            this.employeetype_desc.TabIndex = 18;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(51, 127);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(30, 18);
            this.labelControl3.TabIndex = 15;
            this.labelControl3.Text = "描述";
            // 
            // employeetype_name
            // 
            this.employeetype_name.Location = new System.Drawing.Point(161, 79);
            this.employeetype_name.Name = "employeetype_name";
            this.employeetype_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.employeetype_name.Properties.Appearance.Options.UseBackColor = true;
            this.employeetype_name.Size = new System.Drawing.Size(200, 24);
            this.employeetype_name.TabIndex = 19;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(51, 82);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 18);
            this.labelControl2.TabIndex = 16;
            this.labelControl2.Text = "职称名称";
            // 
            // employeetype_code
            // 
            this.employeetype_code.Enabled = false;
            this.employeetype_code.Location = new System.Drawing.Point(161, 33);
            this.employeetype_code.Name = "employeetype_code";
            this.employeetype_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.employeetype_code.Properties.Appearance.Options.UseBackColor = true;
            this.employeetype_code.Size = new System.Drawing.Size(200, 24);
            this.employeetype_code.TabIndex = 20;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(51, 36);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 18);
            this.labelControl1.TabIndex = 17;
            this.labelControl1.Text = "职称编码";
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 275);
            this.Controls.Add(this.DeleteBut);
            this.Controls.Add(this.UpdateBut);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.employeetype_flag);
            this.Controls.Add(this.employeetype_desc);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.employeetype_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.employeetype_code);
            this.Controls.Add(this.labelControl1);
            this.Name = "update";
            this.Text = "修改";
            this.Load += new System.EventHandler(this.update_Load);
            ((System.ComponentModel.ISupportInitialize)(this.employeetype_flag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeetype_desc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeetype_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeetype_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton DeleteBut;
        private DevExpress.XtraEditors.SimpleButton UpdateBut;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.CheckEdit employeetype_flag;
        private DevExpress.XtraEditors.TextEdit employeetype_desc;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit employeetype_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit employeetype_code;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}