﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.basiccode.employeetype
{
    public partial class insert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }

        public insert()
        {
            InitializeComponent();
            Init();
        }

        public insert(DaoHelper helper, List<Perm> perms)
        {
            _helper = helper;
            _perms = perms;

            InitializeComponent();
            Init();
        }

        #region 其他
        private void Init()
        {
            this.StartPosition = FormStartPosition.CenterParent;
            this.employeetype_code.Properties.MaxLength = 20;
            this.employeetype_name.Properties.MaxLength = 20;
            this.employeetype_desc.Properties.MaxLength = 40;
        }

        private bool Validate()
        {

            if (string.IsNullOrEmpty(employeetype_code.Text))
            {
                MessageForm.Warning("职称编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(employeetype_name.Text))
            {
                MessageForm.Warning("职称名称不能为空!");
                return false;
            }


            return true;
        }
        #endregion

        #region 数据访问
        private bool Insert()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
          
            serviceParamter.ServiceId = "sysBasiccodeEmployeetype_save";
            Dictionary<string, string> dict = new Dictionary<string, string>();
            StringBuilder str = new StringBuilder();
            str.Append(" <root><r ");
            str.Append("employeetype_code=\"" + employeetype_code.Text + "\" ");
            str.Append("employeetype_name=\"" + employeetype_name.Text + "\" ");
            str.Append("employeetype_desc=\"" + employeetype_desc.Text + "\" ");
            int flag = 0;
            if (Convert.ToBoolean(employeetype_flag.EditValue))
                flag = 1;
            else
                flag = 0;

            str.Append("employeetype_flag=\"" + flag + "\" ");
            str.Append("insertFlag=\"2\" ");
            str.Append("></r></root>");

            dict.Add("doc", str.ToString());
            serviceParamter.Paramters = dict;

            return _helper.WirteSql(serviceParamter);

        }
        #endregion

        private void CloseBut_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void AddBut_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Insert();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }

        private void insert_Load(object sender, EventArgs e)
        {

        }
    }
}
