﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.basiccode.knowledgedegree
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        private string _code;
        private List<Perm> _perms { get; set; }

        public update()
        {
            InitializeComponent();
            Init();
        }

        public update(DaoHelper helper, List<Perm> perms, string code)
        {
            _helper = helper;
            _perms = perms;
            _code = code;
            InitializeComponent();
            Init();
        }

        #region 其他
        private void Init()
        {
            this.StartPosition = FormStartPosition.CenterParent;
            this.konwledgedegree_code.Properties.MaxLength = 20;
            this.konwledgedegree_name.Properties.MaxLength = 20;
            this.konwledgedegree_desc.Properties.MaxLength = 40;        
        }

        private bool Validate()
        {
            if (string.IsNullOrEmpty(konwledgedegree_code.Text))
            {
                MessageForm.Warning("学历编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(konwledgedegree_name.Text))
            {
                MessageForm.Warning("学历名称不能为空!");
                return false;
            }
            return true;
        }
        #endregion

        #region 数据访问
        private void LoadDate()
        {
            string sqlstr = "select degree_code _degree_code,degree_name _degree_name,degree_desc degree_desc,is_stop is_stop from sys_emp_degree	where degree_code = '?'";
            sqlstr = sqlstr.Replace("?", _code);
            DataTable dt = _helper.ExecReadSql(sqlstr);
            if (dt != null && dt.Rows.Count != 0)
            {
                this.konwledgedegree_code.Text = dt.Rows[0]["_degree_code"].ToString();
                this.konwledgedegree_name.Text = dt.Rows[0]["_degree_name"].ToString();
                this.konwledgedegree_desc.Text = dt.Rows[0]["degree_desc"].ToString();
             
                this.konwledgedegree_flag.Checked = dt.Rows[0]["is_stop"].ToString().ToLower().Equals("true");
            }

        }
        private bool Update()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r konwledgedegree_code="14" konwledgedegree_name="dd" konwledgedegree_desc=""  konwledgedegree_flag="0"  insertFlag="1"></r></root>
            serviceParamter.ServiceId = "sysBasiccodeKnowledgedegree_save";
            Dictionary<string, string> dict = new Dictionary<string, string>();
            StringBuilder str = new StringBuilder();
            str.Append(" <root><r ");
            str.Append("konwledgedegree_code=\"" + konwledgedegree_code.Text + "\" ");
            str.Append("konwledgedegree_name=\"" + konwledgedegree_name.Text + "\" ");
            str.Append("konwledgedegree_desc=\"" + konwledgedegree_desc.Text + "\" ");
            int flag = 0;
            if (Convert.ToBoolean(konwledgedegree_flag.EditValue))
                flag = 1;
            else
                flag = 0;

            str.Append("konwledgedegree_flag=\"" + flag + "\" ");
            str.Append("insertFlag=\"1\" ");
            str.Append("></r></root>");

            dict.Add("doc", str.ToString());
            serviceParamter.Paramters = dict;

            return _helper.WirteSql(serviceParamter);

        }
        #endregion

        private void CloseBut_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void Updatebut_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Update();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }

        private void update_Load(object sender, EventArgs e)
        {
            LoadDate();
        }
    }
}
