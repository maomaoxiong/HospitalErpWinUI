﻿namespace HospitalErpWinUI.hbos.sys.basiccode.knowledgedegree
{
    partial class insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.konwledgedegree_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.konwledgedegree_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.konwledgedegree_desc = new DevExpress.XtraEditors.TextEdit();
            this.konwledgedegree_flag = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.Addbut = new DevExpress.XtraEditors.SimpleButton();
            this.CloseBut = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.konwledgedegree_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.konwledgedegree_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.konwledgedegree_desc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.konwledgedegree_flag.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(48, 30);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 18);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "学历编码";
            // 
            // konwledgedegree_code
            // 
            this.konwledgedegree_code.Location = new System.Drawing.Point(158, 27);
            this.konwledgedegree_code.Name = "konwledgedegree_code";
            this.konwledgedegree_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.konwledgedegree_code.Properties.Appearance.Options.UseBackColor = true;
            this.konwledgedegree_code.Size = new System.Drawing.Size(200, 24);
            this.konwledgedegree_code.TabIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(48, 76);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 18);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "学历名称";
            // 
            // konwledgedegree_name
            // 
            this.konwledgedegree_name.Location = new System.Drawing.Point(158, 73);
            this.konwledgedegree_name.Name = "konwledgedegree_name";
            this.konwledgedegree_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.konwledgedegree_name.Properties.Appearance.Options.UseBackColor = true;
            this.konwledgedegree_name.Size = new System.Drawing.Size(200, 24);
            this.konwledgedegree_name.TabIndex = 1;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(48, 121);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(30, 18);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "描述";
            // 
            // konwledgedegree_desc
            // 
            this.konwledgedegree_desc.Location = new System.Drawing.Point(158, 118);
            this.konwledgedegree_desc.Name = "konwledgedegree_desc";
            this.konwledgedegree_desc.Size = new System.Drawing.Size(200, 24);
            this.konwledgedegree_desc.TabIndex = 2;
            // 
            // konwledgedegree_flag
            // 
            this.konwledgedegree_flag.Location = new System.Drawing.Point(158, 162);
            this.konwledgedegree_flag.Name = "konwledgedegree_flag";
            this.konwledgedegree_flag.Properties.Caption = "";
            this.konwledgedegree_flag.Size = new System.Drawing.Size(75, 19);
            this.konwledgedegree_flag.TabIndex = 2;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(48, 163);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 18);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "是否停用";
            // 
            // Addbut
            // 
            this.Addbut.Location = new System.Drawing.Point(101, 210);
            this.Addbut.Name = "Addbut";
            this.Addbut.Size = new System.Drawing.Size(80, 25);
            this.Addbut.TabIndex = 3;
            this.Addbut.Text = "新增";
            this.Addbut.Click += new System.EventHandler(this.Addbut_Click);
            // 
            // CloseBut
            // 
            this.CloseBut.Location = new System.Drawing.Point(240, 210);
            this.CloseBut.Name = "CloseBut";
            this.CloseBut.Size = new System.Drawing.Size(80, 25);
            this.CloseBut.TabIndex = 4;
            this.CloseBut.Text = "关闭";
            this.CloseBut.Click += new System.EventHandler(this.CloseBut_Click);
            // 
            // insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 275);
            this.Controls.Add(this.CloseBut);
            this.Controls.Add(this.Addbut);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.konwledgedegree_flag);
            this.Controls.Add(this.konwledgedegree_desc);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.konwledgedegree_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.konwledgedegree_code);
            this.Controls.Add(this.labelControl1);
            this.Name = "insert";
            this.Text = "新增";
            this.Load += new System.EventHandler(this.insert_Load);
            ((System.ComponentModel.ISupportInitialize)(this.konwledgedegree_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.konwledgedegree_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.konwledgedegree_desc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.konwledgedegree_flag.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit konwledgedegree_code;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit konwledgedegree_name;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit konwledgedegree_desc;
        private DevExpress.XtraEditors.CheckEdit konwledgedegree_flag;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton Addbut;
        private DevExpress.XtraEditors.SimpleButton CloseBut;
    }
}