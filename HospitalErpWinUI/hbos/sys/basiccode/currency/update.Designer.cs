﻿namespace HospitalErpWinUI.hbos.sys.basiccode.currency
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CloseBut = new DevExpress.XtraEditors.SimpleButton();
            this.UpdateBut = new DevExpress.XtraEditors.SimpleButton();
            this.currency_way = new DevExpress.XtraEditors.RadioGroup();
            this.currency_stop = new DevExpress.XtraEditors.CheckEdit();
            this.currency_self = new DevExpress.XtraEditors.CheckEdit();
            this.currency_rate = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.currency_digitcurrency_digit = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.currency_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.currency_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.currency_way.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.currency_stop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.currency_self.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.currency_rate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.currency_digitcurrency_digit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.currency_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.currency_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // CloseBut
            // 
            this.CloseBut.Location = new System.Drawing.Point(251, 356);
            this.CloseBut.Name = "CloseBut";
            this.CloseBut.Size = new System.Drawing.Size(80, 25);
            this.CloseBut.TabIndex = 8;
            this.CloseBut.Text = "关闭";
            this.CloseBut.Click += new System.EventHandler(this.CloseBut_Click);
            // 
            // UpdateBut
            // 
            this.UpdateBut.Location = new System.Drawing.Point(121, 356);
            this.UpdateBut.Name = "UpdateBut";
            this.UpdateBut.Size = new System.Drawing.Size(80, 25);
            this.UpdateBut.TabIndex = 7;
            this.UpdateBut.Text = "确定";
            this.UpdateBut.Click += new System.EventHandler(this.UpdateBut_Click);
            // 
            // currency_way
            // 
            this.currency_way.Location = new System.Drawing.Point(180, 192);
            this.currency_way.Name = "currency_way";
            this.currency_way.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(1)), "原币*汇率 = 本位币  "),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(0)), "原币/汇率 = 本位币")});
            this.currency_way.Size = new System.Drawing.Size(200, 96);
            this.currency_way.TabIndex = 4;
            // 
            // currency_stop
            // 
            this.currency_stop.Location = new System.Drawing.Point(295, 306);
            this.currency_stop.Name = "currency_stop";
            this.currency_stop.Properties.Caption = "是否停用";
            this.currency_stop.Size = new System.Drawing.Size(99, 22);
            this.currency_stop.TabIndex = 6;
            // 
            // currency_self
            // 
            this.currency_self.Enabled = false;
            this.currency_self.Location = new System.Drawing.Point(180, 306);
            this.currency_self.Name = "currency_self";
            this.currency_self.Properties.Caption = "是否本币";
            this.currency_self.Size = new System.Drawing.Size(109, 22);
            this.currency_self.TabIndex = 5;
            // 
            // currency_rate
            // 
            this.currency_rate.Location = new System.Drawing.Point(180, 151);
            this.currency_rate.Name = "currency_rate";
            this.currency_rate.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.currency_rate.Properties.Appearance.Options.UseBackColor = true;
            this.currency_rate.Size = new System.Drawing.Size(200, 24);
            this.currency_rate.TabIndex = 3;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(68, 192);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 18);
            this.labelControl5.TabIndex = 5;
            this.labelControl5.Text = "折算方式";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(68, 154);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 18);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "期间汇率";
            // 
            // currency_digitcurrency_digit
            // 
            this.currency_digitcurrency_digit.Location = new System.Drawing.Point(180, 112);
            this.currency_digitcurrency_digit.Name = "currency_digitcurrency_digit";
            this.currency_digitcurrency_digit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.currency_digitcurrency_digit.Properties.Appearance.Options.UseBackColor = true;
            this.currency_digitcurrency_digit.Size = new System.Drawing.Size(200, 24);
            this.currency_digitcurrency_digit.TabIndex = 2;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(68, 115);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(90, 18);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "汇率小数位数";
            // 
            // currency_name
            // 
            this.currency_name.Location = new System.Drawing.Point(180, 70);
            this.currency_name.Name = "currency_name";
            this.currency_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.currency_name.Properties.Appearance.Options.UseBackColor = true;
            this.currency_name.Size = new System.Drawing.Size(200, 24);
            this.currency_name.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(68, 73);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 18);
            this.labelControl2.TabIndex = 8;
            this.labelControl2.Text = "币种名称";
            // 
            // currency_code
            // 
            this.currency_code.Enabled = false;
            this.currency_code.Location = new System.Drawing.Point(180, 30);
            this.currency_code.Name = "currency_code";
            this.currency_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.currency_code.Properties.Appearance.Options.UseBackColor = true;
            this.currency_code.Size = new System.Drawing.Size(200, 24);
            this.currency_code.TabIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(68, 33);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 18);
            this.labelControl1.TabIndex = 9;
            this.labelControl1.Text = "币种编码";
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 411);
            this.Controls.Add(this.CloseBut);
            this.Controls.Add(this.UpdateBut);
            this.Controls.Add(this.currency_way);
            this.Controls.Add(this.currency_stop);
            this.Controls.Add(this.currency_self);
            this.Controls.Add(this.currency_rate);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.currency_digitcurrency_digit);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.currency_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.currency_code);
            this.Controls.Add(this.labelControl1);
            this.Name = "update";
            this.Text = "修改";
            this.Load += new System.EventHandler(this.update_Load);
            ((System.ComponentModel.ISupportInitialize)(this.currency_way.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.currency_stop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.currency_self.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.currency_rate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.currency_digitcurrency_digit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.currency_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.currency_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton CloseBut;
        private DevExpress.XtraEditors.SimpleButton UpdateBut;
        private DevExpress.XtraEditors.RadioGroup currency_way;
        private DevExpress.XtraEditors.CheckEdit currency_stop;
        private DevExpress.XtraEditors.CheckEdit currency_self;
        private DevExpress.XtraEditors.TextEdit currency_rate;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit currency_digitcurrency_digit;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit currency_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit currency_code;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}