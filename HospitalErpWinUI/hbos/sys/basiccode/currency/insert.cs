﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.basiccode.currency
{
    public partial class insert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }

        public insert()
        {
            InitializeComponent();
            Init();
        }

        public insert(DaoHelper helper, List<Perm> perms)
        {
            _helper = helper;
            _perms = perms;

            InitializeComponent();
            Init();
        }
        #region 其他
        private void Init()
        {           
            this.StartPosition = FormStartPosition.CenterParent;
            this.currency_code.Properties.MaxLength = 10;
            this.currency_name.Properties.MaxLength = 20;
          
            currency_way.EditValue = 1;

            currency_digitcurrency_digit.Properties.Mask.MaskType =  DevExpress.XtraEditors.Mask.MaskType.RegEx;
            currency_digitcurrency_digit.Properties.Mask.UseMaskAsDisplayFormat = true;
            currency_digitcurrency_digit.Properties.Mask.EditMask = "[0-9]*";

            currency_rate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            currency_rate.Properties.Mask.UseMaskAsDisplayFormat = true;
            currency_rate.Properties.Mask.EditMask = "([0-9]{1,}[.][0-9]*)";
        }

        private bool Validate()
        {

            if (string.IsNullOrEmpty(currency_code.Text))
            {
                MessageForm.Warning("币种编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(currency_name.Text))
            {
                MessageForm.Warning("币种名称不能为空!");
                return false;
            }
            
            if (string.IsNullOrEmpty(currency_digitcurrency_digit.Text))
            {
                MessageForm.Warning("汇率小数位数不能为空!");
                return false;
            }
            int digit = Convert.ToInt16(currency_digitcurrency_digit.Text);
            if (digit < 2 || digit > 8)
            {
                 MessageForm.Warning("汇率小数位数应在2到8之间!");
                return false;
            }
            if (string.IsNullOrEmpty(currency_rate.Text))
            {
                MessageForm.Warning("期间汇率不能为空!");
                return false;
            }
            Decimal rate = Convert.ToDecimal(currency_rate.Text);
            if (rate < 0)
            {
                MessageForm.Warning("期间汇率值必须大于0！!");
                return false;
            }
            int index = currency_rate.Text.IndexOf(".");
            if (currency_rate.Text.Length- index-1 != digit)
            {
                MessageForm.Warning("期间汇率值的小数位数与汇率小数位数值不一致！!");
                return false;
            }

            if (FormHelper.Text_Length(currency_code.Text) > 10)
            {
                MessageForm.Warning("币种编码字符串长度超过数据库允许的长度10!");
                return false;
            }
            if (FormHelper.Text_Length(currency_name.Text) > 20)
            {
                MessageForm.Warning("币种名称字符串长度超过数据库允许的长度20!");
                return false;
            }
            
            return true;
        }
        #endregion

        #region 数据访问
        private bool Insert()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "sysBasiccodeCurrency_save";
            Dictionary<string, string> dict = new Dictionary<string, string>();
            StringBuilder str = new StringBuilder();
            str.Append(" <root><r ");
            str.Append("currency_code=\"" + currency_code.Text+ "\" ");
            str.Append("currency_name=\"" + currency_name.Text + "\" ");
            str.Append("digit_num=\"" + currency_digitcurrency_digit.Text + "\" ");
            str.Append("currency_rate=\"" + currency_rate.Text + "\" ");
            str.Append("currency_self=\"" + currency_self.EditValue + "\" ");
            str.Append("currency_way=\"" + currency_way.EditValue + "\" ");
            str.Append("currency_stop=\"" + currency_stop.EditValue + "\" ");
            str.Append("insertFlag=\"2\" ");
            str.Append("></r></root>");

            dict.Add("doc", str.ToString()); 
            serviceParamter.Paramters = dict;

            return _helper.WirteSql(serviceParamter);

        }
        #endregion

        #region 事件
        private void insert_Load(object sender, EventArgs e)
        {
            currency_way.SelectedIndex = 0;
        }

        private void CloseBut_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void AddBut_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Insert();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }

        #endregion
    
       
    }
}
