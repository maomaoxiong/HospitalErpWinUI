﻿namespace HospitalErpWinUI.hbos.sys.basiccode.fundsource
{
    partial class insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CloseBut = new DevExpress.XtraEditors.SimpleButton();
            this.AddBut = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.fundsource_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.fundsource_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.fund_source_attr = new DevExpress.XtraEditors.ComboBoxEdit();
            this.fundsource_flag = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.fundsource_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fundsource_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fund_source_attr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fundsource_flag.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // CloseBut
            // 
            this.CloseBut.Location = new System.Drawing.Point(243, 213);
            this.CloseBut.Name = "CloseBut";
            this.CloseBut.Size = new System.Drawing.Size(80, 25);
            this.CloseBut.TabIndex = 13;
            this.CloseBut.Text = "关闭";
            this.CloseBut.Click += new System.EventHandler(this.CloseBut_Click);
            // 
            // AddBut
            // 
            this.AddBut.Location = new System.Drawing.Point(104, 213);
            this.AddBut.Name = "AddBut";
            this.AddBut.Size = new System.Drawing.Size(80, 25);
            this.AddBut.TabIndex = 14;
            this.AddBut.Text = "新增";
            this.AddBut.Click += new System.EventHandler(this.AddBut_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(51, 166);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 18);
            this.labelControl4.TabIndex = 12;
            this.labelControl4.Text = "是否停用";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(51, 124);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 18);
            this.labelControl3.TabIndex = 5;
            this.labelControl3.Text = "资金性质";
            // 
            // fundsource_name
            // 
            this.fundsource_name.Location = new System.Drawing.Point(161, 76);
            this.fundsource_name.Name = "fundsource_name";
            this.fundsource_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.fundsource_name.Properties.Appearance.Options.UseBackColor = true;
            this.fundsource_name.Size = new System.Drawing.Size(200, 24);
            this.fundsource_name.TabIndex = 9;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(51, 79);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(90, 18);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "资金来源名称";
            // 
            // fundsource_code
            // 
            this.fundsource_code.Location = new System.Drawing.Point(161, 30);
            this.fundsource_code.Name = "fundsource_code";
            this.fundsource_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.fundsource_code.Properties.Appearance.Options.UseBackColor = true;
            this.fundsource_code.Size = new System.Drawing.Size(200, 24);
            this.fundsource_code.TabIndex = 10;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(51, 33);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(90, 18);
            this.labelControl1.TabIndex = 7;
            this.labelControl1.Text = "资金来源编码";
            // 
            // fund_source_attr
            // 
            this.fund_source_attr.Location = new System.Drawing.Point(161, 121);
            this.fund_source_attr.Name = "fund_source_attr";
            this.fund_source_attr.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.fund_source_attr.Properties.Appearance.Options.UseBackColor = true;
            this.fund_source_attr.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.fund_source_attr.Size = new System.Drawing.Size(200, 24);
            this.fund_source_attr.TabIndex = 15;
            // 
            // fundsource_flag
            // 
            this.fundsource_flag.Location = new System.Drawing.Point(161, 163);
            this.fundsource_flag.Name = "fundsource_flag";
            this.fundsource_flag.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.fundsource_flag.Properties.Appearance.Options.UseBackColor = true;
            this.fundsource_flag.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.fundsource_flag.Size = new System.Drawing.Size(200, 24);
            this.fundsource_flag.TabIndex = 15;
            // 
            // insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 285);
            this.Controls.Add(this.fundsource_flag);
            this.Controls.Add(this.fund_source_attr);
            this.Controls.Add(this.CloseBut);
            this.Controls.Add(this.AddBut);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.fundsource_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.fundsource_code);
            this.Controls.Add(this.labelControl1);
            this.Name = "insert";
            this.Text = "新增";
            this.Load += new System.EventHandler(this.insert_Load);
            ((System.ComponentModel.ISupportInitialize)(this.fundsource_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fundsource_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fund_source_attr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fundsource_flag.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton CloseBut;
        private DevExpress.XtraEditors.SimpleButton AddBut;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit fundsource_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit fundsource_code;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit fund_source_attr;
        private DevExpress.XtraEditors.ComboBoxEdit fundsource_flag;
    }
}