﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.basiccode.fundsource
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        private string _code;
        private List<Perm> _perms { get; set; }

        public update()
        {
            InitializeComponent();
            Init();
        }

        public update(DaoHelper helper, List<Perm> perms, string code)
        {
            _helper = helper;
            _perms = perms;
            _code = code;
            InitializeComponent();
            Init();
        }
        #region 其他
        private void Init()
        {
            this.StartPosition = FormStartPosition.CenterParent;
            this.fundsource_code.Properties.MaxLength = 3;
            this.fundsource_name.Properties.MaxLength = 20;

        }

        private bool Validate()
        {

            if (string.IsNullOrEmpty(fundsource_code.Text))
            {
                MessageForm.Warning("资金来源编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(fundsource_name.Text))
            {
                MessageForm.Warning("资金来源名称不能为空!");
                return false;
            }

            return true;
        }
        #endregion

        #region 数据访问
        private void LoadYesNo()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "dict_yes_or_no";
            serviceParamter.Paramters = paramters;

            List<CodeValue> codeValues = _helper.ReadDictForData(serviceParamter);
            List<string> objs = new List<string>();
            foreach (CodeValue codeValue in codeValues)
            {
                objs.Add(codeValue.Value);
            }

            fundsource_flag.Properties.Items.AddRange(objs);
            fundsource_flag.SelectedItem = objs[0];


        }
        private void LoadAttr()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "fund_source_attr";
            serviceParamter.Paramters = paramters;

            List<CodeValue> codeValues = _helper.ReadDictForData(serviceParamter);
            List<string> objs = new List<string>();
            foreach (CodeValue codeValue in codeValues)
            {
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }

            fund_source_attr.Properties.Items.AddRange(objs);
            fund_source_attr.SelectedItem = objs[0];


        }


        private bool Update()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r patienttype_code="05" patienttype_name="医疗纠纷病人" patienttype_flag="1" insertFlag="1" is_medicare="0"></r></root>
            serviceParamter.ServiceId = "sysBasiccodeFundsource_update";
            Dictionary<string, string> dict = new Dictionary<string, string>();

            dict.Add("ParaCode", fundsource_code.Text);
            dict.Add("ParaName", fundsource_name.Text);
            dict.Add("ParaAttr", FormHelper.GetValueForComboBoxEdit(fund_source_attr));
            dict.Add("ParaStop", FormHelper.GetValueForComboBoxEdit(fundsource_flag));
            serviceParamter.Paramters = dict;

            return _helper.WirteSql(serviceParamter);

        }

        private void LoadDate()
        {
            string sqlstr = "select fund_source_code _fund_source_code,fund_source_name _fund_source_name,is_stop, fund_source_attr from sys_money_resource where fund_source_code = '?'";
            sqlstr = sqlstr.Replace("?", _code);
            DataTable dt = _helper.ExecReadSql(sqlstr);
            if (dt != null && dt.Rows.Count != 0)
            {
                this.fundsource_code.Text = dt.Rows[0]["_fund_source_code"].ToString();
                this.fundsource_name.Text = dt.Rows[0]["_fund_source_name"].ToString();
                FormHelper.ComboBoxeditAssignment(fund_source_attr, dt.Rows[0]["fund_source_attr"].ToString());
                FormHelper.ComboBoxeditAssignment(fundsource_flag, dt.Rows[0]["is_stop"].ToString());

            }

        }
        #endregion

        private void CloseBut_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void update_Load(object sender, EventArgs e)
        {
            LoadYesNo();
            LoadAttr();
            LoadDate();
        }

        private void updateBut_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Update();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
    }
}
