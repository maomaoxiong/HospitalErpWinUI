﻿namespace HospitalErpWinUI.hbos.sys.basiccode.fundsource
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fundsource_flag = new DevExpress.XtraEditors.ComboBoxEdit();
            this.fund_source_attr = new DevExpress.XtraEditors.ComboBoxEdit();
            this.CloseBut = new DevExpress.XtraEditors.SimpleButton();
            this.updateBut = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.fundsource_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.fundsource_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.fundsource_flag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fund_source_attr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fundsource_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fundsource_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // fundsource_flag
            // 
            this.fundsource_flag.Location = new System.Drawing.Point(161, 166);
            this.fundsource_flag.Name = "fundsource_flag";
            this.fundsource_flag.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.fundsource_flag.Properties.Appearance.Options.UseBackColor = true;
            this.fundsource_flag.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.fundsource_flag.Size = new System.Drawing.Size(200, 24);
            this.fundsource_flag.TabIndex = 24;
            // 
            // fund_source_attr
            // 
            this.fund_source_attr.Location = new System.Drawing.Point(161, 124);
            this.fund_source_attr.Name = "fund_source_attr";
            this.fund_source_attr.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.fund_source_attr.Properties.Appearance.Options.UseBackColor = true;
            this.fund_source_attr.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.fund_source_attr.Size = new System.Drawing.Size(200, 24);
            this.fund_source_attr.TabIndex = 25;
            // 
            // CloseBut
            // 
            this.CloseBut.Location = new System.Drawing.Point(243, 216);
            this.CloseBut.Name = "CloseBut";
            this.CloseBut.Size = new System.Drawing.Size(80, 25);
            this.CloseBut.TabIndex = 22;
            this.CloseBut.Text = "关闭";
            this.CloseBut.Click += new System.EventHandler(this.CloseBut_Click);
            // 
            // updateBut
            // 
            this.updateBut.Location = new System.Drawing.Point(104, 216);
            this.updateBut.Name = "updateBut";
            this.updateBut.Size = new System.Drawing.Size(80, 25);
            this.updateBut.TabIndex = 23;
            this.updateBut.Text = "修改";
            this.updateBut.Click += new System.EventHandler(this.updateBut_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(51, 169);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 18);
            this.labelControl4.TabIndex = 21;
            this.labelControl4.Text = "是否停用";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(51, 127);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 18);
            this.labelControl3.TabIndex = 16;
            this.labelControl3.Text = "资金性质";
            // 
            // fundsource_name
            // 
            this.fundsource_name.Location = new System.Drawing.Point(161, 79);
            this.fundsource_name.Name = "fundsource_name";
            this.fundsource_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.fundsource_name.Properties.Appearance.Options.UseBackColor = true;
            this.fundsource_name.Size = new System.Drawing.Size(200, 24);
            this.fundsource_name.TabIndex = 19;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(51, 82);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(90, 18);
            this.labelControl2.TabIndex = 17;
            this.labelControl2.Text = "资金来源名称";
            // 
            // fundsource_code
            // 
            this.fundsource_code.Enabled = false;
            this.fundsource_code.Location = new System.Drawing.Point(161, 33);
            this.fundsource_code.Name = "fundsource_code";
            this.fundsource_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.fundsource_code.Properties.Appearance.Options.UseBackColor = true;
            this.fundsource_code.Size = new System.Drawing.Size(200, 24);
            this.fundsource_code.TabIndex = 20;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(51, 36);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(90, 18);
            this.labelControl1.TabIndex = 18;
            this.labelControl1.Text = "资金来源编码";
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 275);
            this.Controls.Add(this.fundsource_flag);
            this.Controls.Add(this.fund_source_attr);
            this.Controls.Add(this.CloseBut);
            this.Controls.Add(this.updateBut);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.fundsource_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.fundsource_code);
            this.Controls.Add(this.labelControl1);
            this.Name = "update";
            this.Text = "修改";
            this.Load += new System.EventHandler(this.update_Load);
            ((System.ComponentModel.ISupportInitialize)(this.fundsource_flag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fund_source_attr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fundsource_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fundsource_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.ComboBoxEdit fundsource_flag;
        private DevExpress.XtraEditors.ComboBoxEdit fund_source_attr;
        private DevExpress.XtraEditors.SimpleButton CloseBut;
        private DevExpress.XtraEditors.SimpleButton updateBut;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit fundsource_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit fundsource_code;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}