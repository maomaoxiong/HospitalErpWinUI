﻿namespace HospitalErpWinUI.hbos.sys.basiccode.measureunit
{
    partial class insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.closeBut = new DevExpress.XtraEditors.SimpleButton();
            this.addBut = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.measureunit_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.measureunit_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.measureunit_flag = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.measureunit_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.measureunit_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.measureunit_flag.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // closeBut
            // 
            this.closeBut.AccessibleDescription = "closeBut";
            this.closeBut.Location = new System.Drawing.Point(227, 190);
            this.closeBut.Name = "closeBut";
            this.closeBut.Size = new System.Drawing.Size(80, 25);
            this.closeBut.TabIndex = 13;
            this.closeBut.Text = "关闭";
            this.closeBut.Click += new System.EventHandler(this.closeBut_Click);
            // 
            // addBut
            // 
            this.addBut.Location = new System.Drawing.Point(88, 190);
            this.addBut.Name = "addBut";
            this.addBut.Size = new System.Drawing.Size(80, 25);
            this.addBut.TabIndex = 14;
            this.addBut.Text = "新增";
            this.addBut.Click += new System.EventHandler(this.addBut_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(51, 127);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 18);
            this.labelControl4.TabIndex = 12;
            this.labelControl4.Text = "是否停用";
            // 
            // measureunit_name
            // 
            this.measureunit_name.Location = new System.Drawing.Point(161, 81);
            this.measureunit_name.Name = "measureunit_name";
            this.measureunit_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.measureunit_name.Properties.Appearance.Options.UseBackColor = true;
            this.measureunit_name.Size = new System.Drawing.Size(200, 24);
            this.measureunit_name.TabIndex = 9;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(51, 84);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(90, 18);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "计量单位名称";
            // 
            // measureunit_code
            // 
            this.measureunit_code.Location = new System.Drawing.Point(161, 35);
            this.measureunit_code.Name = "measureunit_code";
            this.measureunit_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.measureunit_code.Properties.Appearance.Options.UseBackColor = true;
            this.measureunit_code.Size = new System.Drawing.Size(200, 24);
            this.measureunit_code.TabIndex = 10;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(51, 38);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(90, 18);
            this.labelControl1.TabIndex = 7;
            this.labelControl1.Text = "计量单位编码";
            // 
            // measureunit_flag
            // 
            this.measureunit_flag.Location = new System.Drawing.Point(161, 124);
            this.measureunit_flag.Name = "measureunit_flag";
            this.measureunit_flag.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.measureunit_flag.Size = new System.Drawing.Size(200, 24);
            this.measureunit_flag.TabIndex = 15;
            // 
            // insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 262);
            this.Controls.Add(this.measureunit_flag);
            this.Controls.Add(this.closeBut);
            this.Controls.Add(this.addBut);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.measureunit_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.measureunit_code);
            this.Controls.Add(this.labelControl1);
            this.Name = "insert";
            this.Text = "新增";
            this.Load += new System.EventHandler(this.insert_Load);
            ((System.ComponentModel.ISupportInitialize)(this.measureunit_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.measureunit_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.measureunit_flag.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton closeBut;
        private DevExpress.XtraEditors.SimpleButton addBut;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit measureunit_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit measureunit_code;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit measureunit_flag;
    }
}