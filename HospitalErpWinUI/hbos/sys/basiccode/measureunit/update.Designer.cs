﻿namespace HospitalErpWinUI.hbos.sys.basiccode.measureunit
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.measureunit_flag = new DevExpress.XtraEditors.ComboBoxEdit();
            this.closeBut = new DevExpress.XtraEditors.SimpleButton();
            this.updateBut = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.measureunit_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.measureunit_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.measureunit_flag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.measureunit_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.measureunit_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // measureunit_flag
            // 
            this.measureunit_flag.Location = new System.Drawing.Point(161, 136);
            this.measureunit_flag.Name = "measureunit_flag";
            this.measureunit_flag.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.measureunit_flag.Size = new System.Drawing.Size(200, 24);
            this.measureunit_flag.TabIndex = 23;
            // 
            // closeBut
            // 
            this.closeBut.AccessibleDescription = "closeBut";
            this.closeBut.Location = new System.Drawing.Point(227, 202);
            this.closeBut.Name = "closeBut";
            this.closeBut.Size = new System.Drawing.Size(80, 25);
            this.closeBut.TabIndex = 21;
            this.closeBut.Text = "关闭";
            this.closeBut.Click += new System.EventHandler(this.closeBut_Click);
            // 
            // updateBut
            // 
            this.updateBut.Location = new System.Drawing.Point(88, 202);
            this.updateBut.Name = "updateBut";
            this.updateBut.Size = new System.Drawing.Size(80, 25);
            this.updateBut.TabIndex = 22;
            this.updateBut.Text = "修改";
            this.updateBut.Click += new System.EventHandler(this.updateBut_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(51, 139);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 18);
            this.labelControl4.TabIndex = 20;
            this.labelControl4.Text = "是否停用";
            // 
            // measureunit_name
            // 
            this.measureunit_name.Location = new System.Drawing.Point(161, 93);
            this.measureunit_name.Name = "measureunit_name";
            this.measureunit_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.measureunit_name.Properties.Appearance.Options.UseBackColor = true;
            this.measureunit_name.Size = new System.Drawing.Size(200, 24);
            this.measureunit_name.TabIndex = 18;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(51, 96);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(90, 18);
            this.labelControl2.TabIndex = 16;
            this.labelControl2.Text = "计量单位名称";
            // 
            // measureunit_code
            // 
            this.measureunit_code.Location = new System.Drawing.Point(161, 47);
            this.measureunit_code.Name = "measureunit_code";
            this.measureunit_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.measureunit_code.Properties.Appearance.Options.UseBackColor = true;
            this.measureunit_code.Size = new System.Drawing.Size(200, 24);
            this.measureunit_code.TabIndex = 19;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(51, 50);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(90, 18);
            this.labelControl1.TabIndex = 17;
            this.labelControl1.Text = "计量单位编码";
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 275);
            this.Controls.Add(this.measureunit_flag);
            this.Controls.Add(this.closeBut);
            this.Controls.Add(this.updateBut);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.measureunit_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.measureunit_code);
            this.Controls.Add(this.labelControl1);
            this.Name = "update";
            this.Text = "修改";
            this.Load += new System.EventHandler(this.update_Load);
            ((System.ComponentModel.ISupportInitialize)(this.measureunit_flag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.measureunit_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.measureunit_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.ComboBoxEdit measureunit_flag;
        private DevExpress.XtraEditors.SimpleButton closeBut;
        private DevExpress.XtraEditors.SimpleButton updateBut;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit measureunit_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit measureunit_code;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}