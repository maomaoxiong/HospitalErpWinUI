﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.basiccode.measureunit
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        private string _code;
        private List<Perm> _perms { get; set; }

        public update()
        {
            InitializeComponent();
            Init();
        }

        public update(DaoHelper helper, List<Perm> perms, string code)
        {
            _helper = helper;
            _perms = perms;
            _code = code;
            InitializeComponent();
            Init();
        }

        #region 其他
        private void Init()
        {
            this.StartPosition = FormStartPosition.CenterParent;
            this.measureunit_code.Properties.MaxLength = 10;
            this.measureunit_name.Properties.MaxLength = 20;

        }

        private bool Validate()
        {

            if (string.IsNullOrEmpty(measureunit_code.Text))
            {
                MessageForm.Warning("单位编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(measureunit_name.Text))
            {
                MessageForm.Warning("单位名称不能为空!");
                return false;
            }

            return true;
        }
        #endregion

        #region 数据访问
        private void LoadYesNo()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "dict_yes_or_no";
            serviceParamter.Paramters = paramters;

            List<CodeValue> codeValues = _helper.ReadDictForData(serviceParamter);
            List<string> objs = new List<string>();
            foreach (CodeValue codeValue in codeValues)
            {
                objs.Add(codeValue.Value);
            }

            measureunit_flag.Properties.Items.AddRange(objs);
            measureunit_flag.SelectedItem = objs[0];
        }

        private void LoadDate()
        {
            string sqlstr = "select unit_code _unit_code,unit_name _unit_name,is_stop from sys_unit where unit_code  = '?'";
            sqlstr = sqlstr.Replace("?", _code);
            DataTable dt = _helper.ExecReadSql(sqlstr);
            if (dt != null && dt.Rows.Count != 0)
            {
                this.measureunit_code.Text = dt.Rows[0]["_unit_code"].ToString();
                this.measureunit_name.Text = dt.Rows[0]["_unit_name"].ToString();
                FormHelper.ComboBoxeditAssignment(measureunit_flag, dt.Rows[0]["is_stop"].ToString());

            }

        }

        private bool Update()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r patienttype_code="05" patienttype_name="医疗纠纷病人" patienttype_flag="1" insertFlag="1" is_medicare="0"></r></root>
            serviceParamter.ServiceId = "sysBasiccodeMeasureunit_update";
            Dictionary<string, string> dict = new Dictionary<string, string>();

            dict.Add("ParaCode", measureunit_code.Text);
            dict.Add("ParaName", measureunit_name.Text);

            dict.Add("ParaStop", FormHelper.GetValueForComboBoxEdit(measureunit_flag));
            serviceParamter.Paramters = dict;

            return _helper.WirteSql(serviceParamter);

        }
        #endregion

        private void update_Load(object sender, EventArgs e)
        {
            LoadYesNo();
            LoadDate();
        }

        private void closeBut_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void updateBut_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Update();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
    }
}
