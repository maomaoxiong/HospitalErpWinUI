﻿namespace HospitalErpWinUI.hbos.sys.basiccode.areacode
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.areacode_flag = new DevExpress.XtraEditors.ComboBoxEdit();
            this.closeBut = new DevExpress.XtraEditors.SimpleButton();
            this.updateBut = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.areacode_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.areacode_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_flag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // areacode_flag
            // 
            this.areacode_flag.Location = new System.Drawing.Point(161, 140);
            this.areacode_flag.Name = "areacode_flag";
            this.areacode_flag.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.areacode_flag.Size = new System.Drawing.Size(200, 24);
            this.areacode_flag.TabIndex = 23;
            // 
            // closeBut
            // 
            this.closeBut.Location = new System.Drawing.Point(238, 202);
            this.closeBut.Name = "closeBut";
            this.closeBut.Size = new System.Drawing.Size(80, 25);
            this.closeBut.TabIndex = 21;
            this.closeBut.Text = "关闭";
            this.closeBut.Click += new System.EventHandler(this.closeBut_Click);
            // 
            // updateBut
            // 
            this.updateBut.Location = new System.Drawing.Point(99, 202);
            this.updateBut.Name = "updateBut";
            this.updateBut.Size = new System.Drawing.Size(80, 25);
            this.updateBut.TabIndex = 22;
            this.updateBut.Text = "修改";
            this.updateBut.Click += new System.EventHandler(this.updateBut_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(51, 143);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 18);
            this.labelControl4.TabIndex = 20;
            this.labelControl4.Text = "是否停用";
            // 
            // areacode_name
            // 
            this.areacode_name.Location = new System.Drawing.Point(161, 94);
            this.areacode_name.Name = "areacode_name";
            this.areacode_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.areacode_name.Properties.Appearance.Options.UseBackColor = true;
            this.areacode_name.Size = new System.Drawing.Size(200, 24);
            this.areacode_name.TabIndex = 18;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(51, 97);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 18);
            this.labelControl2.TabIndex = 16;
            this.labelControl2.Text = "地区名称";
            // 
            // areacode_code
            // 
            this.areacode_code.Enabled = false;
            this.areacode_code.Location = new System.Drawing.Point(161, 48);
            this.areacode_code.Name = "areacode_code";
            this.areacode_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.areacode_code.Properties.Appearance.Options.UseBackColor = true;
            this.areacode_code.Size = new System.Drawing.Size(200, 24);
            this.areacode_code.TabIndex = 19;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(51, 51);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 18);
            this.labelControl1.TabIndex = 17;
            this.labelControl1.Text = "地区编码";
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 275);
            this.Controls.Add(this.areacode_flag);
            this.Controls.Add(this.closeBut);
            this.Controls.Add(this.updateBut);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.areacode_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.areacode_code);
            this.Controls.Add(this.labelControl1);
            this.Name = "update";
            this.Text = "更新";
            this.Load += new System.EventHandler(this.update_Load);
            ((System.ComponentModel.ISupportInitialize)(this.areacode_flag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.ComboBoxEdit areacode_flag;
        private DevExpress.XtraEditors.SimpleButton closeBut;
        private DevExpress.XtraEditors.SimpleButton updateBut;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit areacode_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit areacode_code;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}