﻿namespace HospitalErpWinUI.hbos.sys.basiccode.areacode
{
    partial class insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.closeBut = new DevExpress.XtraEditors.SimpleButton();
            this.addBut = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.areacode_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.areacode_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.areacode_flag = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_flag.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // closeBut
            // 
            this.closeBut.Location = new System.Drawing.Point(237, 188);
            this.closeBut.Name = "closeBut";
            this.closeBut.Size = new System.Drawing.Size(80, 25);
            this.closeBut.TabIndex = 13;
            this.closeBut.Text = "关闭";
            this.closeBut.Click += new System.EventHandler(this.closeBut_Click);
            // 
            // addBut
            // 
            this.addBut.Location = new System.Drawing.Point(98, 188);
            this.addBut.Name = "addBut";
            this.addBut.Size = new System.Drawing.Size(80, 25);
            this.addBut.TabIndex = 14;
            this.addBut.Text = "新增";
            this.addBut.Click += new System.EventHandler(this.addBut_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(50, 129);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 18);
            this.labelControl4.TabIndex = 12;
            this.labelControl4.Text = "是否停用";
            // 
            // areacode_name
            // 
            this.areacode_name.Location = new System.Drawing.Point(160, 80);
            this.areacode_name.Name = "areacode_name";
            this.areacode_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.areacode_name.Properties.Appearance.Options.UseBackColor = true;
            this.areacode_name.Size = new System.Drawing.Size(200, 24);
            this.areacode_name.TabIndex = 9;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(50, 83);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 18);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "地区名称";
            // 
            // areacode_code
            // 
            this.areacode_code.Location = new System.Drawing.Point(160, 34);
            this.areacode_code.Name = "areacode_code";
            this.areacode_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.areacode_code.Properties.Appearance.Options.UseBackColor = true;
            this.areacode_code.Size = new System.Drawing.Size(200, 24);
            this.areacode_code.TabIndex = 10;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(50, 37);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 18);
            this.labelControl1.TabIndex = 7;
            this.labelControl1.Text = "地区编码";
            // 
            // areacode_flag
            // 
            this.areacode_flag.Location = new System.Drawing.Point(160, 126);
            this.areacode_flag.Name = "areacode_flag";
            this.areacode_flag.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.areacode_flag.Size = new System.Drawing.Size(200, 24);
            this.areacode_flag.TabIndex = 15;
            // 
            // insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 250);
            this.Controls.Add(this.areacode_flag);
            this.Controls.Add(this.closeBut);
            this.Controls.Add(this.addBut);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.areacode_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.areacode_code);
            this.Controls.Add(this.labelControl1);
            this.Name = "insert";
            this.Text = "新增";
            this.Load += new System.EventHandler(this.insert_Load);
            ((System.ComponentModel.ISupportInitialize)(this.areacode_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_flag.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton closeBut;
        private DevExpress.XtraEditors.SimpleButton addBut;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit areacode_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit areacode_code;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit areacode_flag;
    }
}