﻿namespace HospitalErpWinUI.hbos.sys.basiccode.emptype
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.empType_flag = new DevExpress.XtraEditors.ComboBoxEdit();
            this.deletebut = new DevExpress.XtraEditors.SimpleButton();
            this.updateBut = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.empType_desc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.empType_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.empType_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.empType_flag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.empType_desc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.empType_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.empType_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // empType_flag
            // 
            this.empType_flag.Location = new System.Drawing.Point(161, 171);
            this.empType_flag.Name = "empType_flag";
            this.empType_flag.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.empType_flag.Properties.Appearance.Options.UseBackColor = true;
            this.empType_flag.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.empType_flag.Size = new System.Drawing.Size(200, 24);
            this.empType_flag.TabIndex = 27;
            // 
            // deletebut
            // 
            this.deletebut.Location = new System.Drawing.Point(243, 221);
            this.deletebut.Name = "deletebut";
            this.deletebut.Size = new System.Drawing.Size(80, 25);
            this.deletebut.TabIndex = 25;
            this.deletebut.Text = "关闭";
            this.deletebut.Click += new System.EventHandler(this.deletebut_Click);
            // 
            // updateBut
            // 
            this.updateBut.Location = new System.Drawing.Point(104, 221);
            this.updateBut.Name = "updateBut";
            this.updateBut.Size = new System.Drawing.Size(80, 25);
            this.updateBut.TabIndex = 26;
            this.updateBut.Text = "修改";
            this.updateBut.Click += new System.EventHandler(this.updateBut_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(51, 174);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 18);
            this.labelControl4.TabIndex = 24;
            this.labelControl4.Text = "是否停用";
            // 
            // empType_desc
            // 
            this.empType_desc.Location = new System.Drawing.Point(161, 129);
            this.empType_desc.Name = "empType_desc";
            this.empType_desc.Size = new System.Drawing.Size(200, 24);
            this.empType_desc.TabIndex = 21;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(51, 132);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(30, 18);
            this.labelControl3.TabIndex = 18;
            this.labelControl3.Text = "描述";
            // 
            // empType_name
            // 
            this.empType_name.Location = new System.Drawing.Point(161, 84);
            this.empType_name.Name = "empType_name";
            this.empType_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.empType_name.Properties.Appearance.Options.UseBackColor = true;
            this.empType_name.Size = new System.Drawing.Size(200, 24);
            this.empType_name.TabIndex = 22;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(51, 87);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(90, 18);
            this.labelControl2.TabIndex = 19;
            this.labelControl2.Text = "职工类别名称";
            // 
            // empType_code
            // 
            this.empType_code.Location = new System.Drawing.Point(161, 38);
            this.empType_code.Name = "empType_code";
            this.empType_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.empType_code.Properties.Appearance.Options.UseBackColor = true;
            this.empType_code.Size = new System.Drawing.Size(200, 24);
            this.empType_code.TabIndex = 23;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(51, 41);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(90, 18);
            this.labelControl1.TabIndex = 20;
            this.labelControl1.Text = "职工类别编码";
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 285);
            this.Controls.Add(this.empType_flag);
            this.Controls.Add(this.deletebut);
            this.Controls.Add(this.updateBut);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.empType_desc);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.empType_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.empType_code);
            this.Controls.Add(this.labelControl1);
            this.Name = "update";
            this.Text = "修改";
            this.Load += new System.EventHandler(this.update_Load);
            ((System.ComponentModel.ISupportInitialize)(this.empType_flag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.empType_desc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.empType_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.empType_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.ComboBoxEdit empType_flag;
        private DevExpress.XtraEditors.SimpleButton deletebut;
        private DevExpress.XtraEditors.SimpleButton updateBut;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit empType_desc;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit empType_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit empType_code;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}