﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraGrid;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.basiccode.emptype
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        private DataTable _emptype;
        public main()
        {
            _helper = new DaoHelper();
            InitializeComponent();
            _emptype = new DataTable();
            gridView1.OptionsSelection.MultiSelect = true;
            gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.RowSelect;
            gridView1.OptionsView.EnableAppearanceOddRow = true;


        }

        #region 访问数据
        private void Query()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysBasiccodeEmptype_select";
            paramters.Add("queryPara", q_empType_code.Text);

            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            _emptype = dt;
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            gridControl1.DataSource = _emptype;
        }

        private bool Delete(string type)
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysBasiccodeEmptype_delete";

            paramters.Add("ParaCode", type);
            paramters.Add("ParaName", null);
            paramters.Add("ParaDesc", null);
            paramters.Add("ParaStop", null);
            serviceParamter.Paramters = paramters;
            bool result = false;
            try
            {
                _helper.WirteSql(serviceParamter);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }

        #endregion


        #region 事件
        private void sysBasiccodeEmptype_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message);
            }
        }

        private void AddBut_Click(object sender, EventArgs e)
        {
            insert insertFrm = new insert(this._helper, this._perms);

            if (insertFrm.ShowDialog() == DialogResult.OK)
            {
                Query();
            }
        }

        private void deleteBut_Click(object sender, EventArgs e)
        {
            bool value = false;
            string strSelected = string.Empty;

            try
            {
                //   string param = "<root><r currency_code=/""++"/"></r></root>";
                List<string> objs = new List<string>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    value = Convert.ToBoolean(gridView1.GetDataRow(i)["checked"]);
                    if (value)
                    {
                        string code = gridView1.GetDataRow(i)["_emp_type_code"].ToString();
                        objs.Add(code);

                    } 
                }
                if (objs.Count == 0)
                {
                    MessageForm.Show("请选择要删除的数据!");
                    return;
                }
                if (MessageForm.ShowMsg("确认是否删除?", MsgType.YesNo) == DialogResult.Yes)
                {
                    foreach (string obj in objs)
                    {                       
                        if (!Delete(obj))
                        {
                            return;
                        }
                    }
                    //       MessageForm.Show("操作成功!");
                    Query();
                }

            }

            catch (Exception ex)
            {
                MessageForm.Warning("此记录在其他地方已经引用，不能删除!", "警告");
            }
              
        }
        #endregion

        private void main_Load(object sender, EventArgs e)
        {

        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, DevExpress.XtraEditors.Controls.OpenLinkEventArgs e)
        {
            if (gridView1.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                int index = gridView1.GetDataSourceRowIndex(gridView1.FocusedRowHandle);
                string url = _emptype.Rows[index]["_emp_type_code"].ToString();
                update updateFrm = new update(this._helper, this._perms, url);

                if (updateFrm.ShowDialog() == DialogResult.OK)
                {
                    Query();
                }

                //    MessageBox.Show(url);
                //     e.EditValue = url;

            }
        }
      

       
    }
}
