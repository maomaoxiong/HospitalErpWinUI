﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.basiccode.emptype
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        private string _code;
        private List<Perm> _perms { get; set; }

        public update()
        {
            InitializeComponent();
            Init();
        }

        public update(DaoHelper helper, List<Perm> perms, string code)
        {
            _helper = helper;
            _perms = perms;
            _code = code;
            InitializeComponent();
            Init();
        }
        #region 其他
        private void Init()
        {
            this.StartPosition = FormStartPosition.CenterParent;
            this.empType_code.Properties.MaxLength = 10;
            this.empType_name.Properties.MaxLength = 20;

        }

        private bool Validate()
        {

            if (string.IsNullOrEmpty(empType_code.Text))
            {
                MessageForm.Warning("职工类别编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(empType_name.Text))
            {
                MessageForm.Warning("职工类别名称不能为空!");
                return false;
            }

            return true;
        }
        #endregion

        #region 数据访问
        private void LoadYesNo()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "dict_yes_or_no";
            serviceParamter.Paramters = paramters;

            List<CodeValue> codeValues = _helper.ReadDictForData(serviceParamter);
            List<string> objs = new List<string>();
            foreach (CodeValue codeValue in codeValues)
            {
                objs.Add(codeValue.Value);
            }

            empType_flag.Properties.Items.AddRange(objs);
            empType_flag.SelectedItem = objs[0];


        }

        private void LoadDate()
        {
            string sqlstr = "select emp_type_code _emp_type_code,emp_type_name _emp_type_name,emp_type_desc emp_type_desc,is_stop is_stop from sys_emp_type where emp_type_code = '?'";
            sqlstr = sqlstr.Replace("?", _code);
            DataTable dt = _helper.ExecReadSql(sqlstr);
            if (dt != null && dt.Rows.Count != 0)
            {
                this.empType_code.Text = dt.Rows[0]["_emp_type_code"].ToString();
                this.empType_name.Text = dt.Rows[0]["_emp_type_name"].ToString();
                this.empType_desc.Text = dt.Rows[0]["emp_type_desc"].ToString();
                FormHelper.ComboBoxeditAssignment(empType_flag, dt.Rows[0]["is_stop"].ToString());
              
            }

        }

        private bool Update()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r patienttype_code="05" patienttype_name="医疗纠纷病人" patienttype_flag="1" insertFlag="1" is_medicare="0"></r></root>
            serviceParamter.ServiceId = "sysBasiccodeEmptype_update";
            Dictionary<string, string> dict = new Dictionary<string, string>();

            dict.Add("ParaCode", empType_code.Text);
            dict.Add("ParaName", empType_name.Text);
            dict.Add("ParaDesc", empType_desc.Text);
            dict.Add("ParaStop", FormHelper.GetValueForComboBoxEdit(empType_flag));
            serviceParamter.Paramters = dict;

            return _helper.WirteSql(serviceParamter);

        }
        #endregion

        #region 事件
        private void deletebut_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void updateBut_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Update();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }

        private void update_Load(object sender, EventArgs e)
        {
            LoadYesNo();
            LoadDate();
        }

      
        #endregion

       
       
    }
}
