﻿namespace HospitalErpWinUI.hbos.sys.privs.dba
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colcomp_code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcomp_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcomp_leader = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colacc_manager = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_pk1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_pk2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.sysPrivsDba_update = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.sysPrivsDba_select = new DevExpress.XtraEditors.SimpleButton();
            this.dba_name = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dba_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.gridControl1);
            this.panelControl1.Controls.Add(this.panelControl3);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Padding = new System.Windows.Forms.Padding(9, 0, 0, 0);
            this.panelControl1.Size = new System.Drawing.Size(1104, 630);
            this.panelControl1.TabIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(11, 66);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1091, 562);
            this.gridControl1.TabIndex = 2;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.colcomp_code,
            this.colcomp_name,
            this.colcomp_leader,
            this.colacc_manager,
            this.col_pk1,
            this.col_pk2});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "选择";
            this.gridColumn1.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn1.FieldName = "checked";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // colcomp_code
            // 
            this.colcomp_code.Caption = "单位编码";
            this.colcomp_code.FieldName = "comp_code";
            this.colcomp_code.Name = "colcomp_code";
            this.colcomp_code.Visible = true;
            this.colcomp_code.VisibleIndex = 1;
            // 
            // colcomp_name
            // 
            this.colcomp_name.Caption = "单位名称";
            this.colcomp_name.FieldName = "comp_name";
            this.colcomp_name.Name = "colcomp_name";
            this.colcomp_name.Visible = true;
            this.colcomp_name.VisibleIndex = 2;
            // 
            // colcomp_leader
            // 
            this.colcomp_leader.Caption = "单位领导";
            this.colcomp_leader.FieldName = "comp_leader";
            this.colcomp_leader.Name = "colcomp_leader";
            this.colcomp_leader.Visible = true;
            this.colcomp_leader.VisibleIndex = 3;
            // 
            // colacc_manager
            // 
            this.colacc_manager.Caption = "财务主管";
            this.colacc_manager.FieldName = "acc_manager";
            this.colacc_manager.Name = "colacc_manager";
            this.colacc_manager.Visible = true;
            this.colacc_manager.VisibleIndex = 4;
            // 
            // col_pk1
            // 
            this.col_pk1.Caption = "_pk1";
            this.col_pk1.FieldName = "_pk1";
            this.col_pk1.Name = "col_pk1";
            // 
            // col_pk2
            // 
            this.col_pk2.Caption = "_pk2";
            this.col_pk2.FieldName = "_pk2";
            this.col_pk2.Name = "col_pk2";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.sysPrivsDba_update);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(11, 34);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1091, 32);
            this.panelControl3.TabIndex = 1;
            // 
            // sysPrivsDba_update
            // 
            this.sysPrivsDba_update.Location = new System.Drawing.Point(5, 6);
            this.sysPrivsDba_update.Name = "sysPrivsDba_update";
            this.sysPrivsDba_update.Size = new System.Drawing.Size(70, 23);
            this.sysPrivsDba_update.TabIndex = 2;
            this.sysPrivsDba_update.Text = "保存";
            this.sysPrivsDba_update.Click += new System.EventHandler(this.sysPrivsDba_update_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.sysPrivsDba_select);
            this.panelControl2.Controls.Add(this.dba_name);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(11, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1091, 32);
            this.panelControl2.TabIndex = 0;
            // 
            // sysPrivsDba_select
            // 
            this.sysPrivsDba_select.Location = new System.Drawing.Point(315, 6);
            this.sysPrivsDba_select.Name = "sysPrivsDba_select";
            this.sysPrivsDba_select.Size = new System.Drawing.Size(70, 23);
            this.sysPrivsDba_select.TabIndex = 2;
            this.sysPrivsDba_select.Text = "查询";
            this.sysPrivsDba_select.Click += new System.EventHandler(this.sysPrivsDba_select_Click);
            // 
            // dba_name
            // 
            this.dba_name.EditValue = " ";
            this.dba_name.Location = new System.Drawing.Point(103, 6);
            this.dba_name.Name = "dba_name";
            this.dba_name.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dba_name.Properties.DisplayMember = "searchlook";
            this.dba_name.Properties.View = this.searchLookUpEdit1View;
            this.dba_name.Size = new System.Drawing.Size(175, 20);
            this.dba_name.TabIndex = 1;
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(5, 8);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(84, 14);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "直属下降管理员";
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1104, 630);
            this.Controls.Add(this.panelControl1);
            this.Name = "main";
            this.Text = "管理员权限";
            this.Load += new System.EventHandler(this.main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dba_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colcomp_code;
        private DevExpress.XtraGrid.Columns.GridColumn colcomp_name;
        private DevExpress.XtraGrid.Columns.GridColumn colcomp_leader;
        private DevExpress.XtraGrid.Columns.GridColumn colacc_manager;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton sysPrivsDba_update;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton sysPrivsDba_select;
        private DevExpress.XtraEditors.SearchLookUpEdit dba_name;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGrid.Columns.GridColumn col_pk2;
        private DevExpress.XtraGrid.Columns.GridColumn col_pk1;
    }
}