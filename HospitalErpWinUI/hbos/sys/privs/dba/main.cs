﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.privs.dba
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        private DataTable _dba_Pem;
        public main()
        {
            _helper = new DaoHelper(); 
            InitializeComponent();
        }

        #region 访问数据

        private void LoadNextLevelDba()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysNextLevelDba_select";        
            paramters.Add("user_id", G_User.user.User_id);
        
            serviceParamter.Paramters = paramters;

            List<CodeValue> objs = new List<CodeValue>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue);
            }
            //    emp_code.Properties.Items.AddRange(emps);
            dba_name.Properties.ValueMember = "Code";
            dba_name.Properties.DisplayMember = "Value";
            dba_name.Properties.DataSource = objs;
        }

        private void Query()
        {
            //sysDictsAcctSubjSys_select
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sysPrivsDba_select";

            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("js_id", G_User.user.User_id);
            dict.Add("temp_str", null);
            dict.Add("dba_code", FormHelper.GetValueForSearchLookUpEdit(dba_name));
            serviceParamter.Paramters = dict;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (string.IsNullOrEmpty(dt.Rows[i]["Column1"].ToString()))
                {
                    dt.Rows[i]["checked"] = "false";
                }
                else
                {
                    dt.Rows[i]["checked"] = "true";
                }
              
                //   string[] arragy = dt.Rows[i]["Column1"].ToString().Split('|');

                //  dt.Rows[i]["Column1"] = arragy[3].ToString();

            }
            _dba_Pem = dt;

            gridControl1.DataSource = _dba_Pem;

        }

        private void Update()
        {
            StringBuilder str = new StringBuilder();
            str.Append("<root><dba_name>" + FormHelper.GetValueForSearchLookUpEdit(dba_name) + "</dba_name>");
           
            for (int i = 0; i < _dba_Pem.Rows.Count; i++)
            {
                if (_dba_Pem.Rows[i]["checked"].ToString().ToLower().Equals("true"))
                {
                    str.Append("<record>");
                    str.Append("<pk1>" + _dba_Pem.Rows[i]["_pk1"].ToString() + "</pk1>");
                    str.Append("<pk2>" + _dba_Pem.Rows[i]["_pk2"].ToString() + "</pk2>");
                    str.Append("</record>");
                }
            }

            str.Append("</root>");

            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysPrivsDba_update";
            paramters.Add("doc", str.ToString());

            serviceParamter.Paramters = paramters;
            _helper.WirteSql(serviceParamter);

        }
      
        #endregion

        #region 事件
        private void sysPrivsDba_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
               
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void sysPrivsDba_update_Click(object sender, EventArgs e)
        {
            //sysPrivsDba_update
            //  <root><dba_name>cjyh</dba_name><record><pk1>1001</pk1><pk2>cjyh</pk2></record><record><pk1>1002</pk1><pk2>cjyh</pk2></record></root>
            if (string.IsNullOrEmpty(FormHelper.GetValueForSearchLookUpEdit(dba_name).Trim()) )
            {
                MessageForm.Warning("请选择下级管理员!");
                return; 
            }
            try
            {
                Update();
                MessageForm.Information("操作成功！");

            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void main_Load(object sender, EventArgs e)
        {
            try
            {
                LoadNextLevelDba();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }

        }
        #endregion
       
    }
}
