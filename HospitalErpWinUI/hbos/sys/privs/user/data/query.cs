﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.privs.user.data
{
    public partial class query : RibbonForm
    {
        protected DaoHelper _helper;
        public query()
        {
            InitializeComponent();
            _helper = new DaoHelper();
        }

        #region 访问数据
        private void LoadUset()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysUser_sel";
            paramters.Add("sj_id", G_User.user.User_id);
            paramters.Add("user_name", null);

            serviceParamter.Paramters = paramters;

            List<CodeValue> objs = new List<CodeValue>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue);
            }
            //    emp_code.Properties.Items.AddRange(emps);
            nextLevelUser_code.Properties.ValueMember = "Code";
            nextLevelUser_code.Properties.DisplayMember = "Value";
            nextLevelUser_code.Properties.DataSource = objs;
        }

        private void Query()
        {
            //sysDictsAcctSubjSys_select
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sysPrivsUserData_detail";


            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("js_id", G_User.user.User_id);
            dict.Add("user_code", FormHelper.GetValueForSearchLookUpEdit(nextLevelUser_code));
         
            serviceParamter.Paramters = dict;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);

            gridControl1.DataSource = dt;
        }
        #endregion

        #region 事件
        private void query_Load(object sender, EventArgs e)
        {
            try
            {
                LoadUset();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void sysPrivsUserData_detail_Click(object sender, EventArgs e)
        {
            try
            {
                Query();

            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        #endregion

       
       
    }
}
