﻿namespace HospitalErpWinUI.hbos.sys.privs.user.select
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.copy_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.sys_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comp_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.mod_name = new DevExpress.XtraEditors.TextEdit();
            this.sysPrivsGroupPrivsSelect_select = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.nextLevelUser_code = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copy_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sys_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mod_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextLevelUser_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.gridControl1);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.panelControl1.Size = new System.Drawing.Size(1262, 810);
            this.panelControl1.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.copy_code);
            this.panelControl2.Controls.Add(this.sys_code);
            this.panelControl2.Controls.Add(this.comp_code);
            this.panelControl2.Controls.Add(this.mod_name);
            this.panelControl2.Controls.Add(this.sysPrivsGroupPrivsSelect_select);
            this.panelControl2.Controls.Add(this.labelControl2);
            this.panelControl2.Controls.Add(this.labelControl5);
            this.panelControl2.Controls.Add(this.labelControl4);
            this.panelControl2.Controls.Add(this.labelControl3);
            this.panelControl2.Controls.Add(this.nextLevelUser_code);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(12, 2);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1248, 84);
            this.panelControl2.TabIndex = 1;
            // 
            // copy_code
            // 
            this.copy_code.Location = new System.Drawing.Point(64, 46);
            this.copy_code.Name = "copy_code";
            this.copy_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.copy_code.Properties.EditValueChanged += new System.EventHandler(this.copy_code_Properties_EditValueChanged);
            this.copy_code.Size = new System.Drawing.Size(200, 24);
            this.copy_code.TabIndex = 4;
            // 
            // sys_code
            // 
            this.sys_code.Location = new System.Drawing.Point(354, 46);
            this.sys_code.Name = "sys_code";
            this.sys_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sys_code.Size = new System.Drawing.Size(200, 24);
            this.sys_code.TabIndex = 4;
            // 
            // comp_code
            // 
            this.comp_code.Location = new System.Drawing.Point(354, 10);
            this.comp_code.Name = "comp_code";
            this.comp_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comp_code.Properties.EditValueChanged += new System.EventHandler(this.comp_code_Properties_EditValueChanged);
            this.comp_code.Size = new System.Drawing.Size(200, 24);
            this.comp_code.TabIndex = 4;
            // 
            // mod_name
            // 
            this.mod_name.Location = new System.Drawing.Point(674, 46);
            this.mod_name.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.mod_name.Name = "mod_name";
            this.mod_name.Size = new System.Drawing.Size(200, 24);
            this.mod_name.TabIndex = 3;
            // 
            // sysPrivsGroupPrivsSelect_select
            // 
            this.sysPrivsGroupPrivsSelect_select.Location = new System.Drawing.Point(785, 10);
            this.sysPrivsGroupPrivsSelect_select.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.sysPrivsGroupPrivsSelect_select.Name = "sysPrivsGroupPrivsSelect_select";
            this.sysPrivsGroupPrivsSelect_select.Size = new System.Drawing.Size(80, 30);
            this.sysPrivsGroupPrivsSelect_select.TabIndex = 2;
            this.sysPrivsGroupPrivsSelect_select.Text = "查询";
            this.sysPrivsGroupPrivsSelect_select.Click += new System.EventHandler(this.sysPrivsGroupPrivsSelect_select_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(303, 11);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(30, 18);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "单位";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(598, 49);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 18);
            this.labelControl5.TabIndex = 0;
            this.labelControl5.Text = "模块名称";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(303, 49);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(30, 18);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "系统";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(12, 49);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(30, 18);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "套账";
            // 
            // nextLevelUser_code
            // 
            this.nextLevelUser_code.EditValue = "";
            this.nextLevelUser_code.Location = new System.Drawing.Point(64, 9);
            this.nextLevelUser_code.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.nextLevelUser_code.Name = "nextLevelUser_code";
            this.nextLevelUser_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.nextLevelUser_code.Properties.View = this.searchLookUpEdit1View;
            this.nextLevelUser_code.Size = new System.Drawing.Size(200, 24);
            this.nextLevelUser_code.TabIndex = 1;
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 11);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(30, 18);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "用户";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControl1.Location = new System.Drawing.Point(12, 86);
            this.gridControl1.MainView = this.gridView4;
            this.gridControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1248, 722);
            this.gridControl1.TabIndex = 2;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView4.GridControl = this.gridControl1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "用户";
            this.gridColumn1.FieldName = "user_name";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "单位";
            this.gridColumn2.FieldName = "comp_name";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "套账";
            this.gridColumn3.FieldName = "copy_name";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "系统";
            this.gridColumn4.FieldName = "mod_name";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "模块";
            this.gridColumn5.FieldName = "perm_name";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "权限来源";
            this.gridColumn6.FieldName = "priv_type";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1262, 810);
            this.Controls.Add(this.panelControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "main";
            this.Text = "权限查询";
            this.Load += new System.EventHandler(this.main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copy_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sys_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mod_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextLevelUser_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.ComboBoxEdit copy_code;
        private DevExpress.XtraEditors.ComboBoxEdit sys_code;
        private DevExpress.XtraEditors.ComboBoxEdit comp_code;
        private DevExpress.XtraEditors.TextEdit mod_name;
        private DevExpress.XtraEditors.SimpleButton sysPrivsGroupPrivsSelect_select;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SearchLookUpEdit nextLevelUser_code;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
    }
}