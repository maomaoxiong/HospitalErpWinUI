﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.privs.mod
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        private DataTable _mod_Pem;
        public main()
        {
            _helper = new DaoHelper();
            InitializeComponent();
        }

        #region 访问数据

        private void LoadNextLevelDba()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysNextLevelDba_select";
            paramters.Add("user_id", G_User.user.User_id);

            serviceParamter.Paramters = paramters;

            List<CodeValue> objs = new List<CodeValue>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue);
            }
            //    emp_code.Properties.Items.AddRange(emps);
            dba_name.Properties.ValueMember = "Code";
            dba_name.Properties.DisplayMember = "Value";
            dba_name.Properties.DataSource = objs;
        }

        private void Query()
        {
            //sysDictsAcctSubjSys_select
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sysPrivsMod_select";

            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("js_id", G_User.user.User_id);
            dict.Add("temp_str", null);
            dict.Add("child_user_code", FormHelper.GetValueForSearchLookUpEdit(dba_name));
            dict.Add("user_code", G_User.user.User_code);
            serviceParamter.Paramters = dict;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (!dt.Rows[i]["Column1"].ToString().Equals("1"))
                {
                    dt.Rows[i]["checked"] = "false";
                }
                else
                {
                    dt.Rows[i]["checked"] = "true";
                }

                //   string[] arragy = dt.Rows[i]["Column1"].ToString().Split('|');

                //  dt.Rows[i]["Column1"] = arragy[3].ToString();

            }
            _mod_Pem = dt;

            gridControl1.DataSource = _mod_Pem;

        }

        private void Update()
        {
            string str = string.Empty;
           

            for (int i = 0; i < _mod_Pem.Rows.Count; i++)
            {
                if (_mod_Pem.Rows[i]["checked"].ToString().ToLower().Equals("true"))
                {
                    str += "'"+_mod_Pem.Rows[i]["mod_code"].ToString()+"'";
                    str += ",";
                   
                }
            }
            str = str.Substring(0, str.Length - 1);
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysPrivsMod_update";
            paramters.Add("sj_id",G_User.user.User_id);
            paramters.Add("temp_str", str.ToString());
            paramters.Add("user_code", FormHelper.GetValueForSearchLookUpEdit(dba_name));

            serviceParamter.Paramters = paramters;
            _helper.WirteSql(serviceParamter);

        }
       
        #endregion

        #region 事件
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            //        string fileName = string.Empty; //文件名
            //        //打开文件
            //        OpenFileDialog dlg = new OpenFileDialog();
            //        dlg.DefaultExt = "txt";
            //        dlg.Filter = "Txt Files|*.txt";
            //        if (dlg.ShowDialog() == DialogResult.OK)
            //            fileName = dlg.FileName;
            //        if (fileName == null)
            //            return;
            //        //读取文件内容
            //        StreamReader sr = new StreamReader(fileName, System.Text.Encoding.Default);
            //        String ls_input = sr.ReadToEnd().TrimStart();           
            //        sr.Close();
            //        string url = ConfigurationManager.AppSettings["ServicesAdress"] + "/Vouch/Add";
            //        Dictionary<string, string> dic = new Dictionary<string, string>();
            ////        dic.Add("paramter", ls_input.Trim());
            //        dic.Add("", ls_input.Trim());    
            //        HttpHelper.CreateHttpPostResponse(url, dic,G_User.user.Ticket,"vouchInsert");

        }

    
        private void sysPrivsDba_update_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(FormHelper.GetValueForSearchLookUpEdit(dba_name).Trim()))
            {
                MessageForm.Warning("请选择下级管理员!");
                return;
            }
            try
            {
                Update();
                MessageForm.Information("操作成功！");

            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void main_Load(object sender, EventArgs e)
        {
            try
            {
                LoadNextLevelDba();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        #endregion

        private void sysPrivsDba_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();

            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void panelControl1_Paint(object sender, PaintEventArgs e)
        {

        }

      
      
    }
}
