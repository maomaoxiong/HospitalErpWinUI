﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.privs.usergroup.select
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        public main()
        {
            InitializeComponent();
            _helper = new DaoHelper();
        }

        #region 访问数据

        private void Query()
        {
            //sysDictsAcctSubjSys_select
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sysPrivsGroupPrivsSelect_select";

            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("js_id", G_User.user.User_id);
            dict.Add("group_code", FormHelper.GetValueForSearchLookUpEdit(nextLevelUserGroup_code));
            dict.Add("comp_code",FormHelper.GetValueForComboBoxEdit(comp_code));
            dict.Add("copy_code",FormHelper.GetValueForComboBoxEdit(copy_code));
            dict.Add("sys_code",FormHelper.GetValueForComboBoxEdit(sys_code));
            dict.Add("mod_name", mod_name.Text);
            serviceParamter.Paramters = dict;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
       
            gridControl1.DataSource = dt;
        }
        private void LoadGroup()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysUserGroup_select";
            paramters.Add("user_id", G_User.user.User_id);

            serviceParamter.Paramters = paramters;

            List<CodeValue> objs = new List<CodeValue>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue);
            }
            //    emp_code.Properties.Items.AddRange(emps);
            nextLevelUserGroup_code.Properties.ValueMember = "Code";
            nextLevelUserGroup_code.Properties.DisplayMember = "Value";
            nextLevelUserGroup_code.Properties.DataSource = objs;
        }

        private void LoadComp()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysCompany_select";
            paramters.Add("user_id", G_User.user.User_id);

            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string> ();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                 CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            comp_code.Properties.Items.AddRange(objs);
        
        }

        private void LoadSys()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysMod_select_perm";
            paramters.Add("user_code", null);
            paramters.Add("user_id", G_User.user.User_id);
            paramters.Add("copy_code",  FormHelper.GetValueForComboBoxEdit( copy_code));
            paramters.Add("comp_code", FormHelper.GetValueForComboBoxEdit(comp_code));

            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            sys_code.Properties.Items.AddRange(objs);

        }

        private void LoadCopy()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysCopy_select";

            
            paramters.Add("comp_code", FormHelper.GetValueForComboBoxEdit(comp_code));

            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            copy_code.Properties.Items.AddRange(objs);

        }
        #endregion
        private void sysPrivsGroupPrivsSelect_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();

            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void main_Load(object sender, EventArgs e)
        {
            try
            { 
                LoadGroup();
                LoadComp();
               
            }
            catch(Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
           
        }

        private void comp_code_Properties_EditValueChanged(object sender, EventArgs e)
        {
            LoadCopy();
        }

        private void copy_code_Properties_EditValueChanged(object sender, EventArgs e)
        {
            LoadSys();
        }
    }
}
