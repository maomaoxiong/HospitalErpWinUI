﻿namespace HospitalErpWinUI.hbos.sys.dicts.unitman
{
    partial class query
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.sysDictsUnitmanQuery_select = new DevExpress.XtraEditors.SimpleButton();
            this.u_copy = new DevExpress.XtraEditors.TextEdit();
            this.u_unit = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colChecked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colComp_code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colComp_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProv = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDis_email = new DevExpress.XtraGrid.Columns.GridColumn();
            this.collink_phone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.collinkman = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltax_no = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcomp_leader = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colacc_manager = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_comp_code = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.u_copy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_unit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.sysDictsUnitmanQuery_select);
            this.panelControl2.Controls.Add(this.u_copy);
            this.panelControl2.Controls.Add(this.u_unit);
            this.panelControl2.Controls.Add(this.label1);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1262, 34);
            this.panelControl2.TabIndex = 1;
            // 
            // sysDictsUnitmanQuery_select
            // 
            this.sysDictsUnitmanQuery_select.Location = new System.Drawing.Point(564, 5);
            this.sysDictsUnitmanQuery_select.Name = "sysDictsUnitmanQuery_select";
            this.sysDictsUnitmanQuery_select.Size = new System.Drawing.Size(80, 25);
            this.sysDictsUnitmanQuery_select.TabIndex = 3;
            this.sysDictsUnitmanQuery_select.Text = "查询";
            this.sysDictsUnitmanQuery_select.Click += new System.EventHandler(this.sysDictsUnitmanQuery_select_Click);
            // 
            // u_copy
            // 
            this.u_copy.Location = new System.Drawing.Point(348, 5);
            this.u_copy.Name = "u_copy";
            this.u_copy.Size = new System.Drawing.Size(200, 24);
            this.u_copy.TabIndex = 2;
            // 
            // u_unit
            // 
            this.u_unit.Location = new System.Drawing.Point(73, 5);
            this.u_unit.Name = "u_unit";
            this.u_unit.Size = new System.Drawing.Size(200, 24);
            this.u_unit.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(279, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "单位名称";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 8);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 18);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "单位编码";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(0, 34);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemHyperLinkEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1262, 641);
            this.gridControl1.TabIndex = 3;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colChecked,
            this.colComp_code,
            this.colComp_name,
            this.colProv,
            this.colCity,
            this.colAddress,
            this.colDis_email,
            this.collink_phone,
            this.collinkman,
            this.coltax_no,
            this.colcomp_leader,
            this.colacc_manager,
            this.col_comp_code});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colChecked
            // 
            this.colChecked.AppearanceHeader.Options.UseTextOptions = true;
            this.colChecked.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colChecked.Caption = "选择";
            this.colChecked.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colChecked.FieldName = "checked";
            this.colChecked.Name = "colChecked";
            this.colChecked.Visible = true;
            this.colChecked.VisibleIndex = 0;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // colComp_code
            // 
            this.colComp_code.Caption = "单位编码";
            this.colComp_code.FieldName = "comp_code";
            this.colComp_code.Name = "colComp_code";
            this.colComp_code.Visible = true;
            this.colComp_code.VisibleIndex = 1;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            // 
            // colComp_name
            // 
            this.colComp_name.Caption = "单位名称";
            this.colComp_name.FieldName = "comp_name";
            this.colComp_name.Name = "colComp_name";
            this.colComp_name.Visible = true;
            this.colComp_name.VisibleIndex = 2;
            // 
            // colProv
            // 
            this.colProv.Caption = "省";
            this.colProv.FieldName = "prov";
            this.colProv.Name = "colProv";
            this.colProv.Visible = true;
            this.colProv.VisibleIndex = 3;
            // 
            // colCity
            // 
            this.colCity.Caption = "市";
            this.colCity.FieldName = "city";
            this.colCity.Name = "colCity";
            this.colCity.Visible = true;
            this.colCity.VisibleIndex = 4;
            // 
            // colAddress
            // 
            this.colAddress.Caption = "地址";
            this.colAddress.FieldName = "address";
            this.colAddress.Name = "colAddress";
            this.colAddress.Visible = true;
            this.colAddress.VisibleIndex = 5;
            // 
            // colDis_email
            // 
            this.colDis_email.Caption = "Email";
            this.colDis_email.FieldName = "dis_email";
            this.colDis_email.Name = "colDis_email";
            this.colDis_email.Visible = true;
            this.colDis_email.VisibleIndex = 6;
            // 
            // collink_phone
            // 
            this.collink_phone.Caption = "联系电话";
            this.collink_phone.FieldName = "link_phone";
            this.collink_phone.Name = "collink_phone";
            this.collink_phone.Visible = true;
            this.collink_phone.VisibleIndex = 7;
            // 
            // collinkman
            // 
            this.collinkman.Caption = "联系人";
            this.collinkman.FieldName = "linkman";
            this.collinkman.Name = "collinkman";
            this.collinkman.Visible = true;
            this.collinkman.VisibleIndex = 8;
            // 
            // coltax_no
            // 
            this.coltax_no.Caption = "税务证号";
            this.coltax_no.FieldName = "tax_no";
            this.coltax_no.Name = "coltax_no";
            this.coltax_no.Visible = true;
            this.coltax_no.VisibleIndex = 9;
            // 
            // colcomp_leader
            // 
            this.colcomp_leader.Caption = "单位领导";
            this.colcomp_leader.FieldName = "comp_leader";
            this.colcomp_leader.Name = "colcomp_leader";
            this.colcomp_leader.Visible = true;
            this.colcomp_leader.VisibleIndex = 10;
            // 
            // colacc_manager
            // 
            this.colacc_manager.Caption = "主管会计";
            this.colacc_manager.FieldName = "acc_manager";
            this.colacc_manager.Name = "colacc_manager";
            this.colacc_manager.Visible = true;
            this.colacc_manager.VisibleIndex = 11;
            // 
            // col_comp_code
            // 
            this.col_comp_code.Caption = "gridColumn1";
            this.col_comp_code.FieldName = "_comp_code";
            this.col_comp_code.Name = "col_comp_code";
            // 
            // query
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1262, 675);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl2);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "query";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "单位编码体系查询";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.u_copy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_unit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton sysDictsUnitmanQuery_select;
        private DevExpress.XtraEditors.TextEdit u_copy;
        private DevExpress.XtraEditors.TextEdit u_unit;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colChecked;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colComp_code;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colComp_name;
        private DevExpress.XtraGrid.Columns.GridColumn colProv;
        private DevExpress.XtraGrid.Columns.GridColumn colCity;
        private DevExpress.XtraGrid.Columns.GridColumn colAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colDis_email;
        private DevExpress.XtraGrid.Columns.GridColumn collink_phone;
        private DevExpress.XtraGrid.Columns.GridColumn collinkman;
        private DevExpress.XtraGrid.Columns.GridColumn coltax_no;
        private DevExpress.XtraGrid.Columns.GridColumn colcomp_leader;
        private DevExpress.XtraGrid.Columns.GridColumn colacc_manager;
        private DevExpress.XtraGrid.Columns.GridColumn col_comp_code;

    }
}