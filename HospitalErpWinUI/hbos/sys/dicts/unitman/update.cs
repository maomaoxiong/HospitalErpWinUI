﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitman
{
    public partial class update : Form
    {
       private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        private string _comp_code;
      //  private string _unit_code;
      
        public update()
        {
            InitializeComponent();
            Init();
            
        }
        public update(DaoHelper helper, List<Perm> perms, string comp_code)
        {
            InitializeComponent();
            this._helper = helper;
            this._perms = perms;
            this._comp_code = comp_code;
          // this._unit_code = unit_code;
           
            Init();
        }

        #region 其他
        private bool Validate()
        {

            if (string.IsNullOrEmpty(u_code.Text))
            {
                MessageForm.Warning("单位编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(u_name.Text))
            {
                MessageForm.Warning("单位名称不能为空!");
                return false;
            }
         

            if (FormHelper.Text_Length(u_code.Text) > 20)
            {
                MessageForm.Warning("单位编码字符串长度超过数据库允许的长度20!");
                return false;
            }
            if (FormHelper.Text_Length(u_name.Text) > 40)
            {
                MessageForm.Warning("单位名称字符串长度超过数据库允许的长度40!");
                return false;
            }
            if (FormHelper.Text_Length(u_prov.Text) > 20)
            {
                MessageForm.Warning("省字符串长度超过数据库允许的长度20!");
                return false;
            }
            if (FormHelper.Text_Length(u_city.Text) > 20)
            {
                MessageForm.Warning("市字符串长度超过数据库允许的长度20!");
                return false;
             
            }
            if (FormHelper.Text_Length(u_addr.Text) > 40)
            {
                MessageForm.Warning("地址符串长度超过数据库允许的长度40!");
                return false;
            }
            if (FormHelper.Text_Length(u_mail.Text) > 40)
            {
                MessageForm.Warning("u_mail字符串长度超过数据库允许的长度40!");
                return false;

            }
            if (FormHelper.Text_Length(u_phon.Text) >20)
            {
                MessageForm.Warning("联系电话字符串长度超过数据库允许的长度20!");
                return false;
            }
            if (FormHelper.Text_Length(u_lman.Text) > 40)
            {
                MessageForm.Warning("联系人字符串长度超过数据库允许的长度40!");
                return false;
            }
            if (FormHelper.Text_Length(u_taxn.Text) > 20)
            {
                MessageForm.Warning("税务证号字符串长度超过数据库允许的长度20!");
                return false;
            }
            if (FormHelper.Text_Length(u_lead.Text) > 40)
            {
                MessageForm.Warning("单位领导字符串长度超过数据库允许的长度40!");
                return false;
            }
            if (FormHelper.Text_Length(u_mana.Text) > 40)
            {
                MessageForm.Warning("财务主管字符串长度超过数据库允许的长度40!");
                return false;
            }
            common on = new common();
            this.u_parent.Text= on.checkRuleCodeFormat(u_code.Text, "0101", true);   
            return true;
        }
            

        private void Init()
        {
          
            this.StartPosition = FormStartPosition.CenterScreen;
            this.u_code.Properties.MaxLength = 20;
            this.u_name.Properties.MaxLength = 40;  
            this.u_prov.Properties.MaxLength = 20;  
            this.u_city.Properties.MaxLength = 20;
            this.u_addr.Properties.MaxLength = 40;
            this.u_mail.Properties.MaxLength = 40;
            this.u_phon.Properties.MaxLength = 20;
            this.u_lman.Properties.MaxLength = 40;
            this.u_taxn.Properties.MaxLength = 20;
            this.u_lead.Properties.MaxLength = 40; 
            this.u_mana.Properties.MaxLength = 40;
            


            this.u_last.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comp_level_code.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comp_type_code.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
        }
      
        private string GetRuleCodeText(string formatId, string prefixText, string comp_copy)
        {
            string rule = string.Empty;
            if (!string.IsNullOrEmpty(comp_copy))
            {
                rule = sys_code_format_comp_copy();
            }
            else
            {
                rule = sys_code_format_para(formatId);
            }
            return  rule;
        }
        #endregion


        #region 访问数据库
        private string sys_code_format_comp_copy()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("comp_code", null);
            dict.Add("copy_code", null);
            dict.Add("code_code", null);
            serviceParamter.ServiceId = "sys_code_format_comp_copy";
            serviceParamter.Paramters = dict;
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            return dt.Rows[0][0].ToString();
        }

        private string sys_code_format_para(string paramter)
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("para_code", paramter);

            serviceParamter.ServiceId = "sys_code_format_para";
            serviceParamter.Paramters = dict;
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            return dt.Rows[0][0].ToString();
        }

        private void LoadYesNo()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "dict_yes_or_no";
            Dictionary<string, string> dict = new Dictionary<string, string>();
            serviceParamter.Paramters = dict;

            List<CodeValue> objs = _helper.ReadDictForData(serviceParamter);
            List<string> codeValues = new List<string>();
            foreach (CodeValue obj in objs)
            {

                codeValues.Add(obj.Value);
            }
            u_last.Properties.Items.AddRange(codeValues);
            u_last.SelectedItem = codeValues[0];


        }

        private void LoadLevel()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sysComLevelDict";
            Dictionary<string, string> dict = new Dictionary<string, string>();
            serviceParamter.Paramters = dict;

            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            comp_level_code.Properties.Items.AddRange(objs);
            //     comp_level_code.SelectedItem = objs[0];

        }
        //comp_type_code
        private void LoadTypeCode()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sysComTypeDict";
            Dictionary<string, string> dict = new Dictionary<string, string>();
            serviceParamter.Paramters = dict;

            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            comp_type_code.Properties.Items.AddRange(objs);
            //   comp_type_code.SelectedItem = objs[0];


        }

        private void LoadData()
        {
            StringBuilder sqlstr = new StringBuilder();
            sqlstr.Append(@"SELECT is_count,comp_code,comp_name,prov,city,address,dis_email,link_phone,linkman,tax_no,comp_leader,acc_manager,is_last,comp_level_code,comp_type_code ");
            sqlstr.Append(@" FROM sys_company where comp_code='?'");


            DataTable dt = _helper.ExecReadSql(sqlstr.ToString().Replace("?", _comp_code));
            if (dt != null && dt.Rows.Count != 0)
            {
                this.u_code.Text = dt.Rows[0]["comp_code"].ToString ();
                this.u_name.Text =dt.Rows[0]["comp_name"].ToString ();
                this.u_prov.Text = dt.Rows[0]["prov"].ToString();
                this.u_city.Text = dt.Rows[0]["city"].ToString();
                this.is_count.EditValue = dt.Rows[0]["is_count"];
                this.u_addr.Text = dt.Rows[0]["address"].ToString();
                this.u_mail.Text = dt.Rows[0]["dis_email"].ToString();
                this.u_phon.Text = dt.Rows[0]["link_phone"].ToString();
                this.u_lman.Text = dt.Rows[0]["linkman"].ToString();

                this.u_taxn.Text = dt.Rows[0]["tax_no"].ToString();
                this.u_lead.Text = dt.Rows[0]["comp_leader"].ToString();
                this.u_mana.Text = dt.Rows[0]["acc_manager"].ToString();
              
                FormHelper.ComboBoxeditAssignment(this.u_last, dt.Rows[0]["is_last"].ToString());
                FormHelper.ComboBoxeditAssignment(this.comp_level_code, dt.Rows[0]["comp_level_code"].ToString());
                FormHelper.ComboBoxeditAssignment(this.comp_type_code, dt.Rows[0]["comp_type_code"].ToString());
              

            }

        }

        private bool Update()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sysDictsUnitman_update";
            List<string> strCtrls = new List<string>();

            strCtrls.Add("u_code");
            strCtrls.Add("u_name");
            strCtrls.Add("u_parent");
            strCtrls.Add("u_prov");

            strCtrls.Add("u_city");
            strCtrls.Add("is_count");
            strCtrls.Add("u_addr");
            strCtrls.Add("u_mail");
            strCtrls.Add("u_phon");
            strCtrls.Add("u_lman");
            strCtrls.Add("u_taxn");

            strCtrls.Add("u_lead");
            strCtrls.Add("u_mana");
            strCtrls.Add("u_last");
            strCtrls.Add("comp_level_code");
            strCtrls.Add("comp_type_code");

            FormHelper.GenerateParamerByControl(this.Controls, ref dict, strCtrls);
            serviceParamter.Paramters = dict;
            return _helper.WirteSql(serviceParamter);

        }

        #endregion

        #region 事件
        private void update_Load(object sender, EventArgs e)
        {
            try
            {
                labelInfo.Text = GetRuleCodeText("0101", "", "");
                LoadYesNo();
                LoadLevel();
                LoadTypeCode();
                LoadData();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void CloseBut_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void AddBut_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Update();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }

           
        }
        #endregion

        private void u_mana_Properties_Click(object sender, EventArgs e)
        {
            AcctManager frm = new AcctManager(_helper, _perms, _comp_code);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                this.u_mana.Text = frm._resultManager;
            } 
        }

      
       
    }
}
