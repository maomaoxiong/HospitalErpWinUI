﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitman 
{
    public partial class query : RibbonForm
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        DataTable _unit;
      
        public query()
        {
            _helper = new DaoHelper();
            InitializeComponent();
            this.Dock = DockStyle.Fill;
           
        }

        private void Query()
        {
            //sysDictsAcctSubjSys_select
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sysDictsUnitmanQuery_select";

            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("c_code", u_unit.Text.Trim());
            dict.Add("c_name", u_copy.Text.Trim());
            serviceParamter.Paramters = dict;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            _unit = dt;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
                //   string[] arragy = dt.Rows[i]["Column1"].ToString().Split('|');

                //  dt.Rows[i]["Column1"] = arragy[3].ToString();

            }
            //    _user = dt;
            gridControl1.DataSource = _unit;

        }

        private void sysDictsUnitmanQuery_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();

            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message); //hbos/sys/privs/dba
            }
        }
    }
}
