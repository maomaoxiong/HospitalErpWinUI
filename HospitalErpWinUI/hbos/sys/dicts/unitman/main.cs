﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitman
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        DataTable _unit;
        public main()
        {
            _helper = new DaoHelper();
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            _unit = new DataTable();
     
        } 

        
        #region  其他
       

        #endregion

        #region 访问数据库
        private bool Delete(string code)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sysDictsUnitman_delete";


            dict.Add("u_code", code);
            dict.Add("u_name","");
            dict.Add("u_parent","");
            dict.Add("u_prov","");

            dict.Add("u_city","");
            dict.Add("is_count","");
            dict.Add("u_addr","");
            dict.Add("u_mail","");
            dict.Add("u_phon","");
            dict.Add("u_lman","");
            dict.Add("u_taxn","");

            dict.Add("u_lead","");
            dict.Add("u_mana","");
            dict.Add("u_last","");
            dict.Add("comp_level_code","");
            dict.Add("comp_type_code","");
            serviceParamter.Paramters = dict;

            bool result = false;
            try
            {
                _helper.WirteSql(serviceParamter);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }
        private void Query()
        {
            //sysDictsAcctSubjSys_select
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sysDictsUnitmanQuery_select";

            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("c_code", u_unit.Text.Trim());
            dict.Add("c_name", u_copy.Text.Trim());
            serviceParamter.Paramters = dict;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            _unit = dt;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
                //   string[] arragy = dt.Rows[i]["Column1"].ToString().Split('|');

                //  dt.Rows[i]["Column1"] = arragy[3].ToString();

            }
            //    _user = dt;
            gridControl1.DataSource = _unit;

        }
      
        #endregion
        #region 事件
        private void Main_Load(object sender, EventArgs e)
        {

        }

        private void sysDictsUnitmanQuery_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
              
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void AddBut_Click(object sender, EventArgs e)
        {
            try
            {
                insert insertFrm = new insert(this._helper, this._perms);

                if (insertFrm.ShowDialog() == DialogResult.OK)
                {
                    Query();
                }
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void DeleteBut_Click(object sender, EventArgs e)
        {
            bool value = false;
            string strSelected = string.Empty;

            try
            {
                List<string> objs = new List<string>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    value = Convert.ToBoolean(gridView1.GetDataRow(i)["checked"]);
                    if (value)
                    {
                        string code = gridView1.GetDataRow(i)["comp_code"].ToString();
                        objs.Add(code);

                    }
                }
                if (objs.Count == 0)
                {
                    MessageForm.Show("请选择要删除的数据!");
                    return;
                }
                if (MessageForm.ShowMsg("确认是否删除?", MsgType.YesNo) == DialogResult.Yes)
                {
                    foreach (string obj in objs)
                    {
                        if (!Delete(obj))
                        {
                            return;
                        }
                    }
                    //       MessageForm.Show("操作成功!");
                    Query();
                }

            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }

        }
        #endregion

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, DevExpress.XtraEditors.Controls.OpenLinkEventArgs e)
        {
            if (gridView1.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                int index = gridView1.GetDataSourceRowIndex(gridView1.FocusedRowHandle);
                string url = _unit.Rows[index]["comp_code"].ToString();
              
                update frm = new update(this._helper, this._perms, url);

                if (frm.ShowDialog() == DialogResult.No)
                {
                    MessageForm.Warning("修改失败！");
                }
                Query();
            }
        }

        private void u_unit_EditValueChanged(object sender, EventArgs e)
        {

        }

       
    }
}
