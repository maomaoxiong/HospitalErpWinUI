﻿using Dao;
using HospitalErpData.MenuHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using HospitalErpWinUI.WinFormUiHelper;

namespace HospitalErpWinUI.hbos.sys.dicts.unitman
{
    public partial class AcctManager : Form
    {
        private DataTable _manager;
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        private string _comp_code ;
        public string _resultManager { get; set; }
      
        public AcctManager()
        {
            InitializeComponent();
            Init();
        }
        public AcctManager( DaoHelper helper,List<Perm> perms, string comp_code)
        {
            this._helper = helper;
            this._perms = perms;
            _comp_code = comp_code;
           
            InitializeComponent();
            Init();
        }
         #region 其他
        private void Init()
        {
            this.StartPosition = FormStartPosition.CenterScreen;

        //    gridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
         //   gridView1.OptionsBehavior.ReadOnly = false;
       //     gridView1.OptionsBehavior.Editable = true;
       //     gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            gridView1.Columns["end_date"].OptionsColumn.AllowEdit = false;//设置列不可以编辑

            
        }
        #endregion


        #region 事件
        private void AcctManager_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void gridView1_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;


            view.SetRowCellValue(e.RowHandle, view.Columns["end_date"], "2099-12-31");

        }

        private void deleteBut_Click(object sender, EventArgs e)
        {
            bool value = false;
            string strSelected = string.Empty;

            try
            {
                List<string> objs = new List<string>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    value = Convert.ToBoolean(gridView1.GetDataRow(i)["checked"]);
                    if (value)
                    {
                        string code = gridView1.GetDataRow(i)["_id"].ToString();
                        objs.Add(code);

                    }
                }
                if (objs.Count == 0)
                {
                    MessageForm.Show("请选择要删除的数据!");
                    return;
                }
                if (MessageForm.ShowMsg("确认是否删除?", MsgType.YesNo) == DialogResult.Yes)
                {
                    foreach (string obj in objs)
                    {
                        if (!Delete(obj))
                        {
                            return;
                        }
                    }
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                    //       MessageForm.Show("操作成功!");
                    LoadData();
                }

            }
            catch (Exception ex)
            {
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                MessageForm.Exception(ex.Message);
            }
        }

        private void saveBut_Click(object sender, EventArgs e)
        {
            string id = _manager.Rows[0]["_id"].ToString();
            try 
            {
                _resultManager = _manager.Rows[0]["acc_manager"].ToString();
                if (string.IsNullOrEmpty(id))
                {
                    Insert();
                  
                }
                else
                {
                    UpdateManager();
                    UpdateComp();
                    
                }
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            catch(Exception ex)
            {
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                MessageForm.Exception(ex.Message);
            }
           
        }
        #endregion

        #region 访问数据
        private bool Insert()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sysacc_manager_insert";
            dict.Add("comp_code", _comp_code);
            dict.Add("acc_manager", _manager.Rows[0]["acc_manager"].ToString());
           
            dict.Add("starts_date", _manager.Rows[0]["starts_date"].ToString());
            dict.Add("end_date", _manager.Rows[0]["end_date"].ToString());
           
       //     dict.Add("_id", _manager.Rows[0]["_id"].ToString());
            serviceParamter.Paramters = dict;
            return _helper.WirteSql(serviceParamter);

        }
        private bool UpdateManager()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sysacc_manager_update";
            dict.Add("comp_code", _comp_code);
            dict.Add("_id", _manager.Rows[0]["_id"].ToString());
            dict.Add("acc_manager", _manager.Rows[0]["acc_manager"].ToString());
            dict.Add("starts_date", _manager.Rows[0]["starts_date"].ToString());
            dict.Add("end_date", _manager.Rows[0]["end_date"].ToString());

            //     dict.Add("_id", _manager.Rows[0]["_id"].ToString());
            serviceParamter.Paramters = dict;
            return _helper.WirteSql(serviceParamter);

        }
        private bool UpdateComp()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "upadte_manager";
            dict.Add("comp_code", _manager.Rows[0]["comp_code"].ToString());
            dict.Add("acc_manager", _manager.Rows[0]["acc_manager"].ToString());
         
            serviceParamter.Paramters = dict;
            return _helper.WirteSql(serviceParamter);

        }

        private void LoadData()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("c_code", _comp_code);
            serviceParamter.ServiceId = "sysacc_manager_select";
            serviceParamter.Paramters = dict;
            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);

            if(dt.Rows.Count == 0 )
            {
                DataColumn acc_manager = new DataColumn("acc_manager", typeof(string));
                DataColumn starts_date = new DataColumn("starts_date", typeof(string));
                DataColumn end_date = new DataColumn("end_date", typeof(string));
                DataColumn comp_code = new DataColumn("comp_code", typeof(string));
                DataColumn _id = new DataColumn("_id", typeof(string));
             
                dt.Columns.Add(acc_manager);
                dt.Columns.Add(starts_date);
                dt.Columns.Add(end_date);
                dt.Columns.Add(comp_code);
                dt.Columns.Add(_id);

                DataRow dr = dt.NewRow();

                dr["acc_manager"] = "";
                dr["starts_date"] = "";
                dr["end_date"] = "2099-12-31";
                dr["comp_code"] = "";
                dr["_id"] = "";              
                dt.Rows.Add(dr);
            }
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
         
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
               
            }
            _manager = dt;
            gridControl1.DataSource = _manager;


        }

        private bool Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
                return true;
            Dictionary<string, string> dict = new Dictionary<string, string>();
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sysacc_manager_delete";
            dict.Add("comp_code",_comp_code);
            dict.Add("id",id);
            serviceParamter.Paramters = dict;

            bool result = false;
            try
            {
                _helper.WirteSql(serviceParamter);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }
     
        #endregion


        /*
       
         SELECT acc_manager,convert(nvarchar(10),starts_date,121)starts_date,convert(nvarchar(10),end_date,121)end_date,comp_code,id _id
                 FROM sys_acc_manager
                     WHERE comp_code=@c_code
         * */


    }
}
