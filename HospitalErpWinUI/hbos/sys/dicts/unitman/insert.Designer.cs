﻿namespace HospitalErpWinUI.hbos.sys.dicts.unitman
{
    partial class insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.u_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.u_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.u_prov = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.u_city = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.u_addr = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.u_mail = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.u_phon = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.u_lman = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.u_taxn = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.u_lead = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.AddBut = new DevExpress.XtraEditors.SimpleButton();
            this.CloseBut = new DevExpress.XtraEditors.SimpleButton();
            this.u_last = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comp_level_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comp_type_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelInfo = new DevExpress.XtraEditors.LabelControl();
            this.is_count = new DevExpress.XtraEditors.RadioGroup();
            this.u_parent = new DevExpress.XtraEditors.TextEdit();
            this.u_mana = new DevExpress.XtraEditors.ButtonEdit();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_prov.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_city.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_addr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_mail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_phon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_lman.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_taxn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_lead.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_last.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp_level_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp_type_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_count.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_parent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_mana.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(33, 79);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 18);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "单位编码";
         //   this.labelControl1.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // u_code
            // 
            this.u_code.Location = new System.Drawing.Point(120, 76);
            this.u_code.Name = "u_code";
            this.u_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.u_code.Properties.Appearance.Options.UseBackColor = true;
            this.u_code.Size = new System.Drawing.Size(200, 24);
            this.u_code.TabIndex = 0;
            //this.u_code.EditValueChanged += new System.EventHandler(this.u_code_EditValueChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(365, 79);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 18);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "单位名称";
          //  this.labelControl2.Click += new System.EventHandler(this.labelControl2_Click);
            // 
            // u_name
            // 
            this.u_name.Location = new System.Drawing.Point(452, 76);
            this.u_name.Name = "u_name";
            this.u_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.u_name.Properties.Appearance.Options.UseBackColor = true;
            this.u_name.Size = new System.Drawing.Size(200, 24);
            this.u_name.TabIndex = 1;
          //  this.u_name.EditValueChanged += new System.EventHandler(this.u_name_EditValueChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(33, 121);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(15, 18);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "省";
        //    this.labelControl3.Click += new System.EventHandler(this.labelControl3_Click);
            // 
            // u_prov
            // 
            this.u_prov.Location = new System.Drawing.Point(120, 118);
            this.u_prov.Name = "u_prov";
            this.u_prov.Size = new System.Drawing.Size(200, 24);
            this.u_prov.TabIndex = 2;
        //    this.u_prov.EditValueChanged += new System.EventHandler(this.u_prov_EditValueChanged);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(365, 121);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(15, 18);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "市";
        //    this.labelControl4.Click += new System.EventHandler(this.labelControl4_Click);
            // 
            // u_city
            // 
            this.u_city.Location = new System.Drawing.Point(452, 118);
            this.u_city.Name = "u_city";
            this.u_city.Size = new System.Drawing.Size(81, 24);
            this.u_city.TabIndex = 3;
          //  this.u_city.EditValueChanged += new System.EventHandler(this.u_city_EditValueChanged);
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(33, 164);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(30, 18);
            this.labelControl5.TabIndex = 0;
            this.labelControl5.Text = "地址";
        //    this.labelControl5.Click += new System.EventHandler(this.labelControl5_Click);
            // 
            // u_addr
            // 
            this.u_addr.Location = new System.Drawing.Point(120, 161);
            this.u_addr.Name = "u_addr";
            this.u_addr.Size = new System.Drawing.Size(200, 24);
            this.u_addr.TabIndex = 6;
       //     this.u_addr.EditValueChanged += new System.EventHandler(this.u_addr_EditValueChanged);
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(365, 164);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(33, 18);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = "Email";
        //    this.labelControl6.Click += new System.EventHandler(this.labelControl6_Click);
            // 
            // u_mail
            // 
            this.u_mail.Location = new System.Drawing.Point(452, 161);
            this.u_mail.Name = "u_mail";
            this.u_mail.Size = new System.Drawing.Size(200, 24);
            this.u_mail.TabIndex = 7;
       //     this.u_mail.EditValueChanged += new System.EventHandler(this.u_mail_EditValueChanged);
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(33, 208);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(60, 18);
            this.labelControl7.TabIndex = 0;
            this.labelControl7.Text = "联系电话";
         //   this.labelControl7.Click += new System.EventHandler(this.labelControl7_Click);
            // 
            // u_phon
            // 
            this.u_phon.Location = new System.Drawing.Point(120, 205);
            this.u_phon.Name = "u_phon";
            this.u_phon.Size = new System.Drawing.Size(200, 24);
            this.u_phon.TabIndex = 8;
         //   this.u_phon.EditValueChanged += new System.EventHandler(this.u_phon_EditValueChanged);
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(365, 208);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(45, 18);
            this.labelControl8.TabIndex = 0;
            this.labelControl8.Text = "联系人";
        //    this.labelControl8.Click += new System.EventHandler(this.labelControl8_Click);
            // 
            // u_lman
            // 
            this.u_lman.Location = new System.Drawing.Point(452, 205);
            this.u_lman.Name = "u_lman";
            this.u_lman.Size = new System.Drawing.Size(200, 24);
            this.u_lman.TabIndex = 9;
           // this.u_lman.EditValueChanged += new System.EventHandler(this.u_lman_EditValueChanged);
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(33, 250);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(60, 18);
            this.labelControl9.TabIndex = 0;
            this.labelControl9.Text = "税务证号";
        //    this.labelControl9.Click += new System.EventHandler(this.labelControl9_Click);
            // 
            // u_taxn
            // 
            this.u_taxn.Location = new System.Drawing.Point(120, 247);
            this.u_taxn.Name = "u_taxn";
            this.u_taxn.Size = new System.Drawing.Size(200, 24);
            this.u_taxn.TabIndex = 10;
         //   this.u_taxn.EditValueChanged += new System.EventHandler(this.u_taxn_EditValueChanged);
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(365, 250);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(60, 18);
            this.labelControl10.TabIndex = 0;
            this.labelControl10.Text = "单位领导";
        //    this.labelControl10.Click += new System.EventHandler(this.labelControl10_Click);
            // 
            // u_lead
            // 
            this.u_lead.Location = new System.Drawing.Point(452, 247);
            this.u_lead.Name = "u_lead";
            this.u_lead.Size = new System.Drawing.Size(200, 24);
            this.u_lead.TabIndex = 11;
         //   this.u_lead.EditValueChanged += new System.EventHandler(this.u_lead_EditValueChanged);
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(33, 292);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(60, 18);
            this.labelControl11.TabIndex = 0;
            this.labelControl11.Text = "财务主管";
        //    this.labelControl11.Click += new System.EventHandler(this.labelControl11_Click);
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(365, 292);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(60, 18);
            this.labelControl12.TabIndex = 0;
            this.labelControl12.Text = "是否末级";
        //    this.labelControl12.Click += new System.EventHandler(this.labelControl12_Click);
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(33, 335);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(60, 18);
            this.labelControl13.TabIndex = 0;
            this.labelControl13.Text = "单位级别";
         //   this.labelControl13.Click += new System.EventHandler(this.labelControl13_Click);
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(365, 335);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(60, 18);
            this.labelControl14.TabIndex = 0;
            this.labelControl14.Text = "单位类别";
          //  this.labelControl14.Click += new System.EventHandler(this.labelControl14_Click);
            // 
            // AddBut
            // 
            this.AddBut.Location = new System.Drawing.Point(238, 384);
            this.AddBut.Name = "AddBut";
            this.AddBut.Size = new System.Drawing.Size(80, 25);
            this.AddBut.TabIndex = 16;
            this.AddBut.Text = "新增";
            this.AddBut.Click += new System.EventHandler(this.AddBut_Click);
            // 
            // CloseBut
            // 
            this.CloseBut.Location = new System.Drawing.Point(396, 384);
            this.CloseBut.Name = "CloseBut";
            this.CloseBut.Size = new System.Drawing.Size(80, 25);
            this.CloseBut.TabIndex = 17;
            this.CloseBut.Text = "关闭";
            this.CloseBut.Click += new System.EventHandler(this.CloseBut_Click);
            // 
            // u_last
            // 
            this.u_last.Location = new System.Drawing.Point(452, 289);
            this.u_last.Name = "u_last";
            this.u_last.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.u_last.Properties.Appearance.Options.UseBackColor = true;
            this.u_last.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_last.Size = new System.Drawing.Size(200, 24);
            this.u_last.TabIndex = 13;
       
            // 
            // comp_level_code
            // 
            this.comp_level_code.Location = new System.Drawing.Point(120, 332);
            this.comp_level_code.Name = "comp_level_code";
            this.comp_level_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comp_level_code.Size = new System.Drawing.Size(198, 24);
            this.comp_level_code.TabIndex = 14;
           
            // 
            // comp_type_code
            // 
            this.comp_type_code.Location = new System.Drawing.Point(452, 332);
            this.comp_type_code.Name = "comp_type_code";
            this.comp_type_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comp_type_code.Size = new System.Drawing.Size(200, 24);
            this.comp_type_code.TabIndex = 15;
          
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(33, 34);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(60, 18);
            this.labelControl15.TabIndex = 7;
            this.labelControl15.Text = "编码规则";
          
            // 
            // labelInfo
            // 
            this.labelInfo.Location = new System.Drawing.Point(120, 34);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(0, 18);
            this.labelInfo.TabIndex = 8;
           
            // 
            // is_count
            // 
            this.is_count.EditValue = ((short)(0));
            this.is_count.Location = new System.Drawing.Point(539, 115);
            this.is_count.Name = "is_count";
            this.is_count.Properties.Columns = 2;
            this.is_count.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(0)), "市"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(1)), "区县")});
            this.is_count.Size = new System.Drawing.Size(119, 27);
            this.is_count.TabIndex = 5;
         
            // 
            // u_parent
            // 
            this.u_parent.Location = new System.Drawing.Point(316, 12);
            this.u_parent.Name = "u_parent";
            this.u_parent.Size = new System.Drawing.Size(200, 24);
            this.u_parent.TabIndex = 1;
            this.u_parent.Visible = false;
         
            // u_mana
            // 
            this.u_mana.Location = new System.Drawing.Point(120, 289);
            this.u_mana.Name = "u_mana";
            this.u_mana.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.u_mana.Properties.Click += new System.EventHandler(this.u_mana_Properties_Click);

            this.u_mana.Size = new System.Drawing.Size(198, 24);
            this.u_mana.TabIndex = 18;
            // 
            // insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(693, 457);
            this.Controls.Add(this.u_mana);
            this.Controls.Add(this.is_count);
            this.Controls.Add(this.labelInfo);
            this.Controls.Add(this.labelControl15);
            this.Controls.Add(this.comp_type_code);
            this.Controls.Add(this.comp_level_code);
            this.Controls.Add(this.u_last);
            this.Controls.Add(this.CloseBut);
            this.Controls.Add(this.AddBut);
            this.Controls.Add(this.labelControl14);
            this.Controls.Add(this.labelControl12);
            this.Controls.Add(this.u_lead);
            this.Controls.Add(this.labelControl10);
            this.Controls.Add(this.u_lman);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.u_mail);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.u_taxn);
            this.Controls.Add(this.u_city);
            this.Controls.Add(this.u_phon);
            this.Controls.Add(this.labelControl13);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl11);
            this.Controls.Add(this.u_addr);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.u_name);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.u_parent);
            this.Controls.Add(this.u_prov);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.u_code);
            this.Controls.Add(this.labelControl1);
            this.Name = "insert";
            this.Text = "新增";
            this.Load += new System.EventHandler(this.insert_Load);
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_prov.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_city.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_addr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_mail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_phon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_lman.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_taxn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_lead.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_last.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp_level_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comp_type_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_count.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_parent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_mana.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit u_code;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit u_name;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit u_prov;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit u_city;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit u_addr;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit u_mail;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit u_phon;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit u_lman;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit u_taxn;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit u_lead;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.SimpleButton AddBut;
        private DevExpress.XtraEditors.SimpleButton CloseBut;
        private DevExpress.XtraEditors.ComboBoxEdit u_last;
        private DevExpress.XtraEditors.ComboBoxEdit comp_level_code;
        private DevExpress.XtraEditors.ComboBoxEdit comp_type_code;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelInfo;
        private DevExpress.XtraEditors.RadioGroup is_count;
        private DevExpress.XtraEditors.TextEdit u_parent;
        private DevExpress.XtraEditors.ButtonEdit u_mana;
    }
}