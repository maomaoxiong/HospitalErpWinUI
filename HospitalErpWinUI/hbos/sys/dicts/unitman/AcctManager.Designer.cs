﻿namespace HospitalErpWinUI.hbos.sys.dicts.unitman
{
    partial class AcctManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colchecked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colacc_manager = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstarts_date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colend_date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcomp_code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.saveBut = new DevExpress.XtraEditors.SimpleButton();
            this.deleteBut = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.gridControl1);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.panelControl1.Size = new System.Drawing.Size(495, 418);
            this.panelControl1.TabIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(12, 36);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1});
            this.gridControl1.Size = new System.Drawing.Size(481, 380);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colchecked,
            this.colacc_manager,
            this.colstarts_date,
            this.colend_date,
            this.colcomp_code,
            this.col_id});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridView1_InitNewRow);
            // 
            // colchecked
            // 
            this.colchecked.AppearanceHeader.Options.UseTextOptions = true;
            this.colchecked.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colchecked.Caption = "选择";
            this.colchecked.FieldName = "checked";
            this.colchecked.Name = "colchecked";
            this.colchecked.Visible = true;
            this.colchecked.VisibleIndex = 0;
            // 
            // colacc_manager
            // 
            this.colacc_manager.Caption = "财务主管";
            this.colacc_manager.FieldName = "acc_manager";
            this.colacc_manager.Name = "colacc_manager";
            this.colacc_manager.Visible = true;
            this.colacc_manager.VisibleIndex = 1;
            // 
            // colstarts_date
            // 
            this.colstarts_date.Caption = "开始时间";
            this.colstarts_date.ColumnEdit = this.repositoryItemDateEdit1;
            this.colstarts_date.FieldName = "starts_date";
            this.colstarts_date.Name = "colstarts_date";
            this.colstarts_date.Visible = true;
            this.colstarts_date.VisibleIndex = 2;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // colend_date
            // 
            this.colend_date.Caption = "结束时间";
            this.colend_date.FieldName = "end_date";
            this.colend_date.Name = "colend_date";
            this.colend_date.Visible = true;
            this.colend_date.VisibleIndex = 3;
            // 
            // colcomp_code
            // 
            this.colcomp_code.Caption = "gridColumn2";
            this.colcomp_code.FieldName = "comp_code";
            this.colcomp_code.Name = "colcomp_code";
            // 
            // col_id
            // 
            this.col_id.Caption = "gridColumn3";
            this.col_id.FieldName = "_id";
            this.col_id.Name = "col_id";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.saveBut);
            this.panelControl2.Controls.Add(this.deleteBut);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(12, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(481, 34);
            this.panelControl2.TabIndex = 0;
            // 
            // saveBut
            // 
            this.saveBut.Location = new System.Drawing.Point(92, 4);
            this.saveBut.Name = "saveBut";
            this.saveBut.Size = new System.Drawing.Size(80, 25);
            this.saveBut.TabIndex = 0;
            this.saveBut.Text = "确认";
            this.saveBut.Click += new System.EventHandler(this.saveBut_Click);
            // 
            // deleteBut
            // 
            this.deleteBut.Location = new System.Drawing.Point(6, 4);
            this.deleteBut.Name = "deleteBut";
            this.deleteBut.Size = new System.Drawing.Size(80, 25);
            this.deleteBut.TabIndex = 0;
            this.deleteBut.Text = "删除";
            this.deleteBut.Click += new System.EventHandler(this.deleteBut_Click);
            // 
            // AcctManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 418);
            this.Controls.Add(this.panelControl1);
            this.Name = "AcctManager";
            this.Text = "设置会计主管";
            this.Load += new System.EventHandler(this.AcctManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton saveBut;
        private DevExpress.XtraEditors.SimpleButton deleteBut;
        private DevExpress.XtraGrid.Columns.GridColumn colchecked;
        private DevExpress.XtraGrid.Columns.GridColumn colacc_manager;
        private DevExpress.XtraGrid.Columns.GridColumn colstarts_date;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colend_date;
        private DevExpress.XtraGrid.Columns.GridColumn colcomp_code;
        private DevExpress.XtraGrid.Columns.GridColumn col_id;
    }
}