﻿using DevExpress.XtraBars.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.store
{
    public partial class main : RibbonForm
    {
        public main()
        {
            InitializeComponent();
            Init();
        }
        #region 其他
        public void Init()
        {
            this.Text = "库房查询";
            this.StartPosition = FormStartPosition.CenterScreen;
            this.gridView1.OptionsView.ShowGroupPanel = false;
        }
        #endregion
    }
}
