﻿namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.bankacco
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.u_bname = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.acctsysvouchtype_insert = new DevExpress.XtraEditors.SimpleButton();
            this.u_stop = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.u_btel = new DevExpress.XtraEditors.TextEdit();
            this.u_bpost = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.u_bman = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.u_badd = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.u_bacco = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.u_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.u_code = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.u_bname.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_stop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_btel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_bpost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_bman.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_badd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_bacco.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // u_bname
            // 
            this.u_bname.Location = new System.Drawing.Point(101, 99);
            this.u_bname.Name = "u_bname";
            this.u_bname.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_bname.Size = new System.Drawing.Size(147, 20);
            this.u_bname.TabIndex = 74;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(157, 362);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 73;
            this.simpleButton2.Text = "关闭";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // acctsysvouchtype_insert
            // 
            this.acctsysvouchtype_insert.Location = new System.Drawing.Point(40, 362);
            this.acctsysvouchtype_insert.Name = "acctsysvouchtype_insert";
            this.acctsysvouchtype_insert.Size = new System.Drawing.Size(75, 23);
            this.acctsysvouchtype_insert.TabIndex = 72;
            this.acctsysvouchtype_insert.Text = "保存";
            this.acctsysvouchtype_insert.Click += new System.EventHandler(this.acctsysvouchtype_insert_Click);
            // 
            // u_stop
            // 
            this.u_stop.Location = new System.Drawing.Point(116, 324);
            this.u_stop.Name = "u_stop";
            this.u_stop.Properties.Caption = "是否停用";
            this.u_stop.Size = new System.Drawing.Size(75, 19);
            this.u_stop.TabIndex = 71;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(38, 253);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(60, 14);
            this.labelControl8.TabIndex = 70;
            this.labelControl8.Text = "联系电话：";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(59, 291);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(36, 14);
            this.labelControl7.TabIndex = 69;
            this.labelControl7.Text = "邮编：";
            // 
            // u_btel
            // 
            this.u_btel.Location = new System.Drawing.Point(101, 250);
            this.u_btel.Name = "u_btel";
            this.u_btel.Size = new System.Drawing.Size(147, 20);
            this.u_btel.TabIndex = 68;
            // 
            // u_bpost
            // 
            this.u_bpost.Location = new System.Drawing.Point(101, 288);
            this.u_bpost.Name = "u_bpost";
            this.u_bpost.Size = new System.Drawing.Size(147, 20);
            this.u_bpost.TabIndex = 67;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(47, 214);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(48, 14);
            this.labelControl6.TabIndex = 66;
            this.labelControl6.Text = "联系人：";
            // 
            // u_bman
            // 
            this.u_bman.Location = new System.Drawing.Point(101, 211);
            this.u_bman.Name = "u_bman";
            this.u_bman.Size = new System.Drawing.Size(147, 20);
            this.u_bman.TabIndex = 65;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(59, 178);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(36, 14);
            this.labelControl5.TabIndex = 64;
            this.labelControl5.Text = "地址：";
            // 
            // u_badd
            // 
            this.u_badd.Location = new System.Drawing.Point(101, 175);
            this.u_badd.Name = "u_badd";
            this.u_badd.Size = new System.Drawing.Size(147, 20);
            this.u_badd.TabIndex = 63;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(59, 142);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(36, 14);
            this.labelControl4.TabIndex = 62;
            this.labelControl4.Text = "账号：";
            // 
            // u_bacco
            // 
            this.u_bacco.Location = new System.Drawing.Point(101, 139);
            this.u_bacco.Name = "u_bacco";
            this.u_bacco.Size = new System.Drawing.Size(147, 20);
            this.u_bacco.TabIndex = 61;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(47, 102);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(48, 14);
            this.labelControl3.TabIndex = 60;
            this.labelControl3.Text = "开户行：";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(38, 66);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 59;
            this.labelControl2.Text = "账户名称：";
            // 
            // u_name
            // 
            this.u_name.Location = new System.Drawing.Point(101, 63);
            this.u_name.Name = "u_name";
            this.u_name.Size = new System.Drawing.Size(147, 20);
            this.u_name.TabIndex = 58;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(38, 28);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 57;
            this.labelControl1.Text = "账户编码：";
            // 
            // u_code
            // 
            this.u_code.Location = new System.Drawing.Point(101, 25);
            this.u_code.Name = "u_code";
            this.u_code.Size = new System.Drawing.Size(147, 20);
            this.u_code.TabIndex = 56;
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 447);
            this.Controls.Add(this.u_bname);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.acctsysvouchtype_insert);
            this.Controls.Add(this.u_stop);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.u_btel);
            this.Controls.Add(this.u_bpost);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.u_bman);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.u_badd);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.u_bacco);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.u_name);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.u_code);
            this.Name = "update";
            this.Text = "update";
            ((System.ComponentModel.ISupportInitialize)(this.u_bname.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_stop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_btel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_bpost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_bman.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_badd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_bacco.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.ComboBoxEdit u_bname;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton acctsysvouchtype_insert;
        private DevExpress.XtraEditors.CheckEdit u_stop;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit u_btel;
        private DevExpress.XtraEditors.TextEdit u_bpost;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit u_bman;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit u_badd;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit u_bacco;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit u_name;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit u_code;
    }
}