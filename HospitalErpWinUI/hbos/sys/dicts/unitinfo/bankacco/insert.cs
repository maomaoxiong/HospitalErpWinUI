﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.bankacco
{
    public partial class insert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        public insert()
        {
            InitializeComponent();
            _helper = new DaoHelper();
        }
        public insert(Dao.DaoHelper helper, List<Perm> perms)
        {
            InitializeComponent();
            this._helper = helper;
            this._perms = perms;
            Load_sys_acco_bname_list_stop();
        }
        private void Load_sys_acco_bname_list_stop()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sys_acco_bname_list_stop";
            paramters.Add("comp_code", G_User.user.Comp_code);
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            u_bname.Properties.Items.AddRange(objs);

        }
        private void acctsysvouchtype_insert_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Insert();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
        private bool Validate()
        {
            if (!Validator.IsInteger(u_code.Text))
            {
                MessageForm.Warning("账户编码请正确输入数字");
                return false;
            }
            if (string.IsNullOrEmpty(u_name.Text))
            {
                MessageForm.Warning("账户名称不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(u_code.Text))
            {
                MessageForm.Warning("账户编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(u_bname.Text))
            {
                MessageForm.Warning("开户行不能为空!");
                return false;
            }

            return true;
        }



        private bool Insert()
        { 
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "sysDictsUnitinfoBankAcco_insert";
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            paramters.Add("areacode_code", u_code.Text);
            paramters.Add("areacode_name", u_name.Text);
            paramters.Add("Comp_code", G_User.user.Comp_code);
                
            paramters.Add("u_bname", FormHelper.GetValueForComboBoxEdit(u_bname));  
            paramters.Add("u_bacco", u_bacco.Text);
            paramters.Add("u_badd", u_badd.Text);
            paramters.Add("u_bman", u_bman.Text);
            paramters.Add("u_btel", u_btel.Text);
            paramters.Add("areacode_flag", u_bpost.Text);
            paramters.Add("u_bpost", FormHelper.GetValueForCheckEdit(u_stop).Trim());
            serviceParamter.Paramters = paramters;

            return _helper.WirteSql(serviceParamter);

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
