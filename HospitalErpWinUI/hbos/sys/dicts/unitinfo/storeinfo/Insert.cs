﻿using Dao;
using HospitalErpData.MenuHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.storeinfo
{
    public partial class Insert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }

        public Insert()
        {
            InitializeComponent();
        }
        public Insert(List<Perm>  perms)
        {
            InitializeComponent();
            this._perms = perms;
            Init();
        }

        #region 其他
            private void Init()
            {
                this._helper = new DaoHelper();
                this.Text = "新增";
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        #endregion

        #region 数据访问
        #endregion

        #region 事件
        #endregion
    }
}
