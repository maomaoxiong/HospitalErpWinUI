﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.storeinfo
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        private DataTable _data;

        public main()
        {
            InitializeComponent();
            Init();
        }
        private void Init()
        {
            this._helper = new DaoHelper();
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "库房信息";
        }

        private void sysDictsUnitinfoStoreInfo_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message);
            }
        }

        private void sysDictsAcctSubjSys_insert_Click(object sender, EventArgs e)
        {

        }

        #region 访问数据
        private void Query()
        {


            DataTable dt = _helper.ReadSqlForDataTable("sysDictsUnitinfoStoreInfo_select",new []{G_User.user.Comp_code, txtFind.Text.Trim()});
            _data = dt;
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            gridControl1.DataSource = _data;
        }

        #endregion

        #region 事件
        #endregion

        #region 其他
        #endregion
    }
}
