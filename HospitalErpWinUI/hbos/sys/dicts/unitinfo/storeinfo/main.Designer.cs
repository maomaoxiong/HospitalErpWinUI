﻿namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.storeinfo
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col_checkbox = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.col_store_level = new DevExpress.XtraGrid.Columns.GridColumn();
            this.store_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.sysDictsAcctSubjSys_insert = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.sysDictsAcctSubjSys_delete = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.sysDictsUnitinfoStoreInfo_select = new DevExpress.XtraEditors.SimpleButton();
            this.txtFind = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.col_comp_code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_store_code = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFind.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.gridControl1);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Controls.Add(this.panelControl3);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.panelControl1.Size = new System.Drawing.Size(1232, 906);
            this.panelControl1.TabIndex = 2;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControl1.Location = new System.Drawing.Point(12, 84);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemHyperLinkEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1218, 820);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col_checkbox,
            this.col_store_level,
            this.store_name,
            this.col_comp_code,
            this.col_store_code});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // col_checkbox
            // 
            this.col_checkbox.AppearanceHeader.Options.UseTextOptions = true;
            this.col_checkbox.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col_checkbox.Caption = "选择";
            this.col_checkbox.ColumnEdit = this.repositoryItemCheckEdit1;
            this.col_checkbox.FieldName = "checked";
            this.col_checkbox.Name = "col_checkbox";
            this.col_checkbox.Visible = true;
            this.col_checkbox.VisibleIndex = 0;
            this.col_checkbox.Width = 70;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // col_store_level
            // 
            this.col_store_level.AppearanceHeader.Options.UseTextOptions = true;
            this.col_store_level.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col_store_level.Caption = "级别";
            this.col_store_level.FieldName = "store_level";
            this.col_store_level.Name = "col_store_level";
            this.col_store_level.Visible = true;
            this.col_store_level.VisibleIndex = 1;
            this.col_store_level.Width = 79;
            // 
            // store_name
            // 
            this.store_name.AppearanceHeader.Options.UseTextOptions = true;
            this.store_name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.store_name.Caption = "仓库名称";
            this.store_name.FieldName = "store_name";
            this.store_name.Name = "store_name";
            this.store_name.Visible = true;
            this.store_name.VisibleIndex = 2;
            this.store_name.Width = 625;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.sysDictsAcctSubjSys_insert);
            this.panelControl2.Controls.Add(this.simpleButton1);
            this.panelControl2.Controls.Add(this.sysDictsAcctSubjSys_delete);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(12, 43);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1218, 41);
            this.panelControl2.TabIndex = 3;
            // 
            // sysDictsAcctSubjSys_insert
            // 
            this.sysDictsAcctSubjSys_insert.Location = new System.Drawing.Point(5, 4);
            this.sysDictsAcctSubjSys_insert.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.sysDictsAcctSubjSys_insert.Name = "sysDictsAcctSubjSys_insert";
            this.sysDictsAcctSubjSys_insert.Size = new System.Drawing.Size(80, 30);
            this.sysDictsAcctSubjSys_insert.TabIndex = 3;
            this.sysDictsAcctSubjSys_insert.Text = "新增";
            this.sysDictsAcctSubjSys_insert.Click += new System.EventHandler(this.sysDictsAcctSubjSys_insert_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(180, 3);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(80, 30);
            this.simpleButton1.TabIndex = 3;
            this.simpleButton1.Text = "删除";
            // 
            // sysDictsAcctSubjSys_delete
            // 
            this.sysDictsAcctSubjSys_delete.Location = new System.Drawing.Point(93, 4);
            this.sysDictsAcctSubjSys_delete.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.sysDictsAcctSubjSys_delete.Name = "sysDictsAcctSubjSys_delete";
            this.sysDictsAcctSubjSys_delete.Size = new System.Drawing.Size(80, 30);
            this.sysDictsAcctSubjSys_delete.TabIndex = 3;
            this.sysDictsAcctSubjSys_delete.Text = "导入";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.sysDictsUnitinfoStoreInfo_select);
            this.panelControl3.Controls.Add(this.txtFind);
            this.panelControl3.Controls.Add(this.labelControl1);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(12, 2);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1218, 41);
            this.panelControl3.TabIndex = 2;
            // 
            // sysDictsUnitinfoStoreInfo_select
            // 
            this.sysDictsUnitinfoStoreInfo_select.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sysDictsUnitinfoStoreInfo_select.Location = new System.Drawing.Point(1042, 5);
            this.sysDictsUnitinfoStoreInfo_select.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.sysDictsUnitinfoStoreInfo_select.Name = "sysDictsUnitinfoStoreInfo_select";
            this.sysDictsUnitinfoStoreInfo_select.Size = new System.Drawing.Size(80, 30);
            this.sysDictsUnitinfoStoreInfo_select.TabIndex = 3;
            this.sysDictsUnitinfoStoreInfo_select.Text = "查询";
            this.sysDictsUnitinfoStoreInfo_select.Click += new System.EventHandler(this.sysDictsUnitinfoStoreInfo_select_Click);
            // 
            // txtFind
            // 
            this.txtFind.Location = new System.Drawing.Point(113, 4);
            this.txtFind.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFind.Name = "txtFind";
            this.txtFind.Size = new System.Drawing.Size(200, 24);
            this.txtFind.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 11);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 18);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "快速定位";
            // 
            // col_comp_code
            // 
            this.col_comp_code.AppearanceHeader.Options.UseTextOptions = true;
            this.col_comp_code.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col_comp_code.Caption = "公司";
            this.col_comp_code.FieldName = "comp_code";
            this.col_comp_code.Name = "col_comp_code";
            this.col_comp_code.Visible = true;
            this.col_comp_code.VisibleIndex = 3;
            this.col_comp_code.Width = 303;
            // 
            // col_store_code
            // 
            this.col_store_code.AppearanceHeader.Options.UseTextOptions = true;
            this.col_store_code.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col_store_code.Caption = "仓库编码";
            this.col_store_code.FieldName = "_store_code";
            this.col_store_code.Name = "col_store_code";
            this.col_store_code.Visible = true;
            this.col_store_code.VisibleIndex = 4;
            this.col_store_code.Width = 123;
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1232, 906);
            this.Controls.Add(this.panelControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "main";
            this.Text = "main";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFind.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn col_checkbox;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn col_store_level;
        private DevExpress.XtraGrid.Columns.GridColumn store_name;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton sysDictsAcctSubjSys_insert;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton sysDictsAcctSubjSys_delete;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton sysDictsUnitinfoStoreInfo_select;
        private DevExpress.XtraEditors.TextEdit txtFind;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGrid.Columns.GridColumn col_comp_code;
        private DevExpress.XtraGrid.Columns.GridColumn col_store_code;
    }
}