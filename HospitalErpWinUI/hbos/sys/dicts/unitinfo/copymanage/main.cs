﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.copymanage
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
   
        private DataTable _copy;
        public main()
        {
            
            InitializeComponent();
            Init();
        }

        #region 其他
        private void Init()
        {
            this. _helper = new DaoHelper();
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "账套管理";
        }
        #endregion

        #region 数据访问
        private DataTable  Query()
        {

            DataTable dt = _helper.ReadSqlForDataTable("sysDictsUnitinfoCopyManage_select", new []{G_User.user.Comp_code,copy_code.Text,copy_name.Text});
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            dt.Columns.Add("yearAcct", System.Type.GetType("System.String"));                    

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
                dt.Rows[i]["yearAcct"] = "设置";            

            }
            this._copy = dt;
            return dt; 

        }

        #endregion

        #region 事件
        private void QueryBut_Click(object sender, EventArgs e)
        {
            try
            {
             gridControl1.DataSource =   Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        #endregion

        private void InsertBut_Click(object sender, EventArgs e)
        {
            try
            {
                Insert insertFrm = new Insert(this._helper, this._perms);

                if (insertFrm.ShowDialog() == DialogResult.OK)
                {
                    gridControl1.DataSource = Query();
                }
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
       

    }
}
