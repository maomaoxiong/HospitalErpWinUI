﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.copymanage
{
    public partial class Insert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        public Insert()
        {
            InitializeComponent();
            Init();
        }

        public Insert(DaoHelper helper, List<Perm> perms)
        {
            InitializeComponent();
            this._helper = helper;
            this._perms = perms;
            Init();
        }
        #region 其他
        private void Init()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.copy_code.Properties.MaxLength = 3;
            this.copy_name.Properties.MaxLength = 50;
            this.start_year.Properties.MaxLength = 4;
            this.Text = "新增账套";

            bool isLastComp = this.Comany_query();
            setEditMode(isLastComp);

          
        }

        private void setEditMode(bool isLastComp)
        {
	        if(isLastComp)
            {
                copy_type.Properties.Items[0].Enabled =true;
                copy_type.EditValue=1;
                copy_type.SelectedIndex=0;
                is_analyse.Enabled =true;
                is_check.Enabled =true;
                copy_type.Properties.Items[1].Enabled =false;
                copy_type.Properties.Items[2].Enabled =false;
		       
	        }
	        else
            {
                   copy_type.Properties.Items[0].Enabled =false;
                     is_analyse.Enabled =false;
                     is_check.Enabled =false;
                 copy_type.Properties.Items[1].Enabled =true;
                copy_type.Properties.Items[2].Enabled =true;
                 copy_type.EditValue=2;
                copy_type.SelectedIndex=1;
		   
	        }
        }

        private bool Validate()
        {

            if (FormHelper.Text_Length(copy_code.Text) != 3)
            {
                MessageForm.Warning("账套编码必须是3位！");
                copy_code.Focus();
                return false;
            }        

            if (string.IsNullOrEmpty( copy_name.Text.Trim())  )
            {
                MessageForm.Warning("账套名称不能为空！");
                copy_name.Focus();
                return false;
            }
            if (FormHelper.Text_Length(start_year.Text) != 4 || Convert.ToInt32(start_year.Text) < 1990 || Convert.ToInt32(start_year.Text) > 9999)
            {
                MessageForm.Warning("请输入正确的年度！");
                start_year.Focus();
                return false;
            }

            return true;
        }
        #endregion

        #region 访问数据
        public bool Comany_query()
        {
            DataTable dt = _helper.ReadSqlForDataTable("comany_query",new []{G_User.user.Comp_code});
            int is_last = Convert.ToInt16(dt.Rows[0][0]);
            return is_last == 1;
        }
        private void LoadMonth()
        {
            List<CodeValue> dt = _helper.ReadDictForData("dict_month_list", null);
            List<string> months = new List<string>();
            foreach (CodeValue codeValue in dt)
            {
                months.Add( codeValue.Value);
            }
            start_month.Properties.Items.AddRange(months);
            start_month.SelectedItem = months[0];
        }

        private void LoadCoCode()
        {
            DataTable dt = _helper.ReadDictForSql("sysAcctSubjType_list", null);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][0].ToString() + " " + dt.Rows[i][1].ToString();
                objs.Add(codeValue.Value);
            }
            co_code.Properties.Items.AddRange(objs);
            co_code.SelectedItem = objs[0];
        }

        private void LoadEndDay()
        {
            List<CodeValue> dt = _helper.ReadDictForData("dict_period_end_date_list", null);
            List<string> obj = new List<string>();
            foreach (CodeValue codeValue in dt)
            {
                obj.Add( codeValue.Value);
            }
            end_day.Properties.Items.AddRange(obj);
            end_day.SelectedItem = obj[0];
        }

        #endregion

        #region 事件
        private void Insert_Load(object sender, EventArgs e)
        {
            LoadMonth();
            LoadCoCode();
            LoadEndDay();
        }

        private void but_close_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
        #endregion

       
       
    }
}
