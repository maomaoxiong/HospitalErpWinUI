﻿namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.copymanage
{
    partial class Insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.copy_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.copy_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.start_year = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.is_copy = new DevExpress.XtraEditors.CheckEdit();
            this.copy_type = new DevExpress.XtraEditors.RadioGroup();
            this.is_analyse = new DevExpress.XtraEditors.CheckEdit();
            this.is_check = new DevExpress.XtraEditors.CheckEdit();
            this.but_save = new DevExpress.XtraEditors.SimpleButton();
            this.but_close = new DevExpress.XtraEditors.SimpleButton();
            this.start_month = new DevExpress.XtraEditors.ComboBoxEdit();
            this.co_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.end_day = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.copy_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.copy_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.start_year.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_copy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.copy_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_analyse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_check.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.start_month.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.co_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.end_day.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(66, 46);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 18);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "账套编码";
            // 
            // copy_code
            // 
            this.copy_code.Location = new System.Drawing.Point(150, 43);
            this.copy_code.Name = "copy_code";
            this.copy_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.copy_code.Properties.Appearance.Options.UseBackColor = true;
            this.copy_code.Size = new System.Drawing.Size(200, 24);
            this.copy_code.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(450, 43);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 18);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "账套名称";
            // 
            // copy_name
            // 
            this.copy_name.Location = new System.Drawing.Point(534, 40);
            this.copy_name.Name = "copy_name";
            this.copy_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.copy_name.Properties.Appearance.Options.UseBackColor = true;
            this.copy_name.Size = new System.Drawing.Size(200, 24);
            this.copy_name.TabIndex = 1;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(66, 100);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 18);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "开始年度";
            // 
            // start_year
            // 
            this.start_year.Location = new System.Drawing.Point(150, 97);
            this.start_year.Name = "start_year";
            this.start_year.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.start_year.Properties.Appearance.Options.UseBackColor = true;
            this.start_year.Size = new System.Drawing.Size(200, 24);
            this.start_year.TabIndex = 1;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(450, 97);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 18);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "开始月份";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(66, 152);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 18);
            this.labelControl5.TabIndex = 0;
            this.labelControl5.Text = "科目体系";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(450, 152);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(75, 18);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = "期间结束日";
            // 
            // is_copy
            // 
            this.is_copy.Location = new System.Drawing.Point(150, 200);
            this.is_copy.Name = "is_copy";
            this.is_copy.Properties.Caption = "按行业性质预置科目";
            this.is_copy.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.is_copy.Size = new System.Drawing.Size(109, 22);
            this.is_copy.TabIndex = 6;
            // 
            // copy_type
            // 
            this.copy_type.Location = new System.Drawing.Point(150, 238);
            this.copy_type.Name = "copy_type";
            this.copy_type.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(1)), "医疗卫生单位"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(2)), "主管单位"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(3)), "核算中心")});
            this.copy_type.Size = new System.Drawing.Size(200, 96);
            this.copy_type.TabIndex = 7;
            // 
            // is_analyse
            // 
            this.is_analyse.Location = new System.Drawing.Point(443, 245);
            this.is_analyse.Name = "is_analyse";
            this.is_analyse.Properties.Caption = "参与主管单位";
            this.is_analyse.Size = new System.Drawing.Size(109, 22);
            this.is_analyse.TabIndex = 8;
            // 
            // is_check
            // 
            this.is_check.Location = new System.Drawing.Point(587, 245);
            this.is_check.Name = "is_check";
            this.is_check.Properties.Caption = "参与核算中心";
            this.is_check.Size = new System.Drawing.Size(109, 22);
            this.is_check.TabIndex = 8;
            // 
            // but_save
            // 
            this.but_save.Location = new System.Drawing.Point(275, 386);
            this.but_save.Name = "but_save";
            this.but_save.Size = new System.Drawing.Size(80, 30);
            this.but_save.TabIndex = 9;
            this.but_save.Text = "保存";
            // 
            // but_close
            // 
            this.but_close.Location = new System.Drawing.Point(430, 386);
            this.but_close.Name = "but_close";
            this.but_close.Size = new System.Drawing.Size(80, 30);
            this.but_close.TabIndex = 9;
            this.but_close.Text = "关闭";
            this.but_close.Click += new System.EventHandler(this.but_close_Click);
            // 
            // start_month
            // 
            this.start_month.Location = new System.Drawing.Point(534, 91);
            this.start_month.Name = "start_month";
            this.start_month.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.start_month.Properties.Appearance.Options.UseBackColor = true;
            this.start_month.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.start_month.Size = new System.Drawing.Size(200, 24);
            this.start_month.TabIndex = 10;
            // 
            // co_code
            // 
            this.co_code.Location = new System.Drawing.Point(150, 144);
            this.co_code.Name = "co_code";
            this.co_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.co_code.Properties.Appearance.Options.UseBackColor = true;
            this.co_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.co_code.Size = new System.Drawing.Size(200, 24);
            this.co_code.TabIndex = 10;
            // 
            // end_day
            // 
            this.end_day.Location = new System.Drawing.Point(534, 147);
            this.end_day.Name = "end_day";
            this.end_day.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.end_day.Properties.Appearance.Options.UseBackColor = true;
            this.end_day.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.end_day.Size = new System.Drawing.Size(200, 24);
            this.end_day.TabIndex = 10;
            // 
            // Insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(819, 491);
            this.Controls.Add(this.end_day);
            this.Controls.Add(this.co_code);
            this.Controls.Add(this.start_month);
            this.Controls.Add(this.but_close);
            this.Controls.Add(this.but_save);
            this.Controls.Add(this.is_check);
            this.Controls.Add(this.is_analyse);
            this.Controls.Add(this.copy_type);
            this.Controls.Add(this.is_copy);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.copy_name);
            this.Controls.Add(this.start_year);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.copy_code);
            this.Controls.Add(this.labelControl1);
            this.Name = "Insert";
            this.Text = "Insert";
            this.Load += new System.EventHandler(this.Insert_Load);
            ((System.ComponentModel.ISupportInitialize)(this.copy_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.copy_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.start_year.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_copy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.copy_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_analyse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_check.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.start_month.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.co_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.end_day.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit copy_code;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit copy_name;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit start_year;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.CheckEdit is_copy;
        private DevExpress.XtraEditors.RadioGroup copy_type;
        private DevExpress.XtraEditors.CheckEdit is_analyse;
        private DevExpress.XtraEditors.CheckEdit is_check;
        private DevExpress.XtraEditors.SimpleButton but_save;
        private DevExpress.XtraEditors.SimpleButton but_close;
        private DevExpress.XtraEditors.ComboBoxEdit start_month;
        private DevExpress.XtraEditors.ComboBoxEdit co_code;
        private DevExpress.XtraEditors.ComboBoxEdit end_day;
    }
}