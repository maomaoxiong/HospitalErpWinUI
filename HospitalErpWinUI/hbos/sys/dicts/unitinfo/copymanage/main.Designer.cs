﻿namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.copymanage
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.col_copy_code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.col_copy_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_copy_start_year = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_copy_start_month = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_co_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_cur_year = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_cur_month = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_yearAcct = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemHyperLinkEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.DeleteBut = new DevExpress.XtraEditors.SimpleButton();
            this.InsertBut = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.QueryBut = new DevExpress.XtraEditors.SimpleButton();
            this.copy_name = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.copy_code = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copy_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.copy_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl4);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Controls.Add(this.panelControl3);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.panelControl1.Size = new System.Drawing.Size(1262, 810);
            this.panelControl1.TabIndex = 1;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.gridControl1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(12, 84);
            this.panelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1248, 724);
            this.panelControl4.TabIndex = 7;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            gridLevelNode1.RelationName = "Level1";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemHyperLinkEdit2,
            this.repositoryItemHyperLinkEdit3,
            this.repositoryItemHyperLinkEdit4});
            this.gridControl1.Size = new System.Drawing.Size(1244, 720);
            this.gridControl1.TabIndex = 9;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.col_copy_code,
            this.col_copy_name,
            this.col_copy_start_year,
            this.col_copy_start_month,
            this.col_co_name,
            this.col_cur_year,
            this.col_cur_month,
            this.col_yearAcct});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "选择";
            this.gridColumn1.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 87;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // col_copy_code
            // 
            this.col_copy_code.Caption = "账套编码";
            this.col_copy_code.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.col_copy_code.FieldName = "copy_code";
            this.col_copy_code.Name = "col_copy_code";
            this.col_copy_code.Visible = true;
            this.col_copy_code.VisibleIndex = 1;
            this.col_copy_code.Width = 116;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            // 
            // col_copy_name
            // 
            this.col_copy_name.Caption = "账套名称";
            this.col_copy_name.FieldName = "copy_name";
            this.col_copy_name.Name = "col_copy_name";
            this.col_copy_name.Visible = true;
            this.col_copy_name.VisibleIndex = 2;
            this.col_copy_name.Width = 351;
            // 
            // col_copy_start_year
            // 
            this.col_copy_start_year.Caption = "开始年";
            this.col_copy_start_year.FieldName = "copy_start_year";
            this.col_copy_start_year.Name = "col_copy_start_year";
            this.col_copy_start_year.Visible = true;
            this.col_copy_start_year.VisibleIndex = 3;
            this.col_copy_start_year.Width = 128;
            // 
            // col_copy_start_month
            // 
            this.col_copy_start_month.Caption = "开始月";
            this.col_copy_start_month.FieldName = "copy_start_month";
            this.col_copy_start_month.Name = "col_copy_start_month";
            this.col_copy_start_month.Visible = true;
            this.col_copy_start_month.VisibleIndex = 4;
            this.col_copy_start_month.Width = 128;
            // 
            // col_co_name
            // 
            this.col_co_name.Caption = "科目体系";
            this.col_co_name.FieldName = "co_name";
            this.col_co_name.Name = "col_co_name";
            this.col_co_name.Visible = true;
            this.col_co_name.VisibleIndex = 5;
            this.col_co_name.Width = 128;
            // 
            // col_cur_year
            // 
            this.col_cur_year.Caption = "当前年";
            this.col_cur_year.FieldName = "cur_year";
            this.col_cur_year.Name = "col_cur_year";
            this.col_cur_year.Visible = true;
            this.col_cur_year.VisibleIndex = 6;
            this.col_cur_year.Width = 128;
            // 
            // col_cur_month
            // 
            this.col_cur_month.Caption = "当前月";
            this.col_cur_month.FieldName = "cur_month";
            this.col_cur_month.Name = "col_cur_month";
            this.col_cur_month.Visible = true;
            this.col_cur_month.VisibleIndex = 7;
            this.col_cur_month.Width = 160;
            // 
            // col_yearAcct
            // 
            this.col_yearAcct.Caption = "年度帐";
            this.col_yearAcct.ColumnEdit = this.repositoryItemHyperLinkEdit4;
            this.col_yearAcct.FieldName = "yearAcct";
            this.col_yearAcct.Name = "col_yearAcct";
            this.col_yearAcct.Visible = true;
            this.col_yearAcct.VisibleIndex = 8;
            // 
            // repositoryItemHyperLinkEdit4
            // 
            this.repositoryItemHyperLinkEdit4.AutoHeight = false;
            this.repositoryItemHyperLinkEdit4.Name = "repositoryItemHyperLinkEdit4";
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            // 
            // repositoryItemHyperLinkEdit3
            // 
            this.repositoryItemHyperLinkEdit3.AutoHeight = false;
            this.repositoryItemHyperLinkEdit3.Name = "repositoryItemHyperLinkEdit3";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.DeleteBut);
            this.panelControl2.Controls.Add(this.InsertBut);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(12, 43);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1248, 41);
            this.panelControl2.TabIndex = 6;
            // 
            // DeleteBut
            // 
            this.DeleteBut.Location = new System.Drawing.Point(106, 4);
            this.DeleteBut.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DeleteBut.Name = "DeleteBut";
            this.DeleteBut.Size = new System.Drawing.Size(80, 30);
            this.DeleteBut.TabIndex = 1;
            this.DeleteBut.Text = "删除";
            // 
            // InsertBut
            // 
            this.InsertBut.Location = new System.Drawing.Point(14, 6);
            this.InsertBut.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.InsertBut.Name = "InsertBut";
            this.InsertBut.Size = new System.Drawing.Size(80, 30);
            this.InsertBut.TabIndex = 0;
            this.InsertBut.Text = "新增";
            this.InsertBut.Click += new System.EventHandler(this.InsertBut_Click);
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.QueryBut);
            this.panelControl3.Controls.Add(this.copy_name);
            this.panelControl3.Controls.Add(this.label3);
            this.panelControl3.Controls.Add(this.copy_code);
            this.panelControl3.Controls.Add(this.label2);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(12, 2);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1248, 41);
            this.panelControl3.TabIndex = 3;
            // 
            // QueryBut
            // 
            this.QueryBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.QueryBut.Location = new System.Drawing.Point(1086, 5);
            this.QueryBut.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.QueryBut.Name = "QueryBut";
            this.QueryBut.Size = new System.Drawing.Size(80, 30);
            this.QueryBut.TabIndex = 3;
            this.QueryBut.Text = "查询";
            this.QueryBut.Click += new System.EventHandler(this.QueryBut_Click);
            // 
            // copy_name
            // 
            this.copy_name.Location = new System.Drawing.Point(538, 8);
            this.copy_name.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.copy_name.Name = "copy_name";
            this.copy_name.Size = new System.Drawing.Size(250, 24);
            this.copy_name.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(455, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 18);
            this.label3.TabIndex = 0;
            this.label3.Text = "账套名称";
            // 
            // copy_code
            // 
            this.copy_code.Location = new System.Drawing.Point(95, 8);
            this.copy_code.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.copy_code.Name = "copy_code";
            this.copy_code.Size = new System.Drawing.Size(250, 24);
            this.copy_code.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 18);
            this.label2.TabIndex = 0;
            this.label2.Text = "账套编码";
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1262, 810);
            this.Controls.Add(this.panelControl1);
            this.Name = "main";
            this.Text = "main";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copy_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.copy_code.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn col_copy_code;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton DeleteBut;
        private DevExpress.XtraEditors.SimpleButton InsertBut;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton QueryBut;
        private DevExpress.XtraEditors.TextEdit copy_name;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit copy_code;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraGrid.Columns.GridColumn col_copy_name;
        private DevExpress.XtraGrid.Columns.GridColumn col_copy_start_year;
        private DevExpress.XtraGrid.Columns.GridColumn col_copy_start_month;
        private DevExpress.XtraGrid.Columns.GridColumn col_co_name;
        private DevExpress.XtraGrid.Columns.GridColumn col_cur_year;
        private DevExpress.XtraGrid.Columns.GridColumn col_cur_month;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn col_yearAcct;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit4;

    }
}