﻿namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.ven
{
    partial class type_insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.u_code = new DevExpress.XtraEditors.TextEdit();
            this.qqq = new DevExpress.XtraEditors.LabelControl();
            this.u_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.u_last = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.u_stop = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.sysDictsUnitinfoVenType_insert = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_last.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_stop.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // u_code
            // 
            this.u_code.Location = new System.Drawing.Point(123, 12);
            this.u_code.Name = "u_code";
            this.u_code.Size = new System.Drawing.Size(168, 20);
            this.u_code.TabIndex = 2;
            // 
            // qqq
            // 
            this.qqq.Location = new System.Drawing.Point(21, 15);
            this.qqq.Name = "qqq";
            this.qqq.Size = new System.Drawing.Size(96, 14);
            this.qqq.TabIndex = 6;
            this.qqq.Text = "供应商类别编码：";
            // 
            // u_name
            // 
            this.u_name.Location = new System.Drawing.Point(123, 54);
            this.u_name.Name = "u_name";
            this.u_name.Size = new System.Drawing.Size(168, 20);
            this.u_name.TabIndex = 3;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(21, 57);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(96, 14);
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "供应商类别名称：";
            // 
            // u_last
            // 
            this.u_last.Location = new System.Drawing.Point(123, 94);
            this.u_last.Name = "u_last";
            this.u_last.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_last.Size = new System.Drawing.Size(168, 20);
            this.u_last.TabIndex = 4;
            // 
            // labelControl37
            // 
            this.labelControl37.Location = new System.Drawing.Point(57, 97);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(60, 14);
            this.labelControl37.TabIndex = 90;
            this.labelControl37.Text = "是否末级：";
            // 
            // u_stop
            // 
            this.u_stop.Location = new System.Drawing.Point(123, 129);
            this.u_stop.Name = "u_stop";
            this.u_stop.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_stop.Size = new System.Drawing.Size(168, 20);
            this.u_stop.TabIndex = 5;
            this.u_stop.EditValueChanged += new System.EventHandler(this.u_stop_EditValueChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(57, 132);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 92;
            this.labelControl2.Text = "是否停用：";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(188, 187);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 7;
            this.simpleButton2.Text = "关闭";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // sysDictsUnitinfoVenType_insert
            // 
            this.sysDictsUnitinfoVenType_insert.Location = new System.Drawing.Point(71, 187);
            this.sysDictsUnitinfoVenType_insert.Name = "sysDictsUnitinfoVenType_insert";
            this.sysDictsUnitinfoVenType_insert.Size = new System.Drawing.Size(75, 23);
            this.sysDictsUnitinfoVenType_insert.TabIndex = 6;
            this.sysDictsUnitinfoVenType_insert.Text = "保存";
            this.sysDictsUnitinfoVenType_insert.Click += new System.EventHandler(this.sysDictsUnitinfoVenType_insert_Click);
            // 
            // type_insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(333, 304);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.sysDictsUnitinfoVenType_insert);
            this.Controls.Add(this.u_stop);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.u_last);
            this.Controls.Add(this.labelControl37);
            this.Controls.Add(this.u_name);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.u_code);
            this.Controls.Add(this.qqq);
            this.Name = "type_insert";
            this.Text = "添加";
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_last.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_stop.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit u_code;
        private DevExpress.XtraEditors.LabelControl qqq;
        private DevExpress.XtraEditors.TextEdit u_name;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit u_last;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.ComboBoxEdit u_stop;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton sysDictsUnitinfoVenType_insert;
    }
}