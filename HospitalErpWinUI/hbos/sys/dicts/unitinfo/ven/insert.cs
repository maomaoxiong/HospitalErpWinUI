﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.ven
{
    public partial class insert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        public insert()
        {
            InitializeComponent();
            //_helper = new DaoHelper();
        }
        public insert(Dao.DaoHelper helper, List<Perm> perms)
        {
            InitializeComponent();
            this._helper = helper;
            this._perms = perms;
            this.StartPosition = FormStartPosition.CenterParent;
            Load_sysDictSelectCodingPara_value();//判断供应商编码是否自动生成
            Load_sysLastLevelVenderType_list_stop();//供应商类别下拉框
            
            Load_vender_type_dict();//是否下拉框
            Load_sysPayCond_list();//付款条件下拉框
            Load_sysTrafMode_list();//到货方式下拉框
        }
        private void Load_sysTrafMode_list()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysTrafMode_list";
            paramters.Add("Comp_code", G_User.user.Comp_code);
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            u_dhfs.Properties.Items.AddRange(objs);
            // u_fktj.SelectedItem = objs[0];
        }
        private void Load_sysDictSelectCodingPara_value()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysDictSelectCodingPara_value";
            
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            if (dt.Rows.Count == 0)
            {

                MessageBox.Show("判断供应商编码是否自动生成失败！");
            }
            else
            {
               
                if (dt.Rows[0][0].ToString() == "False")
                {
                    this.u_code.Text = "";
                    this.u_code.Enabled = true;
                }
                else if (dt.Rows[0][0].ToString() == "True" && dt.Rows[0][1].ToString() == "True")
                {
                    Load_vendor_DictSelectVen_code_Max();//获取供应商编码
                    this.u_code.Enabled = false;
                }
                else if (dt.Rows[0][0].ToString() == "True" && dt.Rows[0][1].ToString() == "False")
                {
                    Load_vendorDictSelect_code_Max();//获取供应商编码
                    this.u_code.Enabled = false;
                }
            }
            // u_fktj.SelectedItem = objs[0];
        }
        private void Load_sysPayCond_list()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysPayCond_list";
            paramters.Add("Comp_code", G_User.user.Comp_code);
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            u_fktj.Properties.Items.AddRange(objs);
           // u_fktj.SelectedItem = objs[0];
        }
        private void Load_vender_type_dict()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "dict_yes_or_no";
            serviceParamter.Paramters = paramters;
            List<string> objs = new List<string>();
            List<CodeValue> codeValues = _helper.ReadDictForData(serviceParamter);
            foreach (CodeValue codeValue in codeValues)
            {
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            u_xyhs.Properties.Items.AddRange(objs);
            u_xyhs.SelectedItem = objs[0];
            u_sfty.Properties.Items.AddRange(objs);
            u_sfty.SelectedItem = objs[0];

        }
        private void Load_sysLastLevelVenderType_list_stop()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysLastLevelVenderType_list_stop";
            paramters.Add("Comp_code", G_User.user.Comp_code);
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            u_ven_type.Properties.Items.AddRange(objs);
            u_ven_type.SelectedItem = objs[0];
        }
        private void Load_vendor_DictSelectVen_code_Max() {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "vendor_DictSelectVen_code_Max";
            paramters.Add("u_unit", FormHelper.GetValueForComboBoxEdit(u_ven_type));

            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("供应商类别为空请先维护！");
            }
            else
            {
                this.u_code.Text = dt.Rows[0]["Column1"].ToString();
            }
             
        }
        private void Load_vendorDictSelect_code_Max()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "vendorDictSelect_code_Max";
            
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("供应商类别为空请先维护！");
            }
            else
            {
                this.u_code.Text = dt.Rows[0]["Column1"].ToString();
            }

        }
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Enter)　　// 按下的是回车键
            {
                foreach (Control c in this.Controls)
                {
                    if (c is System.Windows.Forms.TextBox)　　// 当前控件是文本框控件
                    {
                        keyData = Keys.Tab;
                    }
                }
                keyData = Keys.Tab;
            }
            return base.ProcessDialogKey(keyData);
        }
        private bool Validate()
        {

            if (string.IsNullOrEmpty(u_code.Text))
            {
                MessageForm.Warning("供应商编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(u_name.Text))
            {
                MessageForm.Warning("供应商名称不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(FormHelper.GetValueForComboBoxEdit(u_ven_type)))
            {
                MessageForm.Warning("供应商类别不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(FormHelper.GetValueForComboBoxEdit(u_xyhs)))
            {
                MessageForm.Warning("单价是否含税不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(FormHelper.GetValueForComboBoxEdit(u_sfty)))
            {
                MessageForm.Warning("是否停用不能为空!");
                return false;
            }

            if (!Validator.IsMobilePhoneNumber(u_move.Text) && !string.IsNullOrEmpty(u_move.Text))
                
            {
                MessageForm.Warning("手机格式不正确!");
                return false;
            }

            if (!Validator.IsEmail(u_email.Text) && !string.IsNullOrEmpty(u_email.Text))
                
            {
                MessageForm.Warning("邮箱不正确!");
                return false;
            }

            if (!Validator.IsInteger(u_sypd.Text) && !string.IsNullOrEmpty(u_sypd.Text))
                
            {
                MessageForm.Warning("使用频度必须是数字!");
                return false;
            }
            if( !string.IsNullOrEmpty(u_rcl.Text)){
                if (!Validator.IsNumber(u_rcl.Text))
                {
                    MessageForm.Warning("扣率必须是金额!");
                    return false;
                }
                else if (Convert.ToDouble(u_rcl.Text) > 999999999999 || Convert.ToDouble(u_rcl.Text) < -999999999999)
                    {
                        MessageForm.Warning("扣率范围应在-10000000至10000000之间！");
                        return false;
                    }
                
            }
            
            
            if(!string.IsNullOrEmpty(u_xyed.Text)){
                 if (!Validator.IsNumber(u_xyed.Text)  )
                {
                    MessageForm.Warning("信用额度必须是金额!");
                    return false;
                }
                 else if (Convert.ToDouble(u_xyed.Text) > 999999999999 || Convert.ToDouble(u_xyed.Text) < -999999999999)
                 {
                     MessageForm.Warning("信用额度范围应在-999999999999至999999999999之间！");
                     return false;
                 }
            }

           
            if(!string.IsNullOrEmpty(u_xyqx.Text)){
                 if (!Validator.IsNumber(u_xyqx.Text)   )
                {
                    MessageForm.Warning("信用期限必须是金额!");
                    return false;
                } else if (Convert.ToDouble(u_xyqx.Text) > 255 || Convert.ToDouble(u_xyqx.Text) < 0)
                 {
                     MessageForm.Warning("信用期限范围应在0至255之间！");
                     return false;
                 }
            }

           
           if(!string.IsNullOrEmpty(u_yfje.Text)){
               if (!Validator.IsNumber(u_yfje.Text)   )
                {
                    MessageForm.Warning("应付余额必须是金额!");
                    return false;
                }else if (Convert.ToDouble(u_yfje.Text) > 999999999999 || Convert.ToDouble(u_yfje.Text) < -999999999999)
                {
                    MessageForm.Warning("应付余额范围应在-999999999999至999999999999之间！");
                    return false;
                }
           }

            if(!string.IsNullOrEmpty(u_zhjyjj.Text)){
                if (!Validator.IsNumber(u_zhjyjj.Text)  )
                {
                    MessageForm.Warning("最后交易金额必须是金额!");
                    return false;
                }else if (Convert.ToDouble(u_zhjyjj.Text) > 999999999999 || Convert.ToDouble(u_zhjyjj.Text) < -999999999999)
                {
                    MessageForm.Warning("最后交易金额范围应在-999999999999至999999999999之间!");
                    return false;
                }
            }

            if (!string.IsNullOrEmpty(u_zhfjje.Text))
            {
                if (!Validator.IsNumber(u_zhfjje.Text)  )
                {
                    MessageForm.Warning("最后付款金额必须是金额!");
                    return false;
                }else  if (Convert.ToDouble(u_zhfjje.Text) > 999999999999 || Convert.ToDouble(u_zhfjje.Text) < -999999999999)
                {
                    MessageForm.Warning("最后付款金额范围应在-999999999999至999999999999之间!");
                    return false;
                }
            }
            
            return true;
        }

        private void insert_Load(object sender, EventArgs e)
        {

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
        //供应商类别变化修改供应商编码
        private void u_ven_type_SelectedValueChanged(object sender, EventArgs e)
        {
            Load_sysDictSelectCodingPara_value();
        }
        //供应商名称变化生成拼音码
        private void u_name_EditValueChanged(object sender, EventArgs e)
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "mate_inv_name_spell";
            paramters.Add("u_name", u_name.Text );

            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("供应商类别为空请先维护！");
            }
            else
            {
                this.u_spell.Text = dt.Rows[0]["Column1"].ToString();
            }
        }

        private void sysDictsUnitinfoVenData_insert_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Insert();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
        private bool Insert()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "sysDictsUnitinfoVenData_insert";
            Dictionary<string, string> paramters = new Dictionary<string, string>();

            paramters.Add("u_code", u_code.Text);
            paramters.Add("u_name", u_name.Text);
            paramters.Add("Comp_code", G_User.user.Comp_code);
            paramters.Add("u_sname", u_sname.Text);
            paramters.Add("u_taxno", u_taxno.Text);
            paramters.Add("u_ven_type", FormHelper.GetValueForComboBoxEdit(u_ven_type).Trim());
            paramters.Add("u_hy", u_hy.Text);
            paramters.Add("u_prov", u_prov.Text);
            paramters.Add("u_city", u_city.Text);
            
            paramters.Add("is_count", is_count.EditValue.ToString());
            paramters.Add("u_addr", u_addr.Text);
            paramters.Add("u_post", u_post.Text);
            paramters.Add("u_bankname", u_bankname.Text);
            paramters.Add("u_bankno", u_bankno.Text);

            paramters.Add("u_man", u_man.Text);
            paramters.Add("u_mandate", u_mandate.Text);
            paramters.Add("u_tel", u_tel.Text);
            paramters.Add("u_fax", u_fax.Text);
            paramters.Add("u_email", u_email.Text);
            paramters.Add("u_lman", u_lman.Text);
            paramters.Add("u_bbj", u_bbj.Text);
            paramters.Add("u_move", u_move.Text);
            paramters.Add("u_rcl", u_rcl.Text);
            paramters.Add("u_spell", u_spell.Text);
            paramters.Add("u_xydj", u_xydj.Text);
            paramters.Add("u_xyed", u_xyed.Text);
            paramters.Add("u_xyqx", u_xyqx.Text);
            paramters.Add("u_fktj", FormHelper.GetValueForComboBoxEdit(u_fktj).Trim());
            paramters.Add("u_dhdj", u_dhdj.Text);

            paramters.Add("u_dhfs", FormHelper.GetValueForComboBoxEdit(u_dhfs).Trim());
            paramters.Add("u_fgbm", FormHelper.GetValueForSearchLookUpEdit(u_fgbm).Trim());
            paramters.Add("u_cyyyy", u_cyyyy.Text);
            paramters.Add("u_yfje", u_yfje.Text);
            paramters.Add("u_zhjysj", u_zhjysj.Text);
            paramters.Add("u_zhjyjj", u_zhjyjj.Text);
            paramters.Add("u_zhfjrq", u_zhfjrq.Text);


            paramters.Add("u_zhfjje", u_zhfjje.Text);
            paramters.Add("u_lead", u_lead.Text);// u_lead.DateTime.ToString("yyyy-MM-dd"));
            paramters.Add("u_sypd", u_sypd.Text);
            paramters.Add("u_xyhs", FormHelper.GetValueForComboBoxEdit(u_xyhs).Trim());
            paramters.Add("u_zycp", u_zycp.Text);
            paramters.Add("u_sfty", FormHelper.GetValueForComboBoxEdit(u_sfty).Trim());
            paramters.Add("business_charter", business_charter.Text);
            paramters.Add("cb_mate", FormHelper.GetValueForCheckEdit(cb_mate).Trim());
            paramters.Add("cb_equip", FormHelper.GetValueForCheckEdit(cb_equip).Trim());
            paramters.Add("cb_imma", FormHelper.GetValueForCheckEdit(cb_imma).Trim());
            paramters.Add("cb_drug", FormHelper.GetValueForCheckEdit(cb_drug).Trim());
            serviceParamter.Paramters = paramters;

            return _helper.WirteSql(serviceParamter);

        }
    }
}
