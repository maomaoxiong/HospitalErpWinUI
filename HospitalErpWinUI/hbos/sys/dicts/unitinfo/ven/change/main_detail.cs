﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.ven.change
{
    public partial class main_detail : Form
    {
        private DaoHelper _helper;
        private Validator validator;
        DataTable _user = new DataTable();
        private List<Perm> _perms { get; set; }
        public main_detail()
        {
            InitializeComponent();
             
        }
        public main_detail(DaoHelper helper, List<Perm> perms, string content_code)
        {

            InitializeComponent();
            
            _helper = helper;
            _perms = perms;
            validator = new Validator();
            try
            {
                Query(content_code);
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query(string content_code)
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysDictsUnitinfoVenChange_select_detail";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("ven_code", content_code);
             
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            
            _user = dt;
            gridControl1.DataSource = _user;
        }
    }
}
