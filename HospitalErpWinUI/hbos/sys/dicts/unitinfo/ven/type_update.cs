﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.ven
{
    public partial class type_update : Form
    {
        private DaoHelper _helper;
        private Validator validator;
        private List<Perm> _perms { get; set; }
        public type_update()
        {
            InitializeComponent();
        }
        private void Load_dict_yes_or_no()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "dict_yes_or_no";
            serviceParamter.Paramters = paramters;
            List<string> objs = new List<string>();
            List<CodeValue> codeValues = _helper.ReadDictForData(serviceParamter);
            foreach (CodeValue codeValue in codeValues)
            {
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            u_stop.Properties.Items.AddRange(objs);
            u_stop.SelectedItem = objs[0];
            u_last.Properties.Items.AddRange(objs);
            u_last.SelectedItem = objs[0];

        }
        public type_update(DaoHelper helper, List<Perm> perms, string content_code)
        {

            InitializeComponent();          
            _helper = helper;
            _perms = perms;
            validator = new Validator();
            this.StartPosition = FormStartPosition.CenterParent;
            // Init();
            Load_dict_yes_or_no();//是否下拉框
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysDictsUnitinfoVenType_update_load";
            paramters.Add("Comp_code", G_User.user.Comp_code);
            paramters.Add("content_code1", content_code);
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("查询失败！");
            }
            else
            {
                this.u_code.Text = dt.Rows[0]["ven_type_code"].ToString();
                this.u_name.Text = dt.Rows[0]["ven_type_name"].ToString();
                FormHelper.ComboBoxeditAssignment(u_last, dt.Rows[0]["is_last"].ToString());
                FormHelper.ComboBoxeditAssignment(u_stop, dt.Rows[0]["is_stop"].ToString());
             

            }
        }
        private bool o_Validate()
        {

            if (string.IsNullOrEmpty(u_code.Text))
            {
                MessageForm.Warning("供应商类别编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(u_name.Text))
            {
                MessageForm.Warning("供应商类别名称不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(FormHelper.GetValueForComboBoxEdit(u_last)))
            {
                MessageForm.Warning("是否末级不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(FormHelper.GetValueForComboBoxEdit(u_stop)))
            {
                MessageForm.Warning("是否停用不能为空!");
                return false;
            }


            return true;
        }

        private void sysDictsUnitinfoVenType_update_Click(object sender, EventArgs e)
        {
            if (!o_Validate())
            {
                return;
            }
            try
            {
                o_Unsert();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
        private bool o_Unsert()
        {
            //ServiceParamter serviceParamter = new ServiceParamter();
            //Dictionary<string, string> paramters = new Dictionary<string, string>();
            //serviceParamter.ServiceId = "sys_code_format_comp_copy";
            //paramters.Add("Comp_code", G_User.user.Comp_code);
            //paramters.Add("copy", "");
            //paramters.Add("code", "0202");
            //serviceParamter.Paramters = paramters;
            //List<string> objs = new List<string>();
            //DataTable dt = _helper.ReadDictForSql(serviceParamter);
            //string a = "0";
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "sysDictsUnitinfoVenType_update";
            Dictionary<string, string> paramters = new Dictionary<string, string>();

            paramters.Add("u_code", u_code.Text);
            paramters.Add("u_name", u_name.Text);

            paramters.Add("u_parent", "");
            paramters.Add("u_comp", G_User.user.Comp_code);

            paramters.Add("u_last", FormHelper.GetValueForComboBoxEdit(u_last).Trim());
            paramters.Add("u_stop", FormHelper.GetValueForComboBoxEdit(u_stop).Trim());

            serviceParamter.Paramters = paramters;

            return _helper.WirteSql(serviceParamter);
            //return true;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Enter)　　// 按下的是回车键
            {
                foreach (Control c in this.Controls)
                {
                    if (c is System.Windows.Forms.TextBox)　　// 当前控件是文本框控件
                    {
                        keyData = Keys.Tab;
                    }
                }
                keyData = Keys.Tab;
            }
            return base.ProcessDialogKey(keyData);
        }
        
    }
}
