﻿namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.ven
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.sysDictsUnitinfoVenData_change = new DevExpress.XtraEditors.SimpleButton();
            this.sysDictsUnitinfoVenData_delete = new DevExpress.XtraEditors.SimpleButton();
            this.sysDictsUnitinfoVenData_insert = new DevExpress.XtraEditors.SimpleButton();
            this.sysDictsUnitinfoVenQuery_select = new DevExpress.XtraEditors.SimpleButton();
            this.ven_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ven_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.sysDictsUnitinfoVenData_change);
            this.panelControl1.Controls.Add(this.sysDictsUnitinfoVenData_delete);
            this.panelControl1.Controls.Add(this.sysDictsUnitinfoVenData_insert);
            this.panelControl1.Controls.Add(this.sysDictsUnitinfoVenQuery_select);
            this.panelControl1.Controls.Add(this.ven_name);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(874, 52);
            this.panelControl1.TabIndex = 0;
            // 
            // sysDictsUnitinfoVenData_change
            // 
            this.sysDictsUnitinfoVenData_change.Location = new System.Drawing.Point(665, 15);
            this.sysDictsUnitinfoVenData_change.Name = "sysDictsUnitinfoVenData_change";
            this.sysDictsUnitinfoVenData_change.Size = new System.Drawing.Size(84, 24);
            this.sysDictsUnitinfoVenData_change.TabIndex = 101;
            this.sysDictsUnitinfoVenData_change.Text = "名称变更";
            this.sysDictsUnitinfoVenData_change.Click += new System.EventHandler(this.sysDictsUnitinfoVenData_change_Click);
            // 
            // sysDictsUnitinfoVenData_delete
            // 
            this.sysDictsUnitinfoVenData_delete.Location = new System.Drawing.Point(543, 15);
            this.sysDictsUnitinfoVenData_delete.Name = "sysDictsUnitinfoVenData_delete";
            this.sysDictsUnitinfoVenData_delete.Size = new System.Drawing.Size(75, 24);
            this.sysDictsUnitinfoVenData_delete.TabIndex = 100;
            this.sysDictsUnitinfoVenData_delete.Text = "删除";
            this.sysDictsUnitinfoVenData_delete.Click += new System.EventHandler(this.sysDictsUnitinfoVenData_delete_Click);
            // 
            // sysDictsUnitinfoVenData_insert
            // 
            this.sysDictsUnitinfoVenData_insert.Location = new System.Drawing.Point(432, 15);
            this.sysDictsUnitinfoVenData_insert.Name = "sysDictsUnitinfoVenData_insert";
            this.sysDictsUnitinfoVenData_insert.Size = new System.Drawing.Size(75, 24);
            this.sysDictsUnitinfoVenData_insert.TabIndex = 99;
            this.sysDictsUnitinfoVenData_insert.Text = "添加";
            this.sysDictsUnitinfoVenData_insert.Click += new System.EventHandler(this.sysDictsUnitinfoVenData_insert_Click);
            // 
            // sysDictsUnitinfoVenQuery_select
            // 
            this.sysDictsUnitinfoVenQuery_select.Location = new System.Drawing.Point(318, 15);
            this.sysDictsUnitinfoVenQuery_select.Name = "sysDictsUnitinfoVenQuery_select";
            this.sysDictsUnitinfoVenQuery_select.Size = new System.Drawing.Size(75, 24);
            this.sysDictsUnitinfoVenQuery_select.TabIndex = 98;
            this.sysDictsUnitinfoVenQuery_select.Text = "查询";
            this.sysDictsUnitinfoVenQuery_select.Click += new System.EventHandler(this.sysDictsUnitinfoVenQuery_select_Click);
            // 
            // ven_name
            // 
            this.ven_name.Location = new System.Drawing.Point(106, 17);
            this.ven_name.Name = "ven_name";
            this.ven_name.Size = new System.Drawing.Size(147, 20);
            this.ven_name.TabIndex = 90;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(16, 20);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(72, 14);
            this.labelControl7.TabIndex = 89;
            this.labelControl7.Text = "供应商编码：";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 52);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(874, 301);
            this.panelControl2.TabIndex = 1;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.gridControl1.Size = new System.Drawing.Size(870, 297);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "选择";
            this.gridColumn1.FieldName = "checked";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "供应商编码";
            this.gridColumn2.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.gridColumn2.FieldName = "ven_code";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit1_Click);
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "供应商名称";
            this.gridColumn3.FieldName = "ven_name";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 353);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "main";
            this.Text = "录入";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ven_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.TextEdit ven_name;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.SimpleButton sysDictsUnitinfoVenData_insert;
        private DevExpress.XtraEditors.SimpleButton sysDictsUnitinfoVenQuery_select;
        private DevExpress.XtraEditors.SimpleButton sysDictsUnitinfoVenData_change;
        private DevExpress.XtraEditors.SimpleButton sysDictsUnitinfoVenData_delete;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
    }
}