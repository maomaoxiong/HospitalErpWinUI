﻿namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.ven
{
    partial class change1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.qqq = new DevExpress.XtraEditors.LabelControl();
            this.ven_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.old_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.ven_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.change_explain = new DevExpress.XtraEditors.TextEdit();
            this.u_spell = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.sysDictsUnitinfoVenData_change = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.ven_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.old_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ven_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.change_explain.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_spell.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // qqq
            // 
            this.qqq.Location = new System.Drawing.Point(5, 40);
            this.qqq.Name = "qqq";
            this.qqq.Size = new System.Drawing.Size(72, 14);
            this.qqq.TabIndex = 12;
            this.qqq.Text = "供应商编码：";
            // 
            // ven_code
            // 
            this.ven_code.Enabled = false;
            this.ven_code.Location = new System.Drawing.Point(83, 34);
            this.ven_code.Name = "ven_code";
            this.ven_code.Size = new System.Drawing.Size(178, 20);
            this.ven_code.TabIndex = 13;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(29, 75);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(48, 14);
            this.labelControl1.TabIndex = 14;
            this.labelControl1.Text = "原名称：";
            // 
            // old_name
            // 
            this.old_name.Enabled = false;
            this.old_name.Location = new System.Drawing.Point(83, 72);
            this.old_name.Name = "old_name";
            this.old_name.Size = new System.Drawing.Size(178, 20);
            this.old_name.TabIndex = 15;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(29, 110);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(48, 14);
            this.labelControl2.TabIndex = 16;
            this.labelControl2.Text = "新名称：";
            // 
            // ven_name
            // 
            this.ven_name.Location = new System.Drawing.Point(83, 107);
            this.ven_name.Name = "ven_name";
            this.ven_name.Size = new System.Drawing.Size(178, 20);
            this.ven_name.TabIndex = 17;
            this.ven_name.EditValueChanged += new System.EventHandler(this.ven_name_EditValueChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(17, 147);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 14);
            this.labelControl3.TabIndex = 18;
            this.labelControl3.Text = "变更说明：";
            // 
            // change_explain
            // 
            this.change_explain.Location = new System.Drawing.Point(83, 144);
            this.change_explain.Name = "change_explain";
            this.change_explain.Size = new System.Drawing.Size(178, 20);
            this.change_explain.TabIndex = 19;
            // 
            // u_spell
            // 
            this.u_spell.Location = new System.Drawing.Point(94, 230);
            this.u_spell.Name = "u_spell";
            this.u_spell.Size = new System.Drawing.Size(178, 20);
            this.u_spell.TabIndex = 20;
            this.u_spell.Visible = false;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(167, 186);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 50;
            this.simpleButton2.Text = "关闭";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // sysDictsUnitinfoVenData_change
            // 
            this.sysDictsUnitinfoVenData_change.Location = new System.Drawing.Point(50, 186);
            this.sysDictsUnitinfoVenData_change.Name = "sysDictsUnitinfoVenData_change";
            this.sysDictsUnitinfoVenData_change.Size = new System.Drawing.Size(75, 23);
            this.sysDictsUnitinfoVenData_change.TabIndex = 49;
            this.sysDictsUnitinfoVenData_change.Text = "保存";
            this.sysDictsUnitinfoVenData_change.Click += new System.EventHandler(this.sysDictsUnitinfoVenData_change_Click);
            // 
            // change1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.sysDictsUnitinfoVenData_change);
            this.Controls.Add(this.u_spell);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.change_explain);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.ven_name);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.old_name);
            this.Controls.Add(this.qqq);
            this.Controls.Add(this.ven_code);
            this.Name = "change1";
            this.Text = "名称变更";
            this.Load += new System.EventHandler(this.change_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ven_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.old_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ven_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.change_explain.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_spell.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl qqq;
        private DevExpress.XtraEditors.TextEdit ven_code;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit old_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit ven_name;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit change_explain;
        private DevExpress.XtraEditors.TextEdit u_spell;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton sysDictsUnitinfoVenData_change;
    }
}