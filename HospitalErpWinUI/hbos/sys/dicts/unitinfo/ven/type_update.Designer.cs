﻿namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.ven
{
    partial class type_update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.sysDictsUnitinfoVenType_update = new DevExpress.XtraEditors.SimpleButton();
            this.u_stop = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.u_last = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.u_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.u_code = new DevExpress.XtraEditors.TextEdit();
            this.qqq = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.u_stop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_last.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(174, 207);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 6;
            this.simpleButton2.Text = "关闭";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // sysDictsUnitinfoVenType_update
            // 
            this.sysDictsUnitinfoVenType_update.Location = new System.Drawing.Point(57, 207);
            this.sysDictsUnitinfoVenType_update.Name = "sysDictsUnitinfoVenType_update";
            this.sysDictsUnitinfoVenType_update.Size = new System.Drawing.Size(75, 23);
            this.sysDictsUnitinfoVenType_update.TabIndex = 5;
            this.sysDictsUnitinfoVenType_update.Text = "保存";
            this.sysDictsUnitinfoVenType_update.Click += new System.EventHandler(this.sysDictsUnitinfoVenType_update_Click);
            // 
            // u_stop
            // 
            this.u_stop.Location = new System.Drawing.Point(109, 149);
            this.u_stop.Name = "u_stop";
            this.u_stop.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_stop.Size = new System.Drawing.Size(168, 20);
            this.u_stop.TabIndex = 4;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(43, 152);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 102;
            this.labelControl2.Text = "是否停用：";
            // 
            // u_last
            // 
            this.u_last.Location = new System.Drawing.Point(109, 114);
            this.u_last.Name = "u_last";
            this.u_last.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_last.Size = new System.Drawing.Size(168, 20);
            this.u_last.TabIndex = 3;
            // 
            // labelControl37
            // 
            this.labelControl37.Location = new System.Drawing.Point(43, 117);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(60, 14);
            this.labelControl37.TabIndex = 100;
            this.labelControl37.Text = "是否末级：";
            // 
            // u_name
            // 
            this.u_name.Location = new System.Drawing.Point(109, 74);
            this.u_name.Name = "u_name";
            this.u_name.Size = new System.Drawing.Size(168, 20);
            this.u_name.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(7, 77);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(96, 14);
            this.labelControl1.TabIndex = 98;
            this.labelControl1.Text = "供应商类别名称：";
            // 
            // u_code
            // 
            this.u_code.Enabled = false;
            this.u_code.Location = new System.Drawing.Point(109, 32);
            this.u_code.Name = "u_code";
            this.u_code.Size = new System.Drawing.Size(168, 20);
            this.u_code.TabIndex = 1;
            // 
            // qqq
            // 
            this.qqq.Location = new System.Drawing.Point(7, 35);
            this.qqq.Name = "qqq";
            this.qqq.Size = new System.Drawing.Size(96, 14);
            this.qqq.TabIndex = 96;
            this.qqq.Text = "供应商类别编码：";
            // 
            // type_update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 262);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.sysDictsUnitinfoVenType_update);
            this.Controls.Add(this.u_stop);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.u_last);
            this.Controls.Add(this.labelControl37);
            this.Controls.Add(this.u_name);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.u_code);
            this.Controls.Add(this.qqq);
            this.Name = "type_update";
            this.Text = "修改";
            ((System.ComponentModel.ISupportInitialize)(this.u_stop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_last.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton sysDictsUnitinfoVenType_update;
        private DevExpress.XtraEditors.ComboBoxEdit u_stop;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ComboBoxEdit u_last;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.TextEdit u_name;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit u_code;
        private DevExpress.XtraEditors.LabelControl qqq;
    }
}