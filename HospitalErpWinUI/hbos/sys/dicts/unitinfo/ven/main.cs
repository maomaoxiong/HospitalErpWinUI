﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraGrid;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.ven
{
    public partial class main : RibbonForm
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public main()
        {
            InitializeComponent();
            _helper = new DaoHelper();
        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                int index = gridView1.GetDataSourceRowIndex(gridView1.FocusedRowHandle);
                string ven_code = _user.Rows[index]["ven_code"].ToString();

                update frm = new update(this._helper, this._perms, ven_code);

                if (frm.ShowDialog() == DialogResult.No)
                {
                    MessageForm.Warning("修改失败！");
                }
                Query();
            }
        }

        private void Query()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysDictsUnitinfoVenData_select";
            //，，，，，，，，
            paramters.Add("Comp_code", G_User.user.Comp_code);
            paramters.Add("ven_name", ven_name.Text.ToString());
            
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            //  List<string> objs = Load_acct_cashdire();//方向 下拉
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";

            }
            _user = dt;
            gridControl1.DataSource = _user;
        }

        private void sysDictsUnitinfoVenQuery_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void sysDictsUnitinfoVenData_insert_Click(object sender, EventArgs e)
        {
            try
            {
                insert insertFrm = new insert(this._helper, this._perms);

                if (insertFrm.ShowDialog() == DialogResult.OK)
                {
                    Query();
                }
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void sysDictsUnitinfoVenData_change_Click(object sender, EventArgs e)
        {
            bool value = false;
            string strSelected = string.Empty;

            try
            {
                List<string[]> objs = new List<string[]>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    value = Convert.ToBoolean(gridView1.GetDataRow(i)["checked"]);
                    if (value)
                    {
                        string[] ven_code = { gridView1.GetDataRow(i)["ven_code"].ToString(), gridView1.GetDataRow(i)["ven_name"].ToString() };
                        objs.Add(ven_code);

                    }
                }
                if (objs.Count == 0)
                {
                    MessageForm.Show("请选择要变更的数据!");
                    return;
                }
                if (objs.Count != 1)
                {
                    MessageForm.Show("请选择单个数据进行变更!");
                    return;
                }
                if (MessageForm.ShowMsg("确认是否变更?", MsgType.YesNo) == DialogResult.Yes)
                {
                    change1 frm = new change1(this._helper, this._perms, objs);
                    if (frm.ShowDialog() == DialogResult.No)
                    {
                        MessageForm.Warning("变更失败！");
                    }
                    Query();
                }

            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void sysDictsUnitinfoVenData_delete_Click(object sender, EventArgs e)
        {
            bool value = false;
            string strSelected = string.Empty;

            try
            {
                List<string> objs = new List<string>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    value = Convert.ToBoolean(gridView1.GetDataRow(i)["checked"]);
                    if (value)
                    {
                        string code = gridView1.GetDataRow(i)["ven_code"].ToString();
                        objs.Add(code);

                    }
                }
                if (objs.Count == 0)
                {
                    MessageForm.Show("请选择要删除的数据!");
                    return;
                }
                if (MessageForm.ShowMsg("确认是否删除?", MsgType.YesNo) == DialogResult.Yes)
                {
                    foreach (string obj in objs)
                    {
                        if (!Delete(obj))
                        {
                            return;
                        }
                    }
                    //       MessageForm.Show("操作成功!");
                    Query();
                }

            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private bool Delete(string co_code)
        {

            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysDictsUnitinfoVenData_delete";

            paramters.Add("u_code", co_code);
            paramters.Add("u_name", "");
            paramters.Add("Comp_code", G_User.user.Comp_code);
            paramters.Add("u_sname", "");
            paramters.Add("u_taxno", "");
            paramters.Add("u_ven_type", "");
            paramters.Add("u_hy","");
            paramters.Add("u_prov", "");
            paramters.Add("u_city", "");

            paramters.Add("is_count", "");
            paramters.Add("u_addr", "");
            paramters.Add("u_post", "");
            paramters.Add("u_bankname", "");
            paramters.Add("u_bankno", "");

            paramters.Add("u_man", "");
            paramters.Add("u_mandate", "");
            paramters.Add("u_tel", "");
            paramters.Add("u_fax", "");
            paramters.Add("u_email", "");
            paramters.Add("u_lman", "");
            paramters.Add("u_bbj", "");
            paramters.Add("u_move", "");
            paramters.Add("u_rcl", "");
            paramters.Add("u_spell", "");
            paramters.Add("u_xydj", "");
            paramters.Add("u_xyed", "");
            paramters.Add("u_xyqx", "");
            paramters.Add("u_fktj","");
            paramters.Add("u_dhdj", "");

            paramters.Add("u_dhfs", "");
            paramters.Add("u_fgbm", "");
            paramters.Add("u_cyyyy", "");
            paramters.Add("u_yfje", "");
            paramters.Add("u_zhjysj","");
            paramters.Add("u_zhjyjj", "");
            paramters.Add("u_zhfjrq", "");


            paramters.Add("u_zhfjje", "");
            paramters.Add("u_lead", "");// u_lead.DateTime.ToString("yyyy-MM-dd"));
            paramters.Add("u_sypd", "");
            paramters.Add("u_xyhs", "");
            paramters.Add("u_zycp", "");
            paramters.Add("u_sfty", "");
            paramters.Add("business_charter", "");
            paramters.Add("cb_mate", "");
            paramters.Add("cb_equip", "");
            paramters.Add("cb_imma", "");
            paramters.Add("cb_drug", "");
            serviceParamter.Paramters = paramters;
            bool result = false;
            try
            {
                _helper.WirteSql(serviceParamter);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }
    }
}

