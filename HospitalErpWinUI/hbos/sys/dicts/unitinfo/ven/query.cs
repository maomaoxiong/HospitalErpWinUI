﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.ven
{
    public partial class query : RibbonForm
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public query()
        {
            InitializeComponent();
            _helper = new DaoHelper();
            Load_vender_type_dict();//是否下拉框
        }
        private void Load_vender_type_dict()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "vender_type_dict";

            serviceParamter.Paramters = paramters;
            List<string> objs = new List<string>();
            List<CodeValue> codeValues = _helper.ReadDictForData(serviceParamter);
            foreach (CodeValue codeValue in codeValues)
            {
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            u_type.Properties.Items.AddRange(objs);


        }

        private void sysDictsUnitinfoVenQuery_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void Query()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysDictsUnitinfoVenQuery_select";
            //，，，，，，，，
            paramters.Add("u_unit", u_unit.Text.ToString());
            paramters.Add("u_copy", u_copy.Text.ToString());
            paramters.Add("business_charter", business_charter.Text.ToString());
            paramters.Add("u_uni", u_uni.Text.ToString());
            paramters.Add("u_cop", u_cop.Text.ToString());
            paramters.Add("u_type", FormHelper.GetValueForComboBoxEdit(u_type));
           // ，u_unit，u_copy，business_charter，u_uni，u_cop，u_type
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            //  List<string> objs = Load_acct_cashdire();//方向 下拉
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
                
            }
            _user = dt;
            gridControl1.DataSource = _user;
        }
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Enter)　　// 按下的是回车键
            {
                foreach (Control c in this.Controls)
                {
                    if (c is System.Windows.Forms.TextBox)　　// 当前控件是文本框控件
                    {
                        keyData = Keys.Tab;
                    }
                }
                keyData = Keys.Tab;
            }
            return base.ProcessDialogKey(keyData);
        }
    }
}
