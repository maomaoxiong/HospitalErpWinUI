﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.ven
{
    public partial class change1 : Form
    {
        private DaoHelper _helper;
        private Validator validator;
        private List<Perm> _perms { get; set; }
        public change1()
        {
            InitializeComponent();
        }
        public change1(DaoHelper helper, List<Perm> perms, List<string[]> objes)
        {

            InitializeComponent();
            _helper = helper;
            _perms = perms;
            validator = new Validator();
            this.ven_code.Text = objes[0][0].ToString();
            this.old_name.Text = objes[0][1].ToString();
         }
        private void change_Load(object sender, EventArgs e)
        {

        }

        private void ven_name_EditValueChanged(object sender, EventArgs e)
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "mate_inv_name_spell";
            paramters.Add("u_name", ven_name.Text);

            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
             
                this.u_spell.Text = dt.Rows[0]["Column1"].ToString();
 
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void sysDictsUnitinfoVenData_change_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ven_name.Text))
            {
                MessageBox.Show("新名称不能为空！");
                return;
            }
            try
            {
                Insert();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
        private bool Insert()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "sysDictsUnitinfoVenData_change";
            Dictionary<string, string> paramters = new Dictionary<string, string>();

            paramters.Add("Comp_code1", G_User.user.Comp_code);
            paramters.Add("ven_code", ven_code.Text);
            paramters.Add("old_name", old_name.Text);
            paramters.Add("ven_name", ven_name.Text);
            paramters.Add("change_explain", change_explain.Text);
            paramters.Add("u_spell", u_spell.Text);
            paramters.Add("User_code", G_User.user.User_code);
            paramters.Add("Comp_code2", G_User.user.Comp_code);
            paramters.Add("Copy_code", G_User.user.Copy_code);
            
            serviceParamter.Paramters = paramters;
            return _helper.WirteSql(serviceParamter);
        }
    }
}
