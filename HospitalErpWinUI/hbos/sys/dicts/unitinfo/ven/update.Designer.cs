﻿namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.ven
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.sysDictsUnitinfoVenData_update = new DevExpress.XtraEditors.SimpleButton();
            this.u_lead = new DevExpress.XtraEditors.DateEdit();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.cb_drug = new DevExpress.XtraEditors.CheckEdit();
            this.cb_imma = new DevExpress.XtraEditors.CheckEdit();
            this.cb_equip = new DevExpress.XtraEditors.CheckEdit();
            this.cb_mate = new DevExpress.XtraEditors.CheckEdit();
            this.business_charter = new DevExpress.XtraEditors.TextEdit();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.u_sfty = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.u_zycp = new DevExpress.XtraEditors.TextEdit();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.u_xyhs = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.u_zhfjje = new DevExpress.XtraEditors.TextEdit();
            this.u_zhfjrq = new DevExpress.XtraEditors.DateEdit();
            this.u_sypd = new DevExpress.XtraEditors.TextEdit();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.u_zhjysj = new DevExpress.XtraEditors.DateEdit();
            this.u_zhjyjj = new DevExpress.XtraEditors.TextEdit();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.u_yfje = new DevExpress.XtraEditors.TextEdit();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.u_fgbm = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.u_dhfs = new DevExpress.XtraEditors.ComboBoxEdit();
            this.u_cyyyy = new DevExpress.XtraEditors.TextEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.u_email = new DevExpress.XtraEditors.TextEdit();
            this.u_fktj = new DevExpress.XtraEditors.ComboBoxEdit();
            this.u_dhdj = new DevExpress.XtraEditors.TextEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.u_xyqx = new DevExpress.XtraEditors.TextEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.u_xyed = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.u_xydj = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.u_spell = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.u_rcl = new DevExpress.XtraEditors.TextEdit();
            this.wwawaw = new DevExpress.XtraEditors.LabelControl();
            this.u_move = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.u_bbj = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.u_lman = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.u_fax = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.u_mandate = new DevExpress.XtraEditors.DateEdit();
            this.u_tel = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.u_man = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.u_bankname = new DevExpress.XtraEditors.TextEdit();
            this.u_bankno = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.u_post = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.is_count = new DevExpress.XtraEditors.RadioGroup();
            this.u_city = new DevExpress.XtraEditors.TextEdit();
            this.u_addr = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.u_prov = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.u_ven_type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.u_hy = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.u_taxno = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.u_sname = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.u_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.u_code = new DevExpress.XtraEditors.TextEdit();
            this.qqq = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_lead.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_lead.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_drug.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_imma.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_equip.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_mate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.business_charter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_sfty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_zycp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_xyhs.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_zhfjje.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_zhfjrq.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_zhfjrq.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_sypd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_zhjysj.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_zhjysj.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_zhjyjj.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_yfje.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_fgbm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_dhfs.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_cyyyy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_email.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_fktj.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_dhdj.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_xyqx.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_xyed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_xydj.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_spell.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_rcl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_move.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_bbj.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_lman.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_fax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_mandate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_mandate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_tel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_man.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_bankname.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_bankno.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_post.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_count.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_city.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_addr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_prov.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_ven_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_hy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_taxno.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_sname.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(484, 572);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 46;
            this.simpleButton2.Text = "关闭";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // sysDictsUnitinfoVenData_update
            // 
            this.sysDictsUnitinfoVenData_update.Location = new System.Drawing.Point(367, 572);
            this.sysDictsUnitinfoVenData_update.Name = "sysDictsUnitinfoVenData_update";
            this.sysDictsUnitinfoVenData_update.Size = new System.Drawing.Size(75, 23);
            this.sysDictsUnitinfoVenData_update.TabIndex = 45;
            this.sysDictsUnitinfoVenData_update.Text = "保存";
            this.sysDictsUnitinfoVenData_update.Click += new System.EventHandler(this.sysDictsUnitinfoVenData_update_Click);
            // 
            // u_lead
            // 
            this.u_lead.EditValue = null;
            this.u_lead.Location = new System.Drawing.Point(710, 573);
            this.u_lead.Name = "u_lead";
            this.u_lead.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_lead.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_lead.Size = new System.Drawing.Size(168, 20);
            this.u_lead.TabIndex = 186;
            this.u_lead.Visible = false;
            // 
            // labelControl39
            // 
            this.labelControl39.Location = new System.Drawing.Point(612, 576);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(60, 14);
            this.labelControl39.TabIndex = 47;
            this.labelControl39.Text = "停用日期：";
            this.labelControl39.Visible = false;
            // 
            // cb_drug
            // 
            this.cb_drug.EditValue = true;
            this.cb_drug.Location = new System.Drawing.Point(635, 523);
            this.cb_drug.Name = "cb_drug";
            this.cb_drug.Properties.Caption = "药品";
            this.cb_drug.Size = new System.Drawing.Size(75, 19);
            this.cb_drug.TabIndex = 44;
            // 
            // cb_imma
            // 
            this.cb_imma.EditValue = true;
            this.cb_imma.Location = new System.Drawing.Point(545, 523);
            this.cb_imma.Name = "cb_imma";
            this.cb_imma.Properties.Caption = "无形资产";
            this.cb_imma.Size = new System.Drawing.Size(75, 19);
            this.cb_imma.TabIndex = 43;
            // 
            // cb_equip
            // 
            this.cb_equip.EditValue = true;
            this.cb_equip.Location = new System.Drawing.Point(464, 523);
            this.cb_equip.Name = "cb_equip";
            this.cb_equip.Properties.Caption = "固定资产";
            this.cb_equip.Size = new System.Drawing.Size(75, 19);
            this.cb_equip.TabIndex = 42;
            // 
            // cb_mate
            // 
            this.cb_mate.EditValue = true;
            this.cb_mate.Location = new System.Drawing.Point(383, 523);
            this.cb_mate.Name = "cb_mate";
            this.cb_mate.Properties.Caption = "物资";
            this.cb_mate.Size = new System.Drawing.Size(75, 19);
            this.cb_mate.TabIndex = 41;
            // 
            // business_charter
            // 
            this.business_charter.Location = new System.Drawing.Point(122, 522);
            this.business_charter.Name = "business_charter";
            this.business_charter.Size = new System.Drawing.Size(168, 20);
            this.business_charter.TabIndex = 40;
            // 
            // labelControl38
            // 
            this.labelControl38.Location = new System.Drawing.Point(48, 525);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(60, 14);
            this.labelControl38.TabIndex = 184;
            this.labelControl38.Text = "营业执照：";
            // 
            // u_sfty
            // 
            this.u_sfty.Location = new System.Drawing.Point(733, 485);
            this.u_sfty.Name = "u_sfty";
            this.u_sfty.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_sfty.Size = new System.Drawing.Size(168, 20);
            this.u_sfty.TabIndex = 39;
            // 
            // labelControl37
            // 
            this.labelControl37.Location = new System.Drawing.Point(659, 488);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(60, 14);
            this.labelControl37.TabIndex = 183;
            this.labelControl37.Text = "是否停用：";
            // 
            // u_zycp
            // 
            this.u_zycp.Location = new System.Drawing.Point(433, 485);
            this.u_zycp.Name = "u_zycp";
            this.u_zycp.Size = new System.Drawing.Size(168, 20);
            this.u_zycp.TabIndex = 38;
            // 
            // labelControl36
            // 
            this.labelControl36.Location = new System.Drawing.Point(359, 488);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(60, 14);
            this.labelControl36.TabIndex = 182;
            this.labelControl36.Text = "主要产品：";
            // 
            // u_xyhs
            // 
            this.u_xyhs.Location = new System.Drawing.Point(122, 485);
            this.u_xyhs.Name = "u_xyhs";
            this.u_xyhs.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_xyhs.Size = new System.Drawing.Size(168, 20);
            this.u_xyhs.TabIndex = 37;
            // 
            // labelControl35
            // 
            this.labelControl35.Location = new System.Drawing.Point(24, 488);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(84, 14);
            this.labelControl35.TabIndex = 181;
            this.labelControl35.Text = "单价是否含税：";
            // 
            // u_zhfjje
            // 
            this.u_zhfjje.Location = new System.Drawing.Point(433, 448);
            this.u_zhfjje.Name = "u_zhfjje";
            this.u_zhfjje.Size = new System.Drawing.Size(168, 20);
            this.u_zhfjje.TabIndex = 35;
            // 
            // u_zhfjrq
            // 
            this.u_zhfjrq.EditValue = null;
            this.u_zhfjrq.Location = new System.Drawing.Point(122, 448);
            this.u_zhfjrq.Name = "u_zhfjrq";
            this.u_zhfjrq.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_zhfjrq.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_zhfjrq.Size = new System.Drawing.Size(168, 20);
            this.u_zhfjrq.TabIndex = 34;
            // 
            // u_sypd
            // 
            this.u_sypd.Location = new System.Drawing.Point(733, 448);
            this.u_sypd.Name = "u_sypd";
            this.u_sypd.Size = new System.Drawing.Size(168, 20);
            this.u_sypd.TabIndex = 36;
            // 
            // labelControl32
            // 
            this.labelControl32.Location = new System.Drawing.Point(659, 451);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(60, 14);
            this.labelControl32.TabIndex = 180;
            this.labelControl32.Text = "使用频度：";
            // 
            // labelControl33
            // 
            this.labelControl33.Location = new System.Drawing.Point(335, 451);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(84, 14);
            this.labelControl33.TabIndex = 179;
            this.labelControl33.Text = "最后付款金额：";
            // 
            // labelControl34
            // 
            this.labelControl34.Location = new System.Drawing.Point(24, 451);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(84, 14);
            this.labelControl34.TabIndex = 178;
            this.labelControl34.Text = "最后付款日期：";
            // 
            // u_zhjysj
            // 
            this.u_zhjysj.EditValue = "";
            this.u_zhjysj.Location = new System.Drawing.Point(433, 413);
            this.u_zhjysj.Name = "u_zhjysj";
            this.u_zhjysj.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_zhjysj.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_zhjysj.Size = new System.Drawing.Size(168, 20);
            this.u_zhjysj.TabIndex = 32;
            // 
            // u_zhjyjj
            // 
            this.u_zhjyjj.Location = new System.Drawing.Point(733, 413);
            this.u_zhjyjj.Name = "u_zhjyjj";
            this.u_zhjyjj.Size = new System.Drawing.Size(168, 20);
            this.u_zhjyjj.TabIndex = 33;
            // 
            // labelControl29
            // 
            this.labelControl29.Location = new System.Drawing.Point(635, 416);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(84, 14);
            this.labelControl29.TabIndex = 177;
            this.labelControl29.Text = "最后交易金额：";
            // 
            // labelControl30
            // 
            this.labelControl30.Location = new System.Drawing.Point(335, 416);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(84, 14);
            this.labelControl30.TabIndex = 176;
            this.labelControl30.Text = "最后交易时间：";
            // 
            // u_yfje
            // 
            this.u_yfje.Location = new System.Drawing.Point(122, 413);
            this.u_yfje.Name = "u_yfje";
            this.u_yfje.Size = new System.Drawing.Size(165, 20);
            this.u_yfje.TabIndex = 31;
            // 
            // labelControl31
            // 
            this.labelControl31.Location = new System.Drawing.Point(48, 416);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(60, 14);
            this.labelControl31.TabIndex = 175;
            this.labelControl31.Text = "应付余额：";
            // 
            // u_fgbm
            // 
            this.u_fgbm.EditValue = "";
            this.u_fgbm.Location = new System.Drawing.Point(433, 372);
            this.u_fgbm.Name = "u_fgbm";
            this.u_fgbm.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_fgbm.Properties.NullText = "";
            this.u_fgbm.Properties.View = this.searchLookUpEdit1View;
            this.u_fgbm.Size = new System.Drawing.Size(168, 20);
            this.u_fgbm.TabIndex = 29;
            // 
            // u_dhfs
            // 
            this.u_dhfs.Location = new System.Drawing.Point(122, 372);
            this.u_dhfs.Name = "u_dhfs";
            this.u_dhfs.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_dhfs.Size = new System.Drawing.Size(168, 20);
            this.u_dhfs.TabIndex = 28;
            // 
            // u_cyyyy
            // 
            this.u_cyyyy.Location = new System.Drawing.Point(733, 372);
            this.u_cyyyy.Name = "u_cyyyy";
            this.u_cyyyy.Size = new System.Drawing.Size(168, 20);
            this.u_cyyyy.TabIndex = 30;
            // 
            // labelControl26
            // 
            this.labelControl26.Location = new System.Drawing.Point(647, 375);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(72, 14);
            this.labelControl26.TabIndex = 174;
            this.labelControl26.Text = "专营业务员：";
            // 
            // labelControl27
            // 
            this.labelControl27.Location = new System.Drawing.Point(359, 375);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(60, 14);
            this.labelControl27.TabIndex = 173;
            this.labelControl27.Text = "分管部门：";
            // 
            // labelControl28
            // 
            this.labelControl28.Location = new System.Drawing.Point(48, 375);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(60, 14);
            this.labelControl28.TabIndex = 172;
            this.labelControl28.Text = "到货方式：";
            // 
            // u_email
            // 
            this.u_email.Location = new System.Drawing.Point(433, 223);
            this.u_email.Name = "u_email";
            this.u_email.Size = new System.Drawing.Size(168, 20);
            this.u_email.TabIndex = 17;
            // 
            // u_fktj
            // 
            this.u_fktj.Location = new System.Drawing.Point(433, 334);
            this.u_fktj.Name = "u_fktj";
            this.u_fktj.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_fktj.Size = new System.Drawing.Size(168, 20);
            this.u_fktj.TabIndex = 26;
            // 
            // u_dhdj
            // 
            this.u_dhdj.Location = new System.Drawing.Point(733, 334);
            this.u_dhdj.Name = "u_dhdj";
            this.u_dhdj.Size = new System.Drawing.Size(168, 20);
            this.u_dhdj.TabIndex = 27;
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(659, 337);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(60, 14);
            this.labelControl23.TabIndex = 171;
            this.labelControl23.Text = "到货地址：";
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(359, 337);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(60, 14);
            this.labelControl24.TabIndex = 170;
            this.labelControl24.Text = "付款条件：";
            // 
            // u_xyqx
            // 
            this.u_xyqx.Location = new System.Drawing.Point(122, 334);
            this.u_xyqx.Name = "u_xyqx";
            this.u_xyqx.Size = new System.Drawing.Size(165, 20);
            this.u_xyqx.TabIndex = 25;
            // 
            // labelControl25
            // 
            this.labelControl25.Location = new System.Drawing.Point(48, 337);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(60, 14);
            this.labelControl25.TabIndex = 169;
            this.labelControl25.Text = "信用期限：";
            // 
            // u_xyed
            // 
            this.u_xyed.Location = new System.Drawing.Point(733, 298);
            this.u_xyed.Name = "u_xyed";
            this.u_xyed.Size = new System.Drawing.Size(168, 20);
            this.u_xyed.TabIndex = 24;
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(659, 301);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(60, 14);
            this.labelControl18.TabIndex = 168;
            this.labelControl18.Text = "信用额度：";
            // 
            // u_xydj
            // 
            this.u_xydj.Location = new System.Drawing.Point(433, 298);
            this.u_xydj.Name = "u_xydj";
            this.u_xydj.Size = new System.Drawing.Size(168, 20);
            this.u_xydj.TabIndex = 23;
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(359, 301);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(60, 14);
            this.labelControl21.TabIndex = 167;
            this.labelControl21.Text = "信用等级：";
            // 
            // u_spell
            // 
            this.u_spell.Location = new System.Drawing.Point(122, 298);
            this.u_spell.Name = "u_spell";
            this.u_spell.Size = new System.Drawing.Size(165, 20);
            this.u_spell.TabIndex = 22;
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(60, 301);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(48, 14);
            this.labelControl22.TabIndex = 166;
            this.labelControl22.Text = "拼音码：";
            // 
            // u_rcl
            // 
            this.u_rcl.Location = new System.Drawing.Point(733, 262);
            this.u_rcl.Name = "u_rcl";
            this.u_rcl.Size = new System.Drawing.Size(168, 20);
            this.u_rcl.TabIndex = 21;
            // 
            // wwawaw
            // 
            this.wwawaw.Location = new System.Drawing.Point(683, 265);
            this.wwawaw.Name = "wwawaw";
            this.wwawaw.Size = new System.Drawing.Size(36, 14);
            this.wwawaw.TabIndex = 163;
            this.wwawaw.Text = "扣率：";
            // 
            // u_move
            // 
            this.u_move.Location = new System.Drawing.Point(433, 262);
            this.u_move.Name = "u_move";
            this.u_move.Size = new System.Drawing.Size(168, 20);
            this.u_move.TabIndex = 20;
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(383, 265);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(36, 14);
            this.labelControl19.TabIndex = 159;
            this.labelControl19.Text = "手机：";
            // 
            // u_bbj
            // 
            this.u_bbj.Location = new System.Drawing.Point(122, 262);
            this.u_bbj.Name = "u_bbj";
            this.u_bbj.Size = new System.Drawing.Size(165, 20);
            this.u_bbj.TabIndex = 19;
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(72, 265);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(36, 14);
            this.labelControl20.TabIndex = 156;
            this.labelControl20.Text = "呼机：";
            // 
            // u_lman
            // 
            this.u_lman.Location = new System.Drawing.Point(733, 223);
            this.u_lman.Name = "u_lman";
            this.u_lman.Size = new System.Drawing.Size(168, 20);
            this.u_lman.TabIndex = 18;
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(671, 226);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(48, 14);
            this.labelControl15.TabIndex = 152;
            this.labelControl15.Text = "联系人：";
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(373, 226);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(46, 14);
            this.labelControl16.TabIndex = 150;
            this.labelControl16.Text = "EMAIL：";
            // 
            // u_fax
            // 
            this.u_fax.Location = new System.Drawing.Point(122, 223);
            this.u_fax.Name = "u_fax";
            this.u_fax.Size = new System.Drawing.Size(165, 20);
            this.u_fax.TabIndex = 16;
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(72, 226);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(36, 14);
            this.labelControl17.TabIndex = 147;
            this.labelControl17.Text = "传真：";
            // 
            // u_mandate
            // 
            this.u_mandate.EditValue = null;
            this.u_mandate.Location = new System.Drawing.Point(433, 190);
            this.u_mandate.Name = "u_mandate";
            this.u_mandate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_mandate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_mandate.Size = new System.Drawing.Size(168, 20);
            this.u_mandate.TabIndex = 14;
            // 
            // u_tel
            // 
            this.u_tel.Location = new System.Drawing.Point(733, 187);
            this.u_tel.Name = "u_tel";
            this.u_tel.Size = new System.Drawing.Size(168, 20);
            this.u_tel.TabIndex = 15;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(683, 190);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(36, 14);
            this.labelControl12.TabIndex = 143;
            this.labelControl12.Text = "电话：";
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(359, 190);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(60, 14);
            this.labelControl13.TabIndex = 141;
            this.labelControl13.Text = "发展日期：";
            // 
            // u_man
            // 
            this.u_man.Location = new System.Drawing.Point(122, 187);
            this.u_man.Name = "u_man";
            this.u_man.Size = new System.Drawing.Size(165, 20);
            this.u_man.TabIndex = 13;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(72, 190);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(36, 14);
            this.labelControl14.TabIndex = 138;
            this.labelControl14.Text = "法人：";
            // 
            // u_bankname
            // 
            this.u_bankname.Location = new System.Drawing.Point(433, 152);
            this.u_bankname.Name = "u_bankname";
            this.u_bankname.Size = new System.Drawing.Size(168, 20);
            this.u_bankname.TabIndex = 11;
            // 
            // u_bankno
            // 
            this.u_bankno.Location = new System.Drawing.Point(733, 152);
            this.u_bankno.Name = "u_bankno";
            this.u_bankno.Size = new System.Drawing.Size(168, 20);
            this.u_bankno.TabIndex = 12;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(659, 155);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(60, 14);
            this.labelControl9.TabIndex = 135;
            this.labelControl9.Text = "银行账号：";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(359, 155);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(60, 14);
            this.labelControl10.TabIndex = 133;
            this.labelControl10.Text = "开户银行：";
            // 
            // u_post
            // 
            this.u_post.Location = new System.Drawing.Point(122, 152);
            this.u_post.Name = "u_post";
            this.u_post.Size = new System.Drawing.Size(165, 20);
            this.u_post.TabIndex = 10;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(48, 155);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(60, 14);
            this.labelControl11.TabIndex = 132;
            this.labelControl11.Text = "邮政编码：";
            // 
            // is_count
            // 
            this.is_count.EditValue = "0";
            this.is_count.Location = new System.Drawing.Point(501, 112);
            this.is_count.Name = "is_count";
            this.is_count.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("0", "市"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "区县")});
            this.is_count.Size = new System.Drawing.Size(100, 25);
            this.is_count.TabIndex = 8;
            // 
            // u_city
            // 
            this.u_city.Location = new System.Drawing.Point(433, 115);
            this.u_city.Name = "u_city";
            this.u_city.Size = new System.Drawing.Size(50, 20);
            this.u_city.TabIndex = 7;
            // 
            // u_addr
            // 
            this.u_addr.Location = new System.Drawing.Point(733, 115);
            this.u_addr.Name = "u_addr";
            this.u_addr.Size = new System.Drawing.Size(168, 20);
            this.u_addr.TabIndex = 9;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(683, 117);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(36, 14);
            this.labelControl6.TabIndex = 127;
            this.labelControl6.Text = "地址：";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(395, 118);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(24, 14);
            this.labelControl7.TabIndex = 124;
            this.labelControl7.Text = "市：";
            // 
            // u_prov
            // 
            this.u_prov.Location = new System.Drawing.Point(122, 115);
            this.u_prov.Name = "u_prov";
            this.u_prov.Size = new System.Drawing.Size(165, 20);
            this.u_prov.TabIndex = 6;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(83, 117);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(16, 14);
            this.labelControl8.TabIndex = 122;
            this.labelControl8.Text = "省:";
            // 
            // u_ven_type
            // 
            this.u_ven_type.Enabled = false;
            this.u_ven_type.Location = new System.Drawing.Point(433, 78);
            this.u_ven_type.Name = "u_ven_type";
            this.u_ven_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_ven_type.Size = new System.Drawing.Size(168, 20);
            this.u_ven_type.TabIndex = 4;
            // 
            // u_hy
            // 
            this.u_hy.Location = new System.Drawing.Point(733, 75);
            this.u_hy.Name = "u_hy";
            this.u_hy.Size = new System.Drawing.Size(168, 20);
            this.u_hy.TabIndex = 5;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(659, 78);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 14);
            this.labelControl3.TabIndex = 118;
            this.labelControl3.Text = "所属行业：";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(347, 78);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(72, 14);
            this.labelControl4.TabIndex = 114;
            this.labelControl4.Text = "供应商类别：";
            // 
            // u_taxno
            // 
            this.u_taxno.Location = new System.Drawing.Point(122, 75);
            this.u_taxno.Name = "u_taxno";
            this.u_taxno.Size = new System.Drawing.Size(165, 20);
            this.u_taxno.TabIndex = 3;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(24, 78);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(84, 14);
            this.labelControl5.TabIndex = 111;
            this.labelControl5.Text = "纳税人登记号：";
            // 
            // u_sname
            // 
            this.u_sname.Location = new System.Drawing.Point(733, 37);
            this.u_sname.Name = "u_sname";
            this.u_sname.Size = new System.Drawing.Size(168, 20);
            this.u_sname.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(647, 40);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(72, 14);
            this.labelControl2.TabIndex = 109;
            this.labelControl2.Text = "供应商简称：";
            // 
            // u_name
            // 
            this.u_name.Location = new System.Drawing.Point(433, 37);
            this.u_name.Name = "u_name";
            this.u_name.Size = new System.Drawing.Size(168, 20);
            this.u_name.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(347, 40);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(72, 14);
            this.labelControl1.TabIndex = 105;
            this.labelControl1.Text = "供应商名称：";
            // 
            // u_code
            // 
            this.u_code.Enabled = false;
            this.u_code.Location = new System.Drawing.Point(122, 37);
            this.u_code.Name = "u_code";
            this.u_code.Size = new System.Drawing.Size(165, 20);
            this.u_code.TabIndex = 0;
            // 
            // qqq
            // 
            this.qqq.Location = new System.Drawing.Point(36, 40);
            this.qqq.Name = "qqq";
            this.qqq.Size = new System.Drawing.Size(72, 14);
            this.qqq.TabIndex = 102;
            this.qqq.Text = "供应商编码：";
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 633);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.sysDictsUnitinfoVenData_update);
            this.Controls.Add(this.u_lead);
            this.Controls.Add(this.labelControl39);
            this.Controls.Add(this.cb_drug);
            this.Controls.Add(this.cb_imma);
            this.Controls.Add(this.cb_equip);
            this.Controls.Add(this.cb_mate);
            this.Controls.Add(this.business_charter);
            this.Controls.Add(this.labelControl38);
            this.Controls.Add(this.u_sfty);
            this.Controls.Add(this.labelControl37);
            this.Controls.Add(this.u_zycp);
            this.Controls.Add(this.labelControl36);
            this.Controls.Add(this.u_xyhs);
            this.Controls.Add(this.labelControl35);
            this.Controls.Add(this.u_zhfjje);
            this.Controls.Add(this.u_zhfjrq);
            this.Controls.Add(this.u_sypd);
            this.Controls.Add(this.labelControl32);
            this.Controls.Add(this.labelControl33);
            this.Controls.Add(this.labelControl34);
            this.Controls.Add(this.u_zhjysj);
            this.Controls.Add(this.u_zhjyjj);
            this.Controls.Add(this.labelControl29);
            this.Controls.Add(this.labelControl30);
            this.Controls.Add(this.u_yfje);
            this.Controls.Add(this.labelControl31);
            this.Controls.Add(this.u_fgbm);
            this.Controls.Add(this.u_dhfs);
            this.Controls.Add(this.u_cyyyy);
            this.Controls.Add(this.labelControl26);
            this.Controls.Add(this.labelControl27);
            this.Controls.Add(this.labelControl28);
            this.Controls.Add(this.u_email);
            this.Controls.Add(this.u_fktj);
            this.Controls.Add(this.u_dhdj);
            this.Controls.Add(this.labelControl23);
            this.Controls.Add(this.labelControl24);
            this.Controls.Add(this.u_xyqx);
            this.Controls.Add(this.labelControl25);
            this.Controls.Add(this.u_xyed);
            this.Controls.Add(this.labelControl18);
            this.Controls.Add(this.u_xydj);
            this.Controls.Add(this.labelControl21);
            this.Controls.Add(this.u_spell);
            this.Controls.Add(this.labelControl22);
            this.Controls.Add(this.u_rcl);
            this.Controls.Add(this.wwawaw);
            this.Controls.Add(this.u_move);
            this.Controls.Add(this.labelControl19);
            this.Controls.Add(this.u_bbj);
            this.Controls.Add(this.labelControl20);
            this.Controls.Add(this.u_lman);
            this.Controls.Add(this.labelControl15);
            this.Controls.Add(this.labelControl16);
            this.Controls.Add(this.u_fax);
            this.Controls.Add(this.labelControl17);
            this.Controls.Add(this.u_mandate);
            this.Controls.Add(this.u_tel);
            this.Controls.Add(this.labelControl12);
            this.Controls.Add(this.labelControl13);
            this.Controls.Add(this.u_man);
            this.Controls.Add(this.labelControl14);
            this.Controls.Add(this.u_bankname);
            this.Controls.Add(this.u_bankno);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.labelControl10);
            this.Controls.Add(this.u_post);
            this.Controls.Add(this.labelControl11);
            this.Controls.Add(this.is_count);
            this.Controls.Add(this.u_city);
            this.Controls.Add(this.u_addr);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.u_prov);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.u_ven_type);
            this.Controls.Add(this.u_hy);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.u_taxno);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.u_sname);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.u_name);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.u_code);
            this.Controls.Add(this.qqq);
            this.Name = "update";
            this.Text = "修改";
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_lead.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_lead.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_drug.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_imma.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_equip.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_mate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.business_charter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_sfty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_zycp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_xyhs.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_zhfjje.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_zhfjrq.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_zhfjrq.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_sypd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_zhjysj.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_zhjysj.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_zhjyjj.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_yfje.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_fgbm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_dhfs.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_cyyyy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_email.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_fktj.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_dhdj.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_xyqx.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_xyed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_xydj.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_spell.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_rcl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_move.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_bbj.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_lman.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_fax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_mandate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_mandate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_tel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_man.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_bankname.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_bankno.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_post.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_count.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_city.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_addr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_prov.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_ven_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_hy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_taxno.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_sname.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraEditors.SimpleButton sysDictsUnitinfoVenData_update;
        private DevExpress.XtraEditors.DateEdit u_lead;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.CheckEdit cb_drug;
        private DevExpress.XtraEditors.CheckEdit cb_imma;
        private DevExpress.XtraEditors.CheckEdit cb_equip;
        private DevExpress.XtraEditors.CheckEdit cb_mate;
        private DevExpress.XtraEditors.TextEdit business_charter;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.ComboBoxEdit u_sfty;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.TextEdit u_zycp;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.ComboBoxEdit u_xyhs;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.TextEdit u_zhfjje;
        private DevExpress.XtraEditors.DateEdit u_zhfjrq;
        private DevExpress.XtraEditors.TextEdit u_sypd;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.DateEdit u_zhjysj;
        private DevExpress.XtraEditors.TextEdit u_zhjyjj;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.TextEdit u_yfje;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.SearchLookUpEdit u_fgbm;
        private DevExpress.XtraEditors.ComboBoxEdit u_dhfs;
        private DevExpress.XtraEditors.TextEdit u_cyyyy;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.TextEdit u_email;
        private DevExpress.XtraEditors.ComboBoxEdit u_fktj;
        private DevExpress.XtraEditors.TextEdit u_dhdj;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit u_xyqx;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.TextEdit u_xyed;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit u_xydj;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit u_spell;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.TextEdit u_rcl;
        private DevExpress.XtraEditors.LabelControl wwawaw;
        private DevExpress.XtraEditors.TextEdit u_move;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit u_bbj;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit u_lman;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit u_fax;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.DateEdit u_mandate;
        private DevExpress.XtraEditors.TextEdit u_tel;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit u_man;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit u_bankname;
        private DevExpress.XtraEditors.TextEdit u_bankno;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit u_post;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.RadioGroup is_count;
        private DevExpress.XtraEditors.TextEdit u_city;
        private DevExpress.XtraEditors.TextEdit u_addr;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit u_prov;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.ComboBoxEdit u_ven_type;
        private DevExpress.XtraEditors.TextEdit u_hy;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit u_taxno;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit u_sname;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit u_name;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit u_code;
        private DevExpress.XtraEditors.LabelControl qqq;
    }
}