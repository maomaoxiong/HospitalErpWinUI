﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.ven
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        private Validator validator;
        private List<Perm> _perms { get; set; }
        public update()
        {
            InitializeComponent();
        }
        public update(DaoHelper helper, List<Perm> perms, string content_code)
        {

            InitializeComponent();
            _helper = helper;
            _perms = perms;
            validator = new Validator();
            this.StartPosition = FormStartPosition.CenterParent;
            Load_sysLastLevelVenderType_list_stop();//供应商类别下拉框
            Load_vender_type_dict();//是否下拉框
            Load_sysPayCond_list();//付款条件下拉框
            Load_sysTrafMode_list();//到货方式下拉框
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysDictsUnitinfoVenData_update_load";
            paramters.Add("Comp_code", G_User.user.Comp_code);
            paramters.Add("content_code1", content_code);

            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("查询失败！");

            }
            else
            {
                this.u_code.Text = dt.Rows[0]["ven_code"].ToString();
                this.u_name.Text = dt.Rows[0]["ven_name"].ToString();
                this.u_sname.Text = dt.Rows[0]["ven_abb_name"].ToString();
                this.u_taxno.Text = dt.Rows[0]["ven_reg_code"].ToString();
                FormHelper.ComboBoxeditAssignment(u_ven_type, dt.Rows[0]["ven_type_code"].ToString());
                this.u_hy.Text = dt.Rows[0]["ven_trade"].ToString();
                this.u_prov.Text = dt.Rows[0]["prov"].ToString();
                this.u_city.Text = dt.Rows[0]["city"].ToString(); 
                FormHelper.RadioGroupAssignment(is_count, dt.Rows[0]["is_count"].ToString() == "True" ? "1" : "0");//dt.Rows[0]["is_count"].ToString();
                this.u_addr.Text = dt.Rows[0]["ven_adress"].ToString();
                this.u_post.Text = dt.Rows[0]["ven_postcode"].ToString();
                this.u_bankname.Text = dt.Rows[0]["ven_bank"].ToString();
                this.u_bankno.Text = dt.Rows[0]["ven_bank_acount"].ToString();
                this.u_man.Text = dt.Rows[0]["ven_lperson"].ToString();
                this.u_mandate.Text = dt.Rows[0]["Column1"].ToString();
                this.u_tel.Text = dt.Rows[0]["ven_phone"].ToString();
                this.u_fax.Text = dt.Rows[0]["ven_fax"].ToString();
                this.u_email.Text = dt.Rows[0]["ven_email"].ToString();
                this.u_lman.Text = dt.Rows[0]["ven_person"].ToString();
                this.u_bbj.Text = dt.Rows[0]["ven_bp"].ToString();
                this.u_move.Text = dt.Rows[0]["ven_mobile"].ToString();
                this.u_rcl.Text = dt.Rows[0]["ven_dis_rate"].ToString();
                this.u_spell.Text = dt.Rows[0]["spell"].ToString();
                this.u_xydj.Text = dt.Rows[0]["ven_cre_grade"].ToString();
                this.u_xyed.Text = dt.Rows[0]["ven_cre_line"].ToString();
                this.u_xyqx.Text = dt.Rows[0]["ven_cre_date"].ToString();
                FormHelper.ComboBoxeditAssignment(u_fktj, dt.Rows[0]["ven_pay_con"].ToString());
                this.u_dhdj.Text = dt.Rows[0]["ven_dir_address"].ToString();
                FormHelper.ComboBoxeditAssignment(u_dhfs, dt.Rows[0]["ven_dir_code"].ToString());
                FormHelper.SearchLookUpEditAssignment(u_fgbm, dt.Rows[0]["dept_code"].ToString());
                this.u_cyyyy.Text = dt.Rows[0]["ven_pperson"].ToString();
                this.u_yfje.Text = dt.Rows[0]["Column2"].ToString();
                this.u_zhjysj.Text = dt.Rows[0]["Column3"].ToString();
                this.u_zhjyjj.Text = dt.Rows[0]["Column4"].ToString();
                this.u_zhfjrq.Text = dt.Rows[0]["Column5"].ToString();
                this.u_zhfjje.Text = dt.Rows[0]["Column6"].ToString();
                this.u_lead.Text = dt.Rows[0]["Column7"].ToString();
                this.u_sypd.Text = dt.Rows[0]["frequency"].ToString();
                FormHelper.ComboBoxeditAssignment(u_xyhs, dt.Rows[0]["bven_tax"].ToString()); 
                this.u_zycp.Text = dt.Rows[0]["products"].ToString();
                FormHelper.ComboBoxeditAssignment(u_sfty, dt.Rows[0]["is_stop"].ToString()); 
                this.business_charter.Text = dt.Rows[0]["business_charter"].ToString();
                this.cb_mate.CheckState = CheckState.Unchecked; ;
                if (dt.Rows[0]["is_mate"].ToString() != "False")
                {
                    this.cb_mate.CheckState = CheckState.Checked;
                }
                 this.cb_equip.CheckState = CheckState.Unchecked; ;
                if (dt.Rows[0]["is_equip"].ToString() != "False")
                {
                    this.cb_equip.CheckState = CheckState.Checked;
                }
                 this.cb_imma.CheckState = CheckState.Unchecked; ;
                if (dt.Rows[0]["is_imma"].ToString() != "False")
                {
                    this.cb_imma.CheckState = CheckState.Checked;
                }
                 this.cb_drug.CheckState = CheckState.Unchecked; ;
                if (dt.Rows[0]["is_durg"].ToString() != "False")
                {
                    this.cb_drug.CheckState = CheckState.Checked;
                }

            }
        }
               private void Load_sysTrafMode_list()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysTrafMode_list";
            paramters.Add("Comp_code", G_User.user.Comp_code);
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            u_dhfs.Properties.Items.AddRange(objs);
            // u_fktj.SelectedItem = objs[0];
        }
        
        private void Load_sysPayCond_list()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysPayCond_list";
            paramters.Add("Comp_code", G_User.user.Comp_code);
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            u_fktj.Properties.Items.AddRange(objs);
           // u_fktj.SelectedItem = objs[0];
        }
        private void Load_vender_type_dict()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "dict_yes_or_no";
            serviceParamter.Paramters = paramters;
            List<string> objs = new List<string>();
            List<CodeValue> codeValues = _helper.ReadDictForData(serviceParamter);
            foreach (CodeValue codeValue in codeValues)
            {
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            u_xyhs.Properties.Items.AddRange(objs);
            u_xyhs.SelectedItem = objs[0];
            u_sfty.Properties.Items.AddRange(objs);
            u_sfty.SelectedItem = objs[0];

        }
        private void Load_sysLastLevelVenderType_list_stop()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysLastLevelVenderType_list_stop";
            paramters.Add("Comp_code", G_User.user.Comp_code);
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            u_ven_type.Properties.Items.AddRange(objs);
            u_ven_type.SelectedItem = objs[0];
        }
        private void Load_vendor_DictSelectVen_code_Max() {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "vendor_DictSelectVen_code_Max";
            paramters.Add("u_unit", FormHelper.GetValueForComboBoxEdit(u_ven_type));

            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("供应商类别为空请先维护！");
            }
            else
            {
                this.u_code.Text = dt.Rows[0]["Column1"].ToString();
            }
             
        }
        private void Load_vendorDictSelect_code_Max()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "vendorDictSelect_code_Max";

            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("供应商类别为空请先维护！");
            }
            else
            {
                this.u_code.Text = dt.Rows[0]["Column1"].ToString();
            }

        }
        private bool Validate()
        {

            if (string.IsNullOrEmpty(u_code.Text))
            {
                MessageForm.Warning("供应商编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(u_name.Text))
            {
                MessageForm.Warning("供应商名称不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(FormHelper.GetValueForComboBoxEdit(u_ven_type)))
            {
                MessageForm.Warning("供应商类别不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(FormHelper.GetValueForComboBoxEdit(u_xyhs)))
            {
                MessageForm.Warning("单价是否含税不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(FormHelper.GetValueForComboBoxEdit(u_sfty)))
            {
                MessageForm.Warning("是否停用不能为空!");
                return false;
            }

            if (!Validator.IsMobilePhoneNumber(u_move.Text) && !string.IsNullOrEmpty(u_move.Text))
            {
                MessageForm.Warning("手机格式不正确!");
                return false;
            }

            if (!Validator.IsEmail(u_email.Text) && !string.IsNullOrEmpty(u_email.Text))
            {
                MessageForm.Warning("邮箱不正确!");
                return false;
            }

            if (!Validator.IsInteger(u_sypd.Text) && !string.IsNullOrEmpty(u_sypd.Text))
            {
                MessageForm.Warning("使用频度必须是数字!");
                return false;
            }
            if (!string.IsNullOrEmpty(u_rcl.Text))
            {
                if (!Validator.IsNumber(u_rcl.Text))
                {
                    MessageForm.Warning("扣率必须是金额!");
                    return false;
                }
                else if (Convert.ToDouble(u_rcl.Text) > 999999999999 || Convert.ToDouble(u_rcl.Text) < -999999999999)
                {
                    MessageForm.Warning("扣率范围应在-10000000至10000000之间！");
                    return false;
                }

            }


            if (!string.IsNullOrEmpty(u_xyed.Text))
            {
                if (!Validator.IsNumber(u_xyed.Text))
                {
                    MessageForm.Warning("信用额度必须是金额!");
                    return false;
                }
                else if (Convert.ToDouble(u_xyed.Text) > 999999999999 || Convert.ToDouble(u_xyed.Text) < -999999999999)
                {
                    MessageForm.Warning("信用额度范围应在-999999999999至999999999999之间！");
                    return false;
                }
            }


            if (!string.IsNullOrEmpty(u_xyqx.Text))
            {
                if (!Validator.IsNumber(u_xyqx.Text))
                {
                    MessageForm.Warning("信用期限必须是金额!");
                    return false;
                }
                else if (Convert.ToDouble(u_xyqx.Text) > 255 || Convert.ToDouble(u_xyqx.Text) < 0)
                {
                    MessageForm.Warning("信用期限范围应在0至255之间！");
                    return false;
                }
            }


            if (!string.IsNullOrEmpty(u_yfje.Text))
            {
                if (!Validator.IsNumber(u_yfje.Text))
                {
                    MessageForm.Warning("应付余额必须是金额!");
                    return false;
                }
                else if (Convert.ToDouble(u_yfje.Text) > 999999999999 || Convert.ToDouble(u_yfje.Text) < -999999999999)
                {
                    MessageForm.Warning("应付余额范围应在-999999999999至999999999999之间！");
                    return false;
                }
            }

            if (!string.IsNullOrEmpty(u_zhjyjj.Text))
            {
                if (!Validator.IsNumber(u_zhjyjj.Text))
                {
                    MessageForm.Warning("最后交易金额必须是金额!");
                    return false;
                }
                else if (Convert.ToDouble(u_zhjyjj.Text) > 999999999999 || Convert.ToDouble(u_zhjyjj.Text) < -999999999999)
                {
                    MessageForm.Warning("最后交易金额范围应在-999999999999至999999999999之间!");
                    return false;
                }
            }

            if (!string.IsNullOrEmpty(u_zhfjje.Text))
            {
                if (!Validator.IsNumber(u_zhfjje.Text))
                {
                    MessageForm.Warning("最后付款金额必须是金额!");
                    return false;
                }
                else if (Convert.ToDouble(u_zhfjje.Text) > 999999999999 || Convert.ToDouble(u_zhfjje.Text) < -999999999999)
                {
                    MessageForm.Warning("最后付款金额范围应在-999999999999至999999999999之间!");
                    return false;
                }
            }

            return true;
        }

        private void sysDictsUnitinfoVenData_update_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                u_update();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
        private bool u_update()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "sysDictsUnitinfoVenData_update";
            Dictionary<string, string> paramters = new Dictionary<string, string>();

            paramters.Add("u_code", u_code.Text);
            paramters.Add("u_name", u_name.Text);
            paramters.Add("Comp_code", G_User.user.Comp_code);
            paramters.Add("u_sname", u_sname.Text);
            paramters.Add("u_taxno", u_taxno.Text);
            paramters.Add("u_ven_type", FormHelper.GetValueForComboBoxEdit(u_ven_type).Trim());
            paramters.Add("u_hy", u_hy.Text);
            paramters.Add("u_prov", u_prov.Text);
            paramters.Add("u_city", u_city.Text);

            paramters.Add("is_count", is_count.EditValue.ToString());
            paramters.Add("u_addr", u_addr.Text);
            paramters.Add("u_post", u_post.Text);
            paramters.Add("u_bankname", u_bankname.Text);
            paramters.Add("u_bankno", u_bankno.Text);

            paramters.Add("u_man", u_man.Text);
            paramters.Add("u_mandate", u_mandate.Text);
            paramters.Add("u_tel", u_tel.Text);
            paramters.Add("u_fax", u_fax.Text);
            paramters.Add("u_email", u_email.Text);
            paramters.Add("u_lman", u_lman.Text);
            paramters.Add("u_bbj", u_bbj.Text);
            paramters.Add("u_move", u_move.Text);
            paramters.Add("u_rcl", u_rcl.Text);
            paramters.Add("u_spell", u_spell.Text);
            paramters.Add("u_xydj", u_xydj.Text);
            paramters.Add("u_xyed", u_xyed.Text);
            paramters.Add("u_xyqx", u_xyqx.Text);
            paramters.Add("u_fktj", FormHelper.GetValueForComboBoxEdit(u_fktj).Trim());
            paramters.Add("u_dhdj", u_dhdj.Text);

            paramters.Add("u_dhfs", FormHelper.GetValueForComboBoxEdit(u_dhfs).Trim());
            paramters.Add("u_fgbm", FormHelper.GetValueForSearchLookUpEdit(u_fgbm).Trim());
            paramters.Add("u_cyyyy", u_cyyyy.Text);
            paramters.Add("u_yfje", u_yfje.Text);
            paramters.Add("u_zhjysj", u_zhjysj.Text);
            paramters.Add("u_zhjyjj", u_zhjyjj.Text);
            paramters.Add("u_zhfjrq", u_zhfjrq.Text);


            paramters.Add("u_zhfjje", u_zhfjje.Text);
            paramters.Add("u_lead", u_lead.Text);// u_lead.DateTime.ToString("yyyy-MM-dd"));
            paramters.Add("u_sypd", u_sypd.Text);
            paramters.Add("u_xyhs", FormHelper.GetValueForComboBoxEdit(u_xyhs).Trim());
            paramters.Add("u_zycp", u_zycp.Text);
            paramters.Add("u_sfty", FormHelper.GetValueForComboBoxEdit(u_sfty).Trim());
            paramters.Add("business_charter", business_charter.Text);
            paramters.Add("cb_mate", FormHelper.GetValueForCheckEdit(cb_mate).Trim());
            paramters.Add("cb_equip", FormHelper.GetValueForCheckEdit(cb_equip).Trim());
            paramters.Add("cb_imma", FormHelper.GetValueForCheckEdit(cb_imma).Trim());
            paramters.Add("cb_drug", FormHelper.GetValueForCheckEdit(cb_drug).Trim());
            serviceParamter.Paramters = paramters;

            return _helper.WirteSql(serviceParamter);

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
