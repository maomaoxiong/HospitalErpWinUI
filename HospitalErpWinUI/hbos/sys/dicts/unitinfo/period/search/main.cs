﻿using DevExpress.XtraBars.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.period.search
{
    public partial class main : RibbonForm
    {
        public main()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            // this._helper = new DaoHelper();
            //  this.gridView1.OptionsView.ShowGroupPanel = false;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "期间查询";

        }
    }
}
