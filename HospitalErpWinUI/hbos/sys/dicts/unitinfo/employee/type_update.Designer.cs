﻿namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.employee
{
    partial class type_update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.u_desc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.sysDictsUnitinfoEmployeeDuty_update = new DevExpress.XtraEditors.SimpleButton();
            this.u_stop = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.u_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.u_code = new DevExpress.XtraEditors.TextEdit();
            this.qqq = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.u_desc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_stop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // u_desc
            // 
            this.u_desc.Location = new System.Drawing.Point(91, 109);
            this.u_desc.Name = "u_desc";
            this.u_desc.Size = new System.Drawing.Size(168, 20);
            this.u_desc.TabIndex = 3;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(25, 112);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 14);
            this.labelControl3.TabIndex = 116;
            this.labelControl3.Text = "职工描述：";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(156, 207);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 6;
            this.simpleButton2.Text = "关闭";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // sysDictsUnitinfoEmployeeDuty_update
            // 
            this.sysDictsUnitinfoEmployeeDuty_update.Location = new System.Drawing.Point(39, 207);
            this.sysDictsUnitinfoEmployeeDuty_update.Name = "sysDictsUnitinfoEmployeeDuty_update";
            this.sysDictsUnitinfoEmployeeDuty_update.Size = new System.Drawing.Size(75, 23);
            this.sysDictsUnitinfoEmployeeDuty_update.TabIndex = 5;
            this.sysDictsUnitinfoEmployeeDuty_update.Text = "保存";
            this.sysDictsUnitinfoEmployeeDuty_update.Click += new System.EventHandler(this.sysDictsUnitinfoEmployeeDuty_update_Click);
            // 
            // u_stop
            // 
            this.u_stop.Location = new System.Drawing.Point(91, 149);
            this.u_stop.Name = "u_stop";
            this.u_stop.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_stop.Size = new System.Drawing.Size(168, 20);
            this.u_stop.TabIndex = 4;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(25, 152);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 112;
            this.labelControl2.Text = "是否停用：";
            // 
            // u_name
            // 
            this.u_name.Location = new System.Drawing.Point(91, 74);
            this.u_name.Name = "u_name";
            this.u_name.Size = new System.Drawing.Size(168, 20);
            this.u_name.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(25, 77);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 111;
            this.labelControl1.Text = "职工名称：";
            // 
            // u_code
            // 
            this.u_code.Location = new System.Drawing.Point(91, 32);
            this.u_code.Name = "u_code";
            this.u_code.Size = new System.Drawing.Size(168, 20);
            this.u_code.TabIndex = 1;
            // 
            // qqq
            // 
            this.qqq.Location = new System.Drawing.Point(25, 35);
            this.qqq.Name = "qqq";
            this.qqq.Size = new System.Drawing.Size(60, 14);
            this.qqq.TabIndex = 110;
            this.qqq.Text = "职工编码：";
            // 
            // type_update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.u_desc);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.sysDictsUnitinfoEmployeeDuty_update);
            this.Controls.Add(this.u_stop);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.u_name);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.u_code);
            this.Controls.Add(this.qqq);
            this.Name = "type_update";
            this.Text = "修改";
            ((System.ComponentModel.ISupportInitialize)(this.u_desc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_stop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit u_desc;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton sysDictsUnitinfoEmployeeDuty_update;
        private DevExpress.XtraEditors.ComboBoxEdit u_stop;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit u_name;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit u_code;
        private DevExpress.XtraEditors.LabelControl qqq;
    }
}