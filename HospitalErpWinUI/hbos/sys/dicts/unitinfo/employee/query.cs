﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.employee
{
    public partial class query : RibbonForm
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        //  public List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public query()  
        {
            InitializeComponent();
            this.gridView1.OptionsView.ShowGroupPanel = false;
            _helper = new DaoHelper();
        }
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Enter)　　// 按下的是回车键
            {
                foreach (Control c in this.Controls)
                {
                    if (c is System.Windows.Forms.TextBox)　　// 当前控件是文本框控件
                    {
                        keyData = Keys.Tab;
                    }
                }
                keyData = Keys.Tab;
            }
            return base.ProcessDialogKey(keyData);
        }
        private void labelControl5_Click(object sender, EventArgs e)
        {

        }

        private void sysDictsUnitinfoEmployeeQuery_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            DataTable dt = _helper.ReadSqlForDataTable("sysDictsUnitinfoEmployeeQuery_select",
                new[] { G_User.user.Comp_code
                ,u_unit.Text
                ,u_copy.Text
                ,FormHelper.GetValueForComboBoxEdit(u_sex)
                ,u_kind.Text
                ,u_kind1.Text
                ,FormHelper.GetValueForSearchLookUpEdit(u_ven_type)
                ,FormHelper.GetValueForComboBoxEdit(u_type) 
                ,FormHelper.GetValueForComboBoxEdit(u_sfty) 
                ,u_kind2.Text
                ,u_kind3.Text
                ,FormHelper.GetValueForComboBoxEdit(areacode_nat)
                ,FormHelper.GetValueForComboBoxEdit(zhicheng)
                ,FormHelper.GetValueForComboBoxEdit(zhiwu)
                ,textEdit1.Text
                ,textEdit2.Text
                ,textEdit3.Text
                ,FormHelper.GetValueForComboBoxEdit(kaoqinleibie)
                });
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;
        }
    }
}
