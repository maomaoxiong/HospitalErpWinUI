﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.employee
{
    public partial class type_insert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        public type_insert()
        {
            InitializeComponent();
        }
        public type_insert(Dao.DaoHelper helper, List<Perm> perms)
        {
            InitializeComponent();
            this._helper = helper;
            this._perms = perms;
            Load_dict_yes_or_no();//是否下拉框
            this.StartPosition = FormStartPosition.CenterParent;
        }
        private void Load_dict_yes_or_no()
        {
            List<string> objs = DataDictApi.dict_yes_or_no();
            u_stop.Properties.Items.AddRange(objs);
            u_stop.SelectedItem = objs[0];
            
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Enter)　　// 按下的是回车键
            {
                foreach (Control c in this.Controls)
                {
                    if (c is System.Windows.Forms.TextBox)　　// 当前控件是文本框控件
                    {
                        keyData = Keys.Tab;
                    }
                }
                keyData = Keys.Tab;
            }
            return base.ProcessDialogKey(keyData);
        }
    }
}
