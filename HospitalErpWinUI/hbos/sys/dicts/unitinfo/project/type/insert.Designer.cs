﻿namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.project.type
{
    partial class insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CloseBut = new DevExpress.XtraEditors.SimpleButton();
            this.AddBut = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.is_stop = new DevExpress.XtraEditors.CheckEdit();
            this.typeName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.typeCode = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.is_stop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.typeName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.typeCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // CloseBut
            // 
            this.CloseBut.Location = new System.Drawing.Point(243, 178);
            this.CloseBut.Name = "CloseBut";
            this.CloseBut.Size = new System.Drawing.Size(80, 25);
            this.CloseBut.TabIndex = 13;
            this.CloseBut.Text = "关闭";
            this.CloseBut.Click += new System.EventHandler(this.CloseBut_Click);
            // 
            // AddBut
            // 
            this.AddBut.Location = new System.Drawing.Point(104, 178);
            this.AddBut.Name = "AddBut";
            this.AddBut.Size = new System.Drawing.Size(80, 25);
            this.AddBut.TabIndex = 14;
            this.AddBut.Text = "新增";
            this.AddBut.Click += new System.EventHandler(this.AddBut_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(51, 131);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 18);
            this.labelControl4.TabIndex = 12;
            this.labelControl4.Text = "是否停用";
            this.labelControl4.Click += new System.EventHandler(this.labelControl4_Click);
            // 
            // is_stop
            // 
            this.is_stop.Location = new System.Drawing.Point(161, 130);
            this.is_stop.Name = "is_stop";
            this.is_stop.Properties.Caption = "";
            this.is_stop.Size = new System.Drawing.Size(75, 19);
            this.is_stop.TabIndex = 11;
            this.is_stop.CheckedChanged += new System.EventHandler(this.is_stop_CheckedChanged);
            // 
            // typeName
            // 
            this.typeName.Location = new System.Drawing.Point(161, 84);
            this.typeName.Name = "typeName";
            this.typeName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.typeName.Properties.Appearance.Options.UseBackColor = true;
            this.typeName.Size = new System.Drawing.Size(200, 24);
            this.typeName.TabIndex = 9;
            this.typeName.EditValueChanged += new System.EventHandler(this.typeName_EditValueChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(51, 87);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 18);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "类型名称";
            this.labelControl2.Click += new System.EventHandler(this.labelControl2_Click);
            // 
            // typeCode
            // 
            this.typeCode.Location = new System.Drawing.Point(161, 38);
            this.typeCode.Name = "typeCode";
            this.typeCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.typeCode.Properties.Appearance.Options.UseBackColor = true;
            this.typeCode.Size = new System.Drawing.Size(200, 24);
            this.typeCode.TabIndex = 10;
            this.typeCode.EditValueChanged += new System.EventHandler(this.typeCode_EditValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(51, 41);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 18);
            this.labelControl1.TabIndex = 7;
            this.labelControl1.Text = "类型编码";
            this.labelControl1.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 252);
            this.Controls.Add(this.CloseBut);
            this.Controls.Add(this.AddBut);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.is_stop);
            this.Controls.Add(this.typeName);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.typeCode);
            this.Controls.Add(this.labelControl1);
            this.Name = "insert";
            this.Text = "新增";
            this.Load += new System.EventHandler(this.insert_Load);
            ((System.ComponentModel.ISupportInitialize)(this.is_stop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.typeName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.typeCode.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton CloseBut;
        private DevExpress.XtraEditors.SimpleButton AddBut;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.CheckEdit is_stop;
        private DevExpress.XtraEditors.TextEdit typeName;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit typeCode;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}