﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.project.type
{
    public partial class insert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }

        public insert()
        {
            InitializeComponent();
            Init();
        }

        public insert(DaoHelper helper, List<Perm> perms)
        {
            _helper = helper;
            _perms = perms;

            InitializeComponent();
            Init();
        }

        #region 其他
        private void Init()
        {
            this.StartPosition = FormStartPosition.CenterParent;
            this.typeCode.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.typeCode.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.typeCode.Properties.Mask.EditMask = "[0-9]*";
         //   this.typeCode .Properties.MaxLength = 20;
            this.typeName.Properties.MaxLength = 40;
            
        }

        private bool Validate()
        {

            if (string.IsNullOrEmpty(typeCode.Text))
            {
                MessageForm.Warning("类型编码不能为空!");
                return false;
            }
            if (Convert .ToInt16 (typeCode.Text) <0)
            {
                MessageForm.Warning("类型编码不能小于0!");
                return false;
            }
            if (Convert.ToInt16(typeCode.Text) >255)
            {
                MessageForm.Warning("类型编码不能大于255!");
                return false;
            }
            if (string.IsNullOrEmpty(typeName.Text))
            {
                MessageForm.Warning("类型名称不能为空!");
                return false;
            }


            return true;
        }
        #endregion

        #region 数据访问
        private bool Insert()
        {
            ServiceParamter serviceParamter = new ServiceParamter();

            serviceParamter.ServiceId = "sysDictsUnitinfoProjectType_insert";
            Dictionary<string, string> dict = new Dictionary<string, string>();
            StringBuilder str = new StringBuilder();
           
            string  flag =string.Empty;
            if (Convert.ToBoolean(is_stop.EditValue))
                flag = "1";
            else
                flag = "0";
            dict.Add("type_id", typeCode.Text);
            dict.Add("type_name", typeName.Text);
            dict.Add("is_sys","0");
            dict.Add("is_stop", flag);
            serviceParamter.Paramters = dict;

            return _helper.WirteSql(serviceParamter);

        }
        #endregion

        private void insert_Load(object sender, EventArgs e)
        {

        }

        private void CloseBut_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void AddBut_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Insert();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }

        private void labelControl4_Click(object sender, EventArgs e)
        {

        }

        private void is_stop_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void typeName_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void labelControl2_Click(object sender, EventArgs e)
        {

        }

        private void typeCode_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void labelControl1_Click(object sender, EventArgs e)
        {

        }

    }
}
