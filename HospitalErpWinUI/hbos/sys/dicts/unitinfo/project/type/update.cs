﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.project.type
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        private string _code;
        private List<Perm> _perms { get; set; }

        public update()
        {
            InitializeComponent();
            Init();
        }

        public update(DaoHelper helper, List<Perm> perms, string code)
        {
            _helper = helper;
            _perms = perms;
            _code = code;
            InitializeComponent();
            Init();
        }
        #region 其他
        private void Init()
        {
            this.StartPosition = FormStartPosition.CenterParent;
            this.typeCode.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.typeCode.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.typeCode.Properties.Mask.EditMask = "[0-9]*";
            //   this.typeCode .Properties.MaxLength = 20;
            this.typeName.Properties.MaxLength = 40;

        }

        private bool Validate()
        {

            if (string.IsNullOrEmpty(typeCode.Text))
            {
                MessageForm.Warning("类型编码不能为空!");
                return false;
            }
            if (Convert.ToInt16(typeCode.Text) < 0)
            {
                MessageForm.Warning("类型编码不能小于0!");
                return false;
            }
            if (Convert.ToInt16(typeCode.Text) > 255)
            {
                MessageForm.Warning("类型编码不能大于255!");
                return false;
            }
            if (string.IsNullOrEmpty(typeName.Text))
            {
                MessageForm.Warning("类型名称不能为空!");
                return false;
            }


            return true;
        }
        #endregion

        #region 数据访问
        private void LoadDate()
        {

            string sqlstr = "select type_id,type_name, is_stop  FROM sys_proj_type where type_id='?'";
            sqlstr = sqlstr.Replace("?", _code);
            DataTable dt = _helper.ExecReadSql(sqlstr);
            if (dt != null && dt.Rows.Count != 0)
            {
                this.typeCode.Text = dt.Rows[0]["type_id"].ToString();
                this.typeName.Text = dt.Rows[0]["type_name"].ToString();
                this.is_stop.Checked = dt.Rows[0]["is_stop"].ToString().ToLower().Equals("true");
            }

        }
        private bool Update()
        {
            ServiceParamter serviceParamter = new ServiceParamter();

            serviceParamter.ServiceId = "sysDictsUnitinfoProjectType_update";
            Dictionary<string, string> dict = new Dictionary<string, string>();
            StringBuilder str = new StringBuilder();

            string flag = string.Empty;
            if (Convert.ToBoolean(is_stop.EditValue))
                flag = "1";
            else
                flag = "0";
            dict.Add("type_id", typeCode.Text);
            dict.Add("type_name", typeName.Text);
            dict.Add("is_sys", "0");
            dict.Add("is_stop", flag);
            serviceParamter.Paramters = dict;

            return _helper.WirteSql(serviceParamter);

        }
        #endregion

        private void CloseBut_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void update_Load(object sender, EventArgs e)
        {
            LoadDate();
        }

        private void updateBut_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Update();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
    }
}
