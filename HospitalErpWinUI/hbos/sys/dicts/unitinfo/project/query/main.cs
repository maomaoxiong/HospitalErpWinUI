﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.project.query
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;

        public List<Perm> _perms { get; set; }


        //  public List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public main()   
        {
            InitializeComponent();
            _helper = new DaoHelper();
            
            Load_budg_dict_budg_pro_level();//项目级别
             Load_acct_book_dept_py_new();//填报部门-负责部门
            Load_acct_budg_pro_item_man_list_new();//项目负责人
            Load_sysProjType_list();//项目性质
            Load_sysProjUsage_list();//项目用途
           // this.WindowState == FormWindowState.Maximized;
        }
        //项目性质
        private void Load_sysProjUsage_list()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysProjUsage_list";
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            u_prouse.Properties.Items.AddRange(objs);
           // u_prouse.SelectedItem = objs[0];
        }

        //项目级别
        //private void Load_budg_dict_budg_pro_level()
        //{
        //    //sysNextLevelDba_select
        //    ServiceParamter serviceParamter = new ServiceParamter();
        //    Dictionary<string, string> paramters = new Dictionary<string, string>();
        //    serviceParamter.ServiceId = "budg_dict_budg_pro_level";
        //    paramters.Add("comp_code", G_User.user.Acct_year);
        //    serviceParamter.Paramters = paramters;
        //    List<string> objs = new List<string>();
        //    List<CodeValue> codeValues = _helper.ReadDictForData(serviceParamter);
        //    foreach (CodeValue codeValue in codeValues)
        //    {
        //        objs.Add(codeValue.Code + " " + codeValue.Value);
        //    }
        //    level_code.Properties.Items.AddRange(objs);
        
            
        //}
        private void Load_budg_dict_budg_pro_level()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "budg_dict_budg_pro_level";
            paramters.Add("Acct_year", G_User.user.Acct_year);
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            level_code.Properties.Items.AddRange(objs);


        }
        //项目性质
        private void Load_sysProjType_list()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysProjType_list";
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            u_type.Properties.Items.AddRange(objs);
        }
        
        //填报部门-负责部门
        private void Load_acct_book_dept_py_new()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_book_dept_py_new";
            paramters.Add("code", G_User.user.Comp_code);
            paramters.Add("key", "");
            serviceParamter.Paramters = paramters;

            List<CodeValue> objs = new List<CodeValue>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue);
            }
            //    emp_code.Properties.Items.AddRange(emps);
            u_dept.Properties.ValueMember = "Code";
            u_dept.Properties.DisplayMember = "Value";
            u_dept.Properties.DataSource = objs;
            response_dept.Properties.ValueMember = "Code";
            response_dept.Properties.DisplayMember = "Value";
            response_dept.Properties.DataSource = objs;
            

        }
        //项目负责人
        private void Load_acct_budg_pro_item_man_list_new()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "acct_budg_pro_item_man_list_new";
            paramters.Add("code", G_User.user.Comp_code);
            paramters.Add("key", "");
            serviceParamter.Paramters = paramters;

            List<CodeValue> objs = new List<CodeValue>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue);
            }
            //    emp_code.Properties.Items.AddRange(emps);
            u_fman.Properties.ValueMember = "Code";
            u_fman.Properties.DisplayMember = "Value";
            u_fman.Properties.DataSource = objs;

        }
        private void sysDictsUnitinfoProjectQuery_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysDictsUnitinfoProjectQuery_select";
            //，，，，，，，，
            paramters.Add("proj_code", proj_code.Text.ToString());
            paramters.Add("proj_name", proj_name.Text.ToString());
            paramters.Add("level_code", FormHelper.GetValueForComboBoxEdit(level_code));
            paramters.Add("u_dept", FormHelper.GetValueForSearchLookUpEdit(u_dept));
            paramters.Add("u_fman", FormHelper.GetValueForSearchLookUpEdit(u_fman));
            paramters.Add("u_type", FormHelper.GetValueForComboBoxEdit(u_type));
            paramters.Add("response_dept", FormHelper.GetValueForSearchLookUpEdit(response_dept));
            paramters.Add("u_prouse", FormHelper.GetValueForComboBoxEdit(u_prouse));
            
            paramters.Add("u_date", u_date.Text );
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;
        }
    }
}
