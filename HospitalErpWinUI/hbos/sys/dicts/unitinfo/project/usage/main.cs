﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraGrid;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.project.usage
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        private DataTable _usage;
        public main()
        {
            _helper = new DaoHelper();
            InitializeComponent();
            _usage = new DataTable();
            gridView1.OptionsSelection.MultiSelect = true;
            gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.RowSelect;
            gridView1.OptionsView.EnableAppearanceOddRow = true;


        }
        #region 访问数据
        private bool Delete(string code)
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysDictsUnitinfoProjectUsage_delete";
            paramters.Add("type_id", code);
            paramters.Add("type_name", null);
          
            paramters.Add("is_stop", null);
            serviceParamter.Paramters = paramters;
            bool result = false;
            try
            {
                _helper.WirteSql(serviceParamter);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }
        private void Query()
        {
            string sqlstr = " select use_code,use_name FROM sys_proj_usage where '?'='' or use_code='?'";
            sqlstr = sqlstr.Replace("?", usageCoode.Text);
            DataTable dt = _helper.ExecReadSql(sqlstr);

            _usage = dt;
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            gridControl1.DataSource = _usage;
        }


        #endregion

        private void sysDictsUnitinfoProjectUsage_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message);
            }
        }

        private void deleteBut_Click(object sender, EventArgs e)
        {
            bool value = false;
            string strSelected = string.Empty;

            try
            {
                //   string param = "<root><r currency_code=/""++"/"></r></root>";
                List<string> objs = new List<string>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    value = Convert.ToBoolean(gridView1.GetDataRow(i)["checked"]);
                    if (value)
                    {
                        string code = gridView1.GetDataRow(i)["use_code"].ToString();
                        objs.Add(code);

                    }
                }
                if (objs.Count == 0)
                {
                    MessageForm.Show("请选择要删除的数据!");
                    return;
                }
                if (MessageForm.ShowMsg("确认是否删除?", MsgType.YesNo) == DialogResult.Yes)
                {
                    foreach (string obj in objs)
                    {
                        // string param = "<root><r employeetype_code=\"" + obj + "\"></r></root>";
                        if (!Delete(obj))
                        {
                            return;
                        }
                    }
                    //       MessageForm.Show("操作成功!");
                    Query();
                }

            }

            catch (Exception ex)
            {
                MessageForm.Warning("此记录在其他地方已经引用，不能删除!", "警告");
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, DevExpress.XtraEditors.Controls.OpenLinkEventArgs e)
        {
            if (gridView1.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                int index = gridView1.GetDataSourceRowIndex(gridView1.FocusedRowHandle);
                string url = _usage.Rows[index]["use_code"].ToString();
                update updateFrm = new update(this._helper, this._perms, url);

                if (updateFrm.ShowDialog() == DialogResult.OK)
                {
                    Query();
                }
            }
        }

        private void addBut_Click(object sender, EventArgs e)
        {
            insert insertFrm = new insert(this._helper, this._perms);

            if (insertFrm.ShowDialog() == DialogResult.OK)
            {
                Query();
            }
        }

    }
}
