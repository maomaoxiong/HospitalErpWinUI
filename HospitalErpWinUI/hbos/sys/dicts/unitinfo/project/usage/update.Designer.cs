﻿namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.project.usage
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.closeBut = new DevExpress.XtraEditors.SimpleButton();
            this.updateBut = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.u_stop = new DevExpress.XtraEditors.CheckEdit();
            this.u_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.u_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.u_stop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // closeBut
            // 
            this.closeBut.Location = new System.Drawing.Point(243, 195);
            this.closeBut.Name = "closeBut";
            this.closeBut.Size = new System.Drawing.Size(80, 25);
            this.closeBut.TabIndex = 29;
            this.closeBut.Text = "关闭";
            this.closeBut.Click += new System.EventHandler(this.closeBut_Click);
            // 
            // updateBut
            // 
            this.updateBut.Location = new System.Drawing.Point(104, 195);
            this.updateBut.Name = "updateBut";
            this.updateBut.Size = new System.Drawing.Size(80, 25);
            this.updateBut.TabIndex = 30;
            this.updateBut.Text = "修改";
            this.updateBut.Click += new System.EventHandler(this.updateBut_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(51, 148);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 18);
            this.labelControl4.TabIndex = 28;
            this.labelControl4.Text = "是否停用";
            // 
            // u_stop
            // 
            this.u_stop.Location = new System.Drawing.Point(161, 147);
            this.u_stop.Name = "u_stop";
            this.u_stop.Properties.Caption = "";
            this.u_stop.Size = new System.Drawing.Size(75, 19);
            this.u_stop.TabIndex = 27;
            // 
            // u_name
            // 
            this.u_name.Location = new System.Drawing.Point(161, 101);
            this.u_name.Name = "u_name";
            this.u_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.u_name.Properties.Appearance.Options.UseBackColor = true;
            this.u_name.Size = new System.Drawing.Size(200, 24);
            this.u_name.TabIndex = 25;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(51, 104);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 18);
            this.labelControl2.TabIndex = 23;
            this.labelControl2.Text = "用途名称";
            // 
            // u_code
            // 
            this.u_code.Enabled = false;
            this.u_code.Location = new System.Drawing.Point(161, 55);
            this.u_code.Name = "u_code";
            this.u_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.u_code.Properties.Appearance.Options.UseBackColor = true;
            this.u_code.Size = new System.Drawing.Size(200, 24);
            this.u_code.TabIndex = 26;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(51, 58);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 18);
            this.labelControl1.TabIndex = 24;
            this.labelControl1.Text = "用途编码";
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 275);
            this.Controls.Add(this.closeBut);
            this.Controls.Add(this.updateBut);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.u_stop);
            this.Controls.Add(this.u_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.u_code);
            this.Controls.Add(this.labelControl1);
            this.Name = "update";
            this.Text = "修改";
            this.Load += new System.EventHandler(this.update_Load);
            ((System.ComponentModel.ISupportInitialize)(this.u_stop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton closeBut;
        private DevExpress.XtraEditors.SimpleButton updateBut;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.CheckEdit u_stop;
        private DevExpress.XtraEditors.TextEdit u_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit u_code;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}