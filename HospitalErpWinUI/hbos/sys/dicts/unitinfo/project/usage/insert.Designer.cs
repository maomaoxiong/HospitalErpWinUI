﻿namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.project.usage
{
    partial class insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.closeBut = new DevExpress.XtraEditors.SimpleButton();
            this.addBut = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.u_stop = new DevExpress.XtraEditors.CheckEdit();
            this.u_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.u_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.u_stop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // closeBut
            // 
            this.closeBut.Location = new System.Drawing.Point(240, 172);
            this.closeBut.Name = "closeBut";
            this.closeBut.Size = new System.Drawing.Size(80, 25);
            this.closeBut.TabIndex = 21;
            this.closeBut.Text = "关闭";
            this.closeBut.Click += new System.EventHandler(this.closeBut_Click);
            // 
            // addBut
            // 
            this.addBut.Location = new System.Drawing.Point(101, 172);
            this.addBut.Name = "addBut";
            this.addBut.Size = new System.Drawing.Size(80, 25);
            this.addBut.TabIndex = 22;
            this.addBut.Text = "新增";
            this.addBut.Click += new System.EventHandler(this.addBut_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(48, 125);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 18);
            this.labelControl4.TabIndex = 20;
            this.labelControl4.Text = "是否停用";
           
            // 
            // u_stop
            // 
            this.u_stop.Location = new System.Drawing.Point(158, 124);
            this.u_stop.Name = "u_stop";
            this.u_stop.Properties.Caption = "";
            this.u_stop.Size = new System.Drawing.Size(75, 19);
            this.u_stop.TabIndex = 19;
          
            // 
            // u_name
            // 
            this.u_name.Location = new System.Drawing.Point(158, 78);
            this.u_name.Name = "u_name";
            this.u_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.u_name.Properties.Appearance.Options.UseBackColor = true;
            this.u_name.Size = new System.Drawing.Size(200, 24);
            this.u_name.TabIndex = 17;
        
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(48, 81);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 18);
            this.labelControl2.TabIndex = 15;
            this.labelControl2.Text = "用途名称";
       
            // 
            // u_code
            // 
            this.u_code.Location = new System.Drawing.Point(158, 32);
            this.u_code.Name = "u_code";
            this.u_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.u_code.Properties.Appearance.Options.UseBackColor = true;
            this.u_code.Size = new System.Drawing.Size(200, 24);
            this.u_code.TabIndex = 18;
         
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(48, 35);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 18);
            this.labelControl1.TabIndex = 16;
            this.labelControl1.Text = "用途编码";
          
            // 
            // insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 245);
            this.Controls.Add(this.closeBut);
            this.Controls.Add(this.addBut);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.u_stop);
            this.Controls.Add(this.u_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.u_code);
            this.Controls.Add(this.labelControl1);
            this.Name = "insert";
            this.Text = "新增";
            ((System.ComponentModel.ISupportInitialize)(this.u_stop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton closeBut;
        private DevExpress.XtraEditors.SimpleButton addBut;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.CheckEdit u_stop;
        private DevExpress.XtraEditors.TextEdit u_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit u_code;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}