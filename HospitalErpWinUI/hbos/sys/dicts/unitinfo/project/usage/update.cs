﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.project.usage
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        private string _code;
        private List<Perm> _perms { get; set; }

        public update()
        {
            InitializeComponent();
            Init();
        }

        public update(DaoHelper helper, List<Perm> perms, string code)
        {
            _helper = helper;
            _perms = perms;
            _code = code;
            InitializeComponent();
            Init();
        }
        #region 其他
        private void Init()
        {
            this.StartPosition = FormStartPosition.CenterParent;


            this.u_code.Properties.MaxLength = 20;
            this.u_name.Properties.MaxLength = 40;

        }

        private bool Validate()
        {

            if (string.IsNullOrEmpty(u_code.Text))
            {
                MessageForm.Warning("用途编码不能为空!");
                return false;
            }

            if (string.IsNullOrEmpty(u_name.Text))
            {
                MessageForm.Warning("用途名称不能为空!");
                return false;
            }


            return true;
        }
        #endregion

        #region 数据访问
        private void LoadDate()
        {

            string sqlstr = " select use_code,use_name ,is_stop FROM sys_proj_usage where  use_code='?'";
            sqlstr = sqlstr.Replace("?", _code);
            DataTable dt = _helper.ExecReadSql(sqlstr);
            if (dt != null && dt.Rows.Count != 0)
            {
                this.u_code.Text = dt.Rows[0]["use_code"].ToString();
                this.u_name.Text = dt.Rows[0]["use_name"].ToString();
                this.u_stop.Checked = dt.Rows[0]["is_stop"].ToString().ToLower().Equals("true");
            }

        }

        private bool Update()
        {
            ServiceParamter serviceParamter = new ServiceParamter();

            serviceParamter.ServiceId = "sysDictsUnitinfoProjectUsage_update";
            Dictionary<string, string> dict = new Dictionary<string, string>();
            StringBuilder str = new StringBuilder();

            string flag = string.Empty;
            if (Convert.ToBoolean(u_stop.EditValue))
                flag = "1";
            else
                flag = "0";
            dict.Add("type_id", u_code.Text);
            dict.Add("type_name", u_name.Text);

            dict.Add("is_stop", flag);
            serviceParamter.Paramters = dict;

            return _helper.WirteSql(serviceParamter);

        }
        #endregion
        private void update_Load(object sender, EventArgs e)
        {
            LoadDate();
        }

        private void closeBut_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void updateBut_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Update();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
    }
}
