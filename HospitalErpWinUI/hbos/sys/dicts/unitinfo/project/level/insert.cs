﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.project.level
{
    public partial class insert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }

        public insert()
        {
            InitializeComponent();
            Init();
        }

        public insert(DaoHelper helper, List<Perm> perms)
        {
            _helper = helper;
            _perms = perms;

            InitializeComponent();
            Init();
        }

        #region 其他
        private void Init()
        {
            this.StartPosition = FormStartPosition.CenterParent;
         

            this.u_code.Properties.MaxLength = 20;
            this.u_name.Properties.MaxLength = 20;

        }

        private bool Validate()
        {

            if (string.IsNullOrEmpty(u_code.Text))
            {
                MessageForm.Warning("级别编码不能为空!");
                return false;
            }

            if (string.IsNullOrEmpty(u_name.Text))
            {
                MessageForm.Warning("级别名称不能为空!");
                return false;
            }


            return true;
        }
        #endregion

        #region 数据访问
        private bool Insert()
        {
            ServiceParamter serviceParamter = new ServiceParamter();

            serviceParamter.ServiceId = "sysDictsUnitinfoProjectLevel_insert";
            Dictionary<string, string> dict = new Dictionary<string, string>();
            StringBuilder str = new StringBuilder();

            string flag = string.Empty;
            if (Convert.ToBoolean(u_stop.EditValue))
                flag = "1";
            else
                flag = "0";
            dict.Add("level_code", u_code.Text);
            dict.Add("level_name", u_name.Text);
            dict.Add("temp", null);
            dict.Add("is_stop", flag);
            serviceParamter.Paramters = dict;

            return _helper.WirteSql(serviceParamter);

        }
        #endregion


        private void addBut_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Insert();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }

        private void closeBut_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void u_code_EditValueChanged(object sender, EventArgs e)
        {

        }

      
    }
}
