﻿namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.deptinfo
{
    partial class kind
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.sysDictsUnitinfoDeptKind_insert = new DevExpress.XtraEditors.SimpleButton();
            this.sysDictsUnitinfoDeptKind_select = new DevExpress.XtraEditors.SimpleButton();
            this.sysDictsUnitinfoDeptKind_delete = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.sysDictsUnitinfoDeptKind_insert);
            this.panelControl1.Controls.Add(this.sysDictsUnitinfoDeptKind_select);
            this.panelControl1.Controls.Add(this.sysDictsUnitinfoDeptKind_delete);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(878, 52);
            this.panelControl1.TabIndex = 0;
            // 
            // sysDictsUnitinfoDeptKind_insert
            // 
            this.sysDictsUnitinfoDeptKind_insert.Location = new System.Drawing.Point(465, 12);
            this.sysDictsUnitinfoDeptKind_insert.Name = "sysDictsUnitinfoDeptKind_insert";
            this.sysDictsUnitinfoDeptKind_insert.Size = new System.Drawing.Size(87, 27);
            this.sysDictsUnitinfoDeptKind_insert.TabIndex = 17;
            this.sysDictsUnitinfoDeptKind_insert.Text = "添加";
            this.sysDictsUnitinfoDeptKind_insert.Click += new System.EventHandler(this.sysDictsUnitinfoDeptKind_insert_Click);
            // 
            // sysDictsUnitinfoDeptKind_select
            // 
            this.sysDictsUnitinfoDeptKind_select.Location = new System.Drawing.Point(350, 12);
            this.sysDictsUnitinfoDeptKind_select.Name = "sysDictsUnitinfoDeptKind_select";
            this.sysDictsUnitinfoDeptKind_select.Size = new System.Drawing.Size(87, 27);
            this.sysDictsUnitinfoDeptKind_select.TabIndex = 15;
            this.sysDictsUnitinfoDeptKind_select.Text = "查询";
            this.sysDictsUnitinfoDeptKind_select.Click += new System.EventHandler(this.sysDictsUnitinfoDeptKind_select_Click);
            // 
            // sysDictsUnitinfoDeptKind_delete
            // 
            this.sysDictsUnitinfoDeptKind_delete.Location = new System.Drawing.Point(573, 12);
            this.sysDictsUnitinfoDeptKind_delete.Name = "sysDictsUnitinfoDeptKind_delete";
            this.sysDictsUnitinfoDeptKind_delete.Size = new System.Drawing.Size(87, 27);
            this.sysDictsUnitinfoDeptKind_delete.TabIndex = 16;
            this.sysDictsUnitinfoDeptKind_delete.Text = "删除";
            this.sysDictsUnitinfoDeptKind_delete.Click += new System.EventHandler(this.sysDictsUnitinfoDeptKind_delete_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 52);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(878, 310);
            this.panelControl2.TabIndex = 1;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.gridControl1.Size = new System.Drawing.Size(874, 306);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "选择";
            this.gridColumn1.FieldName = "checked";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "部门类别编码";
            this.gridColumn2.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.gridColumn2.FieldName = "kind_code";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit1_Click);
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "部门类别名称";
            this.gridColumn3.FieldName = "kind_name";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // kind
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(878, 362);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "kind";
            this.Text = "编码类别";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.SimpleButton sysDictsUnitinfoDeptKind_insert;
        private DevExpress.XtraEditors.SimpleButton sysDictsUnitinfoDeptKind_select;
        private DevExpress.XtraEditors.SimpleButton sysDictsUnitinfoDeptKind_delete;
    }
}