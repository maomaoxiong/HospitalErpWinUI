﻿namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.deptinfo
{
    partial class kind_insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.u_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.u_code = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.acctsysvouchtype_insert = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // u_name
            // 
            this.u_name.Location = new System.Drawing.Point(101, 83);
            this.u_name.Name = "u_name";
            this.u_name.Size = new System.Drawing.Size(147, 20);
            this.u_name.TabIndex = 23;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(38, 86);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 22;
            this.labelControl2.Text = "类别名称：";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(38, 50);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 21;
            this.labelControl1.Text = "类别编码：";
            // 
            // u_code
            // 
            this.u_code.Location = new System.Drawing.Point(101, 47);
            this.u_code.Name = "u_code";
            this.u_code.Size = new System.Drawing.Size(147, 20);
            this.u_code.TabIndex = 20;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(163, 130);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 25;
            this.simpleButton2.Text = "关闭";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // acctsysvouchtype_insert
            // 
            this.acctsysvouchtype_insert.Location = new System.Drawing.Point(46, 130);
            this.acctsysvouchtype_insert.Name = "acctsysvouchtype_insert";
            this.acctsysvouchtype_insert.Size = new System.Drawing.Size(75, 23);
            this.acctsysvouchtype_insert.TabIndex = 24;
            this.acctsysvouchtype_insert.Text = "保存";
            this.acctsysvouchtype_insert.Click += new System.EventHandler(this.acctsysvouchtype_insert_Click);
            // 
            // kind_insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.acctsysvouchtype_insert);
            this.Controls.Add(this.u_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.u_code);
            this.Name = "kind_insert";
            this.Text = "kind_insert";
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit u_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit u_code;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton acctsysvouchtype_insert;
    }
}