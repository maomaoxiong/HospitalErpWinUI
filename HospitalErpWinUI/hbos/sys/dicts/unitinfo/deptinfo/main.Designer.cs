﻿namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.deptinfo
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.dept_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.is_stop = new DevExpress.XtraEditors.ComboBoxEdit();
            this.qqq = new DevExpress.XtraEditors.LabelControl();
            this.is_stock = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.is_serve = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.sysDictsUnitinfoDeptInfo_delete = new DevExpress.XtraEditors.SimpleButton();
            this.sysDictsUnitinfoDeptInfo_insert = new DevExpress.XtraEditors.SimpleButton();
            this.sysDictsUnitinfoDeptInfo_select = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dept_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_stop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_stock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_serve.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl3);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.panelControl1.Size = new System.Drawing.Size(930, 295);
            this.panelControl1.TabIndex = 0;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.gridControl1);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(12, 102);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(916, 191);
            this.panelControl3.TabIndex = 1;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.gridControl1.Size = new System.Drawing.Size(912, 187);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "选择";
            this.gridColumn1.FieldName = "checked";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "部门编码";
            this.gridColumn2.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.gridColumn2.FieldName = "dept_code";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit1_Click);
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "部门名称";
            this.gridColumn3.FieldName = "dept_name";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "停用";
            this.gridColumn4.FieldName = "Column1";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.sysDictsUnitinfoDeptInfo_delete);
            this.panelControl2.Controls.Add(this.sysDictsUnitinfoDeptInfo_insert);
            this.panelControl2.Controls.Add(this.sysDictsUnitinfoDeptInfo_select);
            this.panelControl2.Controls.Add(this.is_serve);
            this.panelControl2.Controls.Add(this.labelControl3);
            this.panelControl2.Controls.Add(this.is_stock);
            this.panelControl2.Controls.Add(this.labelControl2);
            this.panelControl2.Controls.Add(this.is_stop);
            this.panelControl2.Controls.Add(this.qqq);
            this.panelControl2.Controls.Add(this.dept_name);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(12, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(916, 100);
            this.panelControl2.TabIndex = 0;
            // 
            // dept_name
            // 
            this.dept_name.Location = new System.Drawing.Point(88, 10);
            this.dept_name.Name = "dept_name";
            this.dept_name.Size = new System.Drawing.Size(165, 20);
            this.dept_name.TabIndex = 19;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(15, 13);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 20;
            this.labelControl1.Text = "部门名称：";
            // 
            // is_stop
            // 
            this.is_stop.Location = new System.Drawing.Point(371, 10);
            this.is_stop.Name = "is_stop";
            this.is_stop.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_stop.Size = new System.Drawing.Size(168, 20);
            this.is_stop.TabIndex = 23;
            // 
            // qqq
            // 
            this.qqq.Location = new System.Drawing.Point(286, 13);
            this.qqq.Name = "qqq";
            this.qqq.Size = new System.Drawing.Size(60, 14);
            this.qqq.TabIndex = 22;
            this.qqq.Text = "是否停用：";
            // 
            // is_stock
            // 
            this.is_stock.Location = new System.Drawing.Point(88, 46);
            this.is_stock.Name = "is_stock";
            this.is_stock.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_stock.Size = new System.Drawing.Size(168, 20);
            this.is_stock.TabIndex = 25;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(15, 49);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 24;
            this.labelControl2.Text = "是否采购：";
            // 
            // is_serve
            // 
            this.is_serve.Location = new System.Drawing.Point(371, 46);
            this.is_serve.Name = "is_serve";
            this.is_serve.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_serve.Size = new System.Drawing.Size(168, 20);
            this.is_serve.TabIndex = 27;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(286, 49);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 14);
            this.labelControl3.TabIndex = 26;
            this.labelControl3.Text = "服务部门：";
            // 
            // sysDictsUnitinfoDeptInfo_delete
            // 
            this.sysDictsUnitinfoDeptInfo_delete.Location = new System.Drawing.Point(805, 63);
            this.sysDictsUnitinfoDeptInfo_delete.Name = "sysDictsUnitinfoDeptInfo_delete";
            this.sysDictsUnitinfoDeptInfo_delete.Size = new System.Drawing.Size(101, 31);
            this.sysDictsUnitinfoDeptInfo_delete.TabIndex = 30;
            this.sysDictsUnitinfoDeptInfo_delete.Text = "删除";
            // 
            // sysDictsUnitinfoDeptInfo_insert
            // 
            this.sysDictsUnitinfoDeptInfo_insert.Location = new System.Drawing.Point(689, 63);
            this.sysDictsUnitinfoDeptInfo_insert.Name = "sysDictsUnitinfoDeptInfo_insert";
            this.sysDictsUnitinfoDeptInfo_insert.Size = new System.Drawing.Size(101, 31);
            this.sysDictsUnitinfoDeptInfo_insert.TabIndex = 29;
            this.sysDictsUnitinfoDeptInfo_insert.Text = "添加";
            this.sysDictsUnitinfoDeptInfo_insert.Click += new System.EventHandler(this.sysDictsUnitinfoDeptInfo_insert_Click);
            // 
            // sysDictsUnitinfoDeptInfo_select
            // 
            this.sysDictsUnitinfoDeptInfo_select.Location = new System.Drawing.Point(572, 63);
            this.sysDictsUnitinfoDeptInfo_select.Name = "sysDictsUnitinfoDeptInfo_select";
            this.sysDictsUnitinfoDeptInfo_select.Size = new System.Drawing.Size(101, 31);
            this.sysDictsUnitinfoDeptInfo_select.TabIndex = 28;
            this.sysDictsUnitinfoDeptInfo_select.Text = "查询";
            this.sysDictsUnitinfoDeptInfo_select.Click += new System.EventHandler(this.sysDictsUnitinfoDeptInfo_select_Click);
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(930, 295);
            this.Controls.Add(this.panelControl1);
            this.Name = "main";
            this.Text = "main";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dept_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_stop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_stock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_serve.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.TextEdit dept_name;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit is_serve;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.ComboBoxEdit is_stock;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ComboBoxEdit is_stop;
        private DevExpress.XtraEditors.LabelControl qqq;
        private DevExpress.XtraEditors.SimpleButton sysDictsUnitinfoDeptInfo_delete;
        private DevExpress.XtraEditors.SimpleButton sysDictsUnitinfoDeptInfo_insert;
        private DevExpress.XtraEditors.SimpleButton sysDictsUnitinfoDeptInfo_select;
    }
}