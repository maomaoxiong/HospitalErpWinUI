﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.deptinfo
{    
    public partial class kind_insert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        public kind_insert()
        {
            InitializeComponent();
            _helper = new DaoHelper();
        }
        public kind_insert(Dao.DaoHelper helper, List<Perm> perms)
        {
            InitializeComponent();
            this._helper = helper;
            this._perms = perms;
        }

        private void acctsysvouchtype_insert_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Insert();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
        private bool Validate()
        {

            if (string.IsNullOrEmpty(u_name.Text))
            {
                MessageForm.Warning("名称不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(u_code.Text))
            {
                MessageForm.Warning("编码不能为空!");
                return false;
            }
            return true;
        }
        private bool Insert()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "sysDictsUnitinfoDeptKind_insert";
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            paramters.Add("u_code", u_code.Text);
            paramters.Add("u_name", u_name.Text);
            paramters.Add("Comp_code", G_User.user.Comp_code);//没有实际意义
            serviceParamter.Paramters = paramters;
            return _helper.WirteSql(serviceParamter);

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
