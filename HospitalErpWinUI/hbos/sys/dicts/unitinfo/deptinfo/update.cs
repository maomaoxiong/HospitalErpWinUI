﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.Common;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.deptinfo
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        private Validator validator;
        private List<Perm> _perms { get; set; }
        public  update()
        {
            InitializeComponent();
        }
        private void Load_dict_yes_or_no()
        {
            List<string> objs = DataDictApi.dict_yes_or_no();
            //u_stop.Properties.Items.AddRange(objs);
            //u_stop.SelectedItem = objs[0];
            this.StartPosition = FormStartPosition.CenterParent;
            this.Text = "修改";
        }
        public update(DaoHelper helper, List<Perm> perms, string content_code)
        {

            InitializeComponent();
            _helper = helper;
            _perms = perms;
            this.StartPosition = FormStartPosition.CenterParent;
           // Load_dict_yes_or_no();//是否下拉框
            //validator = new Validator();
            //// Init();
            //Load_dict_yes_or_no();//是否下拉框
            //ServiceParamter serviceParamter = new ServiceParamter();
            //Dictionary<string, string> paramters = new Dictionary<string, string>();
            //serviceParamter.ServiceId = "sysDictsUnitinfoVenType_update_load";
            //paramters.Add("Comp_code", G_User.user.Comp_code);
            //paramters.Add("content_code1", content_code);
            //serviceParamter.Paramters = paramters;
            //DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            //if (dt.Rows.Count == 0)
            //{
            //    MessageBox.Show("查询失败！");
            //}
            //else
            //{
            //    this.u_code.Text = dt.Rows[0]["ven_type_code"].ToString();
            //    this.u_name.Text = dt.Rows[0]["ven_type_name"].ToString();
            //    FormHelper.ComboBoxeditAssignment(u_last, dt.Rows[0]["is_last"].ToString());
            //    FormHelper.ComboBoxeditAssignment(u_stop, dt.Rows[0]["is_stop"].ToString());


            //}
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Enter)　　// 按下的是回车键
            {
                foreach (Control c in this.Controls)
                {
                    if (c is System.Windows.Forms.TextBox)　　// 当前控件是文本框控件
                    {
                        keyData = Keys.Tab;
                    }
                }
                keyData = Keys.Tab;
            }
            return base.ProcessDialogKey(keyData);
        }
    }
}
