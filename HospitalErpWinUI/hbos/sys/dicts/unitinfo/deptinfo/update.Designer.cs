﻿namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.deptinfo
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.u_manage = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.c_code = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.acctsysvouchtype_insert = new DevExpress.XtraEditors.SimpleButton();
            this.u_service = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.u_inout_type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.u_outer = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.u_stop = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.u_stock = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.is_last = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.u_sys = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.u_type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.u_kind = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.u_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.u_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.u_manage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_service.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_inout_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_outer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_stop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_stock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_last.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_sys.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_kind.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // u_manage
            // 
            this.u_manage.Location = new System.Drawing.Point(159, 243);
            this.u_manage.Name = "u_manage";
            this.u_manage.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_manage.Size = new System.Drawing.Size(165, 20);
            this.u_manage.TabIndex = 13;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(62, 246);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(84, 14);
            this.labelControl13.TabIndex = 77;
            this.labelControl13.Text = "是否职能部门：";
            // 
            // c_code
            // 
            this.c_code.Location = new System.Drawing.Point(159, 207);
            this.c_code.Name = "c_code";
            this.c_code.Size = new System.Drawing.Size(165, 20);
            this.c_code.TabIndex = 11;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(375, 274);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 16;
            this.simpleButton2.Text = "关闭";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // acctsysvouchtype_insert
            // 
            this.acctsysvouchtype_insert.Location = new System.Drawing.Point(258, 274);
            this.acctsysvouchtype_insert.Name = "acctsysvouchtype_insert";
            this.acctsysvouchtype_insert.Size = new System.Drawing.Size(75, 23);
            this.acctsysvouchtype_insert.TabIndex = 15;
            this.acctsysvouchtype_insert.Text = "保存";
            // 
            // u_service
            // 
            this.u_service.Location = new System.Drawing.Point(448, 207);
            this.u_service.Name = "u_service";
            this.u_service.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_service.Size = new System.Drawing.Size(165, 20);
            this.u_service.TabIndex = 12;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(351, 210);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(84, 14);
            this.labelControl11.TabIndex = 76;
            this.labelControl11.Text = "是否服务部门：";
            // 
            // u_inout_type
            // 
            this.u_inout_type.Location = new System.Drawing.Point(448, 171);
            this.u_inout_type.Name = "u_inout_type";
            this.u_inout_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_inout_type.Size = new System.Drawing.Size(165, 20);
            this.u_inout_type.TabIndex = 10;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(74, 210);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(72, 14);
            this.labelControl12.TabIndex = 75;
            this.labelControl12.Text = "自定义编码：";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(375, 174);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(60, 14);
            this.labelControl10.TabIndex = 74;
            this.labelControl10.Text = "支出性质：";
            // 
            // u_outer
            // 
            this.u_outer.Location = new System.Drawing.Point(159, 171);
            this.u_outer.Name = "u_outer";
            this.u_outer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_outer.Size = new System.Drawing.Size(165, 20);
            this.u_outer.TabIndex = 8;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(86, 174);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(60, 14);
            this.labelControl9.TabIndex = 73;
            this.labelControl9.Text = "外部单位：";
            // 
            // u_stop
            // 
            this.u_stop.Location = new System.Drawing.Point(448, 135);
            this.u_stop.Name = "u_stop";
            this.u_stop.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_stop.Size = new System.Drawing.Size(165, 20);
            this.u_stop.TabIndex = 7;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(375, 138);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(60, 14);
            this.labelControl7.TabIndex = 72;
            this.labelControl7.Text = "是否停用：";
            // 
            // u_stock
            // 
            this.u_stock.Location = new System.Drawing.Point(159, 135);
            this.u_stock.Name = "u_stock";
            this.u_stock.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_stock.Size = new System.Drawing.Size(165, 20);
            this.u_stock.TabIndex = 6;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(86, 138);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(60, 14);
            this.labelControl8.TabIndex = 71;
            this.labelControl8.Text = "是否采购：";
            // 
            // is_last
            // 
            this.is_last.Location = new System.Drawing.Point(448, 100);
            this.is_last.Name = "is_last";
            this.is_last.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_last.Size = new System.Drawing.Size(165, 20);
            this.is_last.TabIndex = 5;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(375, 103);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 14);
            this.labelControl5.TabIndex = 70;
            this.labelControl5.Text = "是否末级：";
            // 
            // u_sys
            // 
            this.u_sys.Location = new System.Drawing.Point(159, 100);
            this.u_sys.Name = "u_sys";
            this.u_sys.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_sys.Size = new System.Drawing.Size(165, 20);
            this.u_sys.TabIndex = 4;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(86, 103);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(60, 14);
            this.labelControl6.TabIndex = 69;
            this.labelControl6.Text = "部门性质：";
            // 
            // u_type
            // 
            this.u_type.Location = new System.Drawing.Point(448, 64);
            this.u_type.Name = "u_type";
            this.u_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_type.Size = new System.Drawing.Size(165, 20);
            this.u_type.TabIndex = 3;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(375, 67);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 14);
            this.labelControl4.TabIndex = 68;
            this.labelControl4.Text = "部门类型：";
            // 
            // u_kind
            // 
            this.u_kind.Location = new System.Drawing.Point(159, 64);
            this.u_kind.Name = "u_kind";
            this.u_kind.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_kind.Size = new System.Drawing.Size(165, 20);
            this.u_kind.TabIndex = 2;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(86, 67);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 14);
            this.labelControl3.TabIndex = 67;
            this.labelControl3.Text = "部门类别：";
            // 
            // u_name
            // 
            this.u_name.Location = new System.Drawing.Point(448, 27);
            this.u_name.Name = "u_name";
            this.u_name.Size = new System.Drawing.Size(165, 20);
            this.u_name.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(375, 30);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 66;
            this.labelControl2.Text = "部门名称：";
            // 
            // u_code
            // 
            this.u_code.Location = new System.Drawing.Point(159, 27);
            this.u_code.Name = "u_code";
            this.u_code.Size = new System.Drawing.Size(165, 20);
            this.u_code.TabIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(86, 30);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 65;
            this.labelControl1.Text = "部门编码：";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.u_manage);
            this.panelControl1.Controls.Add(this.labelControl13);
            this.panelControl1.Controls.Add(this.c_code);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.acctsysvouchtype_insert);
            this.panelControl1.Controls.Add(this.u_service);
            this.panelControl1.Controls.Add(this.labelControl11);
            this.panelControl1.Controls.Add(this.u_inout_type);
            this.panelControl1.Controls.Add(this.labelControl12);
            this.panelControl1.Controls.Add(this.labelControl10);
            this.panelControl1.Controls.Add(this.u_outer);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.u_stop);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.u_stock);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.is_last);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.u_sys);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.u_type);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.u_kind);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.u_name);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.u_code);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(674, 324);
            this.panelControl1.TabIndex = 1;
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(674, 324);
            this.Controls.Add(this.panelControl1);
            this.Name = "update";
            this.Text = "update";
            ((System.ComponentModel.ISupportInitialize)(this.u_manage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_service.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_inout_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_outer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_stop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_stock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_last.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_sys.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_kind.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.ComboBoxEdit u_manage;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit c_code;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton acctsysvouchtype_insert;
        private DevExpress.XtraEditors.ComboBoxEdit u_service;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.ComboBoxEdit u_inout_type;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.ComboBoxEdit u_outer;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.ComboBoxEdit u_stop;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.ComboBoxEdit u_stock;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.ComboBoxEdit is_last;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.ComboBoxEdit u_sys;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.ComboBoxEdit u_type;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.ComboBoxEdit u_kind;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit u_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit u_code;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
    }
}