﻿using Dao;
using HospitalErpData.MenuHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.deptinfo
{
    public partial class insert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        public  insert()
        {
            InitializeComponent();
        }
        public  insert(Dao.DaoHelper helper, List<Perm> perms)
        {
            InitializeComponent();
            this._helper = helper;
            this._perms = perms;
            //Load_dict_yes_or_no();//是否下拉框
            this.StartPosition = FormStartPosition.CenterParent;
            this.Text = "录入";
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Enter)　　// 按下的是回车键
            {
                foreach (Control c in this.Controls)
                {
                    if (c is System.Windows.Forms.TextBox)　　// 当前控件是文本框控件
                    {
                        keyData = Keys.Tab;
                    }
                }
                keyData = Keys.Tab;
            }
            return base.ProcessDialogKey(keyData);
        }

        private void acctsysvouchtype_insert_Click(object sender, EventArgs e)
        {

        }
    }
}
