﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraGrid;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;  
using System.Linq;  
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.deptinfo
{
    public partial class kind : RibbonForm
    {
        protected DaoHelper _helper;

        public List<Perm> _perms { get; set; }


        //  public List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public kind()
        {
            InitializeComponent();
            _helper = new DaoHelper();
        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                int index = gridView1.GetDataSourceRowIndex(gridView1.FocusedRowHandle);
                string acct_subj_code = _user.Rows[index]["kind_code"].ToString();
                string acct_subj_name = _user.Rows[index]["kind_name"].ToString();


                kind_update frm = new kind_update(this._helper, this._perms, acct_subj_code, acct_subj_name);

                if (frm.ShowDialog() == DialogResult.No)
                {
                    MessageForm.Warning("修改失败！");
                }
                Query();
            }
        }

        private void sysDictsUnitinfoDeptKind_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysDictsUnitinfoDeptKind_select";
            paramters.Add("summ_code", G_User.user.Comp_code);

            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;
        }

        private void sysDictsUnitinfoDeptKind_insert_Click(object sender, EventArgs e)
        {
            try
            {
                kind_insert insertFrm = new kind_insert(this._helper, this._perms);

                if (insertFrm.ShowDialog() == DialogResult.OK)
                {
                    Query();
                }
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void sysDictsUnitinfoDeptKind_delete_Click(object sender, EventArgs e)
        {
            bool value = false;
            string strSelected = string.Empty;

            try
            {
                List<string> objs = new List<string>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    value = Convert.ToBoolean(gridView1.GetDataRow(i)["checked"]);
                    if (value)
                    {
                        string code = gridView1.GetDataRow(i)["kind_code"].ToString();
                        objs.Add(code);

                    }
                }
                if (objs.Count == 0)
                {
                    MessageForm.Show("请选择要删除的数据!");
                    return;
                }
                if (MessageForm.ShowMsg("确认是否删除?", MsgType.YesNo) == DialogResult.Yes)
                {
                    foreach (string obj in objs)
                    {
                        if (!Delete(obj))
                        {
                            return;
                        }
                    }
                    //       MessageForm.Show("操作成功!");
                    Query();
                }

            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private bool Delete(string co_code)
        {

            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysDictsUnitinfoDeptKind_delete";

            paramters.Add("content_code", co_code);
            paramters.Add("content_name1", "");//名称删除只是占位符
            paramters.Add("Comp_code", G_User.user.Comp_code);//名称删除只是占位符
            
            serviceParamter.Paramters = paramters;
            bool result = false;
            try
            {
                _helper.WirteSql(serviceParamter);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }

    }
}
