﻿namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.deptinfo
{
    partial class kind_update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.sysDictsUnitinfoDeptKind_update = new DevExpress.XtraEditors.SimpleButton();
            this.u_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.u_code = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(162, 128);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 31;
            this.simpleButton2.Text = "关闭";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // sysDictsUnitinfoDeptKind_update
            // 
            this.sysDictsUnitinfoDeptKind_update.Location = new System.Drawing.Point(45, 128);
            this.sysDictsUnitinfoDeptKind_update.Name = "sysDictsUnitinfoDeptKind_update";
            this.sysDictsUnitinfoDeptKind_update.Size = new System.Drawing.Size(75, 23);
            this.sysDictsUnitinfoDeptKind_update.TabIndex = 30;
            this.sysDictsUnitinfoDeptKind_update.Text = "保存";
            this.sysDictsUnitinfoDeptKind_update.Click += new System.EventHandler(this.sysDictsUnitinfoDeptKind_update_Click);
            // 
            // u_name
            // 
            this.u_name.Location = new System.Drawing.Point(100, 81);
            this.u_name.Name = "u_name";
            this.u_name.Size = new System.Drawing.Size(147, 20);
            this.u_name.TabIndex = 29;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(37, 84);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 28;
            this.labelControl2.Text = "类别名称：";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(37, 48);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 27;
            this.labelControl1.Text = "类别编码：";
            // 
            // u_code
            // 
            this.u_code.Enabled = false;
            this.u_code.Location = new System.Drawing.Point(100, 45);
            this.u_code.Name = "u_code";
            this.u_code.Size = new System.Drawing.Size(147, 20);
            this.u_code.TabIndex = 26;
            // 
            // kind_update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.sysDictsUnitinfoDeptKind_update);
            this.Controls.Add(this.u_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.u_code);
            this.Name = "kind_update";
            this.Text = "kind_update";
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton sysDictsUnitinfoDeptKind_update;
        private DevExpress.XtraEditors.TextEdit u_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit u_code;
    }
}