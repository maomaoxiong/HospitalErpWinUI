﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.unitinfo.deptinfo
{
    public partial class query : RibbonForm
    {
         protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        //  public List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        
        public query()
        {
            InitializeComponent();
            _helper = new DaoHelper();
            Load_sys_dept_kind_list();//部门类别下拉框
            Load_sys_dept_type_list();//部门类型下拉框
            Load_sys_dept_attri_list();//部门性质下拉框
            Load_dict_inout_type();//支出性质下拉框
            Load_dict_last_yes_or_no();//支出性质下拉框

        }
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Enter)　　// 按下的是回车键
            {
                foreach (Control c in this.Controls)
                {
                    if (c is System.Windows.Forms.TextBox)　　// 当前控件是文本框控件
                    {
                        keyData = Keys.Tab;
                    }
                }
                keyData = Keys.Tab;
            }
            return base.ProcessDialogKey(keyData);
        }
        private void Load_sys_dept_kind_list()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sys_dept_kind_list";
            paramters.Add("comp_code", G_User.user.Comp_code);
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Value);
            }
            u_kind.Properties.Items.AddRange(objs);

        }
        private void Load_sys_dept_type_list()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sys_dept_type_list";
            
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Value);
            }
            u_type.Properties.Items.AddRange(objs);

        }
        private void Load_sys_dept_attri_list()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sys_dept_attri_list";

            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Value);
            }
            u_sys.Properties.Items.AddRange(objs);

        }
        private void Load_dict_inout_type()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "dict_inout_type";

            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code+' '+codeValue.Value);
            }
            u_inout_type.Properties.Items.AddRange(objs);

        }
        private void Load_dict_last_yes_or_no()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "dict_last_yes_or_no";
            // DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            List<CodeValue> codeValues = _helper.ReadDictForData(serviceParamter);
            foreach (CodeValue codeValue in codeValues)
            {
                objs.Add( codeValue.Value);
            }
            is_last.Properties.Items.AddRange(objs);
            is_stop.Properties.Items.AddRange(objs);
        }

        private void sysDictsUnitinfoDeptQuery_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysDictsUnitinfoDeptQuery_select";
            paramters.Add("comp_code", G_User.user.Comp_code);
            paramters.Add("u_unit", u_unit.Text.Trim());
            paramters.Add("u_copy", u_copy.Text.Trim());

            paramters.Add("u_kind", FormHelper.GetValueForComboBoxEdit(u_kind));
            paramters.Add("u_type", FormHelper.GetValueForComboBoxEdit(u_type));
            paramters.Add("u_sys", FormHelper.GetValueForComboBoxEdit(u_sys));
            paramters.Add("u_inout_type", FormHelper.GetValueForComboBoxEdit(u_inout_type));
            paramters.Add("is_last", FormHelper.GetValueForComboBoxEdit(is_last));
            paramters.Add("is_stop", FormHelper.GetValueForComboBoxEdit(is_stop));

            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;
        }
        

    }
}
