﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.acct.subjtype
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
      //  DaoHelper _helper =new DaoHelper();
        private DataTable _subjtype;
        private string _c_code;
        public main()
        {
            _helper = new DaoHelper();
            InitializeComponent();
            Init();
        }

        #region 其他
        private void Init()
        {

            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.StartPosition = FormStartPosition.CenterScreen;
        }
        #endregion

        #region 访问数据
        private void LoadSubjType()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysAcctSubjType_list";
            serviceParamter.Paramters = paramters;

            List<string> companys = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                companys.Add(codeValue.Code + " " + codeValue.Value);
            }
            c_code.Properties.Items.AddRange(companys);
            c_code.SelectedItem = companys[0];
        }

        private void Query()
        {
          

            //sysDictsAcctSubjSys_select
            ServiceParamter serviceParamter = new ServiceParamter();         
            serviceParamter.ServiceId = "sysDictsAcctSubjType_select";

            Dictionary<string, string> dict = new Dictionary<string, string>();
            string[] strArray1 = System.Text.RegularExpressions.Regex.Split(c_code.Text.Trim(), @"\s{1,}");

            _c_code = strArray1[0];
            dict.Add("c_code", strArray1[0]);
            dict.Add("s_code", u_name.Text);
            dict.Add("s_name", u_parent.Text);          
            serviceParamter.Paramters = dict;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            _subjtype = dt;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
             //   string[] arragy = dt.Rows[i]["Column1"].ToString().Split('|');

              //  dt.Rows[i]["Column1"] = arragy[3].ToString();
               
            }
            //    _user = dt;
            gridControl1.DataSource = _subjtype;

        }

        private bool Delete(string co_code, string subj_type_code )
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysDictsAcctSubjTypeCustom_delete";
            paramters.Add("c_code", co_code);
            paramters.Add("s_code", co_code);
            paramters.Add("s_name", subj_type_code);
            serviceParamter.Paramters = paramters;
            bool result = false;
            try
            {
                _helper.WirteSql(serviceParamter);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }
        #endregion

        #region 事件
        private void gridView1_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            RepositoryItemCheckEdit disItemCheck;
            //  RepositoryItemHyperLinkEdit hyperLinkEdit;
            //若当前进行到了“选择”列，
            if (e.Column.FieldName.Equals("checked"))
            {
                disItemCheck = (RepositoryItemCheckEdit)e.RepositoryItem.Clone();
                disItemCheck.Enabled = false;
                disItemCheck.ReadOnly = true;
                string[] strArray1 = System.Text.RegularExpressions.Regex.Split(c_code.Text.Trim(), @"\s{1,}");
                string fieldName = strArray1[0];
                //     DevExpress.XtraGrid.Views.Grid.GridView gv = sender as DevExpress.XtraGrid.Views.Grid.GridView;
                //      string fieldName = gv.GetRowCellValue(e.RowHandle, gv.Columns["co_code"]).ToString();
                if (fieldName.Equals("01")
                    || fieldName.Equals("02")
                    || fieldName.Equals("03")
                    || fieldName.Equals("04")
                    || fieldName.Equals("05")
                    || fieldName.Equals("06")
                    )
                {
                    e.RepositoryItem = disItemCheck;
                }
            }

        }

       
        private void QueryBut_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
                string co_code = FormHelper.GetValueForComboBoxEdit(this.c_code);
                if (co_code.Equals("01")
                    || co_code.Equals("02")
                    || co_code.Equals("03")
                    || co_code.Equals("04")
                    || co_code.Equals("05")
                    || co_code.Equals("06")
                    )
                {
                    this.InsertBut.Enabled = false;
                    this.DeleteBut.Enabled = false;
                }
                else
                {
                    this.InsertBut.Enabled = true;
                    this.DeleteBut.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void InsertBut_Click(object sender, EventArgs e)
        {
            try
            {
                insert insertFrm = new insert(this._helper, this._perms, _c_code);

                if (insertFrm.ShowDialog() == DialogResult.OK)
                {
                    Query();
                }
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
           
        }

        private void DeleteBut_Click(object sender, EventArgs e)
        {
            // sysDictsAcctSubjSys_delete
            bool value = false;
            string strSelected = string.Empty;

            try
            {
                List<string> objs = new List<string>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    value = Convert.ToBoolean(gridView1.GetDataRow(i)["checked"]);
                    if (value)
                    {
                        string subj_type_code = gridView1.GetDataRow(i)["subj_type_code"].ToString();
                        string co_code = gridView1.GetDataRow(i)["_co_code"].ToString();
                        objs.Add(co_code+"_"+subj_type_code);

                    }
                }
                if (objs.Count == 0)
                {
                    MessageForm.Show("请选择要删除的数据!");
                    return;
                }
                if (MessageForm.ShowMsg("确认是否删除?", MsgType.YesNo) == DialogResult.Yes)
                {
                    foreach (string obj in objs)
                    {
                        string[] array = obj.Split('_');
                        if (!Delete(array[0], array[1]))
                        {
                            return;
                        }
                    }
                    //       MessageForm.Show("操作成功!");
                    Query();
                }

            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void main_Load(object sender, EventArgs e)
        {
            LoadSubjType();
            this.InsertBut.Enabled = false;
            this.DeleteBut.Enabled = false;
        }
        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, DevExpress.XtraEditors.Controls.OpenLinkEventArgs e)
        {
            if (gridView1.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                int index = gridView1.GetDataSourceRowIndex(gridView1.FocusedRowHandle);
                string co_code = _subjtype.Rows[index]["_co_code"].ToString();
                string subj_type_code = _subjtype.Rows[index]["subj_type_code"].ToString();
                if (co_code.Equals("01")
                  || co_code.Equals("02")
                  || co_code.Equals("03")
                  || co_code.Equals("04")
                  || co_code.Equals("05")
                  || co_code.Equals("06")
                  )
                {
                    return;
                }
                update frm = new update(this._helper, this._perms, co_code, subj_type_code);

                if (frm.ShowDialog() == DialogResult.No)
                {
                    MessageForm.Warning("修改失败！");
                }
                Query();
            }
        }
       
        #endregion

        private void c_code_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

      
    }
  
}
