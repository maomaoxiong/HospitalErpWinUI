﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.acct.subjtype
{
    public partial class insert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        private string _c_code;
        public insert()
        {
            InitializeComponent();
            Init();
        }
        public insert(DaoHelper helper, List<Perm> perms, string c_ode)
        {
            InitializeComponent();
            this._helper = helper;
            this._perms = perms;
            this._c_code = c_ode;
            Init();
        }

        #region 其他
        private void Init()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.subj_type_code.Properties.MaxLength = 20;      
            this.subj_type_name.Properties.MaxLength = 20;
        }

        private bool Validate()
        {

            if (string.IsNullOrEmpty(subj_type_code.Text))
            {
                MessageForm.Warning("类别编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(subj_type_name.Text))
            {
                MessageForm.Warning("类别名称不能为空!");
                return false;
            }

            if (FormHelper.Text_Length(subj_type_code.Text) > 20)
            {
                MessageForm.Warning("类别编码字符串长度超过数据库允许的长度10!");
                return false;
            }
            if (FormHelper.Text_Length(subj_type_name.Text) > 20)
            {
                MessageForm.Warning("类别名称字符串长度超过数据库允许的长度40!");
                return false;
            }
           
            return true;
        }
            
        #endregion

        #region 访问数据
        private bool Insert()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sysDictsAcctSubjType_insert";
            dict.Add("c_code", _c_code);
            dict.Add("type_code", subj_type_code.Text);
            dict.Add("type_name", subj_type_name.Text);
            serviceParamter.Paramters = dict;
            return _helper.WirteSql(serviceParamter);
        }

        #endregion
      
        #region 事件

        private void addBut_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Insert();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }

        private void CloseBut_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        #endregion

    }
}
