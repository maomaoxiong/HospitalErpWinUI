﻿namespace HospitalErpWinUI.hbos.sys.dicts.acct.subjtype
{
    partial class insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.subj_type_code = new DevExpress.XtraEditors.TextEdit();
            this.subj_type_name = new DevExpress.XtraEditors.TextEdit();
            this.addBut = new DevExpress.XtraEditors.SimpleButton();
            this.CloseBut = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.subj_type_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subj_type_name.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(58, 51);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 18);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "类别编码";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(58, 109);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 18);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "类别名称";
            // 
            // subj_type_code
            // 
            this.subj_type_code.Location = new System.Drawing.Point(154, 48);
            this.subj_type_code.Name = "subj_type_code";
            this.subj_type_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.subj_type_code.Properties.Appearance.Options.UseBackColor = true;
            this.subj_type_code.Size = new System.Drawing.Size(200, 24);
            this.subj_type_code.TabIndex = 1;
            // 
            // subj_type_name
            // 
            this.subj_type_name.Location = new System.Drawing.Point(154, 103);
            this.subj_type_name.Name = "subj_type_name";
            this.subj_type_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.subj_type_name.Properties.Appearance.Options.UseBackColor = true;
            this.subj_type_name.Size = new System.Drawing.Size(200, 24);
            this.subj_type_name.TabIndex = 1;
            // 
            // addBut
            // 
            this.addBut.Location = new System.Drawing.Point(96, 170);
            this.addBut.Name = "addBut";
            this.addBut.Size = new System.Drawing.Size(80, 25);
            this.addBut.TabIndex = 2;
            this.addBut.Text = "添加";
            this.addBut.Click += new System.EventHandler(this.addBut_Click);
            // 
            // CloseBut
            // 
            this.CloseBut.Location = new System.Drawing.Point(228, 170);
            this.CloseBut.Name = "CloseBut";
            this.CloseBut.Size = new System.Drawing.Size(80, 25);
            this.CloseBut.TabIndex = 2;
            this.CloseBut.Text = "关闭";
            this.CloseBut.Click += new System.EventHandler(this.CloseBut_Click);
            // 
            // insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 255);
            this.Controls.Add(this.CloseBut);
            this.Controls.Add(this.addBut);
            this.Controls.Add(this.subj_type_name);
            this.Controls.Add(this.subj_type_code);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Name = "insert";
            this.Text = "新增科目类别";
            ((System.ComponentModel.ISupportInitialize)(this.subj_type_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subj_type_name.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit subj_type_code;
        private DevExpress.XtraEditors.TextEdit subj_type_name;
        private DevExpress.XtraEditors.SimpleButton addBut;
        private DevExpress.XtraEditors.SimpleButton CloseBut;
    }
}