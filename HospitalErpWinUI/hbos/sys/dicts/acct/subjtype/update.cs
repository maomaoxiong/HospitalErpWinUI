﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.acct.subjtype
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        private string _c_code { get; set; }
        private string _old_subj_type_code { get; set; }

        public update(DaoHelper helper, List<Perm> perms, string c_code ,string old_subj_type_code)
        {
            _helper = helper;
            _perms = perms;
            _c_code = c_code;
            _old_subj_type_code = old_subj_type_code;
            InitializeComponent();
            Init();

        }

        public update()
        {
            InitializeComponent();
        }



        #region 其他
        private void Init()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.subj_type_code.Properties.MaxLength = 20;
            this.subj_type_name.Properties.MaxLength = 20;
        }

        private bool Validate()
        {

            if (string.IsNullOrEmpty(subj_type_code.Text))
            {
                MessageForm.Warning("类别编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(subj_type_name.Text))
            {
                MessageForm.Warning("类别名称不能为空!");
                return false;
            }

            if (FormHelper.Text_Length(subj_type_code.Text) > 20)
            {
                MessageForm.Warning("类别编码字符串长度超过数据库允许的长度10!");
                return false;
            }
            if (FormHelper.Text_Length(subj_type_name.Text) > 20)
            {
                MessageForm.Warning("类别名称字符串长度超过数据库允许的长度40!");
                return false;
            }

            return true;
        }

        #endregion

        #region 访问数据
        private void LoadData()
        {
            StringBuilder sqlstr = new StringBuilder();
            
		
            sqlstr.Append(@"select subj_type_code, subj_type_name from sys_subj_type  ");
            sqlstr.Append(@" where co_code='{0}' and subj_type_code='{1}' ");

            DataTable dt = _helper.ExecReadSql(sqlstr.ToString().Replace("{0}", _c_code).Replace("{1}", _old_subj_type_code));
            if (dt != null && dt.Rows.Count != 0)
            {
                this.subj_type_code.Text = dt.Rows[0]["subj_type_code"].ToString();
                this.subj_type_name.Text = dt.Rows[0]["subj_type_name"].ToString();
              
            }

        }

        private bool Update()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sysDictsAcctSubjTypeCustom_update";

            dict.Add("c_code", _c_code);
            dict.Add("c_code1", _c_code);
            dict.Add("old_code", _old_subj_type_code);
            dict.Add("s_code", subj_type_code.Text);
            dict.Add("s_name", subj_type_name.Text);
            serviceParamter.Paramters = dict;
            return _helper.WirteSql(serviceParamter);
        }
        #endregion

        #region 事件
        private void update_Load(object sender, EventArgs e)
        {
            try
            {
                LoadData();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
       
        private void CloseBut_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void UpdateBut_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Update();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }

        #endregion 

    
    }
}
