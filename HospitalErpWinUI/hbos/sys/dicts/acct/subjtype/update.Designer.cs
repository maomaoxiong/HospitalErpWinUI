﻿namespace HospitalErpWinUI.hbos.sys.dicts.acct.subjtype
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CloseBut = new DevExpress.XtraEditors.SimpleButton();
            this.UpdateBut = new DevExpress.XtraEditors.SimpleButton();
            this.subj_type_name = new DevExpress.XtraEditors.TextEdit();
            this.subj_type_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.subj_type_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subj_type_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // CloseBut
            // 
            this.CloseBut.Location = new System.Drawing.Point(228, 166);
            this.CloseBut.Name = "CloseBut";
            this.CloseBut.Size = new System.Drawing.Size(80, 25);
            this.CloseBut.TabIndex = 7;
            this.CloseBut.Text = "关闭";
            this.CloseBut.Click += new System.EventHandler(this.CloseBut_Click);
            // 
            // UpdateBut
            // 
            this.UpdateBut.Location = new System.Drawing.Point(96, 166);
            this.UpdateBut.Name = "UpdateBut";
            this.UpdateBut.Size = new System.Drawing.Size(80, 25);
            this.UpdateBut.TabIndex = 8;
            this.UpdateBut.Text = "修改";
            this.UpdateBut.Click += new System.EventHandler(this.UpdateBut_Click);
            // 
            // subj_type_name
            // 
            this.subj_type_name.Location = new System.Drawing.Point(154, 99);
            this.subj_type_name.Name = "subj_type_name";
            this.subj_type_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.subj_type_name.Properties.Appearance.Options.UseBackColor = true;
            this.subj_type_name.Size = new System.Drawing.Size(200, 24);
            this.subj_type_name.TabIndex = 5;
            // 
            // subj_type_code
            // 
            this.subj_type_code.Location = new System.Drawing.Point(154, 44);
            this.subj_type_code.Name = "subj_type_code";
            this.subj_type_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.subj_type_code.Properties.Appearance.Options.UseBackColor = true;
            this.subj_type_code.Size = new System.Drawing.Size(200, 24);
            this.subj_type_code.TabIndex = 6;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(58, 105);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 18);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "类别名称";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(58, 47);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 18);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "类别编码";
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 255);
            this.Controls.Add(this.CloseBut);
            this.Controls.Add(this.UpdateBut);
            this.Controls.Add(this.subj_type_name);
            this.Controls.Add(this.subj_type_code);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Name = "update";
            this.Text = "修改科目类别";
            this.Load += new System.EventHandler(this.update_Load);
            ((System.ComponentModel.ISupportInitialize)(this.subj_type_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subj_type_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton CloseBut;
        private DevExpress.XtraEditors.SimpleButton UpdateBut;
        private DevExpress.XtraEditors.TextEdit subj_type_name;
        private DevExpress.XtraEditors.TextEdit subj_type_code;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}