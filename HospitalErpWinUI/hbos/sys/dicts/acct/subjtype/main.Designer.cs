﻿namespace HospitalErpWinUI.hbos.sys.dicts.acct.subjtype
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.subj_type_code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.subj_type_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this._co_code = new DevExpress.XtraGrid.Columns.GridColumn();
            this._type_code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.DeleteBut = new DevExpress.XtraEditors.SimpleButton();
            this.InsertBut = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.c_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.QueryBut = new DevExpress.XtraEditors.SimpleButton();
            this.u_parent = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.u_name = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_parent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl4);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Controls.Add(this.panelControl3);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Padding = new System.Windows.Forms.Padding(9, 0, 0, 0);
            this.panelControl1.Size = new System.Drawing.Size(1104, 630);
            this.panelControl1.TabIndex = 0;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.gridControl1);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(11, 66);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1091, 562);
            this.panelControl4.TabIndex = 7;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemHyperLinkEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1087, 558);
            this.gridControl1.TabIndex = 9;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.subj_type_code,
            this.subj_type_name,
            this._co_code,
            this._type_code});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "选择";
            this.gridColumn1.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 209;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // subj_type_code
            // 
            this.subj_type_code.Caption = "类别编码";
            this.subj_type_code.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.subj_type_code.FieldName = "subj_type_code";
            this.subj_type_code.Name = "subj_type_code";
            this.subj_type_code.Visible = true;
            this.subj_type_code.VisibleIndex = 1;
            this.subj_type_code.Width = 428;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // subj_type_name
            // 
            this.subj_type_name.Caption = "类别名称";
            this.subj_type_name.FieldName = "subj_type_name";
            this.subj_type_name.Name = "subj_type_name";
            this.subj_type_name.Visible = true;
            this.subj_type_name.VisibleIndex = 2;
            this.subj_type_name.Width = 432;
            // 
            // _co_code
            // 
            this._co_code.Caption = "gridColumn4";
            this._co_code.FieldName = "_co_code";
            this._co_code.Name = "_co_code";
            // 
            // _type_code
            // 
            this._type_code.Caption = "gridColumn5";
            this._type_code.FieldName = "_type_code";
            this._type_code.Name = "_type_code";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.DeleteBut);
            this.panelControl2.Controls.Add(this.InsertBut);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(11, 34);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1091, 32);
            this.panelControl2.TabIndex = 6;
            // 
            // DeleteBut
            // 
            this.DeleteBut.Location = new System.Drawing.Point(93, 5);
            this.DeleteBut.Name = "DeleteBut";
            this.DeleteBut.Size = new System.Drawing.Size(70, 23);
            this.DeleteBut.TabIndex = 1;
            this.DeleteBut.Text = "删除";
            this.DeleteBut.Click += new System.EventHandler(this.DeleteBut_Click);
            // 
            // InsertBut
            // 
            this.InsertBut.Location = new System.Drawing.Point(12, 6);
            this.InsertBut.Name = "InsertBut";
            this.InsertBut.Size = new System.Drawing.Size(70, 23);
            this.InsertBut.TabIndex = 0;
            this.InsertBut.Text = "新增";
            this.InsertBut.Click += new System.EventHandler(this.InsertBut_Click);
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.c_code);
            this.panelControl3.Controls.Add(this.QueryBut);
            this.panelControl3.Controls.Add(this.u_parent);
            this.panelControl3.Controls.Add(this.label3);
            this.panelControl3.Controls.Add(this.u_name);
            this.panelControl3.Controls.Add(this.label2);
            this.panelControl3.Controls.Add(this.label1);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(11, 2);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1091, 32);
            this.panelControl3.TabIndex = 3;
            // 
            // c_code
            // 
            this.c_code.Location = new System.Drawing.Point(74, 4);
            this.c_code.Name = "c_code";
            this.c_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.c_code.Size = new System.Drawing.Size(175, 20);
            this.c_code.TabIndex = 0;
            this.c_code.SelectedIndexChanged += new System.EventHandler(this.c_code_SelectedIndexChanged);
            // 
            // QueryBut
            // 
            this.QueryBut.Location = new System.Drawing.Point(657, 4);
            this.QueryBut.Name = "QueryBut";
            this.QueryBut.Size = new System.Drawing.Size(70, 23);
            this.QueryBut.TabIndex = 3;
            this.QueryBut.Text = "查询";
            this.QueryBut.Click += new System.EventHandler(this.QueryBut_Click);
            // 
            // u_parent
            // 
            this.u_parent.Location = new System.Drawing.Point(515, 4);
            this.u_parent.Name = "u_parent";
            this.u_parent.Size = new System.Drawing.Size(131, 20);
            this.u_parent.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(457, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "类别名称";
            // 
            // u_name
            // 
            this.u_name.Location = new System.Drawing.Point(316, 4);
            this.u_name.Name = "u_name";
            this.u_name.Size = new System.Drawing.Size(131, 20);
            this.u_name.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(257, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "类别编码";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "科目体系";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1104, 630);
            this.Controls.Add(this.panelControl1);
            this.Name = "main";
            this.Text = "科目类别";
            this.Load += new System.EventHandler(this.main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_parent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton QueryBut;
        private DevExpress.XtraEditors.TextEdit u_parent;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit u_name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton DeleteBut;
        private DevExpress.XtraEditors.SimpleButton InsertBut;
        private DevExpress.XtraEditors.ComboBoxEdit c_code;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn subj_type_code;
        private DevExpress.XtraGrid.Columns.GridColumn subj_type_name;
        private DevExpress.XtraGrid.Columns.GridColumn _co_code;
        private DevExpress.XtraGrid.Columns.GridColumn _type_code;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;

    }
}