﻿namespace HospitalErpWinUI.hbos.sys.dicts.acct.subjcode
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.acct_subj_code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.acct_subj_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Column1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Column2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Column3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.super_code = new DevExpress.XtraGrid.Columns.GridColumn();
            this._co_code = new DevExpress.XtraGrid.Columns.GridColumn();
            this._acct_subj_code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Addbut = new DevExpress.XtraEditors.SimpleButton();
            this.DeleteBut = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.u_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.sysDictsAcctSubjCode_select = new DevExpress.XtraEditors.SimpleButton();
            this.u_parent = new DevExpress.XtraEditors.TextEdit();
            this.会计科目名称 = new DevExpress.XtraEditors.LabelControl();
            this.u_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.科目体系 = new DevExpress.XtraEditors.LabelControl();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_parent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.gridControl1);
            this.panelControl1.Controls.Add(this.panel1);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.panelControl1.Size = new System.Drawing.Size(1262, 675);
            this.panelControl1.TabIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(12, 70);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemHyperLinkEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1248, 603);
            this.gridControl1.TabIndex = 2;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.acct_subj_code,
            this.acct_subj_name,
            this.Column1,
            this.Column2,
            this.Column3,
            this.super_code,
            this._co_code,
            this._acct_subj_code});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "选择";
            this.gridColumn1.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn1.FieldName = "checked";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // acct_subj_code
            // 
            this.acct_subj_code.Caption = "会计科目编码";
            this.acct_subj_code.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.acct_subj_code.FieldName = "acct_subj_code";
            this.acct_subj_code.Name = "acct_subj_code";
            this.acct_subj_code.Visible = true;
            this.acct_subj_code.VisibleIndex = 1;
            // 
            // acct_subj_name
            // 
            this.acct_subj_name.Caption = "会计科目名称";
            this.acct_subj_name.FieldName = "acct_subj_name";
            this.acct_subj_name.Name = "acct_subj_name";
            this.acct_subj_name.Visible = true;
            this.acct_subj_name.VisibleIndex = 2;
            // 
            // Column1
            // 
            this.Column1.Caption = "科目类别";
            this.Column1.FieldName = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.Visible = true;
            this.Column1.VisibleIndex = 3;
            // 
            // Column2
            // 
            this.Column2.Caption = "科目性质";
            this.Column2.FieldName = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.Visible = true;
            this.Column2.VisibleIndex = 4;
            // 
            // Column3
            // 
            this.Column3.Caption = "余额方向";
            this.Column3.FieldName = "Column3";
            this.Column3.Name = "Column3";
            this.Column3.Visible = true;
            this.Column3.VisibleIndex = 5;
            // 
            // super_code
            // 
            this.super_code.Caption = "上级编码";
            this.super_code.FieldName = "super_code";
            this.super_code.Name = "super_code";
            this.super_code.Visible = true;
            this.super_code.VisibleIndex = 6;
            // 
            // _co_code
            // 
            this._co_code.Caption = "gridColumn2";
            this._co_code.FieldName = "_co_code";
            this._co_code.Name = "_co_code";
            // 
            // _acct_subj_code
            // 
            this._acct_subj_code.Caption = "gridColumn3";
            this._acct_subj_code.FieldName = "_acct_subj_code";
            this._acct_subj_code.Name = "_acct_subj_code";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Addbut);
            this.panel1.Controls.Add(this.DeleteBut);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(12, 36);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1248, 34);
            this.panel1.TabIndex = 1;
            // 
            // Addbut
            // 
            this.Addbut.Location = new System.Drawing.Point(5, 5);
            this.Addbut.Name = "Addbut";
            this.Addbut.Size = new System.Drawing.Size(75, 23);
            this.Addbut.TabIndex = 1;
            this.Addbut.Text = "新增";
            this.Addbut.Click += new System.EventHandler(this.Addbut_Click);
            // 
            // DeleteBut
            // 
            this.DeleteBut.Location = new System.Drawing.Point(86, 6);
            this.DeleteBut.Name = "DeleteBut";
            this.DeleteBut.Size = new System.Drawing.Size(75, 23);
            this.DeleteBut.TabIndex = 0;
            this.DeleteBut.Text = "删除";
            this.DeleteBut.Click += new System.EventHandler(this.DeleteBut_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.u_code);
            this.panelControl2.Controls.Add(this.sysDictsAcctSubjCode_select);
            this.panelControl2.Controls.Add(this.u_parent);
            this.panelControl2.Controls.Add(this.会计科目名称);
            this.panelControl2.Controls.Add(this.u_name);
            this.panelControl2.Controls.Add(this.labelControl2);
            this.panelControl2.Controls.Add(this.科目体系);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(12, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1248, 34);
            this.panelControl2.TabIndex = 0;
            // 
            // u_code
            // 
            this.u_code.Location = new System.Drawing.Point(72, 6);
            this.u_code.Name = "u_code";
            this.u_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_code.Size = new System.Drawing.Size(200, 24);
            this.u_code.TabIndex = 3;
            this.u_code.SelectedIndexChanged += new System.EventHandler(this.c_code_SelectedIndexChanged);
            // 
            // sysDictsAcctSubjCode_select
            // 
            this.sysDictsAcctSubjCode_select.Location = new System.Drawing.Point(822, 4);
            this.sysDictsAcctSubjCode_select.Name = "sysDictsAcctSubjCode_select";
            this.sysDictsAcctSubjCode_select.Size = new System.Drawing.Size(75, 23);
            this.sysDictsAcctSubjCode_select.TabIndex = 2;
            this.sysDictsAcctSubjCode_select.Text = "查询";
            this.sysDictsAcctSubjCode_select.Click += new System.EventHandler(this.queryBut_Click);
            // 
            // u_parent
            // 
            this.u_parent.Location = new System.Drawing.Point(636, 5);
            this.u_parent.Name = "u_parent";
            this.u_parent.Size = new System.Drawing.Size(150, 24);
            this.u_parent.TabIndex = 1;
            // 
            // 会计科目名称
            // 
            this.会计科目名称.Location = new System.Drawing.Point(539, 8);
            this.会计科目名称.Name = "会计科目名称";
            this.会计科目名称.Size = new System.Drawing.Size(90, 18);
            this.会计科目名称.TabIndex = 0;
            this.会计科目名称.Text = "会计科目名称";
            // 
            // u_name
            // 
            this.u_name.Location = new System.Drawing.Point(378, 6);
            this.u_name.Name = "u_name";
            this.u_name.Size = new System.Drawing.Size(150, 24);
            this.u_name.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(281, 9);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(90, 18);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "会计科目编码";
            // 
            // 科目体系
            // 
            this.科目体系.Location = new System.Drawing.Point(5, 9);
            this.科目体系.Name = "科目体系";
            this.科目体系.Size = new System.Drawing.Size(60, 18);
            this.科目体系.TabIndex = 0;
            this.科目体系.Text = "科目体系";
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1262, 675);
            this.Controls.Add(this.panelControl1);
            this.Name = "main";
            this.Text = "科目编码";
            this.Load += new System.EventHandler(this.main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_parent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn acct_subj_code;
        private DevExpress.XtraGrid.Columns.GridColumn acct_subj_name;
        private DevExpress.XtraGrid.Columns.GridColumn Column1;
        private DevExpress.XtraGrid.Columns.GridColumn Column2;
        private DevExpress.XtraGrid.Columns.GridColumn Column3;
        private DevExpress.XtraGrid.Columns.GridColumn super_code;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SimpleButton Addbut;
        private DevExpress.XtraEditors.SimpleButton DeleteBut;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton sysDictsAcctSubjCode_select;
        private DevExpress.XtraEditors.TextEdit u_parent;
        private DevExpress.XtraEditors.LabelControl 会计科目名称;
        private DevExpress.XtraEditors.TextEdit u_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl 科目体系;
        private DevExpress.XtraEditors.ComboBoxEdit u_code;
        private DevExpress.XtraGrid.Columns.GridColumn _co_code;
        private DevExpress.XtraGrid.Columns.GridColumn _acct_subj_code;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
    }
}