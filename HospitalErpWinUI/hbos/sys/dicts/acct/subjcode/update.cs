﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.acct.subjcode
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        private string _c_code;
        private string _s_code;
        private string _parent;
        public update()
        {
            InitializeComponent();
            Init();
            _parent = "";
        }
        public update(DaoHelper helper, List<Perm> perms, string c_ode, string s_code)
        {
            InitializeComponent();
            this._helper = helper;
            this._perms = perms;
            this._c_code = c_ode;
            this._s_code = s_code;
            _parent = "";
            Init();
        }

        #region 其他
        private void Init()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.acct_subj_code.Properties.MaxLength = 30;
            this.acct_subj_name.Properties.MaxLength = 50;
            this.subj_type_code.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.subj_nature_code.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.direction.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
        }

        private string GetParenCode(string format, string code, bool allowLetter)
        {


            code = Regex.Replace(code, @"(^\s+)|\s+$", "");
            string regStr = @"^(\d)*$";
            if (allowLetter == null && allowLetter)
            {
                regStr = @"^([a-zA-Z0-9_.])*$";
            }
            if (string.IsNullOrEmpty(code))
                return null;
            if (Validator.IsMatch(code, regStr) == false)
            {
                return null;
            }
            string[] cs = format.Split('-');
            int len = 0, plen = 0;
            for (int i = 0; len < code.Length && i < cs.Length; i++)
            {
                plen = len;
                len += Convert.ToInt16(cs[i]);
            }
            if (len != code.Length)
                return null;
            if (plen == 0)
                return  "";
            else
                return code.Substring(0, plen);


        }


        private bool Validate()
        {

            if (string.IsNullOrEmpty(acct_subj_code.Text))
            {
                MessageForm.Warning("会计科目编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(acct_subj_name.Text))
            {
                MessageForm.Warning("会计科目名称不能为空!");
                return false;
            }

            if (FormHelper.Text_Length(acct_subj_code.Text) > 30)
            {
                MessageForm.Warning("会计科目编码字符串长度超过数据库允许的长度30!");
                return false;
            }
            if (FormHelper.Text_Length(acct_subj_name.Text) > 50)
            {
                MessageForm.Warning("会计科目名称字符串长度超过数据库允许的长度50!");
                return false;
            }
            string format = this.GetAcctCodeFormat();

            string parentCode = this.GetParenCode(format, acct_subj_code.Text.Trim(), false);
            if (parentCode== null)
            {
                MessageForm.Show("不正确的编码规则，请使用" + format + "形式!");
                return false;
            }
            else
            {
                _parent = parentCode;
            }
            return true;
        }
            
        #endregion

        #region 事件

        private void update_Load(object sender, EventArgs e)
        {
            try
            {
                LoadsAcctSubjClassList();
                LoadSubjNatureCode();
                LoadSubjDirection();
                LoadData();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void CloseBut_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void updateBut_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Update();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
        #endregion

        #region 访问数据

        private void LoadsAcctSubjClassList()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysAcctSubjClass_list";
            paramters.Add("co_code", _c_code);
            serviceParamter.Paramters = paramters;

            List<string> objs = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            subj_type_code.Properties.Items.AddRange(objs);
            subj_type_code.SelectedItem = objs[0];
        }
        private void LoadSubjNatureCode()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysAcctSubjNature_list";

            serviceParamter.Paramters = paramters;

            List<string> objs = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            subj_nature_code.Properties.Items.AddRange(objs);
            subj_nature_code.SelectedItem = objs[0];
        }
        private void LoadSubjDirection()
        {
            // dict_subj_direction
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "dict_subj_direction";

            serviceParamter.Paramters = paramters;


            List<CodeValue> objs = _helper.ReadDictForData(serviceParamter);
            List<string> codeValues = new List<string>();
            foreach (CodeValue obj in objs)
            {

                codeValues.Add(obj.Code + " " + obj.Value);
            }
            direction.Properties.Items.AddRange(codeValues);
            direction.SelectedItem = codeValues[0];
        }
        private string GetAcctCodeFormat()
        {
            string result = string.Empty;
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sys_acct_code_format";
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            paramters.Add("co_code", _c_code);
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            if (dt.Rows.Count > 0)
            {
                result = dt.Rows[0][0].ToString();
            }

            return result;
        }

        private void LoadData()
        {
            StringBuilder sqlstr = new StringBuilder();
            	
			 
            sqlstr.Append(@"select acct_subj_code,acct_subj_name, subj_type_code, subj_nature_code,direction,super_code ");
            sqlstr.Append(@" from sys_acct_subj where co_code='{0}' and acct_subj_code='{1}' ");

            DataTable dt = _helper.ExecReadSql(sqlstr.ToString().Replace("{0}", _c_code).Replace("{1}", _s_code));
            if (dt != null && dt.Rows.Count != 0)
            {
                this.acct_subj_code.Text = dt.Rows[0]["acct_subj_code"].ToString();
                this.acct_subj_name.Text = dt.Rows[0]["acct_subj_name"].ToString();


                FormHelper.ComboBoxeditAssignment(subj_type_code, dt.Rows[0]["subj_type_code"].ToString());
              //  FormHelper.ComboBoxeditAssignment(subj_type_code,"01|||sss");
                FormHelper.ComboBoxeditAssignment(subj_nature_code, dt.Rows[0]["subj_nature_code"].ToString());
                FormHelper.ComboBoxeditAssignment(direction, dt.Rows[0]["direction"].ToString().ToLower().Equals("true")?"1":"0");
            
            }

        }

        private bool Update()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sysDictsAcctSubjCode_update";

            dict.Add("c_code", _c_code);
            dict.Add("S_old_code", acct_subj_code.Text);
            dict.Add("s_code", acct_subj_code.Text);
            dict.Add("s_name", acct_subj_name.Text);
            dict.Add("s_type", FormHelper.GetValueForComboBoxEdit(subj_type_code));
            dict.Add("s_nature", FormHelper.GetValueForComboBoxEdit(subj_nature_code));
            dict.Add("direction", FormHelper.GetValueForComboBoxEdit(direction));
            dict.Add("s_super_code", _parent);
            serviceParamter.Paramters = dict;
            return _helper.WirteSql(serviceParamter);
        }
        #endregion

    }
}
