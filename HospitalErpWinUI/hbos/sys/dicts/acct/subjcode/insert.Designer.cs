﻿namespace HospitalErpWinUI.hbos.sys.dicts.acct.subjcode
{
    partial class insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CloseBut = new DevExpress.XtraEditors.SimpleButton();
            this.AddBut = new DevExpress.XtraEditors.SimpleButton();
            this.direction = new DevExpress.XtraEditors.ComboBoxEdit();
            this.subj_nature_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.subj_type_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.acct_subj_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.acct_subj_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.direction.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subj_nature_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subj_type_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // CloseBut
            // 
            this.CloseBut.Location = new System.Drawing.Point(307, 286);
            this.CloseBut.Name = "CloseBut";
            this.CloseBut.Size = new System.Drawing.Size(80, 25);
            this.CloseBut.TabIndex = 14;
            this.CloseBut.Text = "关闭";
            this.CloseBut.Click += new System.EventHandler(this.CloseBut_Click);
            // 
            // AddBut
            // 
            this.AddBut.Location = new System.Drawing.Point(142, 286);
            this.AddBut.Name = "AddBut";
            this.AddBut.Size = new System.Drawing.Size(80, 25);
            this.AddBut.TabIndex = 15;
            this.AddBut.Text = "新增";
            this.AddBut.Click += new System.EventHandler(this.AddBut_Click);
            // 
            // direction
            // 
            this.direction.Location = new System.Drawing.Point(231, 227);
            this.direction.Name = "direction";
            this.direction.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.direction.Properties.Appearance.Options.UseBackColor = true;
            this.direction.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.direction.Size = new System.Drawing.Size(200, 24);
            this.direction.TabIndex = 11;
            // 
            // subj_nature_code
            // 
            this.subj_nature_code.Location = new System.Drawing.Point(231, 182);
            this.subj_nature_code.Name = "subj_nature_code";
            this.subj_nature_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.subj_nature_code.Properties.Appearance.Options.UseBackColor = true;
            this.subj_nature_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.subj_nature_code.Size = new System.Drawing.Size(200, 24);
            this.subj_nature_code.TabIndex = 12;
            // 
            // subj_type_code
            // 
            this.subj_type_code.Location = new System.Drawing.Point(231, 139);
            this.subj_type_code.Name = "subj_type_code";
            this.subj_type_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.subj_type_code.Properties.Appearance.Options.UseBackColor = true;
            this.subj_type_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.subj_type_code.Size = new System.Drawing.Size(200, 24);
            this.subj_type_code.TabIndex = 13;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(92, 230);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 18);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "余额方向";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(92, 185);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(90, 18);
            this.labelControl4.TabIndex = 5;
            this.labelControl4.Text = "会计科目性质";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(92, 142);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(90, 18);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "会计科目类别";
            // 
            // acct_subj_name
            // 
            this.acct_subj_name.Location = new System.Drawing.Point(231, 94);
            this.acct_subj_name.Name = "acct_subj_name";
            this.acct_subj_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.acct_subj_name.Properties.Appearance.Options.UseBackColor = true;
            this.acct_subj_name.Size = new System.Drawing.Size(200, 24);
            this.acct_subj_name.TabIndex = 9;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(92, 97);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(90, 18);
            this.labelControl2.TabIndex = 7;
            this.labelControl2.Text = "会计科目名称";
            // 
            // acct_subj_code
            // 
            this.acct_subj_code.Location = new System.Drawing.Point(231, 53);
            this.acct_subj_code.Name = "acct_subj_code";
            this.acct_subj_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.acct_subj_code.Properties.Appearance.Options.UseBackColor = true;
            this.acct_subj_code.Size = new System.Drawing.Size(200, 24);
            this.acct_subj_code.TabIndex = 10;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(92, 56);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(90, 18);
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "会计科目编码";
            // 
            // insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(522, 365);
            this.Controls.Add(this.CloseBut);
            this.Controls.Add(this.AddBut);
            this.Controls.Add(this.direction);
            this.Controls.Add(this.subj_nature_code);
            this.Controls.Add(this.subj_type_code);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.acct_subj_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.acct_subj_code);
            this.Controls.Add(this.labelControl1);
            this.Name = "insert";
            this.Text = "新增会计科目";
            this.Load += new System.EventHandler(this.insert_Load);
            ((System.ComponentModel.ISupportInitialize)(this.direction.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subj_nature_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subj_type_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton CloseBut;
        private DevExpress.XtraEditors.SimpleButton AddBut;
        private DevExpress.XtraEditors.ComboBoxEdit direction;
        private DevExpress.XtraEditors.ComboBoxEdit subj_nature_code;
        private DevExpress.XtraEditors.ComboBoxEdit subj_type_code;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit acct_subj_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit acct_subj_code;
        private DevExpress.XtraEditors.LabelControl labelControl1;


    }
}