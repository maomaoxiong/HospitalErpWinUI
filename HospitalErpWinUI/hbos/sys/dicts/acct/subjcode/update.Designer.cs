﻿namespace HospitalErpWinUI.hbos.sys.dicts.acct.subjcode
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.acct_subj_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.subj_type_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.subj_nature_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.direction = new DevExpress.XtraEditors.ComboBoxEdit();
            this.updateBut = new DevExpress.XtraEditors.SimpleButton();
            this.CloseBut = new DevExpress.XtraEditors.SimpleButton();
            this.acct_subj_code = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subj_type_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subj_nature_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.direction.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(84, 48);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(90, 18);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "会计科目编码";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(84, 89);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(90, 18);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "会计科目名称";
            // 
            // acct_subj_name
            // 
            this.acct_subj_name.Location = new System.Drawing.Point(223, 86);
            this.acct_subj_name.Name = "acct_subj_name";
            this.acct_subj_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.acct_subj_name.Properties.Appearance.Options.UseBackColor = true;
            this.acct_subj_name.Size = new System.Drawing.Size(200, 24);
            this.acct_subj_name.TabIndex = 1;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(84, 134);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(90, 18);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "会计科目类别";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(84, 177);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(90, 18);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "会计科目性质";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(84, 222);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 18);
            this.labelControl5.TabIndex = 0;
            this.labelControl5.Text = "余额方向";
            // 
            // subj_type_code
            // 
            this.subj_type_code.Location = new System.Drawing.Point(223, 131);
            this.subj_type_code.Name = "subj_type_code";
            this.subj_type_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.subj_type_code.Properties.Appearance.Options.UseBackColor = true;
            this.subj_type_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.subj_type_code.Size = new System.Drawing.Size(200, 24);
            this.subj_type_code.TabIndex = 2;
            // 
            // subj_nature_code
            // 
            this.subj_nature_code.Location = new System.Drawing.Point(223, 174);
            this.subj_nature_code.Name = "subj_nature_code";
            this.subj_nature_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.subj_nature_code.Properties.Appearance.Options.UseBackColor = true;
            this.subj_nature_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.subj_nature_code.Size = new System.Drawing.Size(200, 24);
            this.subj_nature_code.TabIndex = 2;
            // 
            // direction
            // 
            this.direction.Location = new System.Drawing.Point(223, 219);
            this.direction.Name = "direction";
            this.direction.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.direction.Properties.Appearance.Options.UseBackColor = true;
            this.direction.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.direction.Size = new System.Drawing.Size(200, 24);
            this.direction.TabIndex = 2;
            // 
            // updateBut
            // 
            this.updateBut.Location = new System.Drawing.Point(134, 278);
            this.updateBut.Name = "updateBut";
            this.updateBut.Size = new System.Drawing.Size(80, 25);
            this.updateBut.TabIndex = 3;
            this.updateBut.Text = "修改";
            this.updateBut.Click += new System.EventHandler(this.updateBut_Click);
            // 
            // CloseBut
            // 
            this.CloseBut.Location = new System.Drawing.Point(299, 278);
            this.CloseBut.Name = "CloseBut";
            this.CloseBut.Size = new System.Drawing.Size(80, 25);
            this.CloseBut.TabIndex = 3;
            this.CloseBut.Text = "关闭";
            this.CloseBut.Click += new System.EventHandler(this.CloseBut_Click);
            // 
            // acct_subj_code
            // 
            this.acct_subj_code.Enabled = false;
            this.acct_subj_code.Location = new System.Drawing.Point(223, 45);
            this.acct_subj_code.Name = "acct_subj_code";
            this.acct_subj_code.Size = new System.Drawing.Size(200, 24);
            this.acct_subj_code.TabIndex = 4;
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(522, 365);
            this.Controls.Add(this.acct_subj_code);
            this.Controls.Add(this.CloseBut);
            this.Controls.Add(this.updateBut);
            this.Controls.Add(this.direction);
            this.Controls.Add(this.subj_nature_code);
            this.Controls.Add(this.subj_type_code);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.acct_subj_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Name = "update";
            this.Text = "修改会计科目";
            this.Load += new System.EventHandler(this.update_Load);
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subj_type_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subj_nature_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.direction.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit acct_subj_name;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.ComboBoxEdit subj_type_code;
        private DevExpress.XtraEditors.ComboBoxEdit subj_nature_code;
        private DevExpress.XtraEditors.ComboBoxEdit direction;
        private DevExpress.XtraEditors.SimpleButton updateBut;
        private DevExpress.XtraEditors.SimpleButton CloseBut;
        private DevExpress.XtraEditors.TextEdit acct_subj_code;
    }
}