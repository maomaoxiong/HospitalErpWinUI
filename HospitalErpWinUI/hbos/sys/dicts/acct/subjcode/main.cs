﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraGrid;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.acct.subjcode
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;      
        public List<Perm> _perms { get; set; }

        private string _c_code;
        private DataTable _subjCode;
        public main()
        {
            _helper = new DaoHelper();
            InitializeComponent();
        }

        #region 其他
        private void Init()
        {

            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.StartPosition = FormStartPosition.CenterScreen;
        }
        private bool DelData_check_all(List<string> acct_subj_codes)
        {
           
            if (acct_subj_codes.Count == 0)
            {
                return true;
            }
            foreach (string acct_subj_code in acct_subj_codes)
            {
                if (DelData_check(acct_subj_code))
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        #region 访问数据
        private void LoadSubjType()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysAcctSubjType_list";
            serviceParamter.Paramters = paramters;

            List<string> arrays = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                arrays.Add(codeValue.Code + " " + codeValue.Value);
            }
            u_code.Properties.Items.AddRange(arrays);
            u_code.SelectedItem = arrays[0];
        }
        private void Query()
        {
            //sysDictsAcctSubjSys_select
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sysDictsAcctSubjCode_select";

            Dictionary<string, string> dict = new Dictionary<string, string>();
            string[] strArray1 = System.Text.RegularExpressions.Regex.Split(u_code.Text.Trim(), @"\s{1,}");

            _c_code = strArray1[0];
            dict.Add("c_code", strArray1[0]);
            dict.Add("s_code", u_name.Text);
            dict.Add("s_name", u_parent.Text);
            serviceParamter.Paramters = dict;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            _subjCode = dt;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
               string[] arragy1 = dt.Rows[i]["Column1"].ToString().Split('|');
               string[] arragy2 = dt.Rows[i]["Column2"].ToString().Split('|');
               string[] arragy3 = dt.Rows[i]["Column3"].ToString().Split('|');

                 dt.Rows[i]["Column1"] = arragy1[3].ToString();
                 dt.Rows[i]["Column2"] = arragy2[3].ToString();
                 dt.Rows[i]["Column3"] = arragy3[3].ToString();


            }
            //    _user = dt;
            gridControl1.DataSource = _subjCode;

        }
        private bool Delete(string acct_subj_code)
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysDictsAcctSubjCode_delete";

            paramters.Add("co_code", _c_code);
            paramters.Add("s_code", acct_subj_code);
          
            serviceParamter.Paramters = paramters;
            bool result = false;
            try
            {
                _helper.WirteSql(serviceParamter);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }

        //返回true 表示检查不通过
        private bool DelData_check(string acct_subj_code)  
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sysDictsAcctSubjCode_delete_check";
            
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("acct_subj_code", acct_subj_code);
          
            serviceParamter.Paramters = dict;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            int result = Convert.ToInt16(dt.Rows[0][0]);
            if (result == 1)
            {
                MessageForm.Show(acct_subj_code+"科目为其他科目的上级科目,不能删除");
            }
            return  result == 1;

        }
        
        #endregion

        #region 事件
        private void c_code_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void queryBut_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
               
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void Addbut_Click(object sender, EventArgs e)
        {
            try
            {
                insert insertFrm = new insert(this._helper, this._perms, _c_code);

                if (insertFrm.ShowDialog() == DialogResult.OK)
                {
                    Query();
                }
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
           
        }

        private void DeleteBut_Click(object sender, EventArgs e)
        {
             // sysDictsAcctSubjSys_delete
            bool value = false;
            string strSelected = string.Empty;

            try
            {
                List<string> objs = new List<string>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    value = Convert.ToBoolean(gridView1.GetDataRow(i)["checked"]);
                    if (value)
                    {
                        string subj_type_code = gridView1.GetDataRow(i)["acct_subj_code"].ToString();
                    //    string co_code = gridView1.GetDataRow(i)["_co_code"].ToString();
                       objs.Add(subj_type_code);

                    }
                }
                if (objs.Count == 0)
                {
                    MessageForm.Show("请选择要删除的数据!");
                    return;
                }
                if (MessageForm.ShowMsg("确认是否删除?", MsgType.YesNo) == DialogResult.Yes)
                {

                    if (!DelData_check_all(objs))
                    {
                        foreach (string obj in objs)
                        {

                            if (!Delete(obj))
                            {
                                return;
                            }
                        }
                        //       MessageForm.Show("操作成功!");
                        Query();
                    }
                  
                }

            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void main_Load(object sender, EventArgs e)
        {
            try
            {
                LoadSubjType();
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, DevExpress.XtraEditors.Controls.OpenLinkEventArgs e)
        {
            if (gridView1.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                int index = gridView1.GetDataSourceRowIndex(gridView1.FocusedRowHandle);
                string co_code = _c_code;// _subjtype.Rows[index]["_co_code"].ToString();
                string acct_subj_code = _subjCode.Rows[index]["acct_subj_code"].ToString();

                update frm = new update(this._helper, this._perms, co_code, acct_subj_code);

                if (frm.ShowDialog() == DialogResult.No)
                {
                    MessageForm.Warning("修改失败！");
                }
                Query();
            }
        }
        #endregion

        

     

       
    }
}
