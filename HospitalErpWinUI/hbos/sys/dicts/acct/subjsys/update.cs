﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.acct.subjsys
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        private string _c_code { get; set; }

        public update(DaoHelper helper, List<Perm> perms, string c_code)
        {
            _helper = helper;
            _perms = perms;
            _c_code = c_code;
            InitializeComponent();
            Init();

        }

        public update()
        {
            InitializeComponent();
        }

        #region 事件
        private void close_buttn_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
        private void update_button_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                updateSubjsys();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
        private void update_Load(object sender, EventArgs e)
        {
            try 
            { 
                 LoadKind();
                 LoadData();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
           
        }
        #endregion

        #region 其他
        private void Init()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.c_code.Properties.MaxLength = 10;
            this.c_name.Properties.MaxLength = 40;
            this.c_rule.Properties.MaxLength = 20;
            this.c_kind.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
        }

        private bool Validate()
        {

            if (string.IsNullOrEmpty(c_code.Text))
            {
                MessageForm.Warning("科目体系编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(c_name.Text))
            {
                MessageForm.Warning("科目体系名称不能为空!");
                return false;
            }

            if (string.IsNullOrEmpty(c_rule.Text))
            {
                MessageForm.Warning("科目编码规则不能为空!");
                return false;
            }

            if (FormHelper.Text_Length(c_code.Text) > 10)
            {
                MessageForm.Warning("科目体系编码字符串长度超过数据库允许的长度10!");
                return false;
            }
            if (FormHelper.Text_Length(c_name.Text) > 40)
            {
                MessageForm.Warning("科目体系名称字符串长度超过数据库允许的长度40!");
                return false;
            }
            if (FormHelper.Text_Length(c_rule.Text) > 20)
            {
                MessageForm.Warning("科目编码规则字符串长度超过数据库允许的长度20!");
                return false;
            }
            if (!isRuleCode(c_rule.Text))
            {
                MessageForm.Warning("不正确的编码规则，请用XX-XX-XX ...形式!");
                return false;
            }
            return true;
        }

        private bool isRuleCode(string value)
        {
            var va = value.Replace("-", "");
            if (!value.Contains("-") || value.Contains("--"))
            {
                return false;
            }
            //    if (Validator.IsMatch(va, @"^\d{n}$"))  "^(\\d)*$"
            if (Validator.IsMatch(va, @"^(\\d)*$"))
            {
                return false;
            }
            if (value.Substring(0, 1).Equals("-") || value.Substring(value.Length - 1, 1).Equals("-"))
            {
                return false;
            }
            return true;
        }
        #endregion 
        
        #region 访问数据
        private void LoadKind()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysAcctSubjKind_list";
            serviceParamter.Paramters = paramters;

            List<string> companys = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                companys.Add(codeValue.Code + " " + codeValue.Value);
            }
            c_kind.Properties.Items.AddRange(companys);
            c_kind.SelectedItem = companys[0];
        }
        private void LoadData()
        {
            StringBuilder sqlstr = new StringBuilder();
            sqlstr.Append(@"select t.co_code, t.co_name,t.acc_rule,t.kind_code,kind_name ");
            sqlstr.Append(@" FROM sys_co_type t	LEFT JOIN sys_co_kind k ON t.kind_code=k.kind_code ");
            sqlstr.Append(@" where co_code='?'");
           
            DataTable dt = _helper.ExecReadSql( sqlstr.ToString().Replace("?", _c_code));
            if (dt != null && dt.Rows.Count != 0)
            {
                this.c_code.Text = dt.Rows[0]["co_code"].ToString();
                this.c_name.Text = dt.Rows[0]["co_name"].ToString();
                this.c_rule.Text = dt.Rows[0]["acc_rule"].ToString();
                this.c_kind.Text = dt.Rows[0]["kind_code"].ToString() + " " + dt.Rows[0]["kind_name"].ToString();
            }
 
        }

        private bool updateSubjsys()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sysDictsAcctSubjSys_update";

            List<string> strConstrols = new List<string>();
            List<string> strCtrls = new List<string>();
            dict.Add("c_old_code", _c_code);
            strCtrls.Add("c_code");
            strCtrls.Add("c_name");
            strCtrls.Add("c_rule");
            strCtrls.Add("c_kind");
            FormHelper.GenerateParamerByControl(this.Controls, ref dict, strCtrls);
            serviceParamter.Paramters = dict;
            return _helper.WirteSql(serviceParamter);
        }
        #endregion

    }
}
