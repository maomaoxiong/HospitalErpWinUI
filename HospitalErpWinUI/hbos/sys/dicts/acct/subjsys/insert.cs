﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.acct.subjsys
{
    public partial class insert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        public insert()
        {
            InitializeComponent();
            Init();
        }
        public insert(DaoHelper helper, List<Perm> perms)
        {
            InitializeComponent();
            this._helper = helper;
            this._perms = perms;
            Init();
        }

        #region 其他
        private void Init()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.c_code.Properties.MaxLength = 10;
            this.c_name.Properties.MaxLength = 40;
            this.c_rule.Properties.MaxLength = 20;
            this.c_kind.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
        }

        private bool Validate()
        {
          
            if (string.IsNullOrEmpty(c_code.Text))
            {
                MessageForm.Warning("科目体系编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(c_name.Text))
            {
                MessageForm.Warning("科目体系名称不能为空!");
                return false;
            }
          
            if (string.IsNullOrEmpty(c_rule.Text))
            {
                MessageForm.Warning("科目编码规则不能为空!");
                return false;
            }

            if (FormHelper.Text_Length(c_code.Text) > 10)
            {
                MessageForm.Warning("科目体系编码字符串长度超过数据库允许的长度10!");
                return false;
            }
            if (FormHelper.Text_Length(c_name.Text) > 40)
            {
                MessageForm.Warning("科目体系名称字符串长度超过数据库允许的长度40!");
                return false;
            }
            if (FormHelper.Text_Length(c_rule.Text) > 20)
            {
                MessageForm.Warning("科目编码规则字符串长度超过数据库允许的长度20!");
                return false;
            }
            if (!isRuleCode(c_rule.Text))
            {
                MessageForm.Warning("不正确的编码规则，请用XX-XX-XX ...形式!");
                return false;
            }
            return true;
        }

        private bool isRuleCode(string value)
        {
	        var va=value.Replace("-","");
            if(!value.Contains("-")  || value.Contains("--"))
            {
                return false;
            }
        //    if (Validator.IsMatch(va, @"^\d{n}$"))  "^(\\d)*$"
            if (Validator.IsMatch(va, @"^(\\d)*$"))
            {
                return false;
            }
            if(value.Substring(0,1).Equals("-") || value.Substring(value.Length -1,1).Equals("-"))
            {
                return false;
            }
            return  true;
        }
        #endregion 

        #region 事件
        private void close_buttn_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
        private void insert_Load(object sender, EventArgs e)
        {
            LoadKind();
        }
        private void insert_button_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Insert();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }

        #endregion

        #region 访问数据
        private void LoadKind()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysAcctSubjKind_list";           
            serviceParamter.Paramters = paramters;

            List<string> companys = new List<string>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                companys.Add(codeValue.Code + " " + codeValue.Value);
            }
            c_kind.Properties.Items.AddRange(companys);
            c_kind.SelectedItem = companys[0];
        }

        private bool Insert()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "sysDictsAcctSubjSys_insert";      
            List<string> strCtrls = new List<string>();
            strCtrls.Add("c_code");
            strCtrls.Add("c_name");
            strCtrls.Add("c_rule");
            strCtrls.Add("c_kind");       
            FormHelper.GenerateParamerByControl(this.Controls, ref dict, strCtrls);
            serviceParamter.Paramters = dict;
            return _helper.WirteSql(serviceParamter);

        }
        #endregion

      
    }
}
