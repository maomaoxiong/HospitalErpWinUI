﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.sys.dicts.acct.subjsys
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }

        private DataTable _subjsys;
        public main()
        {
            _helper = new DaoHelper();
            InitializeComponent();
            Init();
        }

        #region 其他
        private void Init()
        {
         
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.StartPosition = FormStartPosition.CenterScreen;
        }
        #endregion

       

        private void gridControl1_Click(object sender, EventArgs e)
        {

        }

        #region 访问数据
        private void Query()
        {
            //sysDictsAcctSubjSys_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysDictsAcctSubjSys_select";
            paramters.Add("c_code", u_name.Text.Trim());
            paramters.Add("c_name", u_parent.Text.Trim());
       
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            _subjsys = dt;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
                string[] arragy =  dt.Rows[i]["Column1"].ToString().Split('|');

                dt.Rows[i]["Column1"] = arragy[3].ToString();
                //if (dt.Rows[i]["is_sys"].Equals("是"))
                //{
                //    ; // this.gridColumn1
                //}
            }
        //    _user = dt;
            gridControl1.DataSource = _subjsys;
            
        }

        private bool Delete(string co_code)
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "sysDictsAcctSubjSys_delete";
            paramters.Add("co_code", co_code);
            serviceParamter.Paramters = paramters;
            bool result = false;
            try
            {
                _helper.WirteSql(serviceParamter);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }

        #endregion


        #region 事件

        private void gridView1_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            RepositoryItemCheckEdit disItemCheck;
          //  RepositoryItemHyperLinkEdit hyperLinkEdit;
            //若当前进行到了“选择”列，
            if (e.Column.FieldName.Equals("checked"))
            {
                disItemCheck = (RepositoryItemCheckEdit)e.RepositoryItem.Clone();
                disItemCheck.Enabled = false;
                disItemCheck.ReadOnly = true;

                DevExpress.XtraGrid.Views.Grid.GridView gv = sender as DevExpress.XtraGrid.Views.Grid.GridView;
                string fieldName = gv.GetRowCellValue(e.RowHandle, gv.Columns["is_sys"]).ToString();
                if (fieldName.Equals("是"))
                {
                    e.RepositoryItem = disItemCheck;
                }
            }
           
        }

        private void sysDictsAcctSubjSys_delete_Click(object sender, EventArgs e)
        {
            // sysDictsAcctSubjSys_delete
            bool value = false;
            string strSelected = string.Empty;

            try
            {
                List<string> objs = new List<string>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    value = Convert.ToBoolean(gridView1.GetDataRow(i)["checked"]);
                    if (value)
                    {
                        string code = gridView1.GetDataRow(i)["co_code"].ToString();
                        objs.Add(code);

                    }
                }
                if (objs.Count == 0)
                {
                    MessageForm.Show("请选择要删除的数据!");
                    return;
                }
                if (MessageForm.ShowMsg("确认是否删除?", MsgType.YesNo) == DialogResult.Yes)
                {
                    foreach (string obj in objs)
                    {
                        if (!Delete(obj))
                        {
                            return;
                        }
                    }
                    //       MessageForm.Show("操作成功!");
                    Query();
                }

            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }

        }

        private void sysDictsAcctSubjSys_insert_Click(object sender, EventArgs e)
        {
            try
            {
                insert insertFrm = new insert(this._helper, this._perms);

                if (insertFrm.ShowDialog() == DialogResult.OK)
                {
                    Query();
                }
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
           
        }

        private void sysDictsAcctSubjSys_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, DevExpress.XtraEditors.Controls.OpenLinkEventArgs e)
        {
            if (gridView1.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                int index = gridView1.GetDataSourceRowIndex(gridView1.FocusedRowHandle);
                string url = _subjsys.Rows[index]["co_code"].ToString();
                string isSys = _subjsys.Rows[index]["is_sys"].ToString();
                if (isSys.Equals("是"))
                {
                    return;
                }
                update frm = new update(this._helper, this._perms, url);
                
                if (frm.ShowDialog() == DialogResult.No)
                {
                    MessageForm.Warning("修改失败！");
                }
                Query();
            }

        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            string fileName = string.Empty; //文件名
            //打开文件
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.DefaultExt = "txt";
            dlg.Filter = "Txt Files|*.txt";
            if (dlg.ShowDialog() == DialogResult.OK)
                fileName = dlg.FileName;
            if (fileName == null)
                return;
            //读取文件内容
            StreamReader sr = new StreamReader(fileName, System.Text.Encoding.Default);
            String ls_input = sr.ReadToEnd().TrimStart();
            sr.Close();
            string url = ConfigurationManager.AppSettings["ServicesAdress"] + "/Vouch/Add";
            Dictionary<string, string> dic = new Dictionary<string, string>();          
            dic.Add("", ls_input.Trim());
            HttpHelper.CreateHttpPostResponse(url, dic, G_User.user.Ticket, "vouchInsert");
        }

     

       

      
        
       
       
    }
}
