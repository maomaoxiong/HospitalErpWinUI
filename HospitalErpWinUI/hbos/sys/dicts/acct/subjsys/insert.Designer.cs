﻿namespace HospitalErpWinUI.hbos.sys.dicts.acct.subjsys
{
    partial class insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.c_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.c_rule = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.insert_button = new DevExpress.XtraEditors.SimpleButton();
            this.close_buttn = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.c_code = new DevExpress.XtraEditors.TextEdit();
            this.c_kind = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.c_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_rule.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_kind.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(76, 106);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(90, 18);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "科目体系名称";
            // 
            // c_name
            // 
            this.c_name.Location = new System.Drawing.Point(192, 103);
            this.c_name.Name = "c_name";
            this.c_name.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.c_name.Properties.Appearance.Options.UseBackColor = true;
            this.c_name.Size = new System.Drawing.Size(200, 24);
            this.c_name.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(76, 161);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(90, 18);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "科目编码规则";
            // 
            // c_rule
            // 
            this.c_rule.Location = new System.Drawing.Point(192, 158);
            this.c_rule.Name = "c_rule";
            this.c_rule.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.c_rule.Properties.Appearance.Options.UseBackColor = true;
            this.c_rule.Size = new System.Drawing.Size(200, 24);
            this.c_rule.TabIndex = 2;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(76, 215);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(90, 18);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "科目体系类别";
            // 
            // insert_button
            // 
            this.insert_button.Location = new System.Drawing.Point(128, 278);
            this.insert_button.Name = "insert_button";
            this.insert_button.Size = new System.Drawing.Size(80, 25);
            this.insert_button.TabIndex = 4;
            this.insert_button.Text = "添加";
            this.insert_button.Click += new System.EventHandler(this.insert_button_Click);
            // 
            // close_buttn
            // 
            this.close_buttn.Location = new System.Drawing.Point(285, 278);
            this.close_buttn.Name = "close_buttn";
            this.close_buttn.Size = new System.Drawing.Size(80, 25);
            this.close_buttn.TabIndex = 5;
            this.close_buttn.Text = "关闭";
            this.close_buttn.Click += new System.EventHandler(this.close_buttn_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(76, 49);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(90, 18);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "科目体系编码";
            // 
            // c_code
            // 
            this.c_code.Location = new System.Drawing.Point(192, 46);
            this.c_code.Name = "c_code";
            this.c_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.c_code.Properties.Appearance.Options.UseBackColor = true;
            this.c_code.Size = new System.Drawing.Size(200, 24);
            this.c_code.TabIndex = 0;
            // 
            // c_kind
            // 
            this.c_kind.Location = new System.Drawing.Point(192, 212);
            this.c_kind.Name = "c_kind";
            this.c_kind.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.c_kind.Properties.Appearance.Options.UseBackColor = true;
            this.c_kind.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.c_kind.Size = new System.Drawing.Size(200, 24);
            this.c_kind.TabIndex = 3;
            // 
            // insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 342);
            this.Controls.Add(this.c_kind);
            this.Controls.Add(this.close_buttn);
            this.Controls.Add(this.insert_button);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.c_rule);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.c_code);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.c_name);
            this.Controls.Add(this.labelControl1);
            this.Name = "insert";
            this.Text = "新增";
            this.Load += new System.EventHandler(this.insert_Load);
            ((System.ComponentModel.ISupportInitialize)(this.c_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_rule.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c_kind.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit c_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit c_rule;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton insert_button;
        private DevExpress.XtraEditors.SimpleButton close_buttn;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit c_code;
        private DevExpress.XtraEditors.ComboBoxEdit c_kind;
    }
}