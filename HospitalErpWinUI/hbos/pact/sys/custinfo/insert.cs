﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.pact.sys.custinfo
{
    public partial class insert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        public insert()
        {
            InitializeComponent();
            _helper = new DaoHelper();
        }
        public insert(Dao.DaoHelper helper, List<Perm> perms)
        {
            InitializeComponent();
            this._helper = helper;
            this._perms = perms;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void pactSysCustinfo_insert_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Insert();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
        private bool Validate()
        {

            if (string.IsNullOrEmpty(u_code.Text))
            {
                MessageForm.Warning("客户编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(u_name.Text))
            {
                MessageForm.Warning("客户名称不能为空!");
                return false;
            }
            
            return true;
        }



        private bool Insert()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "pactSysCustinfo_insert";
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            paramters.Add("u_code", u_code.Text);
            paramters.Add("u_name", u_name.Text);
            paramters.Add("u_postcode", u_postcode.Text);
            paramters.Add("u_reg_code", u_reg_code.Text);
            paramters.Add("u_address", u_address.Text);
            paramters.Add("u_trade", u_trade.Text);
            paramters.Add("u_prov", u_prov.Text);
            paramters.Add("u_city", u_city.Text);
            paramters.Add("is_count", is_count.EditValue.ToString());
            paramters.Add("u_bank", u_bank.Text);
            paramters.Add("u_bank_acount", u_bank_acount.Text);
            paramters.Add("u_lperson", u_lperson.Text);
            paramters.Add("u_mobile", u_mobile.Text);
            paramters.Add("u_phone", u_phone.Text);
            paramters.Add("u_fax", u_fax.Text);
            paramters.Add("u_email", u_email.Text);
            paramters.Add("u_person", u_person.Text);
            paramters.Add("u_stop", FormHelper.GetValueForCheckEdit(u_stop).Trim());
            paramters.Add("Comp_code", G_User.user.Comp_code);
            serviceParamter.Paramters = paramters;

            return _helper.WirteSql(serviceParamter);

        }
    }
}
