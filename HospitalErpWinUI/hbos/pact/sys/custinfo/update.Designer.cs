﻿namespace HospitalErpWinUI.hbos.pact.sys.custinfo
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.pactSysCustinfo_update = new DevExpress.XtraEditors.SimpleButton();
            this.u_stop = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.u_person = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.u_email = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.u_fax = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.u_phone = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.u_mobile = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.u_lperson = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.u_bank_acount = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.u_bank = new DevExpress.XtraEditors.TextEdit();
            this.is_count = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.u_city = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.u_prov = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.u_trade = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.u_address = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.u_reg_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.u_postcode = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.u_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.u_code = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.u_stop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_person.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_email.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_fax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_phone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_mobile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_lperson.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_bank_acount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_bank.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_count.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_city.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_prov.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_trade.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_address.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_reg_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_postcode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(543, 316);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 89;
            this.simpleButton2.Text = "关闭";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // pactSysCustinfo_update
            // 
            this.pactSysCustinfo_update.Location = new System.Drawing.Point(426, 316);
            this.pactSysCustinfo_update.Name = "pactSysCustinfo_update";
            this.pactSysCustinfo_update.Size = new System.Drawing.Size(75, 23);
            this.pactSysCustinfo_update.TabIndex = 88;
            this.pactSysCustinfo_update.Text = "保存";
            this.pactSysCustinfo_update.Click += new System.EventHandler(this.pactSysCustinfo_update_Click);
            // 
            // u_stop
            // 
            this.u_stop.Location = new System.Drawing.Point(202, 313);
            this.u_stop.Name = "u_stop";
            this.u_stop.Properties.Caption = "是否停用";
            this.u_stop.Size = new System.Drawing.Size(75, 19);
            this.u_stop.TabIndex = 87;
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(436, 278);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(48, 14);
            this.labelControl15.TabIndex = 86;
            this.labelControl15.Text = "联系人：";
            // 
            // u_person
            // 
            this.u_person.Location = new System.Drawing.Point(487, 275);
            this.u_person.Name = "u_person";
            this.u_person.Size = new System.Drawing.Size(147, 20);
            this.u_person.TabIndex = 85;
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(160, 278);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(39, 14);
            this.labelControl16.TabIndex = 84;
            this.labelControl16.Text = "Email：";
            // 
            // u_email
            // 
            this.u_email.Location = new System.Drawing.Point(202, 275);
            this.u_email.Name = "u_email";
            this.u_email.Size = new System.Drawing.Size(147, 20);
            this.u_email.TabIndex = 83;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(448, 243);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(36, 14);
            this.labelControl13.TabIndex = 82;
            this.labelControl13.Text = "传真：";
            // 
            // u_fax
            // 
            this.u_fax.Location = new System.Drawing.Point(487, 240);
            this.u_fax.Name = "u_fax";
            this.u_fax.Size = new System.Drawing.Size(147, 20);
            this.u_fax.TabIndex = 81;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(163, 243);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(36, 14);
            this.labelControl14.TabIndex = 80;
            this.labelControl14.Text = "电话：";
            // 
            // u_phone
            // 
            this.u_phone.Location = new System.Drawing.Point(202, 240);
            this.u_phone.Name = "u_phone";
            this.u_phone.Size = new System.Drawing.Size(147, 20);
            this.u_phone.TabIndex = 79;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(448, 212);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(36, 14);
            this.labelControl11.TabIndex = 78;
            this.labelControl11.Text = "手机：";
            // 
            // u_mobile
            // 
            this.u_mobile.Location = new System.Drawing.Point(487, 209);
            this.u_mobile.Name = "u_mobile";
            this.u_mobile.Size = new System.Drawing.Size(147, 20);
            this.u_mobile.TabIndex = 77;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(163, 212);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(36, 14);
            this.labelControl12.TabIndex = 76;
            this.labelControl12.Text = "法人：";
            // 
            // u_lperson
            // 
            this.u_lperson.Location = new System.Drawing.Point(202, 209);
            this.u_lperson.Name = "u_lperson";
            this.u_lperson.Size = new System.Drawing.Size(147, 20);
            this.u_lperson.TabIndex = 75;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(424, 180);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(60, 14);
            this.labelControl9.TabIndex = 74;
            this.labelControl9.Text = "银行帐号：";
            // 
            // u_bank_acount
            // 
            this.u_bank_acount.Location = new System.Drawing.Point(487, 177);
            this.u_bank_acount.Name = "u_bank_acount";
            this.u_bank_acount.Size = new System.Drawing.Size(147, 20);
            this.u_bank_acount.TabIndex = 73;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(139, 180);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(60, 14);
            this.labelControl10.TabIndex = 72;
            this.labelControl10.Text = "开户银行：";
            // 
            // u_bank
            // 
            this.u_bank.Location = new System.Drawing.Point(202, 177);
            this.u_bank.Name = "u_bank";
            this.u_bank.Size = new System.Drawing.Size(147, 20);
            this.u_bank.TabIndex = 71;
            // 
            // is_count
            // 
            this.is_count.EditValue = "0";
            this.is_count.Location = new System.Drawing.Point(557, 137);
            this.is_count.Name = "is_count";
            this.is_count.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("0", "市"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "区县")});
            this.is_count.Size = new System.Drawing.Size(115, 30);
            this.is_count.TabIndex = 70;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(460, 144);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(24, 14);
            this.labelControl7.TabIndex = 69;
            this.labelControl7.Text = "市：";
            // 
            // u_city
            // 
            this.u_city.Location = new System.Drawing.Point(487, 141);
            this.u_city.Name = "u_city";
            this.u_city.Size = new System.Drawing.Size(47, 20);
            this.u_city.TabIndex = 68;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(175, 144);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(24, 14);
            this.labelControl8.TabIndex = 67;
            this.labelControl8.Text = "省：";
            // 
            // u_prov
            // 
            this.u_prov.Location = new System.Drawing.Point(202, 141);
            this.u_prov.Name = "u_prov";
            this.u_prov.Size = new System.Drawing.Size(147, 20);
            this.u_prov.TabIndex = 66;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(424, 108);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 14);
            this.labelControl5.TabIndex = 65;
            this.labelControl5.Text = "所属行业：";
            // 
            // u_trade
            // 
            this.u_trade.Location = new System.Drawing.Point(487, 105);
            this.u_trade.Name = "u_trade";
            this.u_trade.Size = new System.Drawing.Size(147, 20);
            this.u_trade.TabIndex = 64;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(139, 108);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(60, 14);
            this.labelControl6.TabIndex = 63;
            this.labelControl6.Text = "详细地址：";
            // 
            // u_address
            // 
            this.u_address.Location = new System.Drawing.Point(202, 105);
            this.u_address.Name = "u_address";
            this.u_address.Size = new System.Drawing.Size(147, 20);
            this.u_address.TabIndex = 62;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(403, 75);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(84, 14);
            this.labelControl3.TabIndex = 61;
            this.labelControl3.Text = "纳税人登记号：";
            // 
            // u_reg_code
            // 
            this.u_reg_code.Location = new System.Drawing.Point(487, 72);
            this.u_reg_code.Name = "u_reg_code";
            this.u_reg_code.Size = new System.Drawing.Size(147, 20);
            this.u_reg_code.TabIndex = 60;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(139, 75);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 14);
            this.labelControl4.TabIndex = 59;
            this.labelControl4.Text = "邮政编码：";
            // 
            // u_postcode
            // 
            this.u_postcode.Location = new System.Drawing.Point(202, 72);
            this.u_postcode.Name = "u_postcode";
            this.u_postcode.Size = new System.Drawing.Size(147, 20);
            this.u_postcode.TabIndex = 58;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(424, 38);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 57;
            this.labelControl2.Text = "客户名称：";
            // 
            // u_name
            // 
            this.u_name.Location = new System.Drawing.Point(487, 35);
            this.u_name.Name = "u_name";
            this.u_name.Size = new System.Drawing.Size(147, 20);
            this.u_name.TabIndex = 56;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(139, 38);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 55;
            this.labelControl1.Text = "客户编码：";
            // 
            // u_code
            // 
            this.u_code.Enabled = false;
            this.u_code.Location = new System.Drawing.Point(202, 35);
            this.u_code.Name = "u_code";
            this.u_code.Size = new System.Drawing.Size(147, 20);
            this.u_code.TabIndex = 54;
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(810, 374);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.pactSysCustinfo_update);
            this.Controls.Add(this.u_stop);
            this.Controls.Add(this.labelControl15);
            this.Controls.Add(this.u_person);
            this.Controls.Add(this.labelControl16);
            this.Controls.Add(this.u_email);
            this.Controls.Add(this.labelControl13);
            this.Controls.Add(this.u_fax);
            this.Controls.Add(this.labelControl14);
            this.Controls.Add(this.u_phone);
            this.Controls.Add(this.labelControl11);
            this.Controls.Add(this.u_mobile);
            this.Controls.Add(this.labelControl12);
            this.Controls.Add(this.u_lperson);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.u_bank_acount);
            this.Controls.Add(this.labelControl10);
            this.Controls.Add(this.u_bank);
            this.Controls.Add(this.is_count);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.u_city);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.u_prov);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.u_trade);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.u_address);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.u_reg_code);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.u_postcode);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.u_name);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.u_code);
            this.Name = "update";
            this.Text = "修改";
            ((System.ComponentModel.ISupportInitialize)(this.u_stop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_person.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_email.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_fax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_phone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_mobile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_lperson.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_bank_acount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_bank.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_count.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_city.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_prov.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_trade.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_address.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_reg_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_postcode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton pactSysCustinfo_update;
        private DevExpress.XtraEditors.CheckEdit u_stop;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit u_person;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit u_email;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit u_fax;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit u_phone;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit u_mobile;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit u_lperson;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit u_bank_acount;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit u_bank;
        private DevExpress.XtraEditors.RadioGroup is_count;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit u_city;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit u_prov;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit u_trade;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit u_address;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit u_reg_code;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit u_postcode;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit u_name;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit u_code;
    }
}