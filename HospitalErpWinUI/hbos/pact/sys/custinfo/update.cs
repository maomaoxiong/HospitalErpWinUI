﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.pact.sys.custinfo
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        private Validator validator;
        private List<Perm> _perms { get; set; }
        public update()
        {
            InitializeComponent();
        }
        public update(DaoHelper helper, List<Perm> perms, string content_code)
        {

            InitializeComponent();
            _helper = helper;
            _perms = perms;
            validator = new Validator();
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "pactSysCustinfo_update_load";
            paramters.Add("Comp_code", G_User.user.Comp_code);
            paramters.Add("content_code1", content_code);
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("查询失败！");
            }
            else
            {
                this.u_code.Text = dt.Rows[0]["customer_code"].ToString();
                this.u_name.Text = dt.Rows[0]["customer_name"].ToString();
                this.u_postcode.Text = dt.Rows[0]["customer_postcode"].ToString();
                this.u_reg_code.Text = dt.Rows[0]["customer_reg_code"].ToString();
                this.u_address.Text = dt.Rows[0]["customer_adress"].ToString();
                this.u_trade.Text = dt.Rows[0]["customer_trade"].ToString();
                this.u_prov.Text = dt.Rows[0]["prov"].ToString();
                this.u_city.Text = dt.Rows[0]["city"].ToString();
                FormHelper.RadioGroupAssignment(is_count, dt.Rows[0]["is_count"].ToString()=="True"?"1":"0");//dt.Rows[0]["is_count"].ToString();
                this.u_bank.Text = dt.Rows[0]["customer_bank"].ToString();
                this.u_bank_acount.Text = dt.Rows[0]["customer_bank_acount"].ToString();
                this.u_lperson.Text = dt.Rows[0]["customer_lperson"].ToString();
                this.u_mobile.Text = dt.Rows[0]["customer_mobile"].ToString();
                this.u_phone.Text = dt.Rows[0]["customer_phone"].ToString();
                this.u_fax.Text = dt.Rows[0]["customer_fax"].ToString();
                this.u_person.Text = dt.Rows[0]["customer_person"].ToString();
                this.u_email.Text = dt.Rows[0]["customer_email"].ToString();
                this.u_stop.CheckState = CheckState.Unchecked; 
                if (dt.Rows[0]["is_stop"].ToString() != "False")
                {
                    this.u_stop.CheckState = CheckState.Checked;
                }
            }
        }
        private bool Validate()
        {

            if (string.IsNullOrEmpty(u_code.Text))
            {
                MessageForm.Warning("客户编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(u_name.Text))
            {
                MessageForm.Warning("客户名称不能为空!");
                return false;
            }

            return true;
        }

        private void pactSysCustinfo_update_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                u_update();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
        private bool u_update()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "pactSysCustinfo_update";
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            paramters.Add("u_code", u_code.Text);
            paramters.Add("u_name", u_name.Text);
            paramters.Add("u_postcode", u_postcode.Text);
            paramters.Add("u_reg_code", u_reg_code.Text);
            paramters.Add("u_address", u_address.Text);
            paramters.Add("u_trade", u_trade.Text);
            paramters.Add("u_prov", u_prov.Text);
            paramters.Add("u_city", u_city.Text);
            paramters.Add("is_count", is_count.EditValue.ToString());
            paramters.Add("u_bank", u_bank.Text);
            paramters.Add("u_bank_acount", u_bank_acount.Text);
            paramters.Add("u_lperson", u_lperson.Text);
            paramters.Add("u_mobile", u_mobile.Text);
            paramters.Add("u_phone", u_phone.Text);
            paramters.Add("u_fax", u_fax.Text);
            paramters.Add("u_email", u_email.Text);
            paramters.Add("u_person", u_person.Text);
            paramters.Add("u_stop", FormHelper.GetValueForCheckEdit(u_stop).Trim());
            paramters.Add("Comp_code", G_User.user.Comp_code);
            serviceParamter.Paramters = paramters;

            return _helper.WirteSql(serviceParamter);

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
