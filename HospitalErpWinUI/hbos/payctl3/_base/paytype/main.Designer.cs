﻿namespace HospitalErpWinUI.hbos.payctl3._base.paytype
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.payctl3BasePayType_insert = new DevExpress.XtraEditors.SimpleButton();
            this.payctl3BasePayType_query = new DevExpress.XtraEditors.SimpleButton();
            this.payctl3BasePayType_delete = new DevExpress.XtraEditors.SimpleButton();
            this.qqq = new DevExpress.XtraEditors.LabelControl();
            this.q_areacode_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.q_areacode_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.payctl3BasePayType_insert);
            this.panelControl1.Controls.Add(this.payctl3BasePayType_query);
            this.panelControl1.Controls.Add(this.q_areacode_code);
            this.panelControl1.Controls.Add(this.payctl3BasePayType_delete);
            this.panelControl1.Controls.Add(this.qqq);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(910, 46);
            this.panelControl1.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 46);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(910, 249);
            this.panelControl2.TabIndex = 1;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.gridControl1.Size = new System.Drawing.Size(906, 245);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "选择";
            this.gridColumn1.FieldName = "checked";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "方式编码";
            this.gridColumn2.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.gridColumn2.FieldName = "pay_type_code";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "方式名称";
            this.gridColumn3.FieldName = "pay_type_name";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit1_Click);
            // 
            // payctl3BasePayType_insert
            // 
            this.payctl3BasePayType_insert.Location = new System.Drawing.Point(498, 12);
            this.payctl3BasePayType_insert.Name = "payctl3BasePayType_insert";
            this.payctl3BasePayType_insert.Size = new System.Drawing.Size(87, 27);
            this.payctl3BasePayType_insert.TabIndex = 19;
            this.payctl3BasePayType_insert.Text = "添加";
            this.payctl3BasePayType_insert.Click += new System.EventHandler(this.payctl3BasePayType_insert_Click);
            // 
            // payctl3BasePayType_query
            // 
            this.payctl3BasePayType_query.Location = new System.Drawing.Point(383, 12);
            this.payctl3BasePayType_query.Name = "payctl3BasePayType_query";
            this.payctl3BasePayType_query.Size = new System.Drawing.Size(87, 27);
            this.payctl3BasePayType_query.TabIndex = 17;
            this.payctl3BasePayType_query.Text = "查询";
            this.payctl3BasePayType_query.Click += new System.EventHandler(this.payctl3BasePayType_query_Click);
            // 
            // payctl3BasePayType_delete
            // 
            this.payctl3BasePayType_delete.Location = new System.Drawing.Point(606, 12);
            this.payctl3BasePayType_delete.Name = "payctl3BasePayType_delete";
            this.payctl3BasePayType_delete.Size = new System.Drawing.Size(87, 27);
            this.payctl3BasePayType_delete.TabIndex = 18;
            this.payctl3BasePayType_delete.Text = "删除";
            this.payctl3BasePayType_delete.Click += new System.EventHandler(this.payctl3BasePayType_delete_Click);
            // 
            // qqq
            // 
            this.qqq.Location = new System.Drawing.Point(-82, 19);
            this.qqq.Name = "qqq";
            this.qqq.Size = new System.Drawing.Size(60, 14);
            this.qqq.TabIndex = 15;
            this.qqq.Text = "编码名称：";
            // 
            // q_areacode_code
            // 
            this.q_areacode_code.Location = new System.Drawing.Point(92, 12);
            this.q_areacode_code.Name = "q_areacode_code";
            this.q_areacode_code.Size = new System.Drawing.Size(255, 20);
            this.q_areacode_code.TabIndex = 16;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 18);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 20;
            this.labelControl1.Text = "编码名称：";
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(910, 295);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "main";
            this.Text = "结算方式";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.q_areacode_code.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.SimpleButton payctl3BasePayType_insert;
        private DevExpress.XtraEditors.SimpleButton payctl3BasePayType_query;
        private DevExpress.XtraEditors.TextEdit q_areacode_code;
        private DevExpress.XtraEditors.SimpleButton payctl3BasePayType_delete;
        private DevExpress.XtraEditors.LabelControl qqq;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}