﻿namespace HospitalErpWinUI.hbos.payctl3._base.paytype
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.acct_subj = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cheque_type = new DevExpress.XtraEditors.TextEdit();
            this.pay_type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.detail_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.detail_code = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.payctl3BasePayType_udpate = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cheque_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pay_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detail_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detail_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // acct_subj
            // 
            this.acct_subj.EditValue = "";
            this.acct_subj.Location = new System.Drawing.Point(102, 120);
            this.acct_subj.Name = "acct_subj";
            this.acct_subj.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.acct_subj.Properties.NullText = "";
            this.acct_subj.Properties.View = this.searchLookUpEdit1View;
            this.acct_subj.Size = new System.Drawing.Size(147, 20);
            this.acct_subj.TabIndex = 87;
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(36, 123);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 14);
            this.labelControl5.TabIndex = 86;
            this.labelControl5.Text = "会计科目：";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(39, 242);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(180, 14);
            this.labelControl4.TabIndex = 85;
            this.labelControl4.Text = "接收方式默认为空不知道为什么：";
            this.labelControl4.Visible = false;
            // 
            // cheque_type
            // 
            this.cheque_type.Location = new System.Drawing.Point(102, 239);
            this.cheque_type.Name = "cheque_type";
            this.cheque_type.Size = new System.Drawing.Size(147, 20);
            this.cheque_type.TabIndex = 84;
            this.cheque_type.Visible = false;
            // 
            // pay_type
            // 
            this.pay_type.Location = new System.Drawing.Point(102, 80);
            this.pay_type.Name = "pay_type";
            this.pay_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.pay_type.Size = new System.Drawing.Size(147, 20);
            this.pay_type.TabIndex = 83;
            this.pay_type.EditValueChanged += new System.EventHandler(this.pay_type_EditValueChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(36, 83);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 14);
            this.labelControl3.TabIndex = 82;
            this.labelControl3.Text = "支付属性：";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(39, 48);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 81;
            this.labelControl2.Text = "方式名称：";
            // 
            // detail_name
            // 
            this.detail_name.Location = new System.Drawing.Point(102, 45);
            this.detail_name.Name = "detail_name";
            this.detail_name.Size = new System.Drawing.Size(147, 20);
            this.detail_name.TabIndex = 80;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(39, 10);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 79;
            this.labelControl1.Text = "方式编码：";
            // 
            // detail_code
            // 
            this.detail_code.Enabled = false;
            this.detail_code.Location = new System.Drawing.Point(102, 7);
            this.detail_code.Name = "detail_code";
            this.detail_code.Size = new System.Drawing.Size(147, 20);
            this.detail_code.TabIndex = 78;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(162, 159);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 89;
            this.simpleButton2.Text = "关闭";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // payctl3BasePayType_udpate
            // 
            this.payctl3BasePayType_udpate.Location = new System.Drawing.Point(45, 159);
            this.payctl3BasePayType_udpate.Name = "payctl3BasePayType_udpate";
            this.payctl3BasePayType_udpate.Size = new System.Drawing.Size(75, 23);
            this.payctl3BasePayType_udpate.TabIndex = 88;
            this.payctl3BasePayType_udpate.Text = "保存";
            this.payctl3BasePayType_udpate.Click += new System.EventHandler(this.payctl3BasePayType_udpate_Click);
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 288);
            this.Controls.Add(this.acct_subj);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.cheque_type);
            this.Controls.Add(this.pay_type);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.detail_name);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.detail_code);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.payctl3BasePayType_udpate);
            this.Name = "update";
            this.Text = "update";
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cheque_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pay_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detail_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detail_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SearchLookUpEdit acct_subj;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit cheque_type;
        private DevExpress.XtraEditors.ComboBoxEdit pay_type;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit detail_name;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit detail_code;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton payctl3BasePayType_udpate;
    }
}