﻿namespace HospitalErpWinUI.hbos.payctl3._base.paytype
{
    partial class insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.detail_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.detail_name = new DevExpress.XtraEditors.TextEdit();
            this.pay_type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cheque_type = new DevExpress.XtraEditors.TextEdit();
            this.acct_subj = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.payctl3BasePayType_insert = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.detail_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detail_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pay_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cheque_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(40, 32);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 19;
            this.labelControl1.Text = "方式编码：";
            // 
            // detail_code
            // 
            this.detail_code.Location = new System.Drawing.Point(103, 29);
            this.detail_code.Name = "detail_code";
            this.detail_code.Size = new System.Drawing.Size(147, 20);
            this.detail_code.TabIndex = 18;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(40, 73);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 21;
            this.labelControl2.Text = "方式名称：";
            // 
            // detail_name
            // 
            this.detail_name.Location = new System.Drawing.Point(103, 70);
            this.detail_name.Name = "detail_name";
            this.detail_name.Size = new System.Drawing.Size(147, 20);
            this.detail_name.TabIndex = 20;
            // 
            // pay_type
            // 
            this.pay_type.Location = new System.Drawing.Point(103, 105);
            this.pay_type.Name = "pay_type";
            this.pay_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.pay_type.Size = new System.Drawing.Size(147, 20);
            this.pay_type.TabIndex = 69;
            this.pay_type.EditValueChanged += new System.EventHandler(this.pay_type_EditValueChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(37, 108);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 14);
            this.labelControl3.TabIndex = 68;
            this.labelControl3.Text = "支付属性：";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(40, 267);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(180, 14);
            this.labelControl4.TabIndex = 71;
            this.labelControl4.Text = "接收方式默认为空不知道为什么：";
            this.labelControl4.Visible = false;
            // 
            // cheque_type
            // 
            this.cheque_type.Location = new System.Drawing.Point(103, 264);
            this.cheque_type.Name = "cheque_type";
            this.cheque_type.Size = new System.Drawing.Size(147, 20);
            this.cheque_type.TabIndex = 70;
            this.cheque_type.Visible = false;
            // 
            // acct_subj
            // 
            this.acct_subj.EditValue = "";
            this.acct_subj.Location = new System.Drawing.Point(103, 145);
            this.acct_subj.Name = "acct_subj";
            this.acct_subj.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.acct_subj.Properties.NullText = "";
            this.acct_subj.Properties.View = this.searchLookUpEdit1View;
            this.acct_subj.Size = new System.Drawing.Size(147, 20);
            this.acct_subj.TabIndex = 75;
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(37, 148);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 14);
            this.labelControl5.TabIndex = 74;
            this.labelControl5.Text = "会计科目：";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(163, 184);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 77;
            this.simpleButton2.Text = "关闭";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // payctl3BasePayType_insert
            // 
            this.payctl3BasePayType_insert.Location = new System.Drawing.Point(46, 184);
            this.payctl3BasePayType_insert.Name = "payctl3BasePayType_insert";
            this.payctl3BasePayType_insert.Size = new System.Drawing.Size(75, 23);
            this.payctl3BasePayType_insert.TabIndex = 76;
            this.payctl3BasePayType_insert.Text = "保存";
            this.payctl3BasePayType_insert.Click += new System.EventHandler(this.payctl3BasePayType_insert_Click);
            // 
            // insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 373);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.payctl3BasePayType_insert);
            this.Controls.Add(this.acct_subj);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.cheque_type);
            this.Controls.Add(this.pay_type);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.detail_name);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.detail_code);
            this.Name = "insert";
            this.Text = "insert";
            ((System.ComponentModel.ISupportInitialize)(this.detail_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detail_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pay_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cheque_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.acct_subj.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit detail_code;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit detail_name;
        private DevExpress.XtraEditors.ComboBoxEdit pay_type;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit cheque_type;
        private DevExpress.XtraEditors.SearchLookUpEdit acct_subj;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton payctl3BasePayType_insert;
    }
}