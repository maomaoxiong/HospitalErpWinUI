﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.payctl3._base.paytype
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        private Validator validator;
        private List<Perm> _perms { get; set; }
        public update()
        {
            InitializeComponent();
        }
        public update(DaoHelper helper, List<Perm> perms, string content_code)
        {

            InitializeComponent();
            _helper = helper;
            _perms = perms;
            validator = new Validator();
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "payctl3BasePayType_udpate_load";
            paramters.Add("content_code", content_code);
            serviceParamter.Paramters = paramters;
            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            Load_payctl3_pay_type();
            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("查询失败！");

            }
            else
            {
               
                this.detail_code.Text = dt.Rows[0]["pay_type_code"].ToString();
                this.detail_name.Text = dt.Rows[0]["pay_type_name"].ToString();
               // this.pay_type.Text = dt.Rows[0]["subj_nature_code"].ToString();
                //this.acct_subj.Text = dt.Rows[0]["Column1"].ToString();//SearchLookUpEdit
                FormHelper.ComboBoxeditAssignment(pay_type, dt.Rows[0]["subj_nature_code"].ToString());
                if (FormHelper.GetValueForComboBoxEdit(pay_type) == "02" || FormHelper.GetValueForComboBoxEdit(pay_type) == "03" || FormHelper.GetValueForComboBoxEdit(pay_type) == "04")
                {
                    acct_subj.Visible = true;
                    labelControl5.Visible = true;
                    get_payctl3_paytype_acct_subj();
                }
                else
                {
                    acct_subj.Visible = false;
                    labelControl5.Visible = false;
                }
                FormHelper.SearchLookUpEditAssignment(acct_subj, dt.Rows[0]["Column1"].ToString().Replace("|||", " "));
                this.cheque_type.Text = dt.Rows[0]["cheq_type_code"].ToString();
                 
             
            }
        }
        private void Load_payctl3_pay_type()
        {
            //sysNextLevelDba_select
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "payctl3_pay_type";
            // DataTable dt = _helper.ReadDictForSql(serviceParamter);
            List<string> objs = new List<string>();
            List<CodeValue> codeValues = _helper.ReadDictForData(serviceParamter);
            foreach (CodeValue codeValue in codeValues)
            {
                objs.Add(codeValue.Code + " " + codeValue.Value);
            }
            pay_type.Properties.Items.AddRange(objs);
            pay_type.SelectedItem = objs[0];
        }
        //会计科目：
        private void get_payctl3_paytype_acct_subj()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "payctl3_paytype_acct_subj";
            paramters.Add("pay_type", FormHelper.GetValueForComboBoxEdit(pay_type));
            paramters.Add("key", "");
            serviceParamter.Paramters = paramters;

            List<CodeValue> objs = new List<CodeValue>();
            DataTable dt = _helper.ReadDictForSql(serviceParamter);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CodeValue codeValue = new CodeValue();
                codeValue.Code = dt.Rows[i][0].ToString();
                codeValue.Value = dt.Rows[i][1].ToString();
                objs.Add(codeValue);
            }
            //    emp_code.Properties.Items.AddRange(emps);
            acct_subj.Properties.ValueMember = "Code";
            acct_subj.Properties.DisplayMember = "Value";
            acct_subj.Properties.DataSource = objs;

        }

        private void payctl3BasePayType_udpate_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                Insert();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
        private bool Insert()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            serviceParamter.ServiceId = "payctl3BasePayType_udpate";
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            paramters.Add("type_code", detail_code.Text);
            paramters.Add("type_name", detail_name.Text);
            paramters.Add("pay_attr", FormHelper.GetValueForComboBoxEdit(pay_type));
            if (FormHelper.GetValueForComboBoxEdit(pay_type) == "02" || FormHelper.GetValueForComboBoxEdit(pay_type) == "03" || FormHelper.GetValueForComboBoxEdit(pay_type) == "04")
            {
                paramters.Add("acct_subj", FormHelper.GetValueForSearchLookUpEdit(acct_subj));
            }
            else
            {
                paramters.Add("acct_subj", "");
            }
            paramters.Add("cheque_type", cheque_type.Text);

            serviceParamter.Paramters = paramters;
            return _helper.WirteSql(serviceParamter);

        }
        private bool Validate()
        {

            if (string.IsNullOrEmpty(detail_code.Text))
            {
                MessageForm.Warning("方式编码不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(detail_name.Text))
            {
                MessageForm.Warning("方式名称不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(pay_type.Text))
            {
                MessageForm.Warning("支付属性不能为空!");
                return false;
            }
            if (string.IsNullOrEmpty(acct_subj.Text) && (FormHelper.GetValueForComboBoxEdit(pay_type) == "02" || FormHelper.GetValueForComboBoxEdit(pay_type) == "03" || FormHelper.GetValueForComboBoxEdit(pay_type) == "04"))
            {
                MessageForm.Warning("结算方式不能为空!");
                return false;
            }
            return true;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void pay_type_EditValueChanged(object sender, EventArgs e)
        {
            if (FormHelper.GetValueForComboBoxEdit(pay_type) == "02" || FormHelper.GetValueForComboBoxEdit(pay_type) == "03" || FormHelper.GetValueForComboBoxEdit(pay_type) == "04")
            {
                acct_subj.Visible = true;
                labelControl5.Visible = true;
                FormHelper.SearchLookUpEditAssignment(acct_subj, "");
                get_payctl3_paytype_acct_subj();
            }
            else
            {
                acct_subj.Visible = false;
                labelControl5.Visible = false;
            }
        }
    }
}
