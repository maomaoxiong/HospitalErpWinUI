﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.wage.emp.fina
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        private Validator validator;
        private List<Perm> _perms { get; set; }
        public update()
        {
            InitializeComponent();
        }
        public update(DaoHelper helper, List<Perm> perms, string content_code, string content_name )
        {

            InitializeComponent();
            this.content_code.Text = content_code;
            this.content_name.Text = content_name;
            this.StartPosition = FormStartPosition.CenterParent;
            _helper = helper;
            _perms = perms;
            validator = new Validator();
            // Init();

        }
        private bool Validate()
        {

            if (string.IsNullOrEmpty(content_name.Text))
            {
                MessageForm.Warning("名称不能为空!");
                return false;
            }
            return true;
        }

       
        private bool u_update1()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "wageEmpFinaMain_update";
            Dictionary<string, string> paramters = new Dictionary<string, string>();

            paramters.Add("content_code", content_code.Text);
            paramters.Add("content_name", content_name.Text);
            serviceParamter.Paramters = paramters;

            return _helper.WirteSql(serviceParamter);

        }

        

        private void acctsysvouchtype_insert_Click_1(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                u_update1();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }

        private void simpleButton2_Click_1(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
