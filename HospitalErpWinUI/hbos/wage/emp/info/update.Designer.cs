﻿namespace HospitalErpWinUI.hbos.wage.emp.info
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.wageEmpInfoMain_insert = new DevExpress.XtraEditors.SimpleButton();
            this.areacode_rem = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.u_sfty = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.emp_pol_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.is_stock = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.u_account = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.u_fax = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.u_tel = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.u_mandate = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.u_sex = new DevExpress.XtraEditors.ComboBoxEdit();
            this.xxxx = new DevExpress.XtraEditors.PanelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.areacode_class = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.e_title = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.areacode_nat = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.areacode_level = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.areacode_ff = new DevExpress.XtraEditors.ComboBoxEdit();
            this.areacode_classdd = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.areacode_id = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.areacode_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.areacode_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.qqq = new DevExpress.XtraEditors.LabelControl();
            this.searchLookUpEdit1 = new DevExpress.XtraEditors.SearchLookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_rem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_sfty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emp_pol_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_stock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_account.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_fax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_tel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_mandate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_sex.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xxxx)).BeginInit();
            this.xxxx.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_class.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.e_title.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_nat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_level.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_ff.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_classdd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_id.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(415, 304);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 23;
            this.simpleButton2.Text = "关闭";
            // 
            // wageEmpInfoMain_insert
            // 
            this.wageEmpInfoMain_insert.Location = new System.Drawing.Point(298, 304);
            this.wageEmpInfoMain_insert.Name = "wageEmpInfoMain_insert";
            this.wageEmpInfoMain_insert.Size = new System.Drawing.Size(75, 23);
            this.wageEmpInfoMain_insert.TabIndex = 22;
            this.wageEmpInfoMain_insert.Text = "保存";
            // 
            // areacode_rem
            // 
            this.areacode_rem.Location = new System.Drawing.Point(358, 251);
            this.areacode_rem.Name = "areacode_rem";
            this.areacode_rem.Size = new System.Drawing.Size(165, 20);
            this.areacode_rem.TabIndex = 21;
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(316, 254);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(36, 14);
            this.labelControl19.TabIndex = 59;
            this.labelControl19.Text = "备注：";
            // 
            // u_sfty
            // 
            this.u_sfty.Location = new System.Drawing.Point(89, 254);
            this.u_sfty.Name = "u_sfty";
            this.u_sfty.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_sfty.Size = new System.Drawing.Size(169, 20);
            this.u_sfty.TabIndex = 20;
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(23, 257);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(60, 14);
            this.labelControl18.TabIndex = 57;
            this.labelControl18.Text = "是否停用：";
            // 
            // emp_pol_code
            // 
            this.emp_pol_code.Location = new System.Drawing.Point(640, 209);
            this.emp_pol_code.Name = "emp_pol_code";
            this.emp_pol_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.emp_pol_code.Size = new System.Drawing.Size(165, 20);
            this.emp_pol_code.TabIndex = 19;
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(574, 213);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(60, 14);
            this.labelControl17.TabIndex = 55;
            this.labelControl17.Text = "政治面貌：";
            // 
            // is_stock
            // 
            this.is_stock.Location = new System.Drawing.Point(358, 212);
            this.is_stock.Name = "is_stock";
            this.is_stock.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_stock.Size = new System.Drawing.Size(165, 20);
            this.is_stock.TabIndex = 18;
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(292, 215);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(60, 14);
            this.labelControl16.TabIndex = 53;
            this.labelControl16.Text = "是否采购：";
            // 
            // u_account
            // 
            this.u_account.Location = new System.Drawing.Point(89, 215);
            this.u_account.Name = "u_account";
            this.u_account.Size = new System.Drawing.Size(169, 20);
            this.u_account.TabIndex = 17;
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(23, 218);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(60, 14);
            this.labelControl15.TabIndex = 51;
            this.labelControl15.Text = "职工工号：";
            // 
            // u_fax
            // 
            this.u_fax.Location = new System.Drawing.Point(639, 176);
            this.u_fax.Name = "u_fax";
            this.u_fax.Size = new System.Drawing.Size(165, 20);
            this.u_fax.TabIndex = 16;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(587, 179);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(46, 14);
            this.labelControl14.TabIndex = 49;
            this.labelControl14.Text = "EMAIL：";
            // 
            // u_tel
            // 
            this.u_tel.Location = new System.Drawing.Point(358, 176);
            this.u_tel.Name = "u_tel";
            this.u_tel.Size = new System.Drawing.Size(165, 20);
            this.u_tel.TabIndex = 15;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(292, 179);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(60, 14);
            this.labelControl13.TabIndex = 47;
            this.labelControl13.Text = "手机号码：";
            // 
            // u_mandate
            // 
            this.u_mandate.Location = new System.Drawing.Point(89, 176);
            this.u_mandate.Name = "u_mandate";
            this.u_mandate.Size = new System.Drawing.Size(169, 20);
            this.u_mandate.TabIndex = 14;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(23, 179);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(60, 14);
            this.labelControl12.TabIndex = 45;
            this.labelControl12.Text = "工作电话：";
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(640, 135);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Size = new System.Drawing.Size(164, 20);
            this.dateEdit1.TabIndex = 13;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(573, 142);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(60, 14);
            this.labelControl8.TabIndex = 43;
            this.labelControl8.Text = "出生年月：";
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // u_sex
            // 
            this.u_sex.Location = new System.Drawing.Point(358, 139);
            this.u_sex.Name = "u_sex";
            this.u_sex.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.u_sex.Size = new System.Drawing.Size(165, 20);
            this.u_sex.TabIndex = 12;
            // 
            // xxxx
            // 
            this.xxxx.Controls.Add(this.simpleButton2);
            this.xxxx.Controls.Add(this.wageEmpInfoMain_insert);
            this.xxxx.Controls.Add(this.areacode_rem);
            this.xxxx.Controls.Add(this.labelControl19);
            this.xxxx.Controls.Add(this.u_sfty);
            this.xxxx.Controls.Add(this.labelControl18);
            this.xxxx.Controls.Add(this.emp_pol_code);
            this.xxxx.Controls.Add(this.labelControl17);
            this.xxxx.Controls.Add(this.is_stock);
            this.xxxx.Controls.Add(this.labelControl16);
            this.xxxx.Controls.Add(this.u_account);
            this.xxxx.Controls.Add(this.labelControl15);
            this.xxxx.Controls.Add(this.u_fax);
            this.xxxx.Controls.Add(this.labelControl14);
            this.xxxx.Controls.Add(this.u_tel);
            this.xxxx.Controls.Add(this.labelControl13);
            this.xxxx.Controls.Add(this.u_mandate);
            this.xxxx.Controls.Add(this.labelControl12);
            this.xxxx.Controls.Add(this.dateEdit1);
            this.xxxx.Controls.Add(this.labelControl8);
            this.xxxx.Controls.Add(this.u_sex);
            this.xxxx.Controls.Add(this.labelControl7);
            this.xxxx.Controls.Add(this.areacode_class);
            this.xxxx.Controls.Add(this.labelControl11);
            this.xxxx.Controls.Add(this.e_title);
            this.xxxx.Controls.Add(this.labelControl10);
            this.xxxx.Controls.Add(this.areacode_nat);
            this.xxxx.Controls.Add(this.labelControl9);
            this.xxxx.Controls.Add(this.areacode_level);
            this.xxxx.Controls.Add(this.labelControl6);
            this.xxxx.Controls.Add(this.areacode_ff);
            this.xxxx.Controls.Add(this.areacode_classdd);
            this.xxxx.Controls.Add(this.labelControl4);
            this.xxxx.Controls.Add(this.labelControl5);
            this.xxxx.Controls.Add(this.areacode_id);
            this.xxxx.Controls.Add(this.labelControl3);
            this.xxxx.Controls.Add(this.areacode_name);
            this.xxxx.Controls.Add(this.labelControl2);
            this.xxxx.Controls.Add(this.areacode_code);
            this.xxxx.Controls.Add(this.labelControl1);
            this.xxxx.Controls.Add(this.qqq);
            this.xxxx.Controls.Add(this.searchLookUpEdit1);
            this.xxxx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xxxx.Location = new System.Drawing.Point(0, 0);
            this.xxxx.Name = "xxxx";
            this.xxxx.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.xxxx.Size = new System.Drawing.Size(811, 347);
            this.xxxx.TabIndex = 1;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(316, 142);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(36, 14);
            this.labelControl7.TabIndex = 41;
            this.labelControl7.Text = "性别：";
            // 
            // areacode_class
            // 
            this.areacode_class.Location = new System.Drawing.Point(89, 139);
            this.areacode_class.Name = "areacode_class";
            this.areacode_class.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.areacode_class.Size = new System.Drawing.Size(169, 20);
            this.areacode_class.TabIndex = 11;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(23, 142);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(60, 14);
            this.labelControl11.TabIndex = 39;
            this.labelControl11.Text = "职工类别：";
            // 
            // e_title
            // 
            this.e_title.Location = new System.Drawing.Point(639, 102);
            this.e_title.Name = "e_title";
            this.e_title.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.e_title.Size = new System.Drawing.Size(165, 20);
            this.e_title.TabIndex = 10;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(573, 105);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(60, 14);
            this.labelControl10.TabIndex = 37;
            this.labelControl10.Text = "职工职称：";
            // 
            // areacode_nat
            // 
            this.areacode_nat.Location = new System.Drawing.Point(358, 102);
            this.areacode_nat.Name = "areacode_nat";
            this.areacode_nat.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.areacode_nat.Size = new System.Drawing.Size(165, 20);
            this.areacode_nat.TabIndex = 9;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(316, 105);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(36, 14);
            this.labelControl9.TabIndex = 35;
            this.labelControl9.Text = "国籍：";
            // 
            // areacode_level
            // 
            this.areacode_level.Location = new System.Drawing.Point(89, 102);
            this.areacode_level.Name = "areacode_level";
            this.areacode_level.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.areacode_level.Size = new System.Drawing.Size(169, 20);
            this.areacode_level.TabIndex = 8;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(23, 105);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(60, 14);
            this.labelControl6.TabIndex = 29;
            this.labelControl6.Text = "职工学历：";
            // 
            // areacode_ff
            // 
            this.areacode_ff.Location = new System.Drawing.Point(639, 65);
            this.areacode_ff.Name = "areacode_ff";
            this.areacode_ff.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.areacode_ff.Size = new System.Drawing.Size(165, 20);
            this.areacode_ff.TabIndex = 7;
            // 
            // areacode_classdd
            // 
            this.areacode_classdd.Location = new System.Drawing.Point(358, 65);
            this.areacode_classdd.Name = "areacode_classdd";
            this.areacode_classdd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.areacode_classdd.Size = new System.Drawing.Size(165, 20);
            this.areacode_classdd.TabIndex = 6;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(573, 68);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 14);
            this.labelControl4.TabIndex = 26;
            this.labelControl4.Text = "发放方式：";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(280, 68);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(72, 14);
            this.labelControl5.TabIndex = 24;
            this.labelControl5.Text = "是否发工资：";
            // 
            // areacode_id
            // 
            this.areacode_id.Location = new System.Drawing.Point(89, 65);
            this.areacode_id.Name = "areacode_id";
            this.areacode_id.Size = new System.Drawing.Size(169, 20);
            this.areacode_id.TabIndex = 5;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(23, 68);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 14);
            this.labelControl3.TabIndex = 22;
            this.labelControl3.Text = "身份证号：";
            // 
            // areacode_name
            // 
            this.areacode_name.Location = new System.Drawing.Point(639, 27);
            this.areacode_name.Name = "areacode_name";
            this.areacode_name.Size = new System.Drawing.Size(165, 20);
            this.areacode_name.TabIndex = 4;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(573, 30);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 20;
            this.labelControl2.Text = "职工姓名：";
            // 
            // areacode_code
            // 
            this.areacode_code.Location = new System.Drawing.Point(358, 27);
            this.areacode_code.Name = "areacode_code";
            this.areacode_code.Size = new System.Drawing.Size(165, 20);
            this.areacode_code.TabIndex = 3;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(292, 30);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 18;
            this.labelControl1.Text = "职工编码：";
            // 
            // qqq
            // 
            this.qqq.Location = new System.Drawing.Point(23, 30);
            this.qqq.Name = "qqq";
            this.qqq.Size = new System.Drawing.Size(60, 14);
            this.qqq.TabIndex = 17;
            this.qqq.Text = "所属部门：";
            // 
            // searchLookUpEdit1
            // 
            this.searchLookUpEdit1.Location = new System.Drawing.Point(89, 27);
            this.searchLookUpEdit1.Name = "searchLookUpEdit1";
            this.searchLookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.searchLookUpEdit1.Properties.NullText = "";
            this.searchLookUpEdit1.Properties.View = this.searchLookUpEdit1View;
            this.searchLookUpEdit1.Size = new System.Drawing.Size(169, 20);
            this.searchLookUpEdit1.TabIndex = 2;
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 347);
            this.Controls.Add(this.xxxx);
            this.Name = "update";
            this.Text = "修改";
            ((System.ComponentModel.ISupportInitialize)(this.areacode_rem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_sfty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emp_pol_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_stock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_account.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_fax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_tel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_mandate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_sex.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xxxx)).EndInit();
            this.xxxx.ResumeLayout(false);
            this.xxxx.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_class.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.e_title.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_nat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_level.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_ff.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_classdd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_id.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton wageEmpInfoMain_insert;
        private DevExpress.XtraEditors.TextEdit areacode_rem;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.ComboBoxEdit u_sfty;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.ComboBoxEdit emp_pol_code;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.ComboBoxEdit is_stock;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit u_account;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit u_fax;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit u_tel;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit u_mandate;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraEditors.ComboBoxEdit u_sex;
        private DevExpress.XtraEditors.PanelControl xxxx;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.ComboBoxEdit areacode_class;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.ComboBoxEdit e_title;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.ComboBoxEdit areacode_nat;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.ComboBoxEdit areacode_level;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.ComboBoxEdit areacode_ff;
        private DevExpress.XtraEditors.ComboBoxEdit areacode_classdd;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit areacode_id;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit areacode_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit areacode_code;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl qqq;
        private DevExpress.XtraEditors.SearchLookUpEdit searchLookUpEdit1;
    }
}