﻿namespace HospitalErpWinUI.hbos.wage.emp.info
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.areacode_classdd = new DevExpress.XtraEditors.ComboBoxEdit();
            this.q_areacode_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.q_dept_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.qqq = new DevExpress.XtraEditors.LabelControl();
            this.wageEmpInfoMain_delete = new DevExpress.XtraEditors.SimpleButton();
            this.wageEmpInfoMain_insert = new DevExpress.XtraEditors.SimpleButton();
            this.wageEmpInfoMain_select = new DevExpress.XtraEditors.SimpleButton();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_classdd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.q_areacode_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.q_dept_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panelControl2);
            this.panel1.Controls.Add(this.panelControl1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.panel1.Size = new System.Drawing.Size(1247, 306);
            this.panel1.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(10, 82);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1237, 224);
            this.panelControl2.TabIndex = 3;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1233, 220);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "选择";
            this.gridColumn1.FieldName = "checked";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "职工编码";
            this.gridColumn2.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.gridColumn2.FieldName = "emp_code";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit1_Click_1);
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "职工名称";
            this.gridColumn3.FieldName = "emp_name";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "职工编码";
            this.gridColumn4.FieldName = "dept_name";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.areacode_classdd);
            this.panelControl1.Controls.Add(this.q_areacode_code);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.q_dept_code);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.qqq);
            this.panelControl1.Controls.Add(this.wageEmpInfoMain_delete);
            this.panelControl1.Controls.Add(this.wageEmpInfoMain_insert);
            this.panelControl1.Controls.Add(this.wageEmpInfoMain_select);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(10, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1237, 82);
            this.panelControl1.TabIndex = 2;
            // 
            // areacode_classdd
            // 
            this.areacode_classdd.Location = new System.Drawing.Point(123, 12);
            this.areacode_classdd.Name = "areacode_classdd";
            this.areacode_classdd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.areacode_classdd.Size = new System.Drawing.Size(168, 20);
            this.areacode_classdd.TabIndex = 21;
            // 
            // q_areacode_code
            // 
            this.q_areacode_code.Location = new System.Drawing.Point(701, 12);
            this.q_areacode_code.Name = "q_areacode_code";
            this.q_areacode_code.Size = new System.Drawing.Size(165, 20);
            this.q_areacode_code.TabIndex = 19;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(615, 15);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 20;
            this.labelControl2.Text = "职工信息：";
            // 
            // q_dept_code
            // 
            this.q_dept_code.Location = new System.Drawing.Point(401, 12);
            this.q_dept_code.Name = "q_dept_code";
            this.q_dept_code.Size = new System.Drawing.Size(165, 20);
            this.q_dept_code.TabIndex = 17;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(341, 15);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(36, 14);
            this.labelControl1.TabIndex = 18;
            this.labelControl1.Text = "部门：";
            // 
            // qqq
            // 
            this.qqq.Location = new System.Drawing.Point(38, 15);
            this.qqq.Name = "qqq";
            this.qqq.Size = new System.Drawing.Size(60, 14);
            this.qqq.TabIndex = 16;
            this.qqq.Text = "人员类别：";
            // 
            // wageEmpInfoMain_delete
            // 
            this.wageEmpInfoMain_delete.Location = new System.Drawing.Point(1122, 36);
            this.wageEmpInfoMain_delete.Name = "wageEmpInfoMain_delete";
            this.wageEmpInfoMain_delete.Size = new System.Drawing.Size(101, 31);
            this.wageEmpInfoMain_delete.TabIndex = 14;
            this.wageEmpInfoMain_delete.Text = "删除";
            // 
            // wageEmpInfoMain_insert
            // 
            this.wageEmpInfoMain_insert.Location = new System.Drawing.Point(1006, 36);
            this.wageEmpInfoMain_insert.Name = "wageEmpInfoMain_insert";
            this.wageEmpInfoMain_insert.Size = new System.Drawing.Size(101, 31);
            this.wageEmpInfoMain_insert.TabIndex = 13;
            this.wageEmpInfoMain_insert.Text = "添加";
            this.wageEmpInfoMain_insert.Click += new System.EventHandler(this.wageEmpInfoMain_insert_Click);
            // 
            // wageEmpInfoMain_select
            // 
            this.wageEmpInfoMain_select.Location = new System.Drawing.Point(889, 36);
            this.wageEmpInfoMain_select.Name = "wageEmpInfoMain_select";
            this.wageEmpInfoMain_select.Size = new System.Drawing.Size(101, 31);
            this.wageEmpInfoMain_select.TabIndex = 12;
            this.wageEmpInfoMain_select.Text = "查询";
            this.wageEmpInfoMain_select.Click += new System.EventHandler(this.wageEmpInfoMain_select_Click);
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1247, 306);
            this.Controls.Add(this.panel1);
            this.Name = "main";
            this.Text = "职工编码录入";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_classdd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.q_areacode_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.q_dept_code.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit areacode_classdd;
        private DevExpress.XtraEditors.TextEdit q_areacode_code;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit q_dept_code;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl qqq;
        private DevExpress.XtraEditors.SimpleButton wageEmpInfoMain_delete;
        private DevExpress.XtraEditors.SimpleButton wageEmpInfoMain_insert;
        private DevExpress.XtraEditors.SimpleButton wageEmpInfoMain_select;

    }
}