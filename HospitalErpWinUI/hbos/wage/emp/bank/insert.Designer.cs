﻿namespace HospitalErpWinUI.hbos.wage.emp.bank
{
    partial class insert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.acctsysvouchtype_insert = new DevExpress.XtraEditors.SimpleButton();
            this.areacode_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.areacode_code = new DevExpress.XtraEditors.TextEdit();
            this.u_bpost = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.u_badd = new DevExpress.XtraEditors.TextEdit();
            this.u_btel = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.u_bman = new DevExpress.XtraEditors.TextEdit();
            this.areacode_flag = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.sfty = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_bpost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_badd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_btel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_bman.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_flag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sfty.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(148, 323);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 21;
            this.simpleButton2.Text = "关闭";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click_2);
            // 
            // acctsysvouchtype_insert
            // 
            this.acctsysvouchtype_insert.Location = new System.Drawing.Point(31, 323);
            this.acctsysvouchtype_insert.Name = "acctsysvouchtype_insert";
            this.acctsysvouchtype_insert.Size = new System.Drawing.Size(75, 23);
            this.acctsysvouchtype_insert.TabIndex = 20;
            this.acctsysvouchtype_insert.Text = "保存";
            this.acctsysvouchtype_insert.Click += new System.EventHandler(this.acctsysvouchtype_insert_Click_1);
            // 
            // areacode_name
            // 
            this.areacode_name.Location = new System.Drawing.Point(109, 65);
            this.areacode_name.Name = "areacode_name";
            this.areacode_name.Size = new System.Drawing.Size(147, 20);
            this.areacode_name.TabIndex = 19;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(44, 68);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 18;
            this.labelControl2.Text = "银行名称：";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(46, 32);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 17;
            this.labelControl1.Text = "银行编码：";
            // 
            // areacode_code
            // 
            this.areacode_code.Location = new System.Drawing.Point(109, 29);
            this.areacode_code.Name = "areacode_code";
            this.areacode_code.Size = new System.Drawing.Size(147, 20);
            this.areacode_code.TabIndex = 16;
            // 
            // u_bpost
            // 
            this.u_bpost.Location = new System.Drawing.Point(109, 145);
            this.u_bpost.Name = "u_bpost";
            this.u_bpost.Size = new System.Drawing.Size(147, 20);
            this.u_bpost.TabIndex = 25;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(67, 148);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(36, 14);
            this.labelControl3.TabIndex = 24;
            this.labelControl3.Text = "邮编：";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(46, 107);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 14);
            this.labelControl4.TabIndex = 23;
            this.labelControl4.Text = "银行地址：";
            // 
            // u_badd
            // 
            this.u_badd.Location = new System.Drawing.Point(109, 104);
            this.u_badd.Name = "u_badd";
            this.u_badd.Size = new System.Drawing.Size(147, 20);
            this.u_badd.TabIndex = 22;
            // 
            // u_btel
            // 
            this.u_btel.Location = new System.Drawing.Point(109, 222);
            this.u_btel.Name = "u_btel";
            this.u_btel.Size = new System.Drawing.Size(147, 20);
            this.u_btel.TabIndex = 29;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(44, 225);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 14);
            this.labelControl5.TabIndex = 28;
            this.labelControl5.Text = "联系电话：";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(46, 184);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(60, 14);
            this.labelControl6.TabIndex = 27;
            this.labelControl6.Text = "法人代表：";
            // 
            // u_bman
            // 
            this.u_bman.Location = new System.Drawing.Point(109, 181);
            this.u_bman.Name = "u_bman";
            this.u_bman.Size = new System.Drawing.Size(147, 20);
            this.u_bman.TabIndex = 26;
            // 
            // areacode_flag
            // 
            this.areacode_flag.Location = new System.Drawing.Point(109, 257);
            this.areacode_flag.Name = "areacode_flag";
            this.areacode_flag.Size = new System.Drawing.Size(147, 20);
            this.areacode_flag.TabIndex = 31;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(67, 260);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(36, 14);
            this.labelControl7.TabIndex = 30;
            this.labelControl7.Text = "备注：";
            // 
            // sfty
            // 
            this.sfty.Location = new System.Drawing.Point(109, 283);
            this.sfty.Name = "sfty";
            this.sfty.Properties.Caption = "是否停用";
            this.sfty.Size = new System.Drawing.Size(75, 19);
            this.sfty.TabIndex = 32;
            // 
            // insert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(319, 380);
            this.Controls.Add(this.sfty);
            this.Controls.Add(this.areacode_flag);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.u_btel);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.u_bman);
            this.Controls.Add(this.u_bpost);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.u_badd);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.acctsysvouchtype_insert);
            this.Controls.Add(this.areacode_name);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.areacode_code);
            this.Name = "insert";
            this.Text = "insert";
            ((System.ComponentModel.ISupportInitialize)(this.areacode_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_bpost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_badd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_btel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u_bman.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.areacode_flag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sfty.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton acctsysvouchtype_insert;
        private DevExpress.XtraEditors.TextEdit areacode_name;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit areacode_code;
        private DevExpress.XtraEditors.TextEdit u_bpost;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit u_badd;
        private DevExpress.XtraEditors.TextEdit u_btel;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit u_bman;
        private DevExpress.XtraEditors.TextEdit areacode_flag;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.CheckEdit sfty;
    }
}