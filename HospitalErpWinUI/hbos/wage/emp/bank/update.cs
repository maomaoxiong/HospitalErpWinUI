﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.wage.emp.bank
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        private Validator validator;
        private List<Perm> _perms { get; set; }
        public update()
        {
            InitializeComponent();
        }
        public update(DaoHelper helper, List<Perm> perms, string content_code, string content_name)
        {
            
            InitializeComponent();
            _helper = helper;
            _perms = perms;
            validator = new Validator();
            this.StartPosition = FormStartPosition.CenterParent;
            ServiceParamter serviceParamter = new ServiceParamter();
            Dictionary<string, string> paramters = new Dictionary<string, string>();
            serviceParamter.ServiceId = "wageEmpBankMain_update_load";
            paramters.Add("content_code", content_code);
            paramters.Add("content_code1", content_code);
            
            serviceParamter.Paramters = paramters;

            DataTable dt = _helper.ReadSqlForDataTable(serviceParamter);
            if( dt.Rows.Count==0){
                MessageBox.Show("查询失败！");

            }else{
                  
                this.areacode_code.Text = dt.Rows[0]["bank_code"].ToString();
                this.areacode_name.Text = dt.Rows[0]["bank_name"].ToString();
                this.u_badd.Text = dt.Rows[0]["address"].ToString();
                this.u_bpost.Text = dt.Rows[0]["post_code"].ToString();
                this.u_bman.Text = dt.Rows[0]["manager"].ToString();
                this.u_btel.Text = dt.Rows[0]["link_phone"].ToString();
                this.areacode_flag.Text = dt.Rows[0]["remark"].ToString();
                this.sfty.CheckState = CheckState.Unchecked; ;
                if (dt.Rows[0]["is_stop"].ToString() != "False")
                {
                    this.sfty.CheckState = CheckState.Checked;
                }
            }
           
          //  this.content_code.Text = content_code;
          //  this.content_name.Text = content_name;

            // Init();
        }

        private void acctsysvouchtype_insert_Click(object sender, EventArgs e)
        {
            if (!Validate())
            {
                return;
            }
            try
            {
                u_update1();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //     MessageForm.Show("新增角色成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }
        }
        private bool Validate()
        {

            if (string.IsNullOrEmpty(areacode_code.Text))
            {
                MessageForm.Warning("名称不能为空!");
                return false;
            }
            return true;
        }

        private bool u_update1()
        {
            ServiceParamter serviceParamter = new ServiceParamter();
            // <root><r currency_code="qqqq" currency_name="qqqq"  digit_num="2"  currency_rate="3" currency_self="0" currency_way="0" currency_stop="0" insertFlag="2"></r></root>
            serviceParamter.ServiceId = "wageEmpBankMain_update";
            Dictionary<string, string> paramters = new Dictionary<string, string>();

            paramters.Add("areacode_code", areacode_code.Text);
            paramters.Add("areacode_name", areacode_name.Text);
            paramters.Add("u_badd", u_badd.Text);
            paramters.Add("u_bpost", u_bpost.Text);
            paramters.Add("u_bman", u_bman.Text);
            paramters.Add("u_btel", u_btel.Text);
            paramters.Add("areacode_flag", areacode_flag.Text);
            paramters.Add("sfty", "0");
            serviceParamter.Paramters = paramters;

            return _helper.WirteSql(serviceParamter);

        }
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

    }
}
