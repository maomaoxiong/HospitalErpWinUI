﻿namespace HospitalErpWinUI.hbos.wage.emp.bank
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.wageEmpBankMain_insert = new DevExpress.XtraEditors.SimpleButton();
            this.wageEmpBankMain_select = new DevExpress.XtraEditors.SimpleButton();
            this.wageEmpBankMain_delete = new DevExpress.XtraEditors.SimpleButton();
            this.qqq = new DevExpress.XtraEditors.LabelControl();
            this.q_areacode_code = new DevExpress.XtraEditors.TextEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.q_areacode_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.wageEmpBankMain_insert);
            this.panelControl1.Controls.Add(this.wageEmpBankMain_select);
            this.panelControl1.Controls.Add(this.wageEmpBankMain_delete);
            this.panelControl1.Controls.Add(this.qqq);
            this.panelControl1.Controls.Add(this.q_areacode_code);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(891, 55);
            this.panelControl1.TabIndex = 0;
            // 
            // wageEmpBankMain_insert
            // 
            this.wageEmpBankMain_insert.Location = new System.Drawing.Point(551, 12);
            this.wageEmpBankMain_insert.Name = "wageEmpBankMain_insert";
            this.wageEmpBankMain_insert.Size = new System.Drawing.Size(87, 27);
            this.wageEmpBankMain_insert.TabIndex = 14;
            this.wageEmpBankMain_insert.Text = "添加";
            this.wageEmpBankMain_insert.Click += new System.EventHandler(this.wageEmpBankMain_insert_Click);
            // 
            // wageEmpBankMain_select
            // 
            this.wageEmpBankMain_select.Location = new System.Drawing.Point(436, 12);
            this.wageEmpBankMain_select.Name = "wageEmpBankMain_select";
            this.wageEmpBankMain_select.Size = new System.Drawing.Size(87, 27);
            this.wageEmpBankMain_select.TabIndex = 12;
            this.wageEmpBankMain_select.Text = "查询";
            this.wageEmpBankMain_select.Click += new System.EventHandler(this.wageEmpBankMain_select_Click);
            // 
            // wageEmpBankMain_delete
            // 
            this.wageEmpBankMain_delete.Location = new System.Drawing.Point(659, 12);
            this.wageEmpBankMain_delete.Name = "wageEmpBankMain_delete";
            this.wageEmpBankMain_delete.Size = new System.Drawing.Size(87, 27);
            this.wageEmpBankMain_delete.TabIndex = 13;
            this.wageEmpBankMain_delete.Text = "删除";
            this.wageEmpBankMain_delete.Click += new System.EventHandler(this.wageEmpBankMain_delete_Click);
            // 
            // qqq
            // 
            this.qqq.Location = new System.Drawing.Point(29, 16);
            this.qqq.Name = "qqq";
            this.qqq.Size = new System.Drawing.Size(60, 14);
            this.qqq.TabIndex = 10;
            this.qqq.Text = "编码名称：";
            // 
            // q_areacode_code
            // 
            this.q_areacode_code.Location = new System.Drawing.Point(128, 13);
            this.q_areacode_code.Name = "q_areacode_code";
            this.q_areacode_code.Size = new System.Drawing.Size(255, 20);
            this.q_areacode_code.TabIndex = 11;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 55);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(891, 240);
            this.panelControl2.TabIndex = 1;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.gridControl1.Size = new System.Drawing.Size(887, 236);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "选择";
            this.gridColumn1.FieldName = "checked";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "银行编码";
            this.gridColumn2.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.gridColumn2.FieldName = "bank_code";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit1_Click);
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "银行名称";
            this.gridColumn3.FieldName = "bank_name";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(891, 295);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "main";
            this.Text = "main";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.q_areacode_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton wageEmpBankMain_insert;
        private DevExpress.XtraEditors.SimpleButton wageEmpBankMain_select;
        private DevExpress.XtraEditors.SimpleButton wageEmpBankMain_delete;
        private DevExpress.XtraEditors.LabelControl qqq;
        private DevExpress.XtraEditors.TextEdit q_areacode_code;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
    }
}