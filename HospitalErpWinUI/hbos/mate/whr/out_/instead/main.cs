﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraGrid;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.whr.out_.instead
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        //  public List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public main()
        {
            InitializeComponent();
            _helper = new DaoHelper();
            this.StartPosition = FormStartPosition.CenterParent;
            this.Text = "代销出库";
            //sthis.gridView1.BestFitColumns=true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            // this.gridView2.BestFitColumns(true);
            this.gridView1.OptionsView.ShowGroupPanel = false;
        }

        private void mateWhrOutInstead_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            DataTable dt = _helper.ReadSqlForDataTable("mateWhrOutInstead_select",
                new[] { in_out_date_begin.Text.Trim(),
                    in_out_date_end.Text.Trim(),
               
                FormHelper.GetValueForComboBoxEdit(state),
                 FormHelper.GetValueForSearchLookUpEdit(storage),
                make_date_begin.Text.Trim(),
                make_date_end.Text.Trim(),
                 FormHelper.GetValueForComboBoxEdit(stock_type_code),
                iow_no.Text.Trim() 
                
                });

            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                insert insertFrm = new insert(this._helper, this._perms);

                if (insertFrm.ShowDialog() == DialogResult.OK)
                {
                    Query();
                }
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                int index = gridView1.GetDataSourceRowIndex(gridView1.FocusedRowHandle);
                string acct_subj_code = _user.Rows[index]["_iow_id"].ToString();
                update frm = new update(this._helper, this._perms, acct_subj_code);

                if (frm.ShowDialog() == DialogResult.No)
                {
                    MessageForm.Warning("修改失败！");
                }
                Query();
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            try
            {
                create_from_his insertFrm = new create_from_his(this._helper, this._perms);

                if (insertFrm.ShowDialog() == DialogResult.OK)
                {
                    Query();
                }
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
    }
}
