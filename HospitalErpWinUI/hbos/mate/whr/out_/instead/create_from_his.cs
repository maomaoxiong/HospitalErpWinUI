﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.whr.out_.instead
{
    public partial class create_from_his : Form
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        //  public List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
         public create_from_his()
        {
            InitializeComponent();
         }
        public create_from_his(Dao.DaoHelper helper, List<Perm> perms)
        {
            InitializeComponent();
            labelControl5.ForeColor = Color.Red;
            _helper = new DaoHelper();
            this.StartPosition = FormStartPosition.CenterParent;
            this.Text = "根据医嘱生成";
            //sthis.gridView1.BestFitColumns=true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            // this.gridView2.BestFitColumns(true);
            this.gridView1.OptionsView.ShowGroupPanel = false;
        }

        private void mateWhrOutInstead_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            DataTable dt = _helper.ReadSqlForDataTable("mateWhr_creat_fromhis_instead_select",
                new[] { fromdate1.Text.Trim(),
                    todate1.Text.Trim(),
                 FormHelper.GetValueForSearchLookUpEdit(dept_code),
                  hosptial_no.Text.Trim(),
                  mate_inv.Text.Trim(),
                  patient_name.Text.Trim(),
                doc_name.Text.Trim(),
                FormHelper.GetValueForCheckEdit(is_state).Trim()
                });

            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
       
        }
    }
}
