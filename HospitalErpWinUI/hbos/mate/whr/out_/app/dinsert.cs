﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.whr.out_.app
{
    public partial class dinsert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public dinsert()
        {
            InitializeComponent();
        }
        private string storecode;
        public dinsert(Dao.DaoHelper helper, List<Perm> perms, string store_code)
        {
            InitializeComponent();
            storecode = store_code;
            this._helper = helper;
            this._perms = perms;
            this.Text = "批量添加";
            this.StartPosition = FormStartPosition.CenterParent;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;

        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void mateWhrOutApp_insert_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            DataTable dt = _helper.ReadSqlForDataTable("mateWhrOutApp_insert_select",
                new[] { "",
                    FormHelper.GetValueForSearchLookUpEdit(mate_type_code),
                    inv_code.Text.Trim(),
                    storecode,
                    FormHelper.GetValueForCheckEdit(stock_flag).Trim()

                });

            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
        }
    }
}
