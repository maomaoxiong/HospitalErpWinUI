﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.whr.out_.app
{
    public partial class insert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        public insert()
        {
            InitializeComponent();
        }
        public insert(Dao.DaoHelper helper, List<Perm> perms)
        {
            InitializeComponent();
            this._helper = helper;
            this._perms = perms;
            this.Text = "录入";
            this.StartPosition = FormStartPosition.CenterParent;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ColumnAutoWidth = false;

        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                string store_Code = FormHelper.GetValueForSearchLookUpEdit(store_code);
                dinsert insertFrm = new dinsert(this._helper, this._perms,store_Code);

                insertFrm.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
    }
}
