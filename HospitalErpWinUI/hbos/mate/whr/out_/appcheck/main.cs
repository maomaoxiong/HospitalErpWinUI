﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraGrid;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.whr.out_.appcheck
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        //  public List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public main()
        {
            InitializeComponent();
            this.Text = "科室申请审核";

            _helper = new DaoHelper();
            //  this.StartPosition = FormStartPosition.CenterParent;
            //  this.gridView3.OptionsView.ColumnAutoWidth = false;
            // this.gridView3.BestFitColumns(true);
            this.gridView3.OptionsView.ShowGroupPanel = false;

            this.gridView3.BestFitColumns(false);
        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            if (gridView3.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                int index = gridView3.GetDataSourceRowIndex(gridView3.FocusedRowHandle);
                string acct_subj_code = _user.Rows[index]["_id"].ToString();
                update frm = new update(this._helper, this._perms, acct_subj_code);

                if (frm.ShowDialog() == DialogResult.No)
                {
                    MessageForm.Warning("操作失败！");
                }
                Query();
            }
        }
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Enter)　　// 按下的是回车键
            {
                foreach (Control c in this.Controls)
                {
                    if (c is System.Windows.Forms.TextBox)　　// 当前控件是文本框控件
                    {
                        keyData = Keys.Tab;
                    }
                }
                keyData = Keys.Tab;
            }
            return base.ProcessDialogKey(keyData);
        }
        private void mateWhrOutAppCheck_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            DataTable dt = _helper.ReadSqlForDataTable("mateWhrOutAppCheck_select",
                new[] { fromdate.Text.Trim(),
                    todate.Text.Trim(),
                FormHelper.GetValueForSearchLookUpEdit(dept_code),
                FormHelper.GetValueForComboBoxEdit(app_state),
                brief.Text.Trim(),
                  FormHelper.GetValueForSearchLookUpEdit(store_code),
                 FormHelper.GetValueForSearchLookUpEdit(emp_name),
                  FormHelper.GetValueForCheckEdit(px_dept)

                });

            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;

        }
    }
}
