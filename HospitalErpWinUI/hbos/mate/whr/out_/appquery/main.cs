﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.whr.out_.appquery
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        //  public List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public main()
        {
            InitializeComponent();
            this.Text = "申领状况查询";
           
            _helper = new DaoHelper();
          //  this.StartPosition = FormStartPosition.CenterParent;
          //  this.gridView3.OptionsView.ColumnAutoWidth = false;
            // this.gridView3.BestFitColumns(true);
            this.gridView3.OptionsView.ShowGroupPanel = false;

            this.gridView3.BestFitColumns(false);
        }

        private void mateWhrOutAppQuery_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            DataTable dt = _helper.ReadSqlForDataTable("mateWhrOutAppQuery_select",
                new[] { fromdate.Text.Trim(),
                    todate.Text.Trim(),
                FormHelper.GetValueForSearchLookUpEdit(dept_code),
                FormHelper.GetValueForComboBoxEdit(app_state),
                brief.Text.Trim(),
                  FormHelper.GetValueForSearchLookUpEdit(store_code),
                 FormHelper.GetValueForSearchLookUpEdit(emp_name),
                  app_no.Text.Trim(),

                });

            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;
             
        }

        private void panelControl2_Paint(object sender, PaintEventArgs e)
        {

        }
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Enter)　　// 按下的是回车键
            {
                foreach (Control c in this.Controls)
                {
                    if (c is System.Windows.Forms.TextBox)　　// 当前控件是文本框控件
                    {
                        keyData = Keys.Tab;
                    }
                }
                keyData = Keys.Tab;
            }
            return base.ProcessDialogKey(keyData);
        }
    }
}
