﻿using Dao;
using HospitalErpData.MenuHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.whr.in_.common
{    
    public partial class update : Form
    {
        private DaoHelper _helper;
        DataTable _user = new DataTable();
        private List<Perm> _perms { get; set; }
        public update()
        {
            InitializeComponent();
        }
        public update(DaoHelper helper, List<Perm> perms, string iow_id)
        {

            InitializeComponent();
            _helper = helper;
            _perms = perms;
            this.Text = "修改";
            this.StartPosition = FormStartPosition.CenterParent;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            load_detail(iow_id);//加载明细数据
        }
        private void load_detail(string iow_id)
        {
            DataTable dt = _helper.ReadSqlForDataTable("mateWhrInCommon_select_load_detail",
                new[] { iow_id });

            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;
        }
    }
}
