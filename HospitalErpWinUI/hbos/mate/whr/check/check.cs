﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.whr.check
{
    public partial class check : RibbonForm
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        //  public List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public check()
        {
            InitializeComponent();
            this.Text = "未确认单据";
            _helper = new DaoHelper();

            this.gridView1.OptionsView.ShowGroupPanel = false;

            this.gridView1.BestFitColumns(false);
        }

        private void mateWhrFinalCharge_charge_check_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            DataTable dt = _helper.ReadSqlForDataTable("mateWhrFinalCharge_charge_check_select",
                new[] { "201907"
                });

            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;

        
        }
    }
}
