﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.whr.check
{
    public partial class insert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        public insert()
        {
            InitializeComponent();
        }
        public insert(Dao.DaoHelper helper, List<Perm> perms)
        {
            InitializeComponent();
            this._helper = helper;
            this._perms = perms;
            this.Text = "录入";
            this.StartPosition = FormStartPosition.CenterParent;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.BestFitColumns(false);

        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void mateWhrOutAppCheck_select_Click(object sender, EventArgs e)
        {
            try
            {
                ddinsert insertFrm = new ddinsert(this._helper, this._perms, FormHelper.GetValueForSearchLookUpEdit(mate_whr));

                insertFrm.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            try
            {
                make2 insertFrm = new make2(this._helper, this._perms, FormHelper.GetValueForSearchLookUpEdit(mate_whr));

                insertFrm.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
    }
}
