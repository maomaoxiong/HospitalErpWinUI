﻿namespace HospitalErpWinUI.hbos.mate.whr.check
{
    partial class make2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.creat_date = new DevExpress.XtraEditors.DateEdit();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.mate_whr = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.stock_flag = new DevExpress.XtraEditors.CheckEdit();
            this.mate_type_code = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.mateWhrOutAppCheck_select = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.creat_date.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.creat_date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mate_whr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stock_flag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mate_type_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.mateWhrOutAppCheck_select);
            this.panelControl1.Controls.Add(this.mate_type_code);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.stock_flag);
            this.panelControl1.Controls.Add(this.mate_whr);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.creat_date);
            this.panelControl1.Controls.Add(this.labelControl37);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(766, 323);
            this.panelControl1.TabIndex = 0;
            // 
            // creat_date
            // 
            this.creat_date.EditValue = null;
            this.creat_date.Location = new System.Drawing.Point(191, 101);
            this.creat_date.Name = "creat_date";
            this.creat_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.creat_date.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.creat_date.Size = new System.Drawing.Size(180, 20);
            this.creat_date.TabIndex = 180;
            // 
            // labelControl37
            // 
            this.labelControl37.Location = new System.Drawing.Point(95, 107);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(60, 14);
            this.labelControl37.TabIndex = 181;
            this.labelControl37.Text = "盘点日期：";
            // 
            // mate_whr
            // 
            this.mate_whr.Location = new System.Drawing.Point(504, 101);
            this.mate_whr.Name = "mate_whr";
            this.mate_whr.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.mate_whr.Properties.NullText = "";
            this.mate_whr.Properties.View = this.searchLookUpEdit1View;
            this.mate_whr.Size = new System.Drawing.Size(180, 20);
            this.mate_whr.TabIndex = 182;
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(440, 107);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(36, 14);
            this.labelControl1.TabIndex = 183;
            this.labelControl1.Text = "仓库：";
            // 
            // stock_flag
            // 
            this.stock_flag.Location = new System.Drawing.Point(95, 154);
            this.stock_flag.Name = "stock_flag";
            this.stock_flag.Properties.Caption = "包含零库存的材料";
            this.stock_flag.Size = new System.Drawing.Size(188, 19);
            this.stock_flag.TabIndex = 194;
            // 
            // mate_type_code
            // 
            this.mate_type_code.Location = new System.Drawing.Point(504, 154);
            this.mate_type_code.Name = "mate_type_code";
            this.mate_type_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.mate_type_code.Properties.NullText = "";
            this.mate_type_code.Properties.View = this.gridView1;
            this.mate_type_code.Size = new System.Drawing.Size(180, 20);
            this.mate_type_code.TabIndex = 195;
            // 
            // gridView1
            // 
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(416, 157);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 196;
            this.labelControl2.Text = "物资类别：";
            // 
            // mateWhrOutAppCheck_select
            // 
            this.mateWhrOutAppCheck_select.Location = new System.Drawing.Point(213, 209);
            this.mateWhrOutAppCheck_select.Name = "mateWhrOutAppCheck_select";
            this.mateWhrOutAppCheck_select.Size = new System.Drawing.Size(87, 27);
            this.mateWhrOutAppCheck_select.TabIndex = 197;
            this.mateWhrOutAppCheck_select.Text = "确定";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(355, 209);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(87, 27);
            this.simpleButton1.TabIndex = 198;
            this.simpleButton1.Text = "关闭";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // make2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(766, 323);
            this.Controls.Add(this.panelControl1);
            this.Name = "make2";
            this.Text = "make2";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.creat_date.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.creat_date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mate_whr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stock_flag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mate_type_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.DateEdit creat_date;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.SearchLookUpEdit mate_whr;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckEdit stock_flag;
        private DevExpress.XtraEditors.SearchLookUpEdit mate_type_code;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton mateWhrOutAppCheck_select;
    }
}