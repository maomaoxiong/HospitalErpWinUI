﻿using Dao;
using HospitalErpData.MenuHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.whr.insteadcheck
{
    public partial class make2 : Form
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        //  public List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public make2()
        {
            InitializeComponent();
        }
         private string storeCode;
         public make2(Dao.DaoHelper helper, List<Perm> perms, string store_code)
        {
            InitializeComponent();
            this.Text = "引入仓库材料帐面数";
            storeCode = store_code;
            _helper = new DaoHelper();
            //  this.StartPosition = FormStartPosition.CenterParent;
            //  this.gridView3.OptionsView.ColumnAutoWidth = false;
            // this.gridView3.BestFitColumns(true);
            this.gridView1.OptionsView.ShowGroupPanel = false;

            this.gridView1.BestFitColumns(false);
        }
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
