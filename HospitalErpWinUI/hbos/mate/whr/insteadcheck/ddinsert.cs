﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.whr.insteadcheck
{
    public partial class ddinsert : Form
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        //  public List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public ddinsert()
        {
            InitializeComponent();
             
        }
        private string storeCode;
        public ddinsert(Dao.DaoHelper helper, List<Perm> perms,string store_code)
        {
            InitializeComponent();
            this.Text = "引入材料";
            storeCode = store_code;
            _helper = new DaoHelper();
            //  this.StartPosition = FormStartPosition.CenterParent;
            //  this.gridView3.OptionsView.ColumnAutoWidth = false;
            // this.gridView3.BestFitColumns(true);
            this.gridView1.OptionsView.ShowGroupPanel = false;

            this.gridView1.BestFitColumns(false);
        }

        private void mateWhrCheck_insert_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            DataTable dt = _helper.ReadSqlForDataTable("mateWhrinsteadCheck_insert_select",
                new[] {
                    "1",// storeCode,
                    "",
                     FormHelper.GetValueForSearchLookUpEdit(matetype),
                    inv_code.Text.Trim()
                });

            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;

        }
    }
}
