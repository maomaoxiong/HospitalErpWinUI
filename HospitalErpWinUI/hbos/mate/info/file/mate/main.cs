﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraGrid;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.info.file.mate
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        //  public List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public main()
        {
            InitializeComponent();
            _helper = new DaoHelper();
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.Text = "物资材料";
        }

        private void labelControl11_Click(object sender, EventArgs e)
        {

        }

        private void mateInfoFileMate_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            DataTable dt = _helper.ReadSqlForDataTable("mateInfoFileMate_select",
                new[] { G_User.user.Comp_code, G_User.user.Copy_code,
                FormHelper.GetValueForSearchLookUpEdit(mate_type_code),
                inv_code.Text.Trim(),
                inv_name.Text.Trim(),
               "1",// FormHelper.GetValueForComboBoxEdit(mate_amortize),
                FormHelper.GetValueForSearchLookUpEdit(cus_code),
                FormHelper.GetValueForSearchLookUpEdit(factory_code),
                FormHelper.GetValueForComboBoxEdit(mate_is_highvalue1),
                inv_structure.Text.Trim(),
                inv_usage.Text.Trim(),
                FormHelper.GetValueForSearchLookUpEdit(fina_type_code),
                FormHelper.GetValueForComboBoxEdit(mate_is_dura),
                FormHelper.GetValueForComboBoxEdit(mate_is_sec_whg),
                FormHelper.GetValueForComboBoxEdit(mate_is_charge),
                "0"//FormHelper.GetValueForComboBoxEdit(is_stop)
                });

            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            try
            {
                spell insertFrm = new spell(this._helper, this._perms);

                if (insertFrm.ShowDialog() == DialogResult.OK)
                {
                    Query();
                }
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                int index = gridView1.GetDataSourceRowIndex(gridView1.FocusedRowHandle);
                string inv_code = _user.Rows[index]["inv_code"].ToString();
                update frm = new update(this._helper, this._perms, inv_code);

                if (frm.ShowDialog() == DialogResult.No)
                {
                    MessageForm.Warning("修改失败！");
                }
                Query();
            }
        }
    }
}
