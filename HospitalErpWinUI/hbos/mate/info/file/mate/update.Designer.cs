﻿namespace HospitalErpWinUI.hbos.mate.info.file.mate
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.sysDictsUnitinfoVenType_insert = new DevExpress.XtraEditors.SimpleButton();
            this.inv_net = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl49 = new DevExpress.XtraEditors.LabelControl();
            this.inv_local = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.inv_country = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.inv_com_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.inv_usage = new DevExpress.XtraEditors.TextEdit();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.inv_structure = new DevExpress.XtraEditors.TextEdit();
            this.mate_is_highvalue = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.fina_type_code = new DevExpress.XtraEditors.TextEdit();
            this.mate_amortize = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.price_method = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.ref_price = new DevExpress.XtraEditors.TextEdit();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.inv_ID = new DevExpress.XtraEditors.TextEdit();
            this.edate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.sdate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.per_volum = new DevExpress.XtraEditors.TextEdit();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.per_weight = new DevExpress.XtraEditors.TextEdit();
            this.factory_code = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.agent_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.brand_name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.cert_code = new DevExpress.XtraEditors.TextEdit();
            this.is_bar = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.is_tender = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.is_cert = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.is_charge = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.plan_accord = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.is_add_sale = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.is_shel_make = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.is_dura = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.is_overstock = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.is_quality = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.is_batch = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.is_sec_whg = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.stay_time = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.abc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.eco_bat = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.buy_ahead = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.refe_cost = new DevExpress.XtraEditors.TextEdit();
            this.cus_code = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.unit_rate = new DevExpress.XtraEditors.TextEdit();
            this.unit_dicts1 = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.unit_dicts = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.inv_attr_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.mate_type_code = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.alias = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.inv_name = new DevExpress.XtraEditors.TextEdit();
            this.mate_manage_grade = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.inv_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.mateInfoFileMate_select = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inv_net.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inv_local.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inv_country.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inv_com_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inv_usage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inv_structure.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mate_is_highvalue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fina_type_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mate_amortize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.price_method.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ref_price.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inv_ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.per_volum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.per_weight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.factory_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.agent_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brand_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cert_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_bar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_tender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_cert.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_charge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plan_accord.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_add_sale.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.is_shel_make.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_dura.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_overstock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_quality.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_batch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_sec_whg.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stay_time.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.abc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eco_bat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buy_ahead.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.refe_cost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cus_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unit_rate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unit_dicts1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unit_dicts.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inv_attr_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mate_type_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alias.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inv_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mate_manage_grade.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inv_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridView4
            // 
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(588, 658);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 9;
            this.simpleButton2.Text = "关闭";
            // 
            // sysDictsUnitinfoVenType_insert
            // 
            this.sysDictsUnitinfoVenType_insert.Location = new System.Drawing.Point(471, 658);
            this.sysDictsUnitinfoVenType_insert.Name = "sysDictsUnitinfoVenType_insert";
            this.sysDictsUnitinfoVenType_insert.Size = new System.Drawing.Size(75, 23);
            this.sysDictsUnitinfoVenType_insert.TabIndex = 8;
            this.sysDictsUnitinfoVenType_insert.Text = "保存";
            // 
            // inv_net
            // 
            this.inv_net.Location = new System.Drawing.Point(444, 591);
            this.inv_net.Name = "inv_net";
            this.inv_net.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.inv_net.Size = new System.Drawing.Size(172, 20);
            this.inv_net.TabIndex = 140;
            // 
            // labelControl49
            // 
            this.labelControl49.Location = new System.Drawing.Point(365, 597);
            this.labelControl49.Name = "labelControl49";
            this.labelControl49.Size = new System.Drawing.Size(60, 14);
            this.labelControl49.TabIndex = 139;
            this.labelControl49.Text = "是否挂网：";
            // 
            // inv_local
            // 
            this.inv_local.Location = new System.Drawing.Point(127, 591);
            this.inv_local.Name = "inv_local";
            this.inv_local.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.inv_local.Size = new System.Drawing.Size(147, 20);
            this.inv_local.TabIndex = 138;
            // 
            // labelControl50
            // 
            this.labelControl50.Location = new System.Drawing.Point(53, 594);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(60, 14);
            this.labelControl50.TabIndex = 137;
            this.labelControl50.Text = "是否本地：";
            // 
            // inv_country
            // 
            this.inv_country.Location = new System.Drawing.Point(773, 553);
            this.inv_country.Name = "inv_country";
            this.inv_country.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.inv_country.Size = new System.Drawing.Size(147, 20);
            this.inv_country.TabIndex = 136;
            // 
            // labelControl48
            // 
            this.labelControl48.Location = new System.Drawing.Point(699, 556);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(60, 14);
            this.labelControl48.TabIndex = 135;
            this.labelControl48.Text = "是否国产：";
            // 
            // inv_com_code
            // 
            this.inv_com_code.Location = new System.Drawing.Point(444, 553);
            this.inv_com_code.Name = "inv_com_code";
            this.inv_com_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.inv_com_code.Size = new System.Drawing.Size(172, 20);
            this.inv_com_code.TabIndex = 134;
            // 
            // labelControl47
            // 
            this.labelControl47.Location = new System.Drawing.Point(363, 559);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(60, 14);
            this.labelControl47.TabIndex = 133;
            this.labelControl47.Text = "是否代销：";
            // 
            // labelControl46
            // 
            this.labelControl46.Location = new System.Drawing.Point(53, 556);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(60, 14);
            this.labelControl46.TabIndex = 132;
            this.labelControl46.Text = "材料用途：";
            // 
            // inv_usage
            // 
            this.inv_usage.Location = new System.Drawing.Point(127, 553);
            this.inv_usage.Name = "inv_usage";
            this.inv_usage.Size = new System.Drawing.Size(147, 20);
            this.inv_usage.TabIndex = 131;
            // 
            // labelControl45
            // 
            this.labelControl45.Location = new System.Drawing.Point(699, 523);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(60, 14);
            this.labelControl45.TabIndex = 130;
            this.labelControl45.Text = "材料结构：";
            // 
            // inv_structure
            // 
            this.inv_structure.Location = new System.Drawing.Point(773, 514);
            this.inv_structure.Name = "inv_structure";
            this.inv_structure.Size = new System.Drawing.Size(147, 20);
            this.inv_structure.TabIndex = 129;
            // 
            // mate_is_highvalue
            // 
            this.mate_is_highvalue.Location = new System.Drawing.Point(444, 517);
            this.mate_is_highvalue.Name = "mate_is_highvalue";
            this.mate_is_highvalue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.mate_is_highvalue.Size = new System.Drawing.Size(172, 20);
            this.mate_is_highvalue.TabIndex = 128;
            // 
            // labelControl44
            // 
            this.labelControl44.Location = new System.Drawing.Point(363, 523);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(60, 14);
            this.labelControl44.TabIndex = 127;
            this.labelControl44.Text = "是否高值：";
            // 
            // labelControl43
            // 
            this.labelControl43.Location = new System.Drawing.Point(29, 520);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(84, 14);
            this.labelControl43.TabIndex = 126;
            this.labelControl43.Text = "物资财务类别：";
            // 
            // fina_type_code
            // 
            this.fina_type_code.Enabled = false;
            this.fina_type_code.Location = new System.Drawing.Point(127, 517);
            this.fina_type_code.Name = "fina_type_code";
            this.fina_type_code.Size = new System.Drawing.Size(147, 20);
            this.fina_type_code.TabIndex = 125;
            // 
            // mate_amortize
            // 
            this.mate_amortize.Location = new System.Drawing.Point(773, 482);
            this.mate_amortize.Name = "mate_amortize";
            this.mate_amortize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.mate_amortize.Size = new System.Drawing.Size(147, 20);
            this.mate_amortize.TabIndex = 124;
            // 
            // labelControl42
            // 
            this.labelControl42.Location = new System.Drawing.Point(699, 485);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(60, 14);
            this.labelControl42.TabIndex = 123;
            this.labelControl42.Text = "摊销方式：";
            // 
            // price_method
            // 
            this.price_method.Location = new System.Drawing.Point(444, 479);
            this.price_method.Name = "price_method";
            this.price_method.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.price_method.Size = new System.Drawing.Size(172, 20);
            this.price_method.TabIndex = 122;
            // 
            // labelControl41
            // 
            this.labelControl41.Location = new System.Drawing.Point(375, 485);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(48, 14);
            this.labelControl41.TabIndex = 121;
            this.labelControl41.Text = "计价法：";
            // 
            // labelControl40
            // 
            this.labelControl40.Location = new System.Drawing.Point(53, 482);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(60, 14);
            this.labelControl40.TabIndex = 120;
            this.labelControl40.Text = "参考单价：";
            // 
            // ref_price
            // 
            this.ref_price.Location = new System.Drawing.Point(127, 479);
            this.ref_price.Name = "ref_price";
            this.ref_price.Size = new System.Drawing.Size(147, 20);
            this.ref_price.TabIndex = 119;
            // 
            // labelControl39
            // 
            this.labelControl39.Location = new System.Drawing.Point(714, 451);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(45, 14);
            this.labelControl39.TabIndex = 118;
            this.labelControl39.Text = "物资id：";
            // 
            // inv_ID
            // 
            this.inv_ID.Location = new System.Drawing.Point(773, 445);
            this.inv_ID.Name = "inv_ID";
            this.inv_ID.Size = new System.Drawing.Size(147, 20);
            this.inv_ID.TabIndex = 117;
            // 
            // edate
            // 
            this.edate.EditValue = null;
            this.edate.Location = new System.Drawing.Point(444, 445);
            this.edate.Name = "edate";
            this.edate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.edate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.edate.Size = new System.Drawing.Size(172, 20);
            this.edate.TabIndex = 116;
            // 
            // labelControl38
            // 
            this.labelControl38.Location = new System.Drawing.Point(363, 448);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(60, 14);
            this.labelControl38.TabIndex = 115;
            this.labelControl38.Text = "停用日期：";
            // 
            // sdate
            // 
            this.sdate.EditValue = null;
            this.sdate.Location = new System.Drawing.Point(127, 442);
            this.sdate.Name = "sdate";
            this.sdate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sdate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sdate.Size = new System.Drawing.Size(147, 20);
            this.sdate.TabIndex = 114;
            // 
            // labelControl37
            // 
            this.labelControl37.Location = new System.Drawing.Point(53, 447);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(60, 14);
            this.labelControl37.TabIndex = 113;
            this.labelControl37.Text = "启用日期：";
            // 
            // labelControl36
            // 
            this.labelControl36.Location = new System.Drawing.Point(699, 415);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(60, 14);
            this.labelControl36.TabIndex = 112;
            this.labelControl36.Text = "单位体积：";
            // 
            // per_volum
            // 
            this.per_volum.Location = new System.Drawing.Point(773, 406);
            this.per_volum.Name = "per_volum";
            this.per_volum.Size = new System.Drawing.Size(147, 20);
            this.per_volum.TabIndex = 111;
            // 
            // labelControl35
            // 
            this.labelControl35.Location = new System.Drawing.Point(365, 412);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(60, 14);
            this.labelControl35.TabIndex = 110;
            this.labelControl35.Text = "单位重量：";
            // 
            // per_weight
            // 
            this.per_weight.Location = new System.Drawing.Point(444, 409);
            this.per_weight.Name = "per_weight";
            this.per_weight.Size = new System.Drawing.Size(172, 20);
            this.per_weight.TabIndex = 109;
            // 
            // factory_code
            // 
            this.factory_code.Location = new System.Drawing.Point(127, 409);
            this.factory_code.Name = "factory_code";
            this.factory_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.factory_code.Properties.NullText = "";
            this.factory_code.Properties.View = this.gridView4;
            this.factory_code.Size = new System.Drawing.Size(147, 20);
            this.factory_code.TabIndex = 108;
            // 
            // labelControl34
            // 
            this.labelControl34.Location = new System.Drawing.Point(53, 412);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(60, 14);
            this.labelControl34.TabIndex = 107;
            this.labelControl34.Text = "生产厂商：";
            // 
            // labelControl33
            // 
            this.labelControl33.Location = new System.Drawing.Point(711, 376);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(48, 14);
            this.labelControl33.TabIndex = 106;
            this.labelControl33.Text = "代理商：";
            // 
            // agent_name
            // 
            this.agent_name.Location = new System.Drawing.Point(773, 367);
            this.agent_name.Name = "agent_name";
            this.agent_name.Size = new System.Drawing.Size(147, 20);
            this.agent_name.TabIndex = 105;
            // 
            // labelControl32
            // 
            this.labelControl32.Location = new System.Drawing.Point(365, 373);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(60, 14);
            this.labelControl32.TabIndex = 104;
            this.labelControl32.Text = "品牌名称：";
            // 
            // brand_name
            // 
            this.brand_name.Location = new System.Drawing.Point(444, 370);
            this.brand_name.Name = "brand_name";
            this.brand_name.Size = new System.Drawing.Size(172, 20);
            this.brand_name.TabIndex = 103;
            // 
            // labelControl31
            // 
            this.labelControl31.Location = new System.Drawing.Point(53, 376);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(60, 14);
            this.labelControl31.TabIndex = 102;
            this.labelControl31.Text = "注册证号：";
            // 
            // cert_code
            // 
            this.cert_code.Location = new System.Drawing.Point(127, 373);
            this.cert_code.Name = "cert_code";
            this.cert_code.Size = new System.Drawing.Size(147, 20);
            this.cert_code.TabIndex = 101;
            // 
            // is_bar
            // 
            this.is_bar.Location = new System.Drawing.Point(773, 335);
            this.is_bar.Name = "is_bar";
            this.is_bar.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_bar.Size = new System.Drawing.Size(147, 20);
            this.is_bar.TabIndex = 100;
            // 
            // labelControl30
            // 
            this.labelControl30.Location = new System.Drawing.Point(675, 338);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(84, 14);
            this.labelControl30.TabIndex = 99;
            this.labelControl30.Text = "是否条码管理：";
            // 
            // is_tender
            // 
            this.is_tender.Location = new System.Drawing.Point(444, 335);
            this.is_tender.Name = "is_tender";
            this.is_tender.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_tender.Size = new System.Drawing.Size(172, 20);
            this.is_tender.TabIndex = 98;
            // 
            // labelControl29
            // 
            this.labelControl29.Location = new System.Drawing.Point(339, 341);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(84, 14);
            this.labelControl29.TabIndex = 97;
            this.labelControl29.Text = "是否招标产品：";
            // 
            // is_cert
            // 
            this.is_cert.Location = new System.Drawing.Point(127, 335);
            this.is_cert.Name = "is_cert";
            this.is_cert.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_cert.Size = new System.Drawing.Size(147, 20);
            this.is_cert.TabIndex = 96;
            // 
            // labelControl28
            // 
            this.labelControl28.Location = new System.Drawing.Point(53, 338);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(60, 14);
            this.labelControl28.TabIndex = 95;
            this.labelControl28.Text = "证件管理：";
            // 
            // is_charge
            // 
            this.is_charge.Location = new System.Drawing.Point(773, 301);
            this.is_charge.Name = "is_charge";
            this.is_charge.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_charge.Size = new System.Drawing.Size(147, 20);
            this.is_charge.TabIndex = 94;
            // 
            // labelControl27
            // 
            this.labelControl27.Location = new System.Drawing.Point(699, 304);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(60, 14);
            this.labelControl27.TabIndex = 93;
            this.labelControl27.Text = "是否收费：";
            // 
            // plan_accord
            // 
            this.plan_accord.Location = new System.Drawing.Point(444, 298);
            this.plan_accord.Name = "plan_accord";
            this.plan_accord.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.plan_accord.Size = new System.Drawing.Size(172, 20);
            this.plan_accord.TabIndex = 92;
            // 
            // labelControl26
            // 
            this.labelControl26.Location = new System.Drawing.Point(339, 304);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(84, 14);
            this.labelControl26.TabIndex = 91;
            this.labelControl26.Text = "计划制定依据：";
            // 
            // is_add_sale
            // 
            this.is_add_sale.Location = new System.Drawing.Point(127, 298);
            this.is_add_sale.Name = "is_add_sale";
            this.is_add_sale.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_add_sale.Size = new System.Drawing.Size(147, 20);
            this.is_add_sale.TabIndex = 90;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.sysDictsUnitinfoVenType_insert);
            this.panelControl1.Controls.Add(this.inv_net);
            this.panelControl1.Controls.Add(this.labelControl49);
            this.panelControl1.Controls.Add(this.inv_local);
            this.panelControl1.Controls.Add(this.labelControl50);
            this.panelControl1.Controls.Add(this.inv_country);
            this.panelControl1.Controls.Add(this.labelControl48);
            this.panelControl1.Controls.Add(this.inv_com_code);
            this.panelControl1.Controls.Add(this.labelControl47);
            this.panelControl1.Controls.Add(this.labelControl46);
            this.panelControl1.Controls.Add(this.inv_usage);
            this.panelControl1.Controls.Add(this.labelControl45);
            this.panelControl1.Controls.Add(this.inv_structure);
            this.panelControl1.Controls.Add(this.mate_is_highvalue);
            this.panelControl1.Controls.Add(this.labelControl44);
            this.panelControl1.Controls.Add(this.labelControl43);
            this.panelControl1.Controls.Add(this.fina_type_code);
            this.panelControl1.Controls.Add(this.mate_amortize);
            this.panelControl1.Controls.Add(this.labelControl42);
            this.panelControl1.Controls.Add(this.price_method);
            this.panelControl1.Controls.Add(this.labelControl41);
            this.panelControl1.Controls.Add(this.labelControl40);
            this.panelControl1.Controls.Add(this.ref_price);
            this.panelControl1.Controls.Add(this.labelControl39);
            this.panelControl1.Controls.Add(this.inv_ID);
            this.panelControl1.Controls.Add(this.edate);
            this.panelControl1.Controls.Add(this.labelControl38);
            this.panelControl1.Controls.Add(this.sdate);
            this.panelControl1.Controls.Add(this.labelControl37);
            this.panelControl1.Controls.Add(this.labelControl36);
            this.panelControl1.Controls.Add(this.per_volum);
            this.panelControl1.Controls.Add(this.labelControl35);
            this.panelControl1.Controls.Add(this.per_weight);
            this.panelControl1.Controls.Add(this.factory_code);
            this.panelControl1.Controls.Add(this.labelControl34);
            this.panelControl1.Controls.Add(this.labelControl33);
            this.panelControl1.Controls.Add(this.agent_name);
            this.panelControl1.Controls.Add(this.labelControl32);
            this.panelControl1.Controls.Add(this.brand_name);
            this.panelControl1.Controls.Add(this.labelControl31);
            this.panelControl1.Controls.Add(this.cert_code);
            this.panelControl1.Controls.Add(this.is_bar);
            this.panelControl1.Controls.Add(this.labelControl30);
            this.panelControl1.Controls.Add(this.is_tender);
            this.panelControl1.Controls.Add(this.labelControl29);
            this.panelControl1.Controls.Add(this.is_cert);
            this.panelControl1.Controls.Add(this.labelControl28);
            this.panelControl1.Controls.Add(this.is_charge);
            this.panelControl1.Controls.Add(this.labelControl27);
            this.panelControl1.Controls.Add(this.plan_accord);
            this.panelControl1.Controls.Add(this.labelControl26);
            this.panelControl1.Controls.Add(this.is_add_sale);
            this.panelControl1.Controls.Add(this.labelControl25);
            this.panelControl1.Controls.Add(this.is_shel_make);
            this.panelControl1.Controls.Add(this.labelControl24);
            this.panelControl1.Controls.Add(this.is_dura);
            this.panelControl1.Controls.Add(this.labelControl23);
            this.panelControl1.Controls.Add(this.is_overstock);
            this.panelControl1.Controls.Add(this.labelControl22);
            this.panelControl1.Controls.Add(this.is_quality);
            this.panelControl1.Controls.Add(this.labelControl21);
            this.panelControl1.Controls.Add(this.is_batch);
            this.panelControl1.Controls.Add(this.labelControl20);
            this.panelControl1.Controls.Add(this.is_sec_whg);
            this.panelControl1.Controls.Add(this.labelControl19);
            this.panelControl1.Controls.Add(this.labelControl18);
            this.panelControl1.Controls.Add(this.stay_time);
            this.panelControl1.Controls.Add(this.labelControl17);
            this.panelControl1.Controls.Add(this.abc);
            this.panelControl1.Controls.Add(this.labelControl16);
            this.panelControl1.Controls.Add(this.eco_bat);
            this.panelControl1.Controls.Add(this.labelControl15);
            this.panelControl1.Controls.Add(this.buy_ahead);
            this.panelControl1.Controls.Add(this.labelControl14);
            this.panelControl1.Controls.Add(this.refe_cost);
            this.panelControl1.Controls.Add(this.cus_code);
            this.panelControl1.Controls.Add(this.labelControl13);
            this.panelControl1.Controls.Add(this.labelControl12);
            this.panelControl1.Controls.Add(this.unit_rate);
            this.panelControl1.Controls.Add(this.unit_dicts1);
            this.panelControl1.Controls.Add(this.labelControl11);
            this.panelControl1.Controls.Add(this.unit_dicts);
            this.panelControl1.Controls.Add(this.labelControl10);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.textEdit3);
            this.panelControl1.Controls.Add(this.inv_attr_code);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.mate_type_code);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.alias);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.textEdit1);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.inv_name);
            this.panelControl1.Controls.Add(this.mate_manage_grade);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.inv_code);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.textEdit2);
            this.panelControl1.Controls.Add(this.mateInfoFileMate_select);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(999, 671);
            this.panelControl1.TabIndex = 1;
            // 
            // labelControl25
            // 
            this.labelControl25.Location = new System.Drawing.Point(53, 301);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(60, 14);
            this.labelControl25.TabIndex = 89;
            this.labelControl25.Text = "加价销售：";
            // 
            // is_shel_make
            // 
            this.is_shel_make.Location = new System.Drawing.Point(773, 263);
            this.is_shel_make.Name = "is_shel_make";
            this.is_shel_make.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_shel_make.Size = new System.Drawing.Size(147, 20);
            this.is_shel_make.TabIndex = 88;
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(711, 266);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(48, 14);
            this.labelControl24.TabIndex = 87;
            this.labelControl24.Text = "自制品：";
            // 
            // is_dura
            // 
            this.is_dura.Location = new System.Drawing.Point(444, 263);
            this.is_dura.Name = "is_dura";
            this.is_dura.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_dura.Size = new System.Drawing.Size(172, 20);
            this.is_dura.TabIndex = 86;
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(375, 269);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(48, 14);
            this.labelControl23.TabIndex = 85;
            this.labelControl23.Text = "耐用品：";
            // 
            // is_overstock
            // 
            this.is_overstock.Location = new System.Drawing.Point(127, 263);
            this.is_overstock.Name = "is_overstock";
            this.is_overstock.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_overstock.Size = new System.Drawing.Size(147, 20);
            this.is_overstock.TabIndex = 84;
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(29, 266);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(84, 14);
            this.labelControl22.TabIndex = 83;
            this.labelControl22.Text = "呆滞积压管理：";
            // 
            // is_quality
            // 
            this.is_quality.Location = new System.Drawing.Point(773, 223);
            this.is_quality.Name = "is_quality";
            this.is_quality.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_quality.Size = new System.Drawing.Size(147, 20);
            this.is_quality.TabIndex = 82;
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(687, 229);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(72, 14);
            this.labelControl21.TabIndex = 81;
            this.labelControl21.Text = "保质期管理：";
            // 
            // is_batch
            // 
            this.is_batch.Location = new System.Drawing.Point(444, 226);
            this.is_batch.Name = "is_batch";
            this.is_batch.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_batch.Size = new System.Drawing.Size(172, 20);
            this.is_batch.TabIndex = 80;
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(363, 232);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(60, 14);
            this.labelControl20.TabIndex = 79;
            this.labelControl20.Text = "批次管理：";
            // 
            // is_sec_whg
            // 
            this.is_sec_whg.Location = new System.Drawing.Point(127, 226);
            this.is_sec_whg.Name = "is_sec_whg";
            this.is_sec_whg.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.is_sec_whg.Size = new System.Drawing.Size(147, 20);
            this.is_sec_whg.TabIndex = 78;
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(17, 229);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(96, 14);
            this.labelControl19.TabIndex = 77;
            this.labelControl19.Text = "参与科室库管理：";
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(699, 195);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(60, 14);
            this.labelControl18.TabIndex = 76;
            this.labelControl18.Text = "呆滞标准：";
            // 
            // stay_time
            // 
            this.stay_time.Location = new System.Drawing.Point(773, 189);
            this.stay_time.Name = "stay_time";
            this.stay_time.Size = new System.Drawing.Size(147, 20);
            this.stay_time.TabIndex = 75;
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(365, 192);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(58, 14);
            this.labelControl17.TabIndex = 74;
            this.labelControl17.Text = "ABC分类：";
            // 
            // abc
            // 
            this.abc.Location = new System.Drawing.Point(444, 189);
            this.abc.Name = "abc";
            this.abc.Size = new System.Drawing.Size(172, 20);
            this.abc.TabIndex = 73;
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(53, 192);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(60, 14);
            this.labelControl16.TabIndex = 72;
            this.labelControl16.Text = "经济批量：";
            // 
            // eco_bat
            // 
            this.eco_bat.Location = new System.Drawing.Point(127, 189);
            this.eco_bat.Name = "eco_bat";
            this.eco_bat.Size = new System.Drawing.Size(147, 20);
            this.eco_bat.TabIndex = 71;
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(687, 155);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(72, 14);
            this.labelControl15.TabIndex = 70;
            this.labelControl15.Text = "采购提前期：";
            // 
            // buy_ahead
            // 
            this.buy_ahead.Location = new System.Drawing.Point(773, 149);
            this.buy_ahead.Name = "buy_ahead";
            this.buy_ahead.Size = new System.Drawing.Size(147, 20);
            this.buy_ahead.TabIndex = 69;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(363, 155);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(60, 14);
            this.labelControl14.TabIndex = 68;
            this.labelControl14.Text = "参考成本：";
            // 
            // refe_cost
            // 
            this.refe_cost.Location = new System.Drawing.Point(444, 152);
            this.refe_cost.Name = "refe_cost";
            this.refe_cost.Size = new System.Drawing.Size(172, 20);
            this.refe_cost.TabIndex = 67;
            // 
            // cus_code
            // 
            this.cus_code.Location = new System.Drawing.Point(127, 152);
            this.cus_code.Name = "cus_code";
            this.cus_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cus_code.Properties.NullText = "";
            this.cus_code.Properties.View = this.gridView3;
            this.cus_code.Size = new System.Drawing.Size(147, 20);
            this.cus_code.TabIndex = 66;
            // 
            // gridView3
            // 
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(53, 155);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(60, 14);
            this.labelControl13.TabIndex = 65;
            this.labelControl13.Text = "供货单位：";
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(711, 117);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(48, 14);
            this.labelControl12.TabIndex = 64;
            this.labelControl12.Text = "换算率：";
            // 
            // unit_rate
            // 
            this.unit_rate.Location = new System.Drawing.Point(773, 114);
            this.unit_rate.Name = "unit_rate";
            this.unit_rate.Size = new System.Drawing.Size(147, 20);
            this.unit_rate.TabIndex = 63;
            // 
            // unit_dicts1
            // 
            this.unit_dicts1.Location = new System.Drawing.Point(444, 117);
            this.unit_dicts1.Name = "unit_dicts1";
            this.unit_dicts1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.unit_dicts1.Properties.NullText = "";
            this.unit_dicts1.Properties.View = this.gridView2;
            this.unit_dicts1.Size = new System.Drawing.Size(172, 20);
            this.unit_dicts1.TabIndex = 62;
            // 
            // gridView2
            // 
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(339, 123);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(84, 14);
            this.labelControl11.TabIndex = 61;
            this.labelControl11.Text = "辅助计量单位：";
            // 
            // unit_dicts
            // 
            this.unit_dicts.Location = new System.Drawing.Point(127, 117);
            this.unit_dicts.Name = "unit_dicts";
            this.unit_dicts.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.unit_dicts.Properties.NullText = "";
            this.unit_dicts.Properties.View = this.gridView1;
            this.unit_dicts.Size = new System.Drawing.Size(147, 20);
            this.unit_dicts.TabIndex = 60;
            // 
            // gridView1
            // 
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(53, 120);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(60, 14);
            this.labelControl10.TabIndex = 59;
            this.labelControl10.Text = "计量单位：";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(699, 82);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(60, 14);
            this.labelControl9.TabIndex = 57;
            this.labelControl9.Text = "规格型号：";
            // 
            // textEdit3
            // 
            this.textEdit3.Location = new System.Drawing.Point(773, 79);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Size = new System.Drawing.Size(147, 20);
            this.textEdit3.TabIndex = 56;
            // 
            // inv_attr_code
            // 
            this.inv_attr_code.Location = new System.Drawing.Point(444, 82);
            this.inv_attr_code.Name = "inv_attr_code";
            this.inv_attr_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.inv_attr_code.Size = new System.Drawing.Size(172, 20);
            this.inv_attr_code.TabIndex = 55;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(363, 88);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(60, 14);
            this.labelControl8.TabIndex = 54;
            this.labelControl8.Text = "物资属性：";
            // 
            // mate_type_code
            // 
            this.mate_type_code.Enabled = false;
            this.mate_type_code.Location = new System.Drawing.Point(773, 47);
            this.mate_type_code.Name = "mate_type_code";
            this.mate_type_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.mate_type_code.Properties.NullText = "";
            this.mate_type_code.Properties.View = this.searchLookUpEdit1View;
            this.mate_type_code.Size = new System.Drawing.Size(147, 20);
            this.mate_type_code.TabIndex = 50;
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(387, 50);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(36, 14);
            this.labelControl5.TabIndex = 48;
            this.labelControl5.Text = "别名：";
            // 
            // alias
            // 
            this.alias.Location = new System.Drawing.Point(444, 44);
            this.alias.Name = "alias";
            this.alias.Size = new System.Drawing.Size(172, 20);
            this.alias.TabIndex = 47;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(65, 50);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(48, 14);
            this.labelControl1.TabIndex = 46;
            this.labelControl1.Text = "拼音码：";
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(127, 47);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(147, 20);
            this.textEdit1.TabIndex = 45;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(699, 18);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 14);
            this.labelControl3.TabIndex = 44;
            this.labelControl3.Text = "材料名称：";
            // 
            // inv_name
            // 
            this.inv_name.Location = new System.Drawing.Point(773, 15);
            this.inv_name.Name = "inv_name";
            this.inv_name.Size = new System.Drawing.Size(147, 20);
            this.inv_name.TabIndex = 43;
            // 
            // mate_manage_grade
            // 
            this.mate_manage_grade.Location = new System.Drawing.Point(444, 12);
            this.mate_manage_grade.Name = "mate_manage_grade";
            this.mate_manage_grade.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.mate_manage_grade.Size = new System.Drawing.Size(172, 20);
            this.mate_manage_grade.TabIndex = 42;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(363, 18);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 14);
            this.labelControl4.TabIndex = 41;
            this.labelControl4.Text = "管理级别：";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(53, 15);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 40;
            this.labelControl2.Text = "材料编码：";
            // 
            // inv_code
            // 
            this.inv_code.Enabled = false;
            this.inv_code.Location = new System.Drawing.Point(127, 12);
            this.inv_code.Name = "inv_code";
            this.inv_code.Size = new System.Drawing.Size(147, 20);
            this.inv_code.TabIndex = 39;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(29, 85);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(84, 14);
            this.labelControl7.TabIndex = 53;
            this.labelControl7.Text = "物资类别编码：";
            // 
            // textEdit2
            // 
            this.textEdit2.Enabled = false;
            this.textEdit2.Location = new System.Drawing.Point(127, 82);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(147, 20);
            this.textEdit2.TabIndex = 52;
            // 
            // mateInfoFileMate_select
            // 
            this.mateInfoFileMate_select.Location = new System.Drawing.Point(926, 44);
            this.mateInfoFileMate_select.Name = "mateInfoFileMate_select";
            this.mateInfoFileMate_select.Size = new System.Drawing.Size(75, 23);
            this.mateInfoFileMate_select.TabIndex = 51;
            this.mateInfoFileMate_select.Text = "选择";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(675, 50);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(84, 14);
            this.labelControl6.TabIndex = 49;
            this.labelControl6.Text = "物资类别编码：";
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(999, 671);
            this.Controls.Add(this.panelControl1);
            this.Name = "update";
            this.Text = "update";
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inv_net.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inv_local.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inv_country.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inv_com_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inv_usage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inv_structure.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mate_is_highvalue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fina_type_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mate_amortize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.price_method.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ref_price.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inv_ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.per_volum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.per_weight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.factory_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.agent_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brand_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cert_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_bar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_tender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_cert.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_charge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plan_accord.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_add_sale.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.is_shel_make.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_dura.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_overstock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_quality.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_batch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.is_sec_whg.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stay_time.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.abc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eco_bat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buy_ahead.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.refe_cost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cus_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unit_rate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unit_dicts1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unit_dicts.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inv_attr_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mate_type_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alias.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inv_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mate_manage_grade.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inv_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton sysDictsUnitinfoVenType_insert;
        private DevExpress.XtraEditors.ComboBoxEdit inv_net;
        private DevExpress.XtraEditors.LabelControl labelControl49;
        private DevExpress.XtraEditors.ComboBoxEdit inv_local;
        private DevExpress.XtraEditors.LabelControl labelControl50;
        private DevExpress.XtraEditors.ComboBoxEdit inv_country;
        private DevExpress.XtraEditors.LabelControl labelControl48;
        private DevExpress.XtraEditors.ComboBoxEdit inv_com_code;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.LabelControl labelControl46;
        private DevExpress.XtraEditors.TextEdit inv_usage;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.TextEdit inv_structure;
        private DevExpress.XtraEditors.ComboBoxEdit mate_is_highvalue;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.TextEdit fina_type_code;
        private DevExpress.XtraEditors.ComboBoxEdit mate_amortize;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.ComboBoxEdit price_method;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.TextEdit ref_price;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.TextEdit inv_ID;
        private DevExpress.XtraEditors.DateEdit edate;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.DateEdit sdate;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.TextEdit per_volum;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.TextEdit per_weight;
        private DevExpress.XtraEditors.SearchLookUpEdit factory_code;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.TextEdit agent_name;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.TextEdit brand_name;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.TextEdit cert_code;
        private DevExpress.XtraEditors.ComboBoxEdit is_bar;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.ComboBoxEdit is_tender;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.ComboBoxEdit is_cert;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.ComboBoxEdit is_charge;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.ComboBoxEdit plan_accord;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.ComboBoxEdit is_add_sale;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.ComboBoxEdit is_shel_make;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.ComboBoxEdit is_dura;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.ComboBoxEdit is_overstock;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.ComboBoxEdit is_quality;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.ComboBoxEdit is_batch;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.ComboBoxEdit is_sec_whg;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit stay_time;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit abc;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit eco_bat;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit buy_ahead;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit refe_cost;
        private DevExpress.XtraEditors.SearchLookUpEdit cus_code;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit unit_rate;
        private DevExpress.XtraEditors.SearchLookUpEdit unit_dicts1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.SearchLookUpEdit unit_dicts;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.ComboBoxEdit inv_attr_code;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.SearchLookUpEdit mate_type_code;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit alias;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit inv_name;
        private DevExpress.XtraEditors.ComboBoxEdit mate_manage_grade;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit inv_code;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.SimpleButton mateInfoFileMate_select;
        private DevExpress.XtraEditors.LabelControl labelControl6;

    }
}