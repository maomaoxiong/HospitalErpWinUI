﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.info.file.mate
{
    public partial class update : Form
    {
        private DaoHelper _helper;
       
        private List<Perm> _perms { get; set; }
        public update()
        {
            InitializeComponent();
        }
         public  update(DaoHelper helper, List<Perm> perms, string content_code)
        {

            InitializeComponent();
            _helper = helper;
            _perms = perms;
            
        }
    }
}
