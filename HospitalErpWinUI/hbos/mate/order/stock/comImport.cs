﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.order.stock
{
    public partial class comImport : Form
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        //  public List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public comImport()
        {
            InitializeComponent();
        }
        public comImport(DaoHelper helper, List<Perm> perms)
        {

            InitializeComponent();
            _helper = helper;
            _perms = perms;
            this.Text = "代销使用生成";

            this.StartPosition = FormStartPosition.CenterParent;
            this.gridView2.OptionsView.ShowGroupPanel = false;

            this.gridView2.BestFitColumns(false);
             
            
        }
        private void simpleButton3_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void mateOrderSteadStock_comImport_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            DataTable dt = _helper.ReadSqlForDataTable("mateOrderSteadStock_comImport_select",
                new[] { 
                    FormHelper.GetValueForComboBoxEdit(store_name),
                    FormHelper.GetValueForComboBoxEdit(appYear),
                    FormHelper.GetValueForComboBoxEdit(appMonth),
                    FormHelper.GetValueForSearchLookUpEdit(dept_code1),
                });

            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;
            //this.gridView2.BestFitColumns(false);
        }
    }
}
