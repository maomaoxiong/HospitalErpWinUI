﻿using Dao;
using HospitalErpData.MenuHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.order.stock
{
    public partial class insert : Form
    {
         protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        //  public List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
         public insert()
        {
            InitializeComponent();
        }
         
        public insert(DaoHelper helper, List<Perm> perms)
        {

            InitializeComponent();
            _helper = helper;
            _perms = perms;
            this.Text = "录入";
            this.StartPosition = FormStartPosition.CenterParent;
            this.gridView3.OptionsView.ShowGroupPanel = false;

            this.gridView3.BestFitColumns(false);
             
            
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
       
    }
}
