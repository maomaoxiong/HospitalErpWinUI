﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraGrid;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.order.stock
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
         
        DataTable _user = new DataTable();
        public main()
        {
            InitializeComponent();
            this.Text = "代销备货单";
            _helper = new DaoHelper();

            this.gridView1.OptionsView.ShowGroupPanel = false;

            this.gridView1.BestFitColumns(false);
        }

        private void mateOrderSteadStock_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            DataTable dt = _helper.ReadSqlForDataTable("mateOrderSteadStock_select",
                new[] { 
                    FormHelper.GetValueForComboBoxEdit(begin_year),
                    FormHelper.GetValueForComboBoxEdit(begin_month),
                    FormHelper.GetValueForComboBoxEdit(end_year),
                    FormHelper.GetValueForComboBoxEdit(end_month),
                    FormHelper.GetValueForComboBoxEdit(state),
                    ven_code.Text.Trim()
                });

            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;
            //this.gridView3.BestFitColumns(false);
        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                int index = gridView1.GetDataSourceRowIndex(gridView1.FocusedRowHandle);
                string acct_subj_code = _user.Rows[index]["_id"].ToString();
                update frm = new update(this._helper, this._perms, acct_subj_code);

                if (frm.ShowDialog() == DialogResult.No)
                {
                    MessageForm.Warning("修改失败！");
                }
                Query();
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                insert insertFrm = new insert(this._helper, this._perms);

                if (insertFrm.ShowDialog() == DialogResult.OK)
                {
                    Query();
                }
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            try
            {
                comImport insertFrm = new comImport(this._helper, this._perms);

                if (insertFrm.ShowDialog() == DialogResult.OK)
                {
                    Query();
                }
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
    }
}
