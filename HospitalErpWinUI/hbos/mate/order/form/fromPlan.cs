﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.order.form
{
    public partial class fromPlan : Form
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        //  public List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public fromPlan()
        {
            InitializeComponent();
        }
        public fromPlan(Dao.DaoHelper helper, List<Perm> perms )
        {
            InitializeComponent();
            this.Text = "根据采购计划生成";
          
            _helper = new DaoHelper();
             this.StartPosition = FormStartPosition.CenterParent;
            //  this.gridView3.OptionsView.ColumnAutoWidth = false;
            // this.gridView3.BestFitColumns(true);
            this.gridView2.OptionsView.ShowGroupPanel = false;

            this.gridView2.BestFitColumns(false);
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void mateOrderFormFromPlan_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            DataTable dt = _helper.ReadSqlForDataTable("mateOrderFormFromPlan_select",
                new[] {
                    fromdate.Text.Trim(),
                    todate.Text.Trim(),
                    "4011",// FormHelper.GetValueForSearchLookUpEdit(deptcode),
                    "1",//  FormHelper.GetValueForSearchLookUpEdit(storecode),
                    brief.Text.Trim(),""
                });

            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;

        }
    }
}
