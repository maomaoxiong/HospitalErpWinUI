﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.order.invdept
{
    public partial class update : Form
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        //  public List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public update()
        {
            InitializeComponent();
           
        }
        public update(DaoHelper helper, List<Perm> perms, string a,string b,string c)
        {

            InitializeComponent();
            _helper = helper;
            _perms = perms;
            this.Text = "查看详情";
             

            this.gridView1.OptionsView.ShowGroupPanel = false;

            this.gridView1.BestFitColumns(false);
            order_code.Text = a;
            ven_name.Text = b;
            emp_name.Text = c;
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void mateInvDeptDetail_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            DataTable dt = _helper.ReadSqlForDataTable("mateInvDeptDetail_select",
                new[] { order_code.Text.Trim(),
                    ven_name.Text.Trim(),
                    emp_name.Text.Trim() 
                });

            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;
            //this.gridView2.BestFitColumns(false);
        }
    }
}
