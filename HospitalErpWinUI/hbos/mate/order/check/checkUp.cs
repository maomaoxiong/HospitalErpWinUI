﻿using Dao;
using HospitalErpData.MenuHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.order.check
{
    
    public partial class checkUp : Form
    {
        private DaoHelper _helper;

        DataTable _user = new DataTable();
        private List<Perm> _perms { get; set; }
        public checkUp()
        {
            InitializeComponent();
        }
        public checkUp(DaoHelper helper, List<Perm> perms, string iow_id)
        {

            InitializeComponent();
            _helper = helper;
            _perms = perms;
            this.Text = "修改";
            this.StartPosition = FormStartPosition.CenterParent;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
