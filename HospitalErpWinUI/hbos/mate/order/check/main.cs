﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraGrid;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.order.check
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        //  public List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public main()
        {
            InitializeComponent();
            this.Text="订单审核";
            _helper = new DaoHelper();

            this.gridView3.OptionsView.ShowGroupPanel = false;


            this.gridView3.BestFitColumns(false);
        }

        private void mateOrderForm_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            DataTable dt = _helper.ReadSqlForDataTable("mateOrderCheck_select",
                new[] { fromdate.Text.Trim(),
                    todate.Text.Trim(),
                peri_code.Text.Trim(),
                FormHelper.GetValueForSearchLookUpEdit(deptcode),
                
                FormHelper.GetValueForSearchLookUpEdit(ven_code),
                  
                FormHelper.GetValueForComboBoxEdit(state),
                brif.Text.Trim()
                });

            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;
            //this.gridView3.BestFitColumns(false);
        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            if (gridView3.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                int index = gridView3.GetDataSourceRowIndex(gridView3.FocusedRowHandle);
                string acct_subj_code = _user.Rows[index]["_order_code"].ToString();
                checkUp frm = new checkUp(this._helper, this._perms, acct_subj_code);

                if (frm.ShowDialog() == DialogResult.No)
                {
                    MessageForm.Warning("修改失败！");
                }
                Query();
            }
        }
    }
}
