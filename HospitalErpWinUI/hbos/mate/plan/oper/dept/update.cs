﻿using Dao;
using HospitalErpData.MenuHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.plan.oper.dept
{
    public partial class update : Form
    {
        private DaoHelper _helper;
        DataTable _user = new DataTable();
        private List<Perm> _perms { get; set; }
        public update()
        {
            InitializeComponent();
        }
        public update(DaoHelper helper, List<Perm> perms, string req_id)
        {

            InitializeComponent();
            _helper = helper;
            _perms = perms;
            this.StartPosition = FormStartPosition.CenterParent;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            load_detail(req_id);//加载明细数据
        }
        private void load_detail(string req_id)
        {
            DataTable dt = _helper.ReadSqlForDataTable("matePlanOperDept_select_load_detail",
                new[] { req_id   });

            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;
        }
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
