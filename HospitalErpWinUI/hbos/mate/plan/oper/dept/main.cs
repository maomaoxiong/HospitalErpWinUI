﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraGrid;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.plan.oper.dept
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        //  public List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public main()
        {
            InitializeComponent();
            _helper = new DaoHelper();
            this.StartPosition = FormStartPosition.CenterParent;
            this.Text = "科室需求计划";
            this.gridView2.OptionsView.ShowGroupPanel = false;
        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
             
            if (gridView2.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                int index = gridView2.GetDataSourceRowIndex(gridView2.FocusedRowHandle);
                string req_id = _user.Rows[index]["req_id"].ToString();
                update frm = new update(this._helper, this._perms, req_id);

                if (frm.ShowDialog() == DialogResult.No)
                {
                    MessageForm.Warning("修改失败！");
                }
                Query();
            }
        }

        private void matePlanOperDept_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            DataTable dt = _helper.ReadSqlForDataTable("matePlanOperDept_select",
                new[] { fromdate.Text.Trim(),
                    todate.Text.Trim(),
                FormHelper.GetValueForSearchLookUpEdit(deptcode),
                brif.Text.Trim(),
                FormHelper.GetValueForComboBoxEdit(mate_check),
                FormHelper.GetValueForSearchLookUpEdit(stock_dept_code),
                FormHelper.GetValueForComboBoxEdit(plan_type) 
                });

            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            try
            {
                insert insertFrm = new insert(this._helper, this._perms);

                if (insertFrm.ShowDialog() == DialogResult.OK)
                {
                    Query();
                }
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
    }
}
