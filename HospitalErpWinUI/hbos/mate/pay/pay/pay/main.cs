﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraGrid;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.pay.pay.pay
{
    public partial class main : RibbonForm
    {
        protected DaoHelper _helper;
        public List<Perm> _perms { get; set; }
        //  public List<Perm> _perms { get; set; }
        DataTable _user = new DataTable();
        public main()
        {
            InitializeComponent();
            _helper = new DaoHelper();
            //this.StartPosition = FormStartPosition.CenterParent;
            this.Text = "付款管理";
           // this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
        }

        private void labelControl1_Click(object sender, EventArgs e)
        {

        }

        private void labelControl6_Click(object sender, EventArgs e)
        {

        }

        private void matePayPayPay_select_Click(object sender, EventArgs e)
        {
            try
            {
                Query();
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
        private void Query()
        {
            DataTable dt = _helper.ReadSqlForDataTable("matePayPayPay_select",
                new[] { fromdate.Text.Trim(),
                    todate.Text.Trim(),
                    pay_bill_no.Text.Trim(),
                FormHelper.GetValueForSearchLookUpEdit(ven_code),
                FormHelper.GetValueForComboBoxEdit(pay_bill_type),
                FormHelper.GetValueForSearchLookUpEdit(dept_code),
                FormHelper.GetValueForSearchLookUpEdit(emp_code),
                frompay.Text.Trim(),
                topay.Text.Trim(),
                FormHelper.GetValueForComboBoxEdit(state)
                 ,""
               
                });

            dt.Columns.Add("checked", System.Type.GetType("System.Boolean"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["checked"] = "false";
            }
            _user = dt;
            gridControl1.DataSource = _user;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            if (gridView3.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                int index = gridView3.GetDataSourceRowIndex(gridView3.FocusedRowHandle);
                string acct_subj_code = _user.Rows[index]["_pay_bill_id"].ToString();
                update frm = new update(this._helper, this._perms, acct_subj_code);

                if (frm.ShowDialog() == DialogResult.No)
                {
                    MessageForm.Warning("修改失败！");
                }
                Query();
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                insert insertFrm = new insert(this._helper, this._perms);

                if (insertFrm.ShowDialog() == DialogResult.OK)
                {
                    Query();
                }
            }
            catch (Exception ex)
            {
                MessageForm.Exception(ex.Message);
            }
        }
    }
}
