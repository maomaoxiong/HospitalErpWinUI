﻿using Dao;
using HospitalErpData.MenuHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI.hbos.mate.pay.invoice
{
    public partial class dinsert : Form
    {
        private DaoHelper _helper;
        private List<Perm> _perms { get; set; }
        public dinsert()
        {
            InitializeComponent();
        }
        public dinsert(Dao.DaoHelper helper, List<Perm> perms)
        {
            InitializeComponent();
            this._helper = helper;
            this._perms = perms;
            this.Text = "录入";
            this.StartPosition = FormStartPosition.CenterParent;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
