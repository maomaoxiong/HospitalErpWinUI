﻿using Dao;
using DevExpress.DataAccess.Native.DB;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace HospitalErpWinUI.hbos
{
     public class common
    {
         DaoHelper _DaoHelper;
         public common()
         {
            _DaoHelper = new DaoHelper(); 
        }
        /*
        function checkRuleCodeFormat(code,formatId,allowLetter){
        if(formatId=="0101" || formatId=="0103" || formatId=="0104" || formatId=="0105" || formatId=="0201" || formatId=="0202" || formatId=="0203" || formatId=="0203")allowLetter = true;
            return checkRuleCodeFormatBySql(code,allowLetter,"sys_code_format_para","<code>"+formatId+"</code>",true);
        }
          */
         //得到父亲编码
         public  string checkRuleCodeFormat(string code, string formatId, bool allowLetter)
         {
             if (formatId .Equals("0101") 
                 || formatId .Equals( "0103" )
                 || formatId.Equals( "0104" )
                 || formatId.Equals( "0105" )
                 || formatId.Equals( "0201" )
                 || formatId .Equals( "0202"  )
                 || formatId.Equals( "0203") 
                 || formatId.Equals( "0203") 
                )
                 allowLetter = true;
             return checkRuleCodeFormatBySql(code, allowLetter, "sys_code_format_para",  formatId );
         }
         private string checkRuleCodeFormatBySql(string code, bool allowLetter, string sqlId, string formatId)
         {

             string format = GetCodeFormat(sqlId, formatId);
             code = Regex.Replace(code, @"(^\s+)|\s+$", "");
             string regStr = @"^(\d)*$";
             if (allowLetter == null && allowLetter)
             {
                 regStr = @"^([a-zA-Z0-9_.])*$";
             }
             if (string.IsNullOrEmpty(code))
                 return null;
             if (Validator.IsMatch(code, regStr) == false)
             {
                 return null;
             }
             string[] cs = format.Split('-');
             int len = 0, plen = 0;
             for (int i = 0; len < code.Length && i < cs.Length; i++)
             {
                 plen = len;
                 len += Convert.ToInt16(cs[i]);
             }
             if (len != code.Length)
                 return null;
             if (plen == 0)
                 return "";
             else
                 return code.Substring(0, plen);


         }

         private string GetCodeFormat(string sqlId , string para)
         {
             string format = string.Empty;
             ServiceParamter serviceParamter = new ServiceParamter();
             serviceParamter.ServiceId = sqlId;
             Dictionary<string, string> paramters = new Dictionary<string, string>();
             paramters.Add("code", para);
             serviceParamter.Paramters = paramters;
             System.Data.DataTable dt = _DaoHelper.ReadDictForSql(serviceParamter);
             if (dt.Rows.Count > 0)
             {
                 format = dt.Rows[0][0].ToString();
             }

             return format;
         }


    }
}
