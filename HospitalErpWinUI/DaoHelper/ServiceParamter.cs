﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dao
{
      [Serializable]
    public class ServiceParamter
    {
        public ServiceParamter()
        {
            ServiceId = string.Empty;
            Paramters = new Dictionary<string, string>();
        }
        public ServiceParamter(string serviceId ,Dictionary<string, string> paramters)
        {
            ServiceId = serviceId;
            Paramters = paramters;
        }
       
         public string ServiceId { get; set; }
      
         public Dictionary<string, string> Paramters { get; set; }
    }
}
