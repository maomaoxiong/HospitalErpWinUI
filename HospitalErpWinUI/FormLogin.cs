﻿using Dao;
using DevExpress.XtraEditors;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.MenuPem;
using HospitalErpWinUI.MenuPemHelper;
using HospitalErpWinUI.WinFormUiHelper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Security.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace HospitalErpWinUI
{
    public partial class FormLogin : XtraForm
    {
        public FormLogin()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "平台登录";
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void butLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textUser.Text.Trim()))
                    MessageForm.Show("请输入帐户！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else if (string.IsNullOrEmpty(textPass.Text.Trim()))
                    MessageForm.Show("请输入密码！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    Login();
            }
            catch(Exception ex)
            {
                MessageForm.Exception(ex.Message, "异常");
            }            
        }
        private void Login()
        {
          //  string url = @"http://localhost:1278/Services/User/Login";
                 #region:保存用户名
                XmlDocument doc = new XmlDocument();
                string dir = System.Environment.CurrentDirectory.ToLower();
                doc.Load(dir + "\\UserConfig.xml");

                XmlNode root = doc.SelectSingleNode("UserConfig");
                XmlNodeList xnl = doc.SelectSingleNode("UserConfig").ChildNodes;

                foreach (XmlNode xn in xnl)
                {
                    XmlElement xe = (XmlElement)xn;
                    if (xe.InnerText == this.textUser.Text)
                    {
                        root.RemoveChild(xe);//删除一个<UserName>节点
                    }

                }
                XmlElement xe1 = doc.CreateElement("UserName");//创建一个<UserName>节点
                xe1.InnerText = this.textUser.Text;
                root.AppendChild(xe1);
                doc.Save("UserConfig.xml");
                #endregion

                #region 保存密码
                    Properties.Settings.Default.IsSavedPassword = chk_RemenberPassword.Checked ? "1" : "0";
                    if (Properties.Settings.Default.IsSavedPassword == "1")
                    {
                        Properties.Settings.Default.LastLoginUserName = this.textUser.Text;
                        Properties.Settings.Default.LastLoginPassword = this.textPass.Text;
                      
                    }
                    Properties.Settings.Default.Save();
                              
                #endregion

                string url = ConfigurationManager.AppSettings["ServicesAdress"] + "/User/Login";
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("strUser", textUser.Text.Trim());
                dic.Add("strPwd", textPass.Text.Trim());
                try 
                {
                    string data = HttpHelper.CreateHttpGetResponse(url, dic);
                    JObject jo = (JObject)JsonConvert.DeserializeObject(data);
                    UserAuth user = Newtonsoft.Json.JsonConvert.DeserializeObject(jo["Data"].ToString(), typeof(UserAuth)) as UserAuth;
                    G_User.user = user;
                    if (user == null)
                        throw new Exception("用户名或密码错误!");
                }
                 catch(Exception e)
                {
                   MessageForm.Exception(e.Message);
                   return;
                }

                this.Hide();
                //    new SplashContext("ss");
                //    FormMain frmMain = new FormMain();
                //  frmMain.Show();

                SplashContext splashContext = new SplashContext();
                splashContext.ShowSplashScreen();//这里创建和显示启动窗体 
                splashContext.MainFormLoad();//这里创建和显示启动主窗体 
                    
    
        }

        private void FormLogin_Load(object sender, EventArgs e)
        {
            SetBtnStyle(btDbConfig);

            #region:绑定当前登录过登录名
            XmlDocument doc = new XmlDocument();
            string dir = System.Environment.CurrentDirectory.ToLower();
            doc.Load(dir + "\\UserConfig.xml");

            for (int i = 0; i < doc.GetElementsByTagName("UserName").Count; i++)
            {
                string str = doc.GetElementsByTagName("UserName")[i].InnerText;

                this.textUser.Items.Add(str);

            }
            #endregion

            #region:显示最后一次登录的用户名和密码
            chk_RemenberPassword.Checked = (Properties.Settings.Default.IsSavedPassword .Equals("1"));
            if (Properties.Settings.Default.IsSavedPassword.Equals("1"))
            {
                textUser.Text = Properties.Settings.Default.LastLoginUserName.ToString();
                textPass.Text = Properties.Settings.Default.LastLoginPassword.ToString();

            }
            #endregion
        }

        private void SetBtnStyle(Button btn)
        {
            btn.FlatStyle = FlatStyle.Flat;//样式
            btn.ForeColor = Color.Transparent;//前景
            btn.BackColor = Color.Transparent;//去背景
            btn.FlatAppearance.BorderSize = 0;//去边线
            btn.FlatAppearance.MouseOverBackColor = Color.Transparent;//鼠标经过
            btn.FlatAppearance.MouseDownBackColor = Color.Transparent;//鼠标按下
        }

        private void textPass_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void btDbConfig_MouseHover(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            btn.FlatAppearance.BorderSize = 0;
            btn.ForeColor = Color.Red; 
            btn.Text = "设置服务地址";
        }

        private void btDbConfig_MouseLeave(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            btn.FlatAppearance.BorderSize = 0;         
            btn.Text = "";
        }

        private void btDbConfig_Click(object sender, EventArgs e)
        {
            FrmUrlSetp frmSetp = new FrmUrlSetp();
            frmSetp.ShowDialog();
        }

        private void butCancle_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureEdit1_EditValueChanged(object sender, EventArgs e)
        {

        }

    }
}
