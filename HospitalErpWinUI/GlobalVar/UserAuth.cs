﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Principal;

namespace Security.Models
{
   
      public class UserAuth : IIdentity
    {
        public UserAuth() { }
        //public UserAuth(string username, string password, string[] roles, string userId, string ticString, string module_name)
        //{
        //    Name = username;
        //    Password = password;
        //    Roles = roles;
        //    User_id = userId;
        //    Ticket = ticString;
        //    Module_name = module_name;
        //    //  ValidIpAddresses = validIpAddresses;
        //}
        public string UserName { get; set; }
        public string Name { get; set; }

        public string Password { get; set; }

        public string[] Roles { get; set; }

        public string User_id { get; set; }

        public string User_code { get; set; }

        public string Comp_code { get; set; }

        public string Copy_code { get; set; }

        public string Acct_year { get; set; }

        //   public List<string> ValidIpAddresses { get; private set; }

        public bool IsAuthenticated { get { return true; } }

        public string Ticket { get; set; }

        public string AuthenticationType { get { return "Basic"; } }

        public string Module_name { get; set; }

        public string Module_code { get; set; }

        public string LoginDate { get; set; }

        public int Mod_Level { get; set; }

        public DateTime Begin_date { get; set; }

        public DateTime End_date { get; set; }

       // public DataTable  Perms { get; set; }

    }
}