﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HospitalErpWinUI
{
    public class UserInfo
    {
        public string User_name { get; set; }

        public string Comp_code { get; set; }

        public string Copy_code { get; set; }

        public string Acct_year { get; set; }

        public string Ticket { get; set; }
    }
}
