﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace HospitalErpWinUI
{
    public partial class FrmUrlSetp : Form
    {
        public FrmUrlSetp()
        {
            InitializeComponent();
            this.Text = "服务地址设置";
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void FrmUrlSetp_Load(object sender, EventArgs e)
        {
           string url =   ConfigurationManager.AppSettings["ServicesAdress"];
           textServicesUrl.Text = url;
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void save_Click(object sender, EventArgs e)
        {
            ConfigurationManager.AppSettings.Set("ServicesAdress", textServicesUrl.Text.Trim());
        }
    }
}
