<?xml version="1.0" encoding="GB2312"?>
<sql-list>
<sql id="P_PERF_KSMDF" type="proc"><![CDATA[
/*-------------------------------------------------------------------------
|| 过程名称 ：P_PERF_KSMDF
|| 功能描述 ：取本科室月绩效得分,目的将月绩效得分作为科主任的指标值。
|| 参数描述 ：参数标识        名称                输入/输出    类型
||            -------------------------------------------------------------
||            Prm_Factor           基本指标代码        输入       nvarchar 
||            Prm_StratagemNo  	   战略序号            输入       int
||            Prm_Comp_Code        单位编码            输入       nvarchar
||            Prm_Year             核算年度            输入       nvarchar  
||            Prm_Month            核算月份            输入       nvarchar
||
|| 作    者 ：韩光      完成日期 ：2007-12-01
||-------------------------------------------------------------------------
|| 修改记录 ：
||-------------------------------------------------------------------------*/  
                                                   
	@Prm_Factor 				nvarchar(20)          , --基本指标代码      
	@Prm_StratagemNo			int                   , --战略序号      
	@Prm_Comp_Code                          nvarchar(20)           , --单位编码      
	@Prm_Year 				nvarchar(8)           , --核算年度
	@Prm_Month                             	nvarchar(20)            --核算月份
   AS   	                                                                        
BEGIN

    --变量声明
    DECLARE   @str_perf_unit_code      	nvarchar(20)           --科室类别代码        
    DECLARE   @str_dept_code      	nvarchar(20)           --科室代码  
    DECLARE   @str_emp_code      	nvarchar(20)           --职工代码              
    DECLARE   @n_sequence_no            int                    --主表序列号
    DECLARE   @n_count_main        	int 		       --主表记录数
    DECLARE   @n_count_detail      	int 		       --明细表记录数
    DECLARE   @n_factor_value      	numeric(20,6)          --指标值    
        
    --定义科室游标
    DECLARE Cur_Perf_Emp  CURSOR
    FOR 
       SELECT perf_unit_code,  --科室类别代码
              dept_code     ,  --科室代码  
              emp_code         --职工代码
    	from v_perf_emp 
       WHERE stratagem_sequence_no = @Prm_StratagemNo
         AND comp_code = @Prm_Comp_Code
         AND duty_code = '01'
    
   --初始化   
   SET @str_perf_unit_code              = '';
   SET @str_dept_code                   = '';
   SET @str_emp_code                    = '';
   SET @n_sequence_no			= 0 ;	 
   SET @n_count_main	      	        = 0 ;
   SET @n_count_detail                  = 0 ;
   SET @n_factor_value                  = 0 ;
      
   --判断基本指标录入主表是否存在值
   SELECT @n_count_main = count(*)
      FROM perf_factor_input
   WHERE comp_code     = @Prm_Comp_Code 
     AND perf_year     = @Prm_Year
     AND perf_month    = @Prm_Month
     AND factor_code   = @Prm_Factor 
     AND stratagem_sequence_no = @Prm_StratagemNo
     
   IF ( @n_count_main = 0 ) 
   BEGIN   
      --插入基本指标录入主表
      INSERT INTO perf_factor_input (
      stratagem_sequence_no ,	      --战略序号           
                comp_code   ,         --单位代码
                perf_year   ,         --考核年度
                perf_month  ,         --考核期间
                factor_code ,         --基本指标代码
                state_flag            --状态标识
                )
          VALUES(
          	@Prm_StratagemNo    ,
                @Prm_Comp_Code      ,
                @Prm_Year	    , 
                @Prm_Month          , 
                @Prm_Factor         ,
                'new'	 
          )  
   END         
  
   --提取基本指标录入主表序列号
   SELECT @n_sequence_no = sequence_no
      FROM perf_factor_input
   WHERE comp_code     = @Prm_Comp_Code 
     AND perf_year     = @Prm_Year
     AND perf_month    = @Prm_Month
     AND factor_code   = @Prm_Factor 
     AND stratagem_sequence_no = @Prm_StratagemNo
     
   --打开科室游标	       
   open Cur_Perf_Emp    
	     
   --提取科室代码
   fetch next from Cur_Perf_Emp into @str_perf_unit_code,@str_dept_code,@str_emp_code
   
   while @@fetch_status = 0
   begin
      
      --提取科室绩效得分
      SELECT @n_factor_value = isnull( score_month ,0 ) 
      	FROM perf_dept_target
      WHERE stratagem_sequence_no = @Prm_StratagemNo
        AND Perf_month   = @Prm_Month
        AND Perf_unit_code = @str_perf_unit_code  
        AND dept_code    = @str_dept_code
        AND target_code  = 'ROOT'
      
      --判断基本指标录入明细表是否存在值
      SELECT @n_count_detail = count(*) 
         FROM perf_factor_input_detail 
      WHERE perf_unit_code    = @str_perf_unit_code  
        AND dept_code         = @str_dept_code
        AND emp_code          = @str_emp_code 
        AND main_sequence_no  = @n_sequence_no
        AND factor_property   = 'P'
      
      IF ( @n_count_detail = 0 )                
      BEGIN   
         --插入基本指标录入明细表
         INSERT INTO perf_factor_input_detail (     
                   main_sequence_no    ,   --序列号       
                   perf_unit_code      ,   --科室类别代码
                   dept_code           ,   --科室代码
                   emp_code            ,   --人员代码
                   factor_value        ,   --因素值
                   state_flag          ,   --状态标识
                   factor_property         --因素标识 H:全院 U:科别 D:科室 P：干部
                   )
             VALUES(
             	@n_sequence_no       ,
                @str_perf_unit_code  ,
                @str_dept_code       ,
                @str_emp_code        ,
                @n_factor_value      , 
                'new'	     	     ,
                'P'
             ) 
      END
      ELSE
      BEGIN
         --更新基本指标录入明细表
         UPDATE  perf_factor_input_detail 
              SET   factor_value =  @n_factor_value
         WHERE  main_sequence_no =  @n_sequence_no
           AND  perf_unit_code   =  @str_perf_unit_code 
           AND  dept_code        =  @str_dept_code
           AND  emp_code         =  @str_emp_code
           AND  factor_property  =  'P'
      END  
     
     --提取科室代码
     fetch next from Cur_Perf_Emp into @str_perf_unit_code ,@str_dept_code,@str_emp_code    
  END  
  
    --关闭科室游标
    close Cur_Perf_Emp
    deallocate Cur_Perf_Emp                    
END
]]></sql>
</sql-list>