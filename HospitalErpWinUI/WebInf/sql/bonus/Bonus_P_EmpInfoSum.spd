<?xml version="1.0" encoding="GB2312"?>
<sql-list>
<sql id="Bonus_P_EmpInfoSum" type="proc"><![CDATA[
  /*-------------------------------------------------------------------------
  || 过程名称 ：Bonus_P_EmpInfoSum
  || 功能描述 ：取平台人员按科室汇总辅助项信息
  || 参数描述 ：参数标识        名称                输入/输出    类型
  ||            -------------------------------------------------------------
  ||            @Prm_Comp_Code       单位代码            输入       nvarchar 
  ||            @Prm_Type_Id  	     期间类型            输入       int
  ||            @Prm_Year            考核年度            输入       nvarchar
  ||            @Prm_Period          考核期间            输入       nvarchar  
  ||            @Prm_Factor          核算月份            输入       nvarchar
  ||            @Prm_EmpInfo         辅助项码            输入       nvarchar
  ||
  || 作    者 ：韩光      完成日期 ：2010-01-08
  ||-------------------------------------------------------------------------
  || 修改记录 ：
  ||-------------------------------------------------------------------------*/  
  
  @Prm_Comp_Code 				nvarchar(20)          , --单位代码      
	@Prm_Type_Id 				  int                   , --期间类型      
	@Prm_Year             nvarchar(4)           , --考核年度      
	@Prm_Period 				  nvarchar(2)           , --考核期间
	@Prm_Factor           nvarchar(20)          , --基本指标
  @Prm_EmpInfo          nvarchar(20)
   AS   	                                                                        
begin 
    --变量声明
    DECLARE   @str_dept_kind_code   nvarchar(20)           --科室类别代码        
    DECLARE   @str_dept_code      	nvarchar(20)           --科室代码             
    DECLARE   @n_sequence_no        int                    --主表序列号
    DECLARE   @n_count_main        	int 		               --主表记录数
    DECLARE   @n_count_detail      	int 		               --明细表记录数
    DECLARE   @n_factor_value      	numeric(20,6)          --指标值    
    DECLARE   @str_stratagem_no     int                    --战略号
    DECLARE   @str_sql              nvarchar(4000)         --向外提取数据SQL串       

    --定义科室游标
    DECLARE Cur_Bonus_Dept  CURSOR
    FOR 
       SELECT dept_kind_code,  --科室类别代码
              dept_code        --科室代码  
    	   from v_bonus_dept 
        WHERE comp_code = @Prm_Comp_Code
    
   --初始化   
   SET @str_dept_kind_code              = '';
   SET @str_dept_code                   = '';
   SET @n_sequence_no										= 0 ;	 
   SET @n_count_main	      	        	= 0 ;
   SET @n_count_detail                  = 0 ;
   SET @n_factor_value                  = 0 ;
   SET @str_stratagem_no                = -1 ;
   SET @str_sql                         = '';
    
       
   --判断基本指标录入主表是否存在值
   SELECT @n_count_main = count(*)
      FROM perf_factor_input
   WHERE comp_code     = @Prm_Comp_Code 
     AND type_id       = @Prm_Type_Id  
     AND perf_year     = @Prm_Year
     AND period_id     = @Prm_Period
     AND factor_code   = @Prm_Factor   
     
   IF ( @n_count_main = 0 ) 
   BEGIN   
      --插入基本指标录入主表
      INSERT INTO perf_factor_input (            
                comp_code   ,         --单位代码
                type_id     ,         --期间类型
                perf_year   ,         --考核年度
                period_id   ,         --考核期间
                factor_code ,         --基本指标代码
                state_flag            --状态标识
                )
          VALUES(
                @Prm_Comp_Code      ,
                @Prm_Type_Id        , 
                @Prm_Year           , 
                @Prm_Period         ,
                @Prm_Factor         , 
                'confirm'	 
          )  
   END         
  
   --提取基本指标录入主表序列号
   SELECT @n_sequence_no = sequence_no
      FROM perf_factor_input
   WHERE comp_code     = @Prm_Comp_Code 
     AND type_id       = @Prm_Type_Id  
     AND perf_year     = @Prm_Year
     AND period_id     = @Prm_Period
     AND factor_code   = @Prm_Factor
  
    --提取战略序号
    select @str_stratagem_no = sequence_no 
     from perf_stratagem 
    where comp_code  = @Prm_Comp_Code
      and stratagem_year = @Prm_Year 
      and is_dummy       = '0'
      and state_flag  in ( 'new','confirm')
   
    --判断战略号
    if (isnull(@str_stratagem_no,-1) = -1 )
    begin
      deallocate Cur_Bonus_Dept
      RAISERROR( '本年战略未设置或战略已被关闭!' ,16,1)
      return 
    end    

   --打开科室游标	       
   open Cur_Bonus_Dept    
	     
   --提取科室代码
   fetch next from Cur_Bonus_Dept into @str_dept_kind_code,@str_dept_code
   
   while @@fetch_status = 0
   begin
      
      --提取辅助项按科室汇总数据
       set @n_factor_value = 0.0

	     set @str_sql = N'select @n_factor_value = sum(convert(numeric(20,6),isnull(' + @Prm_EmpInfo + ',0 ))) 
                          from sys_additive_items 
                         where comp_code = ''' + @Prm_Comp_Code + '''
                           and item_code in ( select emp_code 
                                                from v_perf_emp
                                               where comp_code = ''' + @Prm_Comp_Code + '''
                                                 and dept_code = ''' + @str_dept_code + '''
                                                 and stratagem_sequence_no = ' + convert(nvarchar(20),@str_stratagem_no) + 
                                           ')'

	     exec SP_EXECUTESQL @str_sql ,N'@n_factor_value numeric(20,6) out', @n_factor_value out
     
       if @n_factor_value is null
       begin
          set @n_factor_value = 0
       end  
      
      --判断基本指标录入明细表是否存在值
      SELECT @n_count_detail = count(*) 
         FROM perf_factor_input_detail 
      WHERE perf_unit_code    = @str_dept_kind_code 
        AND dept_code         = @str_dept_code
        AND main_sequence_no  = @n_sequence_no
      
      IF ( @n_count_detail = 0 )                
      BEGIN   
         --插入基本指标录入明细表
         INSERT INTO perf_factor_input_detail (     
                   main_sequence_no    ,   --序列号       
                   perf_unit_code      ,   --科室类别代码
                   dept_code           ,   --科室代码
                   factor_value        ,   --因素值
                   state_flag          ,   --状态标识
                   factor_property         --因素标识 0:全院 1:科别 2:科室
                   )
             VALUES(
                 	@n_sequence_no       ,
                  @str_dept_kind_code  ,
                  @str_dept_code       ,
                  isnull(@n_factor_value,0) , 
                  'confirm'	           ,
                  '2'
             ) 
      END
      ELSE
      BEGIN
         --更新基本指标录入明细表
         UPDATE  perf_factor_input_detail 
            SET   factor_value =  isnull(@n_factor_value,0)
          WHERE  main_sequence_no = @n_sequence_no
            AND  perf_unit_code   = @str_dept_kind_code
            AND  dept_code        = @str_dept_code
            AND  factor_property  = '2'
      END  
     
     --提取科室代码
     fetch next from Cur_Bonus_Dept into @str_dept_kind_code,@str_dept_code    
  END  
  
    --关闭科室游标
    close Cur_Bonus_Dept
    deallocate Cur_Bonus_Dept      
END
]]></sql>
</sql-list>