﻿using Dao;
using HospitalErpData.MenuHelper;
using HospitalErpWinUI.GlobalVar;
using Security.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace HospitalErpWinUI.MenuPem
{
    public class AdminMenuPemHelper
    {
        //原始菜单
        public static Dictionary<string, List<Menu_node>> originaLeftMenu { get; set; }
        //分析后菜单
        public static Dictionary<string, List<Menu_node>> leftMenu { get; set; }
        //页面中sqlid权限
        public static Dictionary<string, List<Menu_node>> page { get; set; }

        DaoHelper helper;

        public AdminMenuPemHelper()
        {
            helper = new DaoHelper();
            originaLeftMenu = new Dictionary<string, List<Menu_node>>();
            leftMenu = new Dictionary<string, List<Menu_node>>();
            page = new Dictionary<string, List<Menu_node>>();
        }
        //初始化
        public void InitData()
        {
            //得到原始菜单

            List<Menu_node> menus = GetMenu();
            originaLeftMenu.Add("originaLeftMenu", menus);

            //得到右边菜单
            List<Menu_node> tempMenu = new List<Menu_node>();
            foreach( Menu_node node in  originaLeftMenu["originaLeftMenu"])
            {
                if (node.tab !=  null)
                {
                    tempMenu.Add(node);
                 
                }
                if (node.page == null)
                    continue;
                if (!string.IsNullOrEmpty(node.page.src))
                {
                    Menu_node menu_node = originaLeftMenu["originaLeftMenu"].Where(w => w.oid == node.parentId && w.tab == true).FirstOrDefault<Menu_node>();
                    if (menu_node != null)
                    {
                        continue;
                    }
                    if (!tempMenu.Contains(node))
                    {
                        tempMenu.Add(node);
                    }
                }
            }

            List<Menu_node> tempMenu1 = originaLeftMenu["originaLeftMenu"];

            foreach (Menu_node nod in tempMenu)
            {    
                //给page 赋值
                List<Menu_node> query = tempMenu1.Where(w => w.parentId == nod.oid).ToList();
                page.Add(nod.key.ToString(), query);

                tempMenu1.RemoveAll(w => w.parentId == nod.oid);
            }
            leftMenu.Add("leftMenu", tempMenu1);
            //得到页面对应的sqlId

           
        }

        private List<Menu_node> GetMenu()
        {
            string xml = string.Empty;
            Dictionary<string, string> dic = new Dictionary<string, string>();
            ServiceParamter paramter = new ServiceParamter("menu_sys", dic);
            xml = helper.ReadMenuForText(paramter);

            List<Menu_node> nodes = new List<Menu_node>();
            XElement sqls = XElement.Parse(@"" + xml.ToString().Trim() + "");

            Menu_node nod = new Menu_node();
            int id = 0;
            MenuHelp.GetMenusByXml(sqls, ref nodes, ref id, ref nod);
            return nodes;
        }
       

    }
}
