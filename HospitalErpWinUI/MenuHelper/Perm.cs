﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HospitalErpData.MenuHelper
{
    public class Perm
    {
        public Perm()
        {
           perms = new List<Perm>();
        }
        public string desc { get; set; }

        public string name { get; set; }

        public bool isSelected { get; set; }

        public List<Perm> perms { get; set; }
    }
}
