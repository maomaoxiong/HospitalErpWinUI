﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HospitalErpData.MenuHelper
{
    public class Page
    {
        public Page()
        {
            perms = new List<Perm>();
        }
        public string src { get; set; }

        public List<Perm>  perms { get; set; }
        public bool isSelected { get; set; }
    }
}
