﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HospitalErpData.MenuHelper
{
    public class Menu_node
    {
       public Menu_node()
        {
           menu_nodes = new List<Menu_node>();
        }
        public int oid { get; set; }

        public string id { get; set; }

        public string name { get; set; }

        public  bool ? tab { get; set; }

        public Page page { get; set; }

        public List<Menu_node> menu_nodes { get; set; }

        public int parentId { get; set; }
        public bool isSelected { get; set; }

        public string key { get; set; }


    }
}
