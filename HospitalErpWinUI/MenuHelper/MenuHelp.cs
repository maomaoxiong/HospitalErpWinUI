﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace HospitalErpData.MenuHelper
{
   public class MenuHelp
   {

       #region left菜单
       /*
            XElement sqls = doc.Element("root");
            List<Menu_node> nodes = new List<Menu_node>();
            Menu_node nod = new Menu_node();
            int id= 0;
            MenuHelp.GetMenusByXml(sqls, ref nodes, ref id, ref nod);
        
        */
       public static void GetPerms(XElement menu_xml, ref Perm obj)
       {
           IEnumerable<XElement> perms = menu_xml.Elements("perm");
           foreach (var perm in perms)
           {
               Perm o = new Perm();
               o.desc = perm.Attribute("desc") == null ? "" : perm.Attribute("desc").Value;
               o.name = perm.Attribute("name") == null ? "" : perm.Attribute("name").Value;
               obj.perms.Add(o);

           }
       }

       public static void GetPage(XElement menu_xml, ref Page obj)
       {
           IEnumerable<XElement> perms = menu_xml.Elements("perm");
           foreach (var perm in perms)
           {
               Perm o = new Perm();
               o.desc = perm.Attribute("desc") == null ? "" : perm.Attribute("desc").Value;
               o.name = perm.Attribute("name") == null ? "" : perm.Attribute("name").Value;
               obj.perms.Add(o);
               GetPerms(perm, ref o);
           }
       }
       /**/
       public static void GetObjByXml(XElement menu_xml, ref Menu_node obj)
       {
           IEnumerable<XElement> perms = menu_xml.Elements("perm");
           foreach (var perm in perms)
           {
               Perm o = new Perm();
               o.desc = perm.Attribute("desc") == null ? "" : perm.Attribute("desc").Value;
               o.name = perm.Attribute("name") == null ? "" : perm.Attribute("name").Value;
               // obj.perms.Add(o);
               GetPerms(perm, ref o);

           }

           XElement page = menu_xml.Element("page");
           if (page != null)
           {
               Page o = new Page();
               o.src = page.Attribute("src") == null ? "" : page.Attribute("src").Value;

               XElement pagex = menu_xml.Element("perm");
               // Perm perm = new Perm();
               //   perm.desc = pagex.Attribute("desc") == null ? "" : pagex.Attribute("desc").Value;
               //  perm.name = pagex.Attribute("name") == null ? "" : pagex.Attribute("name").Value;


               obj.page = o;
               GetPage(page, ref o);

           }


           IEnumerable<XElement> menus = menu_xml.Elements("menu_node");
           foreach (XElement menu in menus)
           {
               Menu_node o = new Menu_node();
               o.id = menu.Attribute("id") == null ? "" : menu.Attribute("id").Value;
               o.name = menu.Attribute("name") == null ? "" : menu.Attribute("name").Value;
               if (menu.Attribute("tab") == null)
               {
                   o.tab = null;
               }
               else
               {
                   o.tab = menu.Attribute("tab").Value.ToString().ToLower().Equals("true") ? true : false;
               }
               //  o.checkMethod = menu.Attribute("checkMethod") == null ? "" : menu.Attribute("checkMethod").Value;

               obj.menu_nodes.Add(o);

               GetObjByXml(menu, ref o);
           }

       }

       public static void GetMenusByXml(XElement menu_xml, ref List<Menu_node> objs, ref int oid, ref Menu_node obj)
       {

        
           XElement page = menu_xml.Element("page");
           if (page != null)
           {
               Page o = new Page();
               o.src = page.Attribute("src") == null ? "" : page.Attribute("src").Value;

               //  XElement pagex = menu_xml.Element("perm");
               obj.page = o;
               GetPage(page, ref o);

           }
           IEnumerable<XElement> menus = menu_xml.Elements("menu_node");
           foreach (XElement menu in menus)
           {
               oid++;
               Menu_node o = new Menu_node();
               o.id = menu.Attribute("id") == null ? "" : menu.Attribute("id").Value;
               o.name = menu.Attribute("name") == null ? "" : menu.Attribute("name").Value;
               if (menu.Attribute("tab") == null)
               {
                   o.tab =null;
               }
               else
               {
                   o.tab = menu.Attribute("tab").Value.ToString().ToLower().Equals("true") ? true : false;
               }
               string checkMethod = menu.Attribute("checkMethod") == null ? "" : menu.Attribute("checkMethod").Value;


               if (!checkMethod.Equals("1"))
               {
                   o.key = oid.ToString();
                   o.oid = oid;
                   o.parentId = obj.oid;
                   objs.Add(o);
                   GetMenusByXml(menu, ref objs, ref oid, ref o);
               }
           }

       }
       #endregion
     

       #region  权限菜单树 
       /*
                 XElement sqls = doc.Element("root");
                 List<MenuTree> trees = new List<MenuTree>();
                 MenuTree tree = new MenuTree();
                 tree.Id = 0;
                 int id = 0;
                 MenuHelp.GetMenuTreesByXml(sqls, ref trees, ref  id, ref tree);
        */
       public static void GetPermsTree(XElement menu_xml, ref List<MenuTree> objs, ref int oid, ref MenuTree obj)
       {
         
           IEnumerable<XElement> perms = menu_xml.Elements("perm");
           foreach (var perm in perms)
           {
               oid++;
               MenuTree o = new MenuTree();
               o.desc = perm.Attribute("desc") == null ? "" : perm.Attribute("desc").Value;
               o.name = perm.Attribute("name") == null ? "" : perm.Attribute("name").Value;
               o.Id = oid;
               o.ParentId = obj.Id;
               objs.Add(o);

           }
       }
       public static void GetPageTree(XElement menu_xml,  ref List<MenuTree> objs, ref int oid, ref MenuTree obj)
       {
          
           IEnumerable<XElement> perms = menu_xml.Elements("perm");
           foreach (var perm in perms)
           {
               oid++;
               MenuTree o = new MenuTree();
               o.desc = perm.Attribute("desc") == null ? "" : perm.Attribute("desc").Value;
               o.name = perm.Attribute("name") == null ? "" : perm.Attribute("name").Value;
               o.Id = oid;
               o.ParentId =obj.Id;
               objs.Add(o);
               GetPermsTree(perm, ref objs,ref oid, ref o);
           }
       }

       public static void GetMenuTreesByXml(XElement menu_xml, ref List<MenuTree> objs, ref int oid, ref MenuTree obj)
       {
          
           XElement page = menu_xml.Element("page");
           if (page != null)
           {
               Page o = new Page();
               o.src = page.Attribute("src") == null ? "" : page.Attribute("src").Value;

               XElement pagex = menu_xml.Element("perm");

               GetPageTree(page, ref objs, ref oid , ref obj);

           }
           IEnumerable<XElement> menus = menu_xml.Elements("menu_node");
           foreach (XElement menu in menus)
           {
               oid++;
               MenuTree o = new MenuTree();
               o.Id = oid;
               o.name = menu.Attribute("name") == null ? "" : menu.Attribute("name").Value;
             
           
               string checkMethod = menu.Attribute("checkMethod") == null ? "" : menu.Attribute("checkMethod").Value;
               o.ParentId = obj.Id;
               if (!checkMethod.Equals("1"))
               {
                
                
                   objs.Add(o);
                   GetMenuTreesByXml(menu, ref objs, ref oid, ref o);
               }


              
           }

       }
       #endregion
    }
}
