﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HospitalErpData.MenuHelper
{
   public  class MenuTree
    {
       public int Id {get; set;}

       public int ParentId { get; set; }

       public string desc { get; set; }

       public string name { get; set; }
    }
}
