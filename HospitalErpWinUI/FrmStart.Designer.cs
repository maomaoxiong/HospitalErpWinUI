﻿namespace HMMS.WinUI
{
    partial class FrmStart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loadingProgress1 = new CSharpWin.LoadingProgress();
            this.SuspendLayout();
            // 
            // loadingProgress1
            // 
            this.loadingProgress1.Location = new System.Drawing.Point(0, 0);
            this.loadingProgress1.Name = "loadingProgress1";
            this.loadingProgress1.Size = new System.Drawing.Size(100, 84);
            this.loadingProgress1.Style = CSharpWin.LoadingProgressStyle.Circle;
            this.loadingProgress1.TabIndex = 0;
            this.loadingProgress1.Text = "loadingProgress1";
            // 
            // FrmStart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(104, 96);
            this.Controls.Add(this.loadingProgress1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmStart";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmStart";
            this.TransparencyKey = System.Drawing.Color.White;
            this.Load += new System.EventHandler(this.FrmStart_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private CSharpWin.LoadingProgress loadingProgress1;




    }
}