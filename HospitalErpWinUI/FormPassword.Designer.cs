﻿namespace HospitalErpWinUI
{
    partial class FormPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.old_passwd = new DevExpress.XtraEditors.TextEdit();
            this.new_passwd = new DevExpress.XtraEditors.TextEdit();
            this.again_passwd = new DevExpress.XtraEditors.TextEdit();
            this.but_save = new DevExpress.XtraEditors.SimpleButton();
            this.but_close = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.old_passwd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.new_passwd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.again_passwd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // old_passwd
            // 
            this.old_passwd.Location = new System.Drawing.Point(212, 53);
            this.old_passwd.Name = "old_passwd";
            this.old_passwd.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.old_passwd.Properties.Appearance.Options.UseBackColor = true;
            this.old_passwd.Size = new System.Drawing.Size(224, 24);
            this.old_passwd.TabIndex = 0;
            // 
            // new_passwd
            // 
            this.new_passwd.Location = new System.Drawing.Point(212, 112);
            this.new_passwd.Name = "new_passwd";
            this.new_passwd.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.new_passwd.Properties.Appearance.Options.UseBackColor = true;
            this.new_passwd.Size = new System.Drawing.Size(224, 24);
            this.new_passwd.TabIndex = 1;
            // 
            // again_passwd
            // 
            this.again_passwd.Location = new System.Drawing.Point(212, 168);
            this.again_passwd.Name = "again_passwd";
            this.again_passwd.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.again_passwd.Properties.Appearance.Options.UseBackColor = true;
            this.again_passwd.Size = new System.Drawing.Size(224, 24);
            this.again_passwd.TabIndex = 2;
            // 
            // but_save
            // 
            this.but_save.Location = new System.Drawing.Point(161, 250);
            this.but_save.Name = "but_save";
            this.but_save.Size = new System.Drawing.Size(80, 25);
            this.but_save.TabIndex = 3;
            this.but_save.Text = "确认";
            this.but_save.Click += new System.EventHandler(this.but_save_Click);
            // 
            // but_close
            // 
            this.but_close.Location = new System.Drawing.Point(292, 250);
            this.but_close.Name = "but_close";
            this.but_close.Size = new System.Drawing.Size(80, 25);
            this.but_close.TabIndex = 4;
            this.but_close.Text = "关闭";
            this.but_close.Click += new System.EventHandler(this.but_close_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(107, 56);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(45, 18);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "原密码";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(107, 115);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(50, 18);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "新密码 ";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(107, 171);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 18);
            this.labelControl3.TabIndex = 3;
            this.labelControl3.Text = "确认密码";
            // 
            // FormPassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(562, 335);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.but_close);
            this.Controls.Add(this.but_save);
            this.Controls.Add(this.again_passwd);
            this.Controls.Add(this.new_passwd);
            this.Controls.Add(this.old_passwd);
            this.Name = "FormPassword";
            this.Text = "密码修改";
            this.Load += new System.EventHandler(this.FormPassword_Load);
            ((System.ComponentModel.ISupportInitialize)(this.old_passwd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.new_passwd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.again_passwd.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit old_passwd;
        private DevExpress.XtraEditors.TextEdit new_passwd;
        private DevExpress.XtraEditors.TextEdit again_passwd;
        private DevExpress.XtraEditors.SimpleButton but_save;
        private DevExpress.XtraEditors.SimpleButton but_close;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
    }
}