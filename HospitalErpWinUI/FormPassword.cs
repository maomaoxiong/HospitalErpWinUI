﻿using Dao;
using DevExpress.XtraBars.Ribbon;
using HospitalErpWinUI.BaseClass;
using HospitalErpWinUI.Common;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace HospitalErpWinUI
{
    public partial class FormPassword : Form
    {
        private SysApi commomApi;
        protected DaoHelper _helper;
        public FormPassword()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.commomApi = new SysApi();
            this._helper = new DaoHelper();
        }

        #region 事件
            private void FormPassword_Load(object sender, EventArgs e)
            {
                this.old_passwd.Properties.PasswordChar = '*';
                this.new_passwd.Properties.PasswordChar = '*';
                this.again_passwd.Properties.PasswordChar = '*';
                this.old_passwd.Focus();
            }

            private void but_close_Click(object sender, EventArgs e)
            {
                this.Close();
            }

         private void but_save_Click(object sender, EventArgs e)
        {
            if (!G_User.user.Password.Equals(MD5Encrypt16(old_passwd.Text)))
            {
                MessageForm.Warning("原始密码不正确！");
                return;
            }
            if (!again_passwd.Text.Equals(new_passwd.Text))
            {
                MessageForm.Warning("原始密码不正确！");
                return;
            }
            string prardata0108 = commomApi.GetSysParaData("","","0108");
            if (prardata0108.Equals("是"))
            {
                string reg1 = "/[0-9|A-Z|a-z]{6,18}/";
			    string reg2 = "/[a-zA-Z]+/";
			    string reg3 = "/[0-9]+/";
                if ( Validator.IsMatch(new_passwd.Text,reg1) &&
                     Validator.IsMatch(new_passwd.Text, reg2) &&                     
                    Validator.IsMatch(new_passwd.Text, reg3) 
                    )
                {
			
			    }else if( !Validator.IsMatch(new_passwd.Text, reg1) )
                {
				    MessageForm.Warning("需要长度在6-18位的字母数字组合，请重新输入");
				   
			    }else if(!Validator.IsMatch(new_passwd.Text, reg2) )
                {
				    MessageForm.Warning("需含有字母");
				   
			    }else if(!Validator.IsMatch(new_passwd.Text, reg3) )
                {
				    MessageForm.Warning("需含有数字");				   
			    }
             
            }
            string info=UpdatePasssword();
             
            MessageForm.Warning(info);
            if (info.Contains("成功"))
            {
                G_User.user.Password = MD5Encrypt16(this.new_passwd.Text);
            }
        }
        #endregion

        #region 访问数据

            public string UpdatePasssword()
            {
                string result = string.Empty;

                ServiceParamter serviceParamter = new ServiceParamter();
                Dictionary<string, string> paramters = new Dictionary<string, string>();
                serviceParamter.ServiceId = "update_passwd";

                paramters.Add("user_code", G_User.user.User_code);
                paramters.Add("old_password", MD5Encrypt16(old_passwd.Text));
                paramters.Add("new_password", MD5Encrypt16(new_passwd.Text));
                serviceParamter.Paramters = paramters;

                DataTable dt = this._helper.ReadSqlForDataTable(serviceParamter);
                result = dt.Rows[0][0].ToString();
                return result;
            }
        #endregion

            private static string MD5Encrypt16(string password)
            {
                var md5 = new MD5CryptoServiceProvider();
                string t2 = BitConverter.ToString(md5.ComputeHash(Encoding.Default.GetBytes(password)), 4, 8);
                t2 = t2.Replace("-", "");
                t2 = t2.Substring(1, 15).ToLower();
                return t2;
            }
    }
}
