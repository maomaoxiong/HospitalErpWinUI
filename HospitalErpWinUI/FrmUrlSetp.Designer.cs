﻿namespace HospitalErpWinUI
{
    partial class FrmUrlSetp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textServicesUrl = new DevExpress.XtraEditors.TextEdit();
            this.save = new DevExpress.XtraEditors.SimpleButton();
            this.cancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.textServicesUrl.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "服务地址:";
            // 
            // textServicesUrl
            // 
            this.textServicesUrl.Location = new System.Drawing.Point(103, 25);
            this.textServicesUrl.Name = "textServicesUrl";
            this.textServicesUrl.Size = new System.Drawing.Size(515, 24);
            this.textServicesUrl.TabIndex = 1;
            // 
            // save
            // 
            this.save.Location = new System.Drawing.Point(217, 80);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(80, 28);
            this.save.TabIndex = 12;
            this.save.Text = "保存";
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // cancel
            // 
            this.cancel.Location = new System.Drawing.Point(331, 80);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(80, 28);
            this.cancel.TabIndex = 11;
            this.cancel.Text = "关闭";
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // FrmUrlSetp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 151);
            this.Controls.Add(this.save);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.textServicesUrl);
            this.Controls.Add(this.label1);
            this.Name = "FrmUrlSetp";
            this.Text = "FrmUrlSetp";
            this.Load += new System.EventHandler(this.FrmUrlSetp_Load);
            ((System.ComponentModel.ISupportInitialize)(this.textServicesUrl.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit textServicesUrl;
        private DevExpress.XtraEditors.SimpleButton save;
        private DevExpress.XtraEditors.SimpleButton cancel;
    }
}