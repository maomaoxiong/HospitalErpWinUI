﻿namespace HospitalErpWinUI
{
    partial class FormSetEnv
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comp_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.copy_code = new DevExpress.XtraEditors.ComboBoxEdit();
            this.login_date = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.but_save = new DevExpress.XtraEditors.SimpleButton();
            this.but_quit = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.comp_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.copy_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.login_date.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.login_date.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // comp_code
            // 
            this.comp_code.Location = new System.Drawing.Point(176, 45);
            this.comp_code.Name = "comp_code";
            this.comp_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.comp_code.Properties.Appearance.Options.UseBackColor = true;
            this.comp_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comp_code.Size = new System.Drawing.Size(303, 24);
            this.comp_code.TabIndex = 0;
            // 
            // copy_code
            // 
            this.copy_code.Location = new System.Drawing.Point(176, 97);
            this.copy_code.Name = "copy_code";
            this.copy_code.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.copy_code.Properties.Appearance.Options.UseBackColor = true;
            this.copy_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.copy_code.Size = new System.Drawing.Size(303, 24);
            this.copy_code.TabIndex = 0;
            // 
            // login_date
            // 
            this.login_date.EditValue = null;
            this.login_date.Location = new System.Drawing.Point(176, 148);
            this.login_date.Name = "login_date";
            this.login_date.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.login_date.Properties.Appearance.Options.UseBackColor = true;
            this.login_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.login_date.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.login_date.Size = new System.Drawing.Size(303, 24);
            this.login_date.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(72, 48);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 18);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "单位信息";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(72, 100);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 18);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "账套信息";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(72, 151);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 18);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "登陆日期";
            // 
            // but_save
            // 
            this.but_save.Location = new System.Drawing.Point(150, 240);
            this.but_save.Name = "but_save";
            this.but_save.Size = new System.Drawing.Size(80, 25);
            this.but_save.TabIndex = 5;
            this.but_save.Text = "确定";
            this.but_save.Click += new System.EventHandler(this.but_save_Click);
            // 
            // but_quit
            // 
            this.but_quit.Location = new System.Drawing.Point(269, 240);
            this.but_quit.Name = "but_quit";
            this.but_quit.Size = new System.Drawing.Size(121, 25);
            this.but_quit.TabIndex = 5;
            this.but_quit.Text = "退出系统";
            this.but_quit.Click += new System.EventHandler(this.but_quit_Click);
            // 
            // FormSetEnv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(562, 335);
            this.Controls.Add(this.but_quit);
            this.Controls.Add(this.but_save);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.login_date);
            this.Controls.Add(this.copy_code);
            this.Controls.Add(this.comp_code);
            this.Name = "FormSetEnv";
            this.Text = "工作环境设置";
            this.Load += new System.EventHandler(this.FormSetEnv_Load);
            ((System.ComponentModel.ISupportInitialize)(this.comp_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.copy_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.login_date.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.login_date.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.ComboBoxEdit comp_code;
        private DevExpress.XtraEditors.ComboBoxEdit copy_code;
        private DevExpress.XtraEditors.DateEdit login_date;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton but_save;
        private DevExpress.XtraEditors.SimpleButton but_quit;
    }
}