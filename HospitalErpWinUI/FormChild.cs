﻿using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraTab;
using HospitalErpWinUI;
using HospitalErpWinUI.GlobalVar;
using HospitalErpWinUI.WinFormUiHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace HospitalErpWinUI
{
    public partial class FormChild : RibbonForm
    {
        private AutoSizeFormClass asc = new AutoSizeFormClass();
        public List<PageFormInf> _pageFormInfs {get;set;}
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        public FormChild()
        {
            InitializeComponent();
         //   _pageFormInfs = new List<PageFormInf>();
            //PageFormInf inf = new PageFormInf();
            //inf.PageName = "page1";
            //inf.FormName = "hbos.sys.dicts.unitman.query";
            //inf.Perms = null;
            //t
            //_pageFormInfs.Add(inf);
            this.DoubleBuffered = true;
            //this.AutoSize = true;
          
        }

        #region 事件
            private void FormChild_Load(object sender, EventArgs e)
            {
                try
                { 
                    AddPages();
                }
                catch(Exception ex)
                {
                    MessageForm.Exception(ex.Message, "异常");
                }
           
            }

            private void FormChild_FormClosing(object sender, FormClosingEventArgs e)
            {
                try
                {
                    RemovePages();
                }
                catch (Exception ex)
                {
                    MessageForm.Exception(ex.Message, "异常");
                }
                

            }

            private void xtraTabControl1_Resize(object sender, EventArgs e)
            {
               
                asc.controllInitializeSize(this);

            }

             private void FormChild_Resize(object sender, EventArgs e)
            {
                asc.controllInitializeSize(this);
                foreach (XtraTabPage item in xtraTabControl1.TabPages)
                {

                //    item.Refresh();
                //    item.Controls[0].Refresh();
                  //  ((Form)item.Controls[0]).Dock = DockStyle.Fill;
                    xtraTabControl1.Width = this.Width;
                    this.xtraScrollableControl1.Width = this.Width;
                    item.Width = this.Width;
                    item.Controls[0].Width = this.Width;
                    item.Controls[0].Height = item.Height;
                    item.Controls[0].Visible = true;
            //        MessageBox.Show(item.Name + "    " + item.Width + "    " + item.Controls[0].Width + "     " + this.Width);

                }

            }

            private void xtraTabControl1_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
            {
                if (e.Page != null)
                    e.Page.Controls[0].Visible = true;
            }
        #endregion
       
        #region 辅助方法
        //移除窗口中全部page
            public void RemovePages()
            {
                 if (_pageFormInfs == null)
                    return;
                try
                { 
                     foreach (PageFormInf inf in _pageFormInfs)
                    {
                        TabPageHelper.FormOper.RemoveTabPage(xtraTabControl1, inf);
                    }
                }
                catch(Exception e)
                {
                    MessageForm.Exception(e.Message, "异常");
                }
              
              
            }
        //向窗口中增加page
            public void AddPages()
            {
                if (_pageFormInfs == null)
                    return;

                try
                { 
                    asc.controllInitializeSize(this);
                    foreach (PageFormInf inf in _pageFormInfs)
                    {
                        TabPageHelper.FormOper.AddTabpage(xtraTabControl1, inf);
                    }
                }
                catch(Exception e)
                {
                    MessageForm.Exception(e.Message, "异常");
                }
                
            }
  
        #endregion

            private void FormChild_Activated(object sender, EventArgs e)
            {
                string name =this.Text;
            }

           

    }
}
